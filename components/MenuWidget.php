<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

class MenuWidget extends Widget{
	public $html;
	
	public function init(){
		parent::init();
              /**
               * Получаем все родительские ссылки меню
               * */ 
              $menumas= (new \yii\db\Query())
                            ->select('*')
                            ->from('mainmenu')
                            ->where(['parent' => null])
                            ->orderBy('position ASC')
                            ->all();
              /**
               * Добавляем в родительские дочерние ссылки меню
               * */ 

               foreach($menumas as $item)
               {
                      $child= (new \yii\db\Query())
                                    ->select('*')
                                    ->from('mainmenu')
                                    ->where(['parent' => $item['id']])
                                    ->orderBy('position ASC')
                                    ->all();
                     $mas[$item['title']]=$child;               
               } 
                
               /**
                ** Строим главное меню
                **/
             $html='<ul id="menu">';
             $html.='<span id="logomini"></span>';
             for ($i=0;$i<count($menumas);$i++)
             {
                    if ($menumas[$i]['link']=='#')
                    {
                        $html.='<li>';
                        ($menumas[$i]['cclass']!='') ? $html.='<i class="'.$menumas[$i]['cclass'].'"></i>': ' ';
                        $html.='<a href="javascript:void(0);">'.$menumas[$i]['title'].'</a>';
                    }
                    else
                    {
                       $html.='<li>';
                       ($menumas[$i]['cclass']!='') ? $html.='<i class="'.$menumas[$i]['cclass'].'"></i>': ' ';
                        $html.='<a href="'.Url::toRoute($menumas[$i]['link']).'">'.$menumas[$i]['title'].'</a>'; 
                    }    
                    
                    // дочерние ссылки
                    if (!empty($mas[$menumas[$i]['title']]))
                    {
                       for ($j=0;$j<count( $mas[$menumas[$i]['title']] );$j++ )
                       {
                          if ($j==0) {$html.='<ul>';}  
                          $html.='<li>'.Html::a($mas[$menumas[$i]['title']][$j]['title'],[$mas[$menumas[$i]['title']][$j]['link']]).'</li>'; 
                          if ($j==count( $mas[$menumas[$i]['title']])-1) {$html.='</ul>';} 
                        }     
                    }    
                     
                   $html.='</li>';
             }

             $html.='</ul>';                                    
                                    
          $this->html=$html;
	}
	
	public function run(){
		return $this->html;
	}
}
