<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

class ValutaWidget extends Widget{
	public $valuta;
	


	public function init(){
		parent::init();

            function browser($url) {
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)");
                $html = curl_exec($ch);
                curl_close($ch);
                return $html;
             }
        $url="http://www.nationalbank.kz/?&switch=russian";        
        preg_match_all('~<td width="30" class="gen3_2" >(.*?)<table border="0" width="100%">~is', browser($url), $text);
        $valuta=strip_tags($text[1][0],'<img alt>');
        $valuta=trim(preg_replace('/\s{2,}/', ' ', $valuta));
        $valuta=explode(" ",$valuta);
        
        $j=0; $mas=[];
         for($i=0;$i<count($valuta);$i++)
         {
             if( $valuta[$i]=='TOD')
             {
                $mas[$j]=$i+1;
                $j++;
             }
         }
              
        if (!empty($mas))
        {
            if ($this->valuta===null){
                $this->valuta=  '<span id="usd">USD '.$valuta[0].'</span>'.
                              '<span id="eur">EUR '.$valuta[$mas[0]].'</span>'.
                              '<span id="rub">RUB '.$valuta[$mas[1]].'</span>'.'<span class="txt11 red">(по курсу нацбанка РК)</span>';
            } else {
                switch ($this->valuta) {
                    case 'USD':
                        $valuta='<span class="txt">'.$this->valuta.' '.$valuta[0].'</span>';
                        break;
                    case 'EUR':
                        $valuta='<span class="txt">'.$this->valuta.' '.$valuta[6].'</span>';
                        break;
                    case 'RUB':
                        $valuta='<span class="txt">'.$this->valuta.' '.$valuta[12].'</span>';
                        break;
                }

               $this->valuta=$valuta;
            }
        }
        else
        {
               $this->valuta='нет доступа к данным';
        }
        
	}
	
	public function run(){
		return $this->valuta;
	}
}
