<?php

namespace app\controllers;

use Yii;
use app\models\Mainmenu;
use app\models\MainmenuSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * MainmenuController implements the CRUD actions for mainmenu model.
 */
class MainmenuController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }



    /**
     * Lists all mainmenu models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        if (Yii::$app->user->can('superadmin')) 
        { 
            $searchModel = new MainmenuSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->pagination->pageSizeParam = false;    
        
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }         
    }

    /**
     * Displays a single mainmenu model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->user->can('superadmin')) 
        { 

                return $this->render('view', [
                    'model' => $this->findModel($id),
                ]);
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }         

    }

    /**
     * Creates a new mainmenu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->can('superadmin')) 
        { 
            $model = new Mainmenu();
    
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                 return $this->redirect(['index']);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }         
    }

    /**
     * Updates an existing mainmenu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        if (Yii::$app->user->can('superadmin')) 
        { 
            $model = $this->findModel($id);
    
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['index']);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }         
    }

    /**
     * Deletes an existing mainmenu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->can('superadmin')) 
        { 
            $ids= explode(',',$id);
            
            for($i=0;$i<count($ids);$i++)
            {
              $this->findModel($ids[$i])->delete();
            }
            return $this->redirect(['index']);
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }         
    }

    /**
     * Finds the mainmenu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mainmenu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (Yii::$app->user->can('superadmin')) 
        { 
            if (($model = Mainmenu::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }         
    }
}
