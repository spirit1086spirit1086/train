<?php

namespace app\controllers;
use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Vagon;
use app\models\VagonSearch;
use app\models\Vvagon;
use app\models\Spr;
use app\models\Prichina;
use app\models\Mremont;
use app\models\Company;
use app\models\Price;
use app\models\Valuta;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

class ReportController extends  Controller
{
  
  
    public function Strlink($string,$col)   //функция вывода превью объявлений
    {
          $string = strip_tags($string); //убираем теги
          $string = iconv('UTF-8','windows-1251//TRANSLIT',$string); //Меняем кодировку на windows-1251
          if ($col!='') {
            $string = substr($string, 0, $col); // обрежем его на определённое количество символов
          }
          else
          {
            $string = substr($string, 0); 
          }
          $string = iconv('windows-1251','UTF-8//TRANSLIT',$string); //Возвращаем кодировку в utf-8      
      return $string;
    }
  
     /** clean - подача и уборка  * detail - детали  * prichina - справка 2612 * promiv - промывка * revizia - ревизия * zhd - жд тариф */

    public function actionIndex()
    {
       if ( Yii::$app->user->can('author') )
       {           
            $model= new Vagon;
            $valuta = (new \yii\db\Query())->select('*')->from('valuta')->where('id!=1')->all(); // получаем валюту кроме тенге
            
            $report=[];$val=[];
             if ( Yii::$app->request->post() ) /**  ТОР 1  | ДР 2 | КР 3 **/ 
             {
                /** Получаем масив собственников сгруппированных ТВТ, ТТ, СГ-ТРАНС, КАРГОТРАНС,**/
                 for ($i=0;$i<count($_POST['val_id']);$i++)
                 {
                    $valid[]=$_POST['val_id'][$i];
                    $val[]=$_POST['val'][$i];
                 }
                
                 $querysobs = (new \yii\db\Query())->select('*')->from('sobstvenik')->all();
                   
                  if ($_POST['firm']!='all') // получить данные одной фирмы или всех
                  {
                           $greben=$this->Greben(null,$_POST['firm'],$valid,$val,$model,$_POST['date1'],$_POST['date2'], $querysobs,null,null);    
                           $tor=$this->Osn(1,$_POST['firm'],$valid,$val,$model,$_POST['date1'],$_POST['date2'], $querysobs,null,null);    
                           $torsobs=$this->Sobstvenik(1,$_POST['firm'],$valid,$val,$model,$_POST['date1'],$_POST['date2'], $querysobs,null,null);    
                           // ДР 
                           $dr=$this->Osn(2,$_POST['firm'],$valid,$val,$model,$_POST['date1'],$_POST['date2'], $querysobs,null,null);    
                           $drsobs=$this->Sobstvenik(2,$_POST['firm'],$valid,$val,$model,$_POST['date1'],$_POST['date2'], $querysobs,null,null);    
                          // Уборка
                           $promivka=$this->Pr_ub_zhd('Промывка',$_POST['firm'],$valid,$val,$model,$_POST['date1'],$_POST['date2'], null,null,null);    
                           //Подача и уборка
                           $uborka=$this->Pr_ub_zhd('Подача и уборка',$_POST['firm'],$valid,$val,$model,$_POST['date1'],$_POST['date2'], null,null,null);    
                           //Жд тариф 
                           $zhd=$this->Pr_ub_zhd('Жд тариф',$_POST['firm'],$valid,$val,$model,$_POST['date1'],$_POST['date2'], null,null,null);    
                           // Посредник    
                           $posrednik=$this->Posrednik(null,$_POST['firm'],$valid,$val,$model,$_POST['date1'],$_POST['date2'], $querysobs,null,null);    

                           $all=[$greben,$tor,$torsobs,$dr,$drsobs,$promivka,$uborka,$zhd]; 
                           
                           for($i=0;$i<count($posrednik);$i++)
                           {
                              $t[][]=$posrednik[$i];  
                           }
                           
                           if (!empty($t))
                           {  
                                for($i=0;$i<count($t);$i++)
                                {
                                   $all[]=$t[$i];  
                                }
                           }
                                
                           for($i=0;$i<count($all);$i++)
                           {
                               if (!empty($all[$i])) // если массив не пустой   
                               {
                                   for($j=0;$j<count($all[$i]);$j++)
                                   {
                                        $report[]=$all[$i][$j];
                                   }
                               }         
                           }
                           
                   }
                   else
                   {
                         $otch=$this->Allorg($valid, $val, $model, $_POST['date1'],$_POST['date2'], $querysobs, 1,null);
                         $all[]=[$otch];  
                          // получаем список фирм
                          $firms = (new \yii\db\Query())->select('*')->from('mremont')->all();  
                         
                           foreach($firms as $item)
                           {    $t=[];
                                $greben=$this->Greben(null,$item['id'],$valid,$val,$model,$_POST['date1'],$_POST['date2'], $querysobs,null,null);    
                                $tor=$this->Osn(1,$item['id'],$valid,$val,$model,$_POST['date1'],$_POST['date2'], $querysobs,null,null);    
                                $torsobs=$this->Sobstvenik(1,$item['id'],$valid,$val,$model,$_POST['date1'],$_POST['date2'], $querysobs,null,null);    
                                // ДР 
                                $dr=$this->Osn(2,$item['id'],$valid,$val,$model,$_POST['date1'],$_POST['date2'], $querysobs,null,null);    
                                $drsobs=$this->Sobstvenik(2,$item['id'],$valid,$val,$model,$_POST['date1'],$_POST['date2'], $querysobs,null,null);    
                               // Уборка
                                $promivka=$this->Pr_ub_zhd('Промывка',$item['id'],$valid,$val,$model,$_POST['date1'],$_POST['date2'], null,null,null);    
                                //Подача и уборка
                                $uborka=$this->Pr_ub_zhd('Подача и уборка',$item['id'],$valid,$val,$model,$_POST['date1'],$_POST['date2'], null,null,null);    
                                //Жд тариф 
                                $zhd=$this->Pr_ub_zhd('Жд тариф',$item['id'],$valid,$val,$model,$_POST['date1'],$_POST['date2'], null,null,null);    
                                // Посредник    
                                $posrednik=$this->Posrednik(null,$item['id'],$valid,$val,$model,$_POST['date1'],$_POST['date2'], $querysobs,null,null);    

                                $all[]=[$greben,$tor,$torsobs,$dr,$drsobs,$promivka,$uborka,$zhd];
                                for($i=0;$i<count($posrednik);$i++)
                                {
                                   $t[][][]=$posrednik[$i];  
                                }

                                if (!empty($t))
                                {  
                                    for($i=0;$i<count($t);$i++)
                                    {
                                       $all[]=$t[$i];  
                                    }
                                }
                           } 
                             
                           for($i=0;$i<count($all);$i++)
                           {
                            
                                for($j=0;$j<count($all[$i]);$j++)
                                {
                                   if (!empty($all[$i][$j]) ) // если массив не пустой   
                                   {
                                       for($k=0;$k<count($all[$i][$j]);$k++)
                                       {
                                            $report[]=$all[$i][$j][$k];
                                       }
                                   }         
                                }
                           }
                   } 
                   
                return $this->render('index',['model'=>$model,'report'=>$report,'valuta'=>$valuta,'val'=>$val]);
             }
             else
             {
                return $this->render('index',['model'=>$model,'valuta'=>$valuta]);
             }
        }
        else
        {
            throw new HttpException(403,'Доступ закрыт');  
        }     
    }



    protected function ConstructTable($vvagon,$array,$identi)
    {
                               $random=rand(100,10000);
                               $dataProvider = new ArrayDataProvider(['allModels' => $array,'pagination' => ['pageSize' => '100000000']]);    
                               if ($vvagon==1)
                               {
                                $html=GridView::widget([
                                                            'dataProvider' => $dataProvider,
                                                            'layout'=>'{items}',
                                                            'tableOptions' =>['class' => 'dop','id'=>$random],
                                                            'rowOptions' => function ($allModels){
                                                                (isset($allModels['color'])) ? $color=$allModels['color'] : $color='';
                                                                return ['class'=>$color];
                                                             },
                                                            'columns' =>[
                                                                            [
                                                                              'attribute' => 'title',
                                                                              'label'=>'Наименование',  
                                                                            ], 
                                                                            [
                                                                              'headerOptions'=>['class'=>'c'],
                                                                              'contentOptions'=>['class'=>'c'],
                                                                              'attribute' => 'vvagon_id',
                                                                              'label'=>'Вид вагона',  
                                                                              'value'=> function ($allModels)  {  (isset($allModels['vvagon_id']))  ? $vvagon_id=$allModels['vvagon_id'] : $vvagon_id='';
return $vvagon_id;
                                                                                                               },                                                                             ], 
                                                                            [
                                                                              'headerOptions'=>['class'=>'c'],
                                                                              'contentOptions'=>['class'=>'c'],
                                                                              'attribute' => 'count',
                                                                              'label'=>'Кол-во',  
                                                                            ], 
                                                                            [
                                                                              'headerOptions'=>['class'=>'c'],
                                                                              'contentOptions'=>['class'=>'c'],
                                                                              'attribute' => 'cenaed',
                                                                              'label'=>'Цена за ед.',  
                                                                              'value'=> function ($allModels)  { (isset($allModels['cenaed'])) ? $cenaed=$allModels['cenaed'] : $cenaed='';
return $cenaed;                                                                                                                
                                                                                                               },                                                                             ], 
                                                                            [
                                                                              'headerOptions'=>['class'=>'c'],
                                                                              'contentOptions'=>['class'=>'c'],
                                                                              'attribute' => 'cena',
                                                                              'label'=>'Цена',  
                                                                            ], 
                                                                            [
                                                                              'headerOptions'=>['class'=>'c'],
                                                                              'contentOptions'=>['class'=>'c'],
                                                                              'attribute' => 'ndc',
                                                                              'label'=>'НДС',  
                                                                            ], 
                                                                            [
                                                                              'headerOptions'=>['class'=>'c'],
                                                                              'contentOptions'=>['class'=>'c'],
                                                                              'attribute' => 'itog',
                                                                              'label'=>'Итог',  
                                                                            ], 
                                                                         ],
                                                            ]);                                              
                                $label='"'.$array[0]['mremont_id'].'" '.$identi;              
                                $mas=['label'=>$label,'content'=>$html]; 
                               }
                               elseif($vvagon==2) // полный отчет
                               {
                                        $html=GridView::widget([
                                                            'dataProvider' => $dataProvider,
                                                            'layout'=>'{items}',
                                                            'tableOptions' =>['class' => 'dop','id'=>$random],
                                                            'rowOptions' => function ($allModels){
                                                                (isset($allModels['color'])) ? $color=$allModels['color'] : $color='';
                                                                return ['class'=>$color];
                                                             },
                                                            'columns' =>[
                                                                            [
                                                                              'attribute' => 'title',
                                                                              'label'=>'Наименование',  
                                                                            ], 
                                                                            [
                                                                              'headerOptions'=>['class'=>'c'],
                                                                              'contentOptions'=>['class'=>'c'],
                                                                              'attribute' => 'cena',
                                                                              'label'=>'Общая цена (тенге)',  
                                                                            ], 
                                                                            [
                                                                              'headerOptions'=>['class'=>'c'],
                                                                              'contentOptions'=>['class'=>'c'],
                                                                              'attribute' => 'ndc',
                                                                              'label'=>'НДС',  
                                                                            ], 
                                                                            [
                                                                              'headerOptions'=>['class'=>'c'],
                                                                              'contentOptions'=>function ($allModels){  
                                                                                  (isset($allModels['column'])) ? $color=$allModels['column'] : $color='';
                                                                                   return ['class'=>'w11 '.$color];
                                                                               },
                                                                              'format'=>'html', 
                                                                              'attribute' => 'itog',
                                                                              'label'=>'Итого(тенге)',  
                                                                            ],
                                                                            [
                                                                              'headerOptions'=>['class'=>'c'],
                                                                              'contentOptions'=>['class'=>'c'],
                                                                              'attribute' => 'ucena',
                                                                              'label'=>'Общая цена (доллар)',  
                                                                            ], 
                                                                            [
                                                                              'headerOptions'=>['class'=>'c'],
                                                                              'contentOptions'=>['class'=>'c'],
                                                                              'attribute' => 'undc',
                                                                              'label'=>'НДС',  
                                                                            ],
                                                                            [
                                                                              'headerOptions'=>['class'=>'c'],
                                                                              'contentOptions'=>function ($allModels){  
                                                                                  (isset($allModels['column'])) ? $color=$allModels['column'] : $color='';
                                                                                   return ['class'=>'c '.$color];
                                                                               },
                                                                              'format'=>'html',
                                                                              'attribute' => 'uitog',
                                                                              'label'=>'Итого (доллар)',  
                                                                            ], 
                                                                         ],
                                                            ]);                                              
                                    $label='Сводная';              
                                    $mas=['label'=>$label,'content'=>$html];                                   
                               }    
                               else
                               {
                                $html=GridView::widget([
                                                            'dataProvider' => $dataProvider,
                                                            'layout'=>'{items}',
                                                            'tableOptions' =>['class' => 'dop','id'=>$random],
                                                            'rowOptions' => function ($allModels){
                                                                (isset($allModels['color'])) ? $color=$allModels['color'] : $color='';
                                                                return ['class'=>$color];
                                                             },
                                                            'columns' =>[
                                                                            [
                                                                              'attribute' => 'title',
                                                                              'label'=>'Наименование',  
                                                                            ], 
                                                                            [
                                                                              'headerOptions'=>['class'=>'c'],
                                                                              'contentOptions'=>['class'=>'c'],
                                                                              'attribute' => 'count',
                                                                              'label'=>'Кол-во',  
                                                                            ], 
                                                                            [
                                                                              'headerOptions'=>['class'=>'c'],
                                                                              'contentOptions'=>['class'=>'c'],
                                                                              'attribute' => 'cenaed',
                                                                              'label'=>'Цена за ед.',  
                                                                              'value'=> function ($allModels)  { (isset($allModels['cenaed'])) ? $cenaed=$allModels['cenaed'] : $cenaed='';
return $cenaed;                                                                                                                
                                                                                                               },                  
                                                                              
                                                                            ], 
                                                                            [
                                                                              'headerOptions'=>['class'=>'c'],
                                                                              'contentOptions'=>['class'=>'c'],
                                                                              'attribute' => 'cena',
                                                                              'label'=>'Цена',  
                                                                            ], 
                                                                            [
                                                                              'headerOptions'=>['class'=>'c'],
                                                                              'contentOptions'=>['class'=>'c'],
                                                                              'attribute' => 'ndc',
                                                                              'label'=>'НДС',  
                                                                            ], 
                                                                            [
                                                                              'headerOptions'=>['class'=>'c'],
                                                                              'contentOptions'=>['class'=>'c'],
                                                                              'attribute' => 'itog',
                                                                              'label'=>'Итог',  
                                                                            ], 
                                                                         ],
                                                            ]);                                              
                                    $label='"'.$array[0]['mremont_id'].'" '.$identi;              
                                    $mas=['label'=>$label,'content'=>$html]; 
                                }                
                                            
                         
             return $mas;                   
    } 

   

     /** **************************************************
      ****************** Тонкий гребень *******************
      *****************************************************
      **/ 
      protected function Greben($tip,$id,$valid,$val,$model,$date1,$date2, $querysobs,$finish,$export)
      {
                $firm = Mremont::findOne(['id'=>$id]); 
                $kaz = (new \yii\db\Query())->select('*')->from('mremont')->where('id=1')->one();
                
                $query='';
                    if ($date1!='' &&  $date2!='')
                    {
                       $query=' && (dates>="'.date('Y-m-d',strtotime($date1)).'" && dates<="'.date('Y-m-d',strtotime($date2)).'")'; 
                    }
                    else
                    {
                        if ($date1!='')
                        {
                          $query=' && dates>="'.date('Y-m-d',strtotime($date1)).'"'; 
                        }
                        elseif($date2!='')
                        {
                            $query=' && dates<="'.date('Y-m-d',strtotime($date2)).'"';
                        }
                    }   
                  
                // запрос для получения id тонкого гребня
                $greben=(new \yii\db\Query())
                                    ->select(['*']) 
                                    ->from('price')
                                    ->where('code IN (101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,130,132,133,134,148,150,151,152,153,154,155,156,157,158)')
                                    ->all();    
                                        
                foreach ($greben as $item) // получаем все id, которые входят в тонкий гребень
                {
                    $grebid[]=$item['id'];
                }
                  
                  // получаю все записи причин   
                $allquery = (new \yii\db\Query())
                                    ->select(['*']) 
                                    ->from('prichina')
                                    ->where('mremont_id='.$id.$query)
                                    ->all();    

                //получаю записи = выше перечисленым кодам относящиеся к тонкому гребню
                $nmas = (new \yii\db\Query())
                                    ->select(['*']) 
                                    ->from('prichina')
                                    ->where('mremont_id='.$id.$query.' && spr_id IN ('.implode(',',$grebid).')')
                                    ->all();    
                $sortmas=[];
                foreach($nmas as $key=>$value)   // массив с кодами причин
                {
                   foreach ($allquery as $ikey=>$item) // все записи причин
                   {
                      if ($value['spr_id']==$item['spr_id'] && $value['vagon_id']==$item['vagon_id']) 
                      {
                          $sortmas[$key][]=['spr_id'=>$value['spr_id'],'cenaed'=>$value['cenaed'],'mremont_id'=>$value['mremont_id']];
                      }
                      
                      if ($value['vagon_id']==$item['vagon_id'] && $value['spr_id']!=$item['spr_id'])
                      {
                          $sortmas[$key][]=['spr_id'=>$item['spr_id'],'cenaed'=>$item['cenaed'],'mremont_id'=>$item['mremont_id']];
                      }
                   }
                }
                
                $qmas=[];
                for($i=0; $i<count($sortmas);$i++) // добавляем к  причинам гребня еще причины если они указаны в одной счет-фактуре 
                {
                   unset($tmp); 
                   for($j=0;$j<count($sortmas[$i]);$j++)
                   {
                     $tmp[]=$model->DataPrice[$sortmas[$i][$j]['spr_id']];
                     $cenaed=$sortmas[$i][$j]['cenaed'];
                     $mremont_id=$sortmas[$i][$j]['mremont_id'];
                   }
                 
                   $qmas[]=['spr_id'=>implode(',',$tmp),'cenaed'=>$cenaed,'mremont_id'=>$mremont_id,'count'=>1]; 
                }
  
                $frmas=[]; // собираем сгруппированный массив для построения таблицы Гребня
                for($i=0; $i<count($qmas);$i++)
                {    
                      if (!in_array(['spr_id'=>$qmas[$i]['spr_id'],'cenaed'=>$qmas[$i]['cenaed'],'mremont_id'=>$qmas[$i]['mremont_id'],'count'=>$qmas[$i]['count']],$frmas) )
                      {
                        $frmas[]=['spr_id'=>$qmas[$i]['spr_id'],'cenaed'=>$qmas[$i]['cenaed'],'mremont_id'=>$qmas[$i]['mremont_id'],'count'=>$qmas[$i]['count']];
                      }
                      else
                      {
                        $keyind=array_search(['spr_id'=>$qmas[$i]['spr_id'],'cenaed'=>$qmas[$i]['cenaed'],'mremont_id'=>$qmas[$i]['mremont_id'],'count'=>$qmas[$i]['count']], $frmas);
                        $frmas[$keyind]['count']+=1;
                      }
                }
               
                $greben=[];$kol=0;$cena=0;$finally=[];    
                foreach($frmas as $key=>$value)    
                {
                     $frmas[$key]['rep']='greben';
                     $frmas[$key]['title']=$value['spr_id'];
                     $kol+=$value['count']; 
                     $cena+=round($value['cenaed']*$value['count'],2);
                             
                     // подсчитываем цены одной строки
                     $onecena=round($value['cenaed']*$value['count'],2);
                     $onendc=round($onecena*($firm->ndc/100),2);
                     $oneitog=round($onecena+$onendc,2);

                     $frmas[$key]['mremont_id']=$model->DataMremont[$value['mremont_id']];
                     $frmas[$key]['lastname']=$model->DataMremont[$value['mremont_id']].' тонкий гребень';
                     $frmas[$key]['cena']=$onecena;
                     $frmas[$key]['ndc']=$onendc;
                     $frmas[$key]['itog']=$oneitog;
             
                                 
                    // Конец подсчитываем итоги
                    if ($key==count($frmas)-1)
                    {   
                        //  считаем итоги 
                        if ($firm->valuta==1) // если тенге
                        {
                            $ndc=round($cena*($firm->ndc/100),2);
                            $itog=round($cena+$ndc,2);
                            $key_val=  array_search(3, $valid); // получаем индекс, в котором хранится значение доллара
                            $cenausd=round($cena/$val[$key_val],2);
                            $ndcusd=round($cenausd*($firm->ndc/100),2);
                            $usditog=round($cenausd+$ndcusd,2);
                            
                            $frmas[]=['title'=>'Итого в тенге','count'=>$kol,'cenaed'=>'','cena'=>$cena,'ndc'=>$ndc,'itog'=>$itog,'color'=>'blue bgblue','mremont_id'=>$model->DataMremont[$value['mremont_id']],'rep'=>'greben'];
                            $frmas[]=['title'=>'Итого в долларах','count'=>$kol,'cenaed'=>'','cena'=>$cenausd,'ndc'=>$ndcusd,'itog'=>$usditog,'color'=>'red bgblue','mremont_id'=>$model->DataMremont[$value['mremont_id']],'rep'=>'greben'];
                            
                            // сохраняем в массив для отчета по всем организациям
                            $finally[]=['title'=>$frmas[0]['mremont_id'].' тонкий гребень (не суммировать)','cenaed'=>'','cena'=>$cena,'ndc'=>$ndc,'itog'=>$itog,'ucena'=>$cenausd,'undc'=>$ndcusd,'uitog'=>$usditog,'rep'=>'greben'];
                        }
                        elseif($firm->valuta==3) //если доллары
                        {
                            // ндс (в долларах) * на ставку ндс страны поставщика
                            $ndc=round($cena*($firm->ndc/100),2);
                            $key_val=  array_search(3, $valid); // получаем индекс, в котором хранится значение валюты (valid содержит значения id валюты)
                            
                            // ндс (в тенге)
                            $tgcena= round($cena*$val[$key_val],2);
                            $tgndc=round($tgcena*($kaz['ndc']/100),2);
                            
                            $frmas[]=['title'=>'Итого в долларах','count'=>$kol,'cenaed'=>'','cena'=>$cena,'ndc'=>$ndc,'itog'=>round($ndc+$cena,2),'color'=>'red bgblue','mremont_id'=>$model->DataMremont[$value['mremont_id']],'rep'=>'greben'];
                            $frmas[]=['title'=>'Итого в тенге','count'=>$kol,'cenaed'=>'','cena'=>$tgcena,'ndc'=>$tgndc,'itog'=>round($tgcena+$tgndc,2),'color'=>'blue bgblue','mremont_id'=>$model->DataMremont[$value['mremont_id']],'rep'=>'greben'];
                        
                            // сохраняем в массив для отчета по всем организациям
                            $finally[]=['title'=>$frmas[0]['mremont_id'].' тонкий гребень','cenaed'=>'','cena'=>$tgcena,'ndc'=>$tgndc,'itog'=>round($tgcena+$tgndc,2),'ucena'=>$cena,'undc'=>$ndc,'uitog'=>round($ndc+$cena,2),'rep'=>'greben'];
                        }
                        else //если рубли или другая валюта кроме доллара и тенге
                        {
                            $ndc=round($cena*($firm->ndc/100),2);
                            $itog=round($cena+$ndc,2);
                            // курс рубля к тенге
                            $key_val=  array_search($firm->valuta, $valid); // получаем индекс, в котором хранится значение валюты (valid содержит значения id валюты)
                            ($firm->valuta=2)? $cenatg=round($cena*$val[$key_val],2) : ''; // $val[] содержит значения курсов валют 
                            $ndctg=round($cenatg*($kaz['ndc']/100),2);
                            $itogtg=round($cenatg+$ndctg,2);
                            //
                            $key_val=  array_search(3, $valid); // получаем индекс, в котором хранится значение доллара
                            $cenausd=round($cenatg/$val[$key_val],2);
                            $ndcusd=round($cenausd*($kaz['ndc']/100),2);
                            $itogusd=round($cenausd+$ndcusd,2);

                            $frmas[]=['title'=>'Итого в рублях','count'=>$kol,'cenaed'=>'','cena'=>$cena,'ndc'=>$ndc,'itog'=>$itog,'color'=>'green bgblue','mremont_id'=>$model->DataMremont[$value['mremont_id']],'rep'=>'greben'];
                            $frmas[]=['title'=>'Итого в тенге','count'=>$kol,'cenaed'=>'','cena'=>$cenatg,'ndc'=>$ndctg,'itog'=>$itogtg,'color'=>'blue bgblue','mremont_id'=>$model->DataMremont[$value['mremont_id']],'rep'=>'greben'];
                            $frmas[]=['title'=>'Итого в долларах','count'=>$kol,'cena'=>$cenausd,'ndc'=>$ndcusd,'itog'=>$itogusd,'color'=>'red bgblue','mremont_id'=>$model->DataMremont[$value['mremont_id']],'rep'=>'greben'];
                           
                            // сохраняем в массив для отчета по всем организациям
                            $finally[]=['title'=>$frmas[0]['mremont_id'].' тонкий гребень','cenaed'=>'','cena'=>$cenatg,'ndc'=>$ndctg,'itog'=>$itogtg,'ucena'=>$cenausd,'undc'=>$ndcusd,'uitog'=>$itogusd,'rep'=>'greben'];
                        } 
                        
                    }        
                }

                if (empty($finish))
                {   
                    if (empty($export))
                    {    
                        if ( !empty($frmas))
                        {  
                           $greben[]=$this->ConstructTable(null,$frmas,'тонкий гребень');
                        }
                    }
                    else
                    {
                      $greben=$frmas;  
                    }    
                }
                else // сохраняем данные для финального отчета 
                {
                  $greben=$finally;
                }
                
        return $greben; 
    }


     /** *********************************************************************************************************************************************
      ****************** ТОР  ДР  КР *****************************************************************************************************************
      ***********************************************************************************************************************************************
      **/ 
      protected function Osn($tip,$id,$valid,$val,$model,$date1,$date2, $querysobs,$finish,$export)
      {
                $tor=[];$tipmas=[];
                $firm = Mremont::findOne(['id'=>$id]); 
                $kaz = (new \yii\db\Query())->select('*')->from('mremont')->where('id=1')->one();
               
                $query='';
                    if ($date1!='' &&  $date2!='')
                    {
                       $query=' && (dates>="'.date('Y-m-d',strtotime($date1)).'" && dates<="'.date('Y-m-d',strtotime($date2)).'")'; 
                    }
                    else
                    {
                        if ($date1!='')
                        {
                          $query=' && dates>="'.date('Y-m-d',strtotime($date1)).'"'; 
                        }
                        elseif($date2!='')
                        {
                            $query=' && dates<="'.date('Y-m-d',strtotime($date2)).'"';
                        }
                    }   
                     
                     
                     if ($tip==1)
                     {
                         $tor_pr = (new \yii\db\Query())
                                    ->select(['*','COUNT(vagon_id) AS count']) 
                                    ->from('prichina')
                                    ->where('mremont_id='.$id.' && tip='.$tip.$query)
                                    ->groupBy(['spr_id','remont_id','cenaed']) 
                                    ->all();    
                        
                     }
                     elseif($tip==2)
                     {
                         $tor_pr = (new \yii\db\Query())
                                    ->select(['*','COUNT(vagon_id) AS count']) 
                                    ->from('tordrkr')
                                    ->where('mremont_id='.$id.' && tip='.$tip.$query)
                                    ->groupBy(['remont_id','cenaed']) 
                                    ->all();    
                     }
                      
                      $tor_detail = (new \yii\db\Query())
                                    ->select(['*','SUM(kol) AS count']) 
                                    ->from('detail')
                                    ->where('mremont_id='.$id.' && tip='.$tip.$query)
                                    ->groupBy(['detailspr_id','cenaed']) 
                                    ->all();    
                      
                     $revizia=(new \yii\db\Query())
                                    ->select(['*','SUM(kol) AS count']) 
                                    ->from('revizia')
                                    ->where('mremont_id='.$id.' && tip='.$tip.$query) 
                                    ->groupBy(['reviziaspr_id','cenaed']) 
                                    ->all();   

                     $usluga=(new \yii\db\Query())
                                    ->select(['*','SUM(kol) AS count']) 
                                    ->from('usluga')
                                    ->where('mremont_id='.$id.' && tip='.$tip.$query) 
                                    ->groupBy(['uslugaspr_id','cenaed']) 
                                    ->all();   
                      
                     for ($i=0;$i<count($tor_pr);$i++){ $tor_pr[$i]['priznak']='Причины';  }
                     for ($i=0;$i<count($tor_detail);$i++){$tor_detail[$i]['priznak']='Детали'; }
                     for ($i=0;$i<count($revizia);$i++){ $revizia[$i]['priznak']=$model->DataPrice[$revizia[$i]['reviziaspr_id']]; } 
                     for ($i=0;$i<count($usluga);$i++){ $usluga[$i]['priznak']=$model->DataPrice[$usluga[$i]['uslugaspr_id']]; } 
                     
                    (!empty($tor_pr)) ? $all=[$tor_pr,$tor_detail,$revizia,$usluga] : $all=[]; // если нет причин, то отчеты ТОР и ДР не показываем 
                    /** объединяем массив детали и массив причины **/
                     for($i=0;$i<count($all);$i++)
                     {
                        if(count($tor)>0)
                        {
                             $k=count($tor);
                             for ($j=0;$j<count($all[$i]);$j++) 
                             {
                                 $tor[$k]=$all[$i][$j];
                                 $k++;
                             }
                        }
                        else
                        {
                             for($j=0;$j<count($all[$i]);$j++)
                             {
                                 $tor[$j]=$all[$i][$j];
                             } 
                        }
                     }

                    if ( !empty($tor) )
                    {
                    /** считаем все причрины и детали **/
                        $ndc=0;$itog=0;$cena=0;$kol=0;$cenausd=0;$ndcusd=0;$onecena=0;$onendc=0;$oneitog=0;$dcena=0;$dcenausd=0;$kol=0;$dkol=0;$nottor_kol=0;$nottor_cena=0;
                        foreach($tor as $key=>$value)    
                        {
                            //  Просчитываем сразу  цены у каждой строки (цена, ндс, итог) чтобы просто выводить в представлении ***/
                                 if (isset($value['spr_id']))  
                                  {
                                    $tor[$key]['title']=$model->DataPrice[$value['spr_id']];
                                  }
                                  elseif(isset($value['detailspr_id']))
                                  {
                                    $tor[$key]['title']=$model->DataPrice[$value['detailspr_id']];
                                  }
                                  elseif(isset($value['reviziaspr_id']))
                                  {
                                    $tor[$key]['title']=$model->DataPrice[$value['reviziaspr_id']];
                                  }
                                  elseif(isset($value['uslugaspr_id']))
                                  {
                                    $tor[$key]['title']=$model->DataPrice[$value['uslugaspr_id']];
                                  }
                                  elseif($tip==2)
                                  {
                                    $tor[$key]['title']=$model->DataPrice[$value['remont_id']];
                                  }
                               
                               $titlename=$model->DataMremont[$value['mremont_id']];   
                               $tor[$key]['rep']='ТОР';
                               $tor[$key]['mremont_id']=$model->DataMremont[$value['mremont_id']];
                               $tor[$key]['lastname']=$model->DataMremont[$value['mremont_id']].' '.$model->DataOmas[$value['tip']];
                               $tor[$key]['cena']=round($value['cenaed']*$value['count'],2);
                               $tor[$key]['ndc']=round($tor[$key]['cena']*($firm->ndc/100),2);
                               $tor[$key]['itog']=round($tor[$key]['cena']+$tor[$key]['ndc'],2);
                            
                             //** *****************Конец *********************/
                             if ($key==count($tor)-1)
                             {
                                // считаем общее кол-во деталей причин и т.д. 
                                 $index=[];$ind='';$sum_all=0;
                                 
                                 foreach($tor as $key=>$value)
                                 {
                                      
                                      // определяем индекс принадлежности к детали или причине и т.д.
                                      if (!in_array($value['priznak'],$index) )
                                      {
                                        $index[]=$value['priznak'];
                                        $parent=array_search($value['priznak'],$index);
                                        $ind=$parent;
                                      }
                                      else
                                      {
                                        $parent=array_search($value['priznak'],$index);
                                        $ind=$parent;
                                      }
                                      
                                      $finally=[];
                                      if ($tip==1) {$identi='ТОР';} elseif($tip==2) {$identi='ДР';} elseif($tip==3) {$identi='КР';} 
                                      
                                      if ($firm->valuta==1)//тенге
                                      {
                                          if ($value['priznak']=='Причины')
                                          {
                                            $kol+= $value['count'];
                                          }
                                        
                                          $mas[$ind][0]['priznak']=$value['priznak'];
                                          $mas[$ind][0]['title']=$value['priznak'].' в тенге';
                                          if (!isset($mas[$ind][0]['count'])) { $mas[$ind][0]['count']=''; } $mas[$ind][0]['count']+=$value['count']; 
                                          $mas[$ind][0]['cenaed']='';
                                          if (!isset($mas[$ind][0]['cena'])) {$mas[$ind][0]['cena']='';} $mas[$ind][0]['cena']+=$value['cena']; 
                                          //причины ндс итог
                                           $ndc=round($value['cena']*($firm->ndc/100),2);
                                           $itog=round($value['cena']+$ndc,2);
                                          
                                          if (!isset($mas[$ind][0]['ndc'])) {$mas[$ind][0]['ndc']='';} $mas[$ind][0]['ndc']+=$ndc;
                                          if (!isset($mas[$ind][0]['itog'])){$mas[$ind][0]['itog']='';} $mas[$ind][0]['itog']+=$itog;
                                          $mas[$ind][0]['color']='blue';
                                          $mas[$ind][0]['rep']='ТОР';
                                          //------------ДОЛЛАРЫ----------------------------- 
                                          $mas[$ind][1]['priznak']=$value['priznak'];
                                          $mas[$ind][1]['title']=$value['priznak'].' в долларах';
                                          if (!isset($mas[$ind][1]['count'])) { $mas[$ind][1]['count']=''; } $mas[$ind][1]['count']+=$value['count'];
                                          $mas[$ind][1]['cenaed']='';
                                          // получаем индекс, в котором хранится значение доллара
                                          $key_val=  array_search(3, $valid); 
                                          if (!isset($c[$ind][1]['cena'])) { $c[$ind][1]['cena']=''; } $c[$ind][1]['cena']+=$value['cena']/$val[$key_val];// если сразу переводить в долларах и окргулять теряются сотые 
                                          $mas[$ind][1]['cena']=round($c[$ind][1]['cena'],2);
                                          // ндс итог
                                          $usdcena=round($c[$ind][1]['cena'],2);
                                          
                                          $mas[$ind][1]['ndc']=round($usdcena*($firm->ndc/100),2);
                                          $mas[$ind][1]['itog']=round($usdcena+$ndc,2);
                                          $mas[$ind][1]['color']='red';
                                          $mas[$ind][1]['rep']='ТОР';
                                          //----------------- ИТОГО последние строки таблицы
                                          $sum_all+=$value['cena']; // подсчитываем цены все в тенге
                                          
                                          if ( $key==count($tor)-1 )
                                          {
                                            $ndc=round($sum_all*($firm->ndc/100),2);
                                            $usd=round($sum_all/$val[$key_val],2);
                                            $usdndc=round($usd*($firm->ndc/100),2);
                                            $mas[count($mas)][0]=['title'=>'Итого в тенге','count'=>$kol,'cena'=>$sum_all,'ndc'=>$ndc,'itog'=>round($sum_all+$ndc,2),'color'=>'blue bgblue','rep'=>'ТОР'];
                                            $mas[count($mas)][0]=['title'=>'Итого в долларах','count'=>$kol,'cena'=>$usd,'ndc'=>$usdndc,'itog'=>round($usd+$usdndc,2),'color'=>'red bgblue','rep'=>'ТОР'];
                                          
                                            $finally[]=['title'=>$titlename.' '.$identi,'cenaed'=>'','cena'=>$sum_all,'ndc'=>$ndc,'itog'=>round($sum_all+$ndc,2),'ucena'=>$usd,'undc'=>$usdndc,'uitog'=>round($usd+$usdndc,2),'column'=>'blue','rep'=>'ТОР'];      
                                          }                                          
                                      }
                                      elseif($firm->valuta==3)//доллары
                                      {   
                                          if ($value['priznak']=='Причины')
                                          {
                                            $kol+= $value['count'];
                                          }

                                          $mas[$ind][0]['priznak']=$value['priznak'];
                                          $mas[$ind][0]['title']=$value['priznak'].' в долларах';
                                          if (!isset($mas[$ind][0]['count'])) { $mas[$ind][0]['count']=''; } $mas[$ind][0]['count']+=$value['count']; 
                                          $mas[$ind][0]['cenaed']='';
                                          if (!isset($mas[$ind][0]['cena'])) {$mas[$ind][0]['cena']='';} $mas[$ind][0]['cena']+=$value['cena']; 
                                          //причины ндс итог
                                           $ndc=round($value['cena']*($firm->ndc/100),2);
                                           $itog=round($value['cena']+$ndc,2);
                                          if (!isset($mas[$ind][0]['ndc'])) {$mas[$ind][0]['ndc']='';} $mas[$ind][0]['ndc']+=$ndc;
                                          if (!isset($mas[$ind][0]['itog'])){$mas[$ind][0]['itog']='';} $mas[$ind][0]['itog']+=$itog;
                                          $mas[$ind][0]['color']='red';
                                          $mas[$ind][0]['rep']='ТОР';

                                          //----------------- ИТОГО последние строки таблицы
                                          $sum_all+=$value['cena']; // подсчитываем цены все в тенге

                                          if ( $key==count($tor)-1 )
                                          {
                                            $ndc=round($sum_all*($firm->ndc/100),2);
                                            // получаем индекс, в котором хранится значение курса доллара
                                            $key_val=  array_search(3, $valid); 
                                            $tg=round($sum_all*$val[$key_val],2);
                                            $tgndc=round($tg*($kaz['ndc']/100),2);
                                            
                                            $mas[count($mas)][0]=['title'=>'Итого в долларах','cenaed'=>'','count'=>$kol,'cena'=>$sum_all,'ndc'=>$ndc,'itog'=>round($sum_all+$ndc,2),'color'=>'red bgblue','rep'=>'ТОР'];
                                            $mas[count($mas)][0]=['title'=>'Итого в тенге','cenaed'=>'','count'=>$kol,'cena'=>$tg,'ndc'=>$tgndc,'itog'=>round($tg+$tgndc,2),'color'=>'blue bgblue','rep'=>'ТОР'];
                                          
                                           $finally[]=['title'=>$titlename.' '.$identi,'cenaed'=>'','cena'=>$tg,'ndc'=>$tgndc,'itog'=>round($tg+$tgndc,2),'ucena'=>$sum_all,'undc'=>$ndc,'uitog'=>round($sum_all+$ndc,2),'column'=>'blue','rep'=>'ТОР'];      
                                          }
                                      }
                                      else //рубли или другая валюта
                                      {
                                          if ($value['priznak']=='Причины')
                                          {
                                            $kol+= $value['count'];
                                          }
 
                                          $mas[$ind][0]['priznak']=$value['priznak'];
                                          $mas[$ind][0]['title']=$value['priznak'].' в рублях';
                                          if (!isset($mas[$ind][0]['count'])) { $mas[$ind][0]['count']=''; } $mas[$ind][0]['count']+=$value['count']; 
                                          $mas[$ind][0]['cenaed']='';
                                          if (!isset($mas[$ind][0]['cena'])) {$mas[$ind][0]['cena']='';} $mas[$ind][0]['cena']+=$value['cena']; 
                                          //причины ндс итог
                                           $ndc=round($value['cena']*($firm->ndc/100),2);
                                           $itog=round($value['cena']+$ndc,2);
                                          
                                          if (!isset($mas[$ind][0]['ndc'])) {$mas[$ind][0]['ndc']='';} $mas[$ind][0]['ndc']+=$ndc;
                                          if (!isset($mas[$ind][0]['itog'])){$mas[$ind][0]['itog']='';} $mas[$ind][0]['itog']+=$itog;
                                          $mas[$ind][0]['color']='green';
                                          $mas[$ind][0]['rep']='ТОР';
                                          //----------------- ИТОГО последние строки таблицы
                                          $sum_all+=$value['cena']; // подсчитываем цены все в тенге

                                          if ( $key==count($tor)-1 )
                                          {
                                            $ndc=round($sum_all*($firm->ndc/100),2);
                                            // получаем индекс, в котором хранится значение курса текущей валюты к тенге
                                            $key_val=  array_search($firm->valuta, $valid); 
                                            $tg=round($sum_all*$val[$key_val],2);
                                            $tgndc=round($tg*($kaz['ndc']/100),2);
                                            // получаем индекс, в котором хранится значение курса доллара к тенге
                                            $key_val=  array_search(3, $valid); 
                                            $usd=round($tg/$val[$key_val],2);
                                            $usdndc=round($usd*($kaz['ndc']/100),2);
                                            $mas[count($mas)][0]=['title'=>'Итого в рублях','count'=>$kol,'cenaed'=>'','cena'=>$sum_all,'ndc'=>$ndc,'itog'=>round($sum_all+$ndc,2),'color'=>'green bgblue','rep'=>'ТОР'];
                                            $mas[count($mas)][0]=['title'=>'Итого в тенге','count'=>$kol,'cenaed'=>'','cena'=>$tg,'ndc'=>$tgndc,'itog'=>round($tg+$tgndc,2),'color'=>'blue bgblue','rep'=>'ТОР'];
                                            $mas[count($mas)][0]=['title'=>'Итого в долларах','count'=>$kol,'cenaed'=>'','cena'=>$usd,'ndc'=>$usdndc,'itog'=>round($usd+$usdndc,2),'color'=>'red bgblue','rep'=>'ТОР'];
                                          
                                            $finally[]=['title'=>$titlename.' '.$identi,'cenaed'=>'','cena'=>$tg,'ndc'=>$tgndc,'itog'=>round($tg+$tgndc,2),'ucena'=>$usd,'undc'=>$usdndc,'uitog'=>round($usd+$usdndc,2),'column'=>'blue','rep'=>'ТОР'];     
                                          }
                                      }
                                 }
                             } // если прошли все элементы считаем общее кол-во по признакам
                        } // основной цикл

                        // разбивку по признаку записываем в главный массив
                        
                        if ( !empty($tor) )
                        {  
                                foreach($mas as $key=>$item)
                                {  
                                   foreach($item as $value)
                                   {
                                        $tor[]=['title'=>$value['title'],'count'=>$value['count'],'cenaed'=>'','cena'=>$value['cena'],'ndc'=>$value['ndc'],'itog'=>$value['itog'],'color'=>$value['color'],'rep'=>'ТОР' ];
                                   }
                                }

                                 
                                
                                if (empty($finish))
                                {
                                   if (empty($export))
                                   {
                                    $tipmas[]=$this->ConstructTable(null,$tor,$identi);   
                                   }
                                   else
                                   {
                                     $tipmas=$tor;   
                                   }   
                                }
                                else // сохраняем итоги в главный отчет
                                {
                                    $tipmas=$finally;
                                }   
                        }
                 }    
           
           return $tipmas;    
    }



     /** **************************************************
      ****************** СОБСТВЕННИК **********************
      *****************************************************
      **/ 
      protected function Sobstvenik($tip,$id,$valid,$val,$model,$date1,$date2, $querysobs,$finish,$export)
      {
             $tor=[];$sobstvenik=[];
             $firm = Mremont::findOne(['id'=>$id]); 
             $kaz = (new \yii\db\Query())->select('*')->from('mremont')->where('id=1')->one();
             
                     /** Получаем виды вагонов****/
                      $vvagon=(new \yii\db\Query())->select('title')->from('vvagon')->all();
                      foreach($vvagon as $item)
                      {
                        $vv[]=$item['title'];
                      }
                      $allnames=mb_strtolower(implode(',',$vv),'UTF-8');
                     
                    /** Получаем масив собственников сгруппированных ТВТ, ТТ, СГ-ТРАНС, КАРГОТРАНС,**/
                     //$querysobs = (new \yii\db\Query())->select('*')->from('sobstvenik')->all();    
                     foreach ($querysobs as $item)
                     {
                          if ($item['type']=='')
                          {
                             $sobs[]=$item['id'];
                             $temp[]=$item['id'];               
                          } 
                          else
                          {
                              $k=array_search($item['type'],$temp);
                              $sobs[$k]=$sobs[$k].','.$item['id'];
                          }
                     }
             
                    /** Запрос даты**/
                    $query='';
                    if ($date1!='' &&  $date2!='')
                    {
                       $query=' && (dates>="'.date('Y-m-d',strtotime($date1)).'" && dates<="'.date('Y-m-d',strtotime($date2)).'")'; 
                    }
                    else
                    {
                        if ($date1!='')
                        {
                          $query=' && dates>="'.date('Y-m-d',strtotime($date1)).'"'; 
                        }
                        elseif($date2!='')
                        {
                            $query=' && dates<="'.date('Y-m-d',strtotime($date2)).'"';
                        }
                    }   
        
                  // сбрасываем используемый ранее массив 
                   /** Формируем запрос Собствеников с группировкой, + основа тип вагона (крытый, газ, нефть) **/   
                  for ($i=0;$i<count($sobs);$i++)
                  {   
                      $temp=[];
                      if ($tip==1)
                      {
                       $pr = (new \yii\db\Query())
                                    ->select(['*','COUNT(vagon_id) AS count']) 
                                    ->from('prichina')
                                    ->where('mremont_id='.$id.' && tip='.$tip.' && sobstvenik_id IN ('.$sobs[$i].')'.$query) 
                                    ->groupBy(['spr_id','remont_id','vvagon_id','cenaed']) 
                                    ->all();    
                      }
                      elseif($tip==2)
                      {
                       $pr = (new \yii\db\Query())
                                    ->select(['*','COUNT(vagon_id) AS count']) 
                                    ->from('tordrkr')
                                    ->where('mremont_id='.$id.' && tip='.$tip.' && sobstvenik_id IN ('.$sobs[$i].')'.$query) 
                                    ->groupBy(['remont_id','vvagon_id','cenaed']) 
                                    ->all();    
                      }
                      $detail = (new \yii\db\Query())
                                    ->select(['*','SUM(kol) AS count']) 
                                    ->from('detail')
                                    ->where('mremont_id='.$id.' && tip='.$tip.' && sobstvenik_id IN ('.$sobs[$i].')'.$query)
                                    ->groupBy(['detailspr_id','vvagon_id','cenaed']) 
                                    ->all();    
                     $revizia=(new \yii\db\Query())
                                    ->select(['*','SUM(kol) AS count']) 
                                    ->from('revizia')
                                    ->where('mremont_id='.$id.' && tip='.$tip.' && sobstvenik_id IN ('.$sobs[$i].')'.$query) 
                                    ->groupBy(['reviziaspr_id','vvagon_id','cenaed']) 
                                    ->all();   
                     $usluga=(new \yii\db\Query())
                                    ->select(['*','SUM(kol) AS count']) 
                                    ->from('usluga')
                                    ->where('mremont_id='.$id.' && tip='.$tip.' && sobstvenik_id IN ('.$sobs[$i].')'.$query) 
                                    ->groupBy(['uslugaspr_id','vvagon_id','cenaed']) 
                                    ->all();   
                   
                     for ($j=0;$j<count($pr);$j++){ $pr[$j]['priznak']='Причины';  }
                     for ($j=0;$j<count($detail);$j++){ $detail[$j]['priznak']='Детали'; }
                     for ($j=0;$j<count($revizia);$j++){ $revizia[$j]['priznak']=$model->DataPrice[$revizia[$j]['reviziaspr_id']]; } 
                     for ($j=0;$j<count($usluga);$j++){ $usluga[$j]['priznak']=$model->DataPrice[$usluga[$j]['uslugaspr_id']]; } 
                    
                    (!empty($pr)) ? $all=[$pr,$detail,$revizia,$usluga] : $all=[]; 
  
                    /** объединяем массив детали и массив причины и т.д. по собственникам**/
                       $keyind=count($tor);// индекс для проверки есть ли хоть один собсвенник в массиве
                      
                       for($z=0;$z<count($all);$z++)
                       {
                            if (count($all[$z])>0) // проверяем не пустые ли массивы причин деталей услуг и т.д.
                            {
                                    if(!empty($tor[$keyind])) // проверяем колво данных в отчете по типу собственника
                                    {
                                         $k=count($tor[$keyind]);
                                         for ($j=0;$j<count($all[$z]);$j++) 
                                         {
                                             $tor[$keyind][$k]=$all[$z][$j];
                                             $k++;
                                         }
                                    }
                                    else
                                    {
                                         for($j=0;$j<count($all[$z]);$j++)
                                         {
                                             $tor[$keyind][$j]=$all[$z][$j];
                                         } 
                                    }
                            }        
                       }

                  }   

                  /** *********************************************
                  ****Высчитываем цены и помещаем данные в массив * 
                  *************************************************
                  *    Массив $tor[$i] смодержит один из отчетов 
                  *    В зависимости от индекса - ТВТ, ТТ, СГ-Транс и Карго
                  *    Массив $mas[] высчитывает по каждому итоги
                  **/
                   $new=[];   
                   $finally=[];
                   for($i=0;$i<count($tor);$i++)  // массив содержит ТВТ, ТТ, СГ-Транс и Карго (всех собственников)
                   {        // обнуление цен для следующего собственника
                            $kol=0;$cena=0;$ndc=0;$itog=0;
                             $mas=[];
                             $rumas=[];
                             $index=[];
                             $vvagin_sbros_index=[];
                            unset($gl_ind);
                            unset($ind);
                            unset($gl);
                            unset($parent);
                            unset($q); // для фильтровки названия причин деталей по типу вагона 
                            // Собираем отчет либо ТВТ либо ТТ или СГ или Краго $tor[$i] это один отчет в данном массиве 4 отчета
                            foreach($tor[$i] as $key=>$value)    
                            {
                                  // определяем индекс принадлежности к виду вагона, фильтрацию делаем ГАЗ нефть и т.д.
                                  if (!in_array($value['vvagon_id'],$vvagin_sbros_index))
                                  {
                                    $vvagin_sbros_index[]=$value['vvagon_id'];
                                    $gl=array_search($value['vvagon_id'],$vvagin_sbros_index);
                                    $gl_ind=$gl;
                                    $index[$gl_ind]=[];// создаем массив принадлежности к детали или причине
                                    $q=0;
                                  }
                                  else
                                  {
                                    $gl=array_search($value['vvagon_id'],$vvagin_sbros_index);
                                    $gl_ind=$gl;
                                    $q=count($new[$i][$gl_ind]);
                                  }

                                  // определяем индекс принадлежности к детали или причине
                                  if (!in_array($value['priznak'],$index[$gl_ind]) )
                                  {
                                    $index[$gl_ind][]=$value['priznak'];
                                    $parent=array_search($value['priznak'],$index[$gl_ind]);
                                    $ind=$parent;
                                    $mas[$gl_ind][$ind][0]['count']=0;
                                    $mas[$gl_ind][$ind][0]['cena']=0;  
                                  }
                                  else
                                  {
                                    $parent=array_search($value['priznak'],$index[$gl_ind]);
                                    $ind=$parent;
                                  }
                                  
                                //  Просчитываем сразу  цены у каждой строки (цена, ндс, итог) чтобы просто выводить в представлении 
                                 if (isset($value['spr_id']))  
                                  {
                                    $new[$i][$gl_ind][$q]['title']=$model->DataPrice[$value['spr_id']]; // вместо id'шников названия 
                                    $tt[$i][]=$value['id'];
                                  }
                                  elseif(isset($value['detailspr_id']))
                                  {
                                    $new[$i][$gl_ind][$q]['title']=$model->DataPrice[$value['detailspr_id']];
                                  }
                                  elseif(isset($value['reviziaspr_id']))
                                  {
                                    $new[$i][$gl_ind][$q]['title']=$model->DataPrice[$value['reviziaspr_id']];
                                  }
                                  elseif(isset($value['uslugaspr_id']))
                                  {
                                    $new[$i][$gl_ind][$q]['title']=$model->DataPrice[$value['uslugaspr_id']];
                                  }
                                  elseif($tip==2)
                                  {
                                    $new[$i][$gl_ind][$q]['title']=$model->DataPrice[$value['remont_id']];
                                  }
                                  

                                  if ($tip==1) {$identi='ТОР';} elseif($tip==2) {$identi='ДР';} elseif($tip==3) {$identi='КР';} 
                                  
                                  foreach ($querysobs as $item) // чтобы в названиях label tab'ов были главные названия собствеников, а не дочерние
                                  {
                                      if ($item['id']==$value['sobstvenik_id'])
                                      {
                                         if ($item['type']=='')
                                         {
                                            $new[$i][$gl_ind][$q]['sobstvenik_id']=$model->DataSobstvenik[$item['id']];
                                            $titlename=$model->DataMremont[$value['mremont_id']].' '.$identi.' '.$model->DataSobstvenik[$item['id']].' (не суммировать)'; 
                                            $new[$i][$gl_ind][$q]['lastname']=$model->DataMremont[$value['mremont_id']].' '.$identi.' '.$model->DataSobstvenik[$item['id']];
                                         }
                                         else
                                         {
                                           $new[$i][$gl_ind][$q]['sobstvenik_id']=$model->DataSobstvenik[$item['type']];
                                           $titlename=$model->DataMremont[$value['mremont_id']].' '.$identi.' '.$model->DataSobstvenik[$item['type']].' (не суммировать)'; 
                                           $new[$i][$gl_ind][$q]['lastname']=$model->DataMremont[$value['mremont_id']].' '.$identi.' '.$model->DataSobstvenik[$item['type']];
                                         }
                                      } 
                                  }
                                  
                                  $new[$i][$gl_ind][$q]['rep']='SOBS';
                                  $new[$i][$gl_ind][$q]['vvagon_id']=$model->DataVvagon[$value['vvagon_id']];
                                  $new[$i][$gl_ind][$q]['mremont_id']=$model->DataMremont[$value['mremont_id']];
                                  $new[$i][$gl_ind][$q]['count']=$value['count'];
                                  /** Считаем каждую строку ЦЕНА НДС ИТОГ столбцы**/
                                  $cena=round($value['cenaed']*$value['count'],2);
                                  $ndc=round($cena*($firm->ndc/100),2);
                                  
                                  $itog=round($cena+$ndc,2);
                                  /** Создаем в массиве новые динамические переменные**/
                                  $new[$i][$gl_ind][$q]['cenaed']=$value['cenaed'];
                                  $new[$i][$gl_ind][$q]['cena']=$cena;
                                  $new[$i][$gl_ind][$q]['ndc']=$ndc;
                                  $new[$i][$gl_ind][$q]['itog']=$itog;
                                  if (isset($value['reviziaspr_id'])) 
                                  {
                                    $new[$i][$gl_ind][$q]['priznak']='Ревизия';
                                  }
                                  elseif(isset($value['uslugaspr_id']))
                                  {
                                    $new[$i][$gl_ind][$q]['priznak']='Услуга';
                                  }
                                  else
                                  {
                                    $new[$i][$gl_ind][$q]['priznak']=$value['priznak'];
                                  }
                                  $q++;
                                  /** Создание массива для подсчета общего кол-ва причин, деталей, ревизии отсортированный по типу вагонов **/
                                  if($cena>0)
                                  { 
                                      $mas[$gl_ind][$ind][0]['rep']='SOBS';
                                      $mas[$gl_ind][$ind][0]['priznak']=$value['priznak'];
                                      $mas[$gl_ind][$ind][0]['vvagon_id_tit']=$model->DataVvagon[$value['vvagon_id']];
                                      $mas[$gl_ind][$ind][0]['title']=$value['priznak'].' ('.$model->DataVvagon[$value['vvagon_id']].')';
                                      $mas[$gl_ind][$ind][0]['count']+=$value['count'];
                                      $mas[$gl_ind][$ind][0]['cena']+=$cena;
                                      $mas[$gl_ind][$ind][0]['cenaed']='';
                                              
                                      $mas[$gl_ind][$ind][0]['ndc']=round($mas[$gl_ind][$ind][0]['cena']*($firm->ndc/100),2);
                                      $mas[$gl_ind][$ind][0]['itog']=round($mas[$gl_ind][$ind][0]['cena']+$mas[$gl_ind][$ind][0]['ndc'],2);
                                     if($firm->valuta==1)  // валюта тенге ндс(РК)
                                     {
                                      $mas[$gl_ind][$ind][0]['color']='normal';
                                     }
                                     elseif($firm->valuta==2)// рубли ндс(России) 
                                     {
                                      $mas[$gl_ind][$ind][0]['color']='green';
                                     }  
                                     elseif($firm->valuta==3)// доллары ндс страны поставщика
                                     {
                                      $mas[$gl_ind][$ind][0]['color']='red';
                                     } 
                                 }  
                            }
                            
                            $allcount=0;
                            $allcena=0;
                            /** Подсчет итогов по одному типу вагона**/
                                    for ($j=0;$j<count($mas);$j++)
                                    {
                                        $icount=0;
                                        $icena=0;
        
                                            foreach($mas[$j] as $value) // здесь проходим причины и детали по одному виду вагона для каждого считаем цены и помещяем вконец каждого
                                            {
                                                foreach($value as $item)
                                                {
                                                   if ($item['priznak']=='Причины')
                                                   {
                                                    $icount+=$item['count'];
                                                   }
                                                   $icena+=$item['cena'];
                                                   $title='Итого ('.mb_strtolower($item['vvagon_id_tit'],'UTF-8').') в тенге';  
                                                   $usdtitle='Итого ('.mb_strtolower($item['vvagon_id_tit'],'UTF-8').') в долларах';
                                                   $titlerub='Итого ('.mb_strtolower($item['vvagon_id_tit'],'UTF-8').') в рублях';  
                                                } 
                                            }                              
                                        
                                            $razmer=count($mas[$j]);
                                           
                                            if ($firm->valuta==1) // если тенге
                                            {
                                              $ndc=round($icena*($firm->ndc/100),2);
                                                $key_val=  array_search(3, $valid); // получаем индекс, в котором хранится значение доллара
                                                $usdcena=round($icena/$val[$key_val],2);
                                                $usdndc=round($usdcena*($firm->ndc/100),2);
                                              
                                              $mas[$j][$razmer][0]=['title'=>$title,'count'=>$icount,'cenaed'=>'','cena'=>$icena,'ndc'=>$ndc,'itog'=>round($icena+$ndc,2),'color'=>'blue itog','rep'=>'SOBS'];   
                                              $mas[$j][$razmer+1][0]=['title'=>$usdtitle,'count'=>$icount,'cenaed'=>'','cena'=>$usdcena,'ndc'=>$usdndc,'itog'=>round($usdcena+$usdndc,2),'color'=>'red itog','rep'=>'SOBS']; 
                                            }
                                            elseif($firm->valuta==3) // если доллары
                                            {
                                                $ndc=round($icena*($firm->ndc/100),2);

                                                $mas[$j][$razmer][0]=['title'=>$usdtitle,'count'=>$icount,'cenaed'=>'','cena'=>$icena,'ndc'=>$ndc,'itog'=>round($icena+$ndc,2),'color'=>'red itog','rep'=>'SOBS'];   
                                            }
                                            else // если рубли или другая валюта кроме тенге и доллара
                                            {
                                                $ndc=round($icena*($firm->ndc/100),2);

                                                $mas[$j][$razmer][0]=['title'=>$titlerub,'count'=>$icount,'cenaed'=>'','cena'=>$icena,'ndc'=>$ndc,'itog'=>round($icena+$ndc,2),'color'=>'green itog','rep'=>'SOBS'];   
                                            }
                                           
                                        $allcount+=$icount;
                                        $allcena+=$icena;
                                    }                              
                            /** **** Итоги по всем типам вагонов одного отчета**** **/ 
                            $rubtitle='Итого ('.$allnames.') в рублях';
                            $alltitle='Итого ('.$allnames.') в тенге';
                            $usd_alltitle='Итого ('.$allnames.') в долларах';
                            //$razmer=count($mas[$j]);
                            
                            if ($firm->valuta==1) // считаем тенге
                            {
                                $allndc=round($allcena*($firm->ndc/100),2);
                                //перезаписываем итоговые переменные (крытый, нефть, газ)
                                $key_val=  array_search(3, $valid); // получаем индекс, в котором хранится значение доллара
                                $usd_allcena=round($allcena/$val[$key_val],2);
                                $usd_allndc=round($usd_allcena*($firm->ndc/100),2);
                                
                                $mas[count($mas)][0][0]=['title'=>$alltitle,'count'=>$allcount,'cenaed'=>'','cena'=>$allcena,'ndc'=>$allndc,'itog'=>round($allcena+$allndc,2),'color'=>'blue bgblue','rep'=>'SOBS'];
                                $mas[count($mas)][0][0]=['title'=>$usd_alltitle,'count'=>$allcount,'cenaed'=>'','cena'=>$usd_allcena,'ndc'=>$usd_allndc,'itog'=>round($usd_allcena+$usd_allndc,2),'color'=>'red bgblue','rep'=>'SOBS'];
                                // массив для финального отчета
                                $finally[]=['title'=>$titlename,'cenaed'=>'','cena'=>$allcena,'ndc'=>$allndc,'itog'=>round($allcena+$allndc,2),'ucena'=>$usd_allcena,'undc'=>$usd_allndc,'uitog'=>round($usd_allcena+$usd_allndc,2),'rep'=>'SOBS'];
                            }
                           elseif($firm->valuta==3)// доллары
                           {
                                 // тенге
                                $allndc=round($allcena*($firm->ndc/100),2);
                                //перезаписываем итоговые переменные (крытый, нефть, газ)
                                $key_val=  array_search(3, $valid); // получаем индекс, в котором хранится значение доллара
                                $tg_allcena=round($allcena*$val[$key_val],2);
                                $tg_allndc=round($tg_allcena*($kaz['ndc']/100),2);

                                $mas[count($mas)][0][0]=['title'=>$usd_alltitle,'count'=>$allcount,'cenaed'=>'','cena'=>$allcena,'ndc'=>$allndc,'itog'=>round($allcena+$allndc,2),'color'=>'red bgblue','rep'=>'SOBS'];
                                $mas[count($mas)][0][0]=['title'=>$alltitle,'count'=>$allcount,'cenaed'=>'','cena'=>$tg_allcena,'ndc'=>$tg_allndc,'itog'=>round($tg_allcena+$tg_allndc,2),'color'=>'blue bgblue','rep'=>'SOBS'];
                                // массив для финального отчета
                                $finally[]=['title'=>$titlename,'cenaed'=>'','cena'=>$tg_allcena,'ndc'=>$tg_allndc,'itog'=>round($tg_allcena+$tg_allndc,2),'ucena'=>$allcena,'undc'=>$allndc,'uitog'=>round($allcena+$allndc,2),'rep'=>'SOBS'];
                           } 
                           else // рубли или другая валюта кроме тенге и доллара
                            {
                                $allndc=round($allcena*($firm->ndc/100),2);
                                // тенге
                                $key_val=  array_search($firm->valuta, $valid); //получаем индекс в курса валюты к тенге 
                                $tg_allcena=round($allcena*$val[$key_val],2);
                                $tg_allndc=round($tg_allcena*($kaz['ndc']/100),2);
                                // доллары
                                $key_val=  array_search(3, $valid); //получаем индекс в курса валюты к тенге
                                $usd_allcena=round($tg_allcena/$val[$key_val],2);
                                $usd_allndc=round($usd_allcena*($kaz['ndc']/100),2);
                                                       //$razmer
                                $mas[count($mas)][0][0]=['title'=>$rubtitle,'count'=>$allcount,'cenaed'=>'','cena'=>$allcena,'ndc'=>$allndc,'itog'=>round($allcena+$allndc,2),'color'=>'green bgblue','rep'=>'SOBS'];
                                $mas[count($mas)][0][0]=['title'=>$alltitle,'count'=>$allcount,'cenaed'=>'','cena'=>$tg_allcena,'ndc'=>$tg_allndc,'itog'=>round($tg_allcena+$tg_allndc,2),'color'=>'blue bgblue','rep'=>'SOBS'];
                                $mas[count($mas)][0][0]=['title'=>$usd_alltitle,'count'=>$allcount,'cenaed'=>'','cena'=>$usd_allcena,'ndc'=>$usd_allndc,'itog'=>round($usd_allcena+$usd_allndc,2),'color'=>'red bgblue','rep'=>'SOBS'];
                                // массив для финального отчета
                                $finally[]=['title'=>$titlename,'cenaed'=>'','cena'=>$tg_allcena,'ndc'=>$tg_allndc,'itog'=>round($tg_allcena+$tg_allndc,2),'ucena'=>$usd_allcena,'undc'=>$usd_allndc,'uitog'=>round($usd_allcena+$usd_allndc,2),'rep'=>'SOBS'];
                            }                           
                                                             
                       $tmp[]=$mas;
                       
                   }  
                     
                              
                  /**
                   * Массив new содржит детальные данные отчетов
                   * Массив $tmp их итоги
                   /** Готовим таблицу**/
                   $t=[];
                   for($i=0;$i<count($new);$i++)  
                   { 
                    
                      unset($label);
                      unset($html);
                      unset($m);
                      unset($t);
                      unset($z);
                      
                       if ( count($new[$i])>0 )
                       { 
                                    foreach($new[$i] as $dval)       
                                    {
                                       
                                        for($j=0;$j<count($dval);$j++)
                                        {
                                             $m[]=$dval[$j]; 
                                             if ($tip==1) {$tip='ТОР';} elseif($tip==2) {$tip='ДР';} elseif($tip==3) {$tip='КР';}
                                             $identi=$tip.' '.$dval[$j]['sobstvenik_id'];
                                        } 
                                    } 
                           
                                      
                                    for($j=0;$j<count($tmp[$i]);$j++) // Строки с итогами вывод
                                    { 
                                            for($k=0;$k<count($tmp[$i][$j]);$k++)
                                            {
                                               $t[]=$tmp[$i][$j][$k]; 
                                            } 
                                                
                                    }              

                                      
                                    for($j=0;$j<count($t);$j++) // Уменьшаем вложеность массива итогов
                                    { 
                                        for($k=0;$k<count($t[$j]);$k++)
                                        {
                                          $m[]=$t[$j][$k];        
                                        }
                                    }              

                                    if (empty($finish))
                                    {
                                         if (empty($export))
                                         {
                                           $sobstvenik[]=$this->ConstructTable(1,$m,$identi);
                                         }
                                         else
                                         {
                                           $sobstvenik[]=$m;  
                                         }    

                                     }
                                     else
                                     {
                                         $sobstvenik=$finally;
                                     }
                                    
                                    
                      }
                      
                      
                 }
                   
         return $sobstvenik;  
      }   
   

     /** ***************************************************************
      ****************** ПРОМЫВКА УБОРКА ЖД ТАРИФ **********************
      ******************************************************************
      **/ 

      protected function Pr_ub_zhd($tip,$id,$valid,$val,$model,$date1,$date2,$querysobs,$finish,$export)
      {
                $pr_ub_zhd=[];
                $firm = Mremont::findOne(['id'=>$id]); 
                $kaz = (new \yii\db\Query())->select('*')->from('mremont')->where('id=1')->one();
                $query='';
                    if ($date1!='' &&  $date2!='')
                    {
                       $query=' && (dates>="'.date('Y-m-d',strtotime($date1)).'" && dates<="'.date('Y-m-d',strtotime($date2)).'")'; 
                    }
                    else
                    {
                        if ($date1!='')
                        {
                          $query=' && dates>="'.date('Y-m-d',strtotime($date1)).'"'; 
                        }
                        elseif($date2!='')
                        {
                            $query=' && dates<="'.date('Y-m-d',strtotime($date2)).'"';
                        }
                    }   
                     
                      
                     if ($tip=='Промывка')
                     { 
                          $new = (new \yii\db\Query())
                                    ->select(['*','SUM(kol) AS count']) 
                                    ->from('promiv')
                                    ->where('mremont_id='.$id.$query)
                                    ->groupBy(['promivspr_id','vvagon_id','cenaed']) 
                                    ->all();   
                                    $identi='Промывка'; 
                     }
                     elseif($tip=='Подача и уборка')
                     {
                          $new = (new \yii\db\Query())
                                    ->select(['*','SUM(kol) AS count']) 
                                    ->from('clean')
                                    ->where('mremont_id='.$id.$query)
                                    ->groupBy(['cleanspr_id','vvagon_id','cenaed']) 
                                    ->all();    
                                    $identi='Подача и уборка';
                     }
                     elseif($tip=='Жд тариф')
                     {
                          $new = (new \yii\db\Query())
                                    ->select(['*','SUM(kol) AS count']) 
                                    ->from('zhd')
                                    ->where('mremont_id='.$id.$query)
                                    ->groupBy(['zhdspr_id','vvagon_id','cenaed']) 
                                    ->all();    
                                    $identi='Жд тариф';
                     }
                      
                      
                if(count($new)>0)      
                {
                            $kol=0;$cena=0;$ndc=0;$itog=0;
                            $mas=[];
                            $tmp=[];
                            $vvagin_sbros_index=[];
                            $m=[]; 
                       foreach($new as $value) 
                       {
                              // определяем индекс принадлежности к виду вагона, фильтрацию делаем ГАЗ нефть и т.д.
                               if (!in_array($value['vvagon_id'],$vvagin_sbros_index))
                               {
                                   $vvagin_sbros_index[]=$value['vvagon_id'];
                                   $gl=array_search($value['vvagon_id'],$vvagin_sbros_index);
                                   $gl_ind=$gl;
                                   $q=0; 
                               }
                               else
                               {
                                   $gl=array_search($value['vvagon_id'],$vvagin_sbros_index);
                                   $gl_ind=$gl;
                                   $q=count($tmp[$gl_ind]);
                               }
                               $tmp[$gl_ind][$q]['rep']='pr_ubr_zhd';
                               $tmp[$gl_ind][$q]['lastname']=$model->DataMremont[$value['mremont_id']].' '.$tip;
                               $tmp[$gl_ind][$q]['mremont_id']=$model->DataMremont[$value['mremont_id']];
                               $tmp[$gl_ind][$q]['vvagon_id']=$model->DataVvagon[$value['vvagon_id']];
                               if ($tip=='Промывка')
                               {
                                $title=$model->DataPrice[$value['promivspr_id']];
                               }
                               elseif ($tip=='Подача и уборка')
                               {
                                $title=$model->DataPrice[$value['cleanspr_id']];
                               }
                               elseif($tip=='Жд тариф')
                               {
                                $title=$model->DataPrice[$value['zhdspr_id']];
                               }
                               $tmp[$gl_ind][$q]['title']=$title;
                               // $titlename для финального общего отчета
                               $titlename=$model->DataMremont[$value['mremont_id']].' '.$tip;
                               $tmp[$gl_ind][$q]['mremont_id']=$model->DataMremont[$value['mremont_id']];
                               $tmp[$gl_ind][$q]['count']=$value['count'];
                               // Считаем каждую строку ЦЕНА НДС ИТОГ столбцы
                                 $cena=round($value['cenaed']*$value['count'],2);
                                 $ndc=round($cena*($firm->ndc/100),2);
                                 $itog=round($cena+$ndc,2);
                              // Создаем в массиве новые динамические переменные
                              $tmp[$gl_ind][$q]['cenaed']=$value['cenaed'];
                              $tmp[$gl_ind][$q]['cena']=$cena;
                              $tmp[$gl_ind][$q]['ndc']=$ndc;
                              $tmp[$gl_ind][$q]['itog']=$itog;
                              $tmp[$gl_ind][$q]['priznak']=$tip;
                       }
                   
                       // считаем общее кол-во по видам вагонов
                       foreach($tmp as $key=>$vvagon)
                       {
                          foreach($vvagon as $item)
                          {
                              // Итоги по типам вагонов
                              
                             if ($firm->valuta==1) // тенге
                             {
                                 $ndc=round($item['cena']*($firm->ndc/100),2);
                                  
                                  $mas[$key][0]['title']=$item['priznak'].' в тенге ('.$item['vvagon_id'].')';
                                  if (!isset($mas[$key][0]['count'])) {$mas[$key][0]['count']='';} $mas[$key][0]['count']+=$item['count'];
                                  if (!isset($mas[$key][0]['cena'])) {$mas[$key][0]['cena']='';} $mas[$key][0]['cena']+=$item['cena'];
                                  if (!isset($mas[$key][0]['ndc'])) {$mas[$key][0]['ndc']='';} $mas[$key][0]['ndc']+=$ndc;
                                  if (!isset($mas[$key][0]['itog'])) {$mas[$key][0]['itog']='';} $mas[$key][0]['itog']+=round($item['cena']+$ndc,2);
                                  $mas[$key][0]['color']='blue';   
                                  $mas[$key][0]['rep']='pr_ubr_zhd';
                                  $mas[$key][0]['cenaed']='';
                                  // в долларах 
                                  $kol=$item['count'];
                                  $key_val=  array_search(3, $valid); // получаем индекс, в котором хранится значение доллара
                                  $usdcena=round($item['cena']/$val[$key_val],2); 
                                  $usdndc=round($usdcena*($firm->ndc/100),2);
                                   
                                  $mas[$key][1]['title']=$item['priznak'].' в долларах ('.$item['vvagon_id'].')';
                                  if (!isset($mas[$key][1]['count'])) {$mas[$key][1]['count']='';} $mas[$key][1]['count']+=$kol;
                                  if (!isset($mas[$key][1]['cena'])) {$mas[$key][1]['cena']='';} $mas[$key][1]['cena']+=$usdcena;
                                  if (!isset($mas[$key][1]['ndc'])) {$mas[$key][1]['ndc']='';} $mas[$key][1]['ndc']+=$usdndc;
                                  if (!isset($mas[$key][1]['itog'])) {$mas[$key][1]['itog']='';} $mas[$key][1]['itog']+=round($usdcena+$usdndc,2);
                                  $mas[$key][1]['color']='red';   
                                  $mas[$key][1]['val']='usd';
                                  $mas[$key][1]['rep']='pr_ubr_zhd';
                                  $mas[$key][1]['cenaed']='';
                             }
                             elseif ($firm->valuta==3) // доллары
                             {
                                  $ndc=round($item['cena']*($firm->ndc/100),2);
                                  // в долларах
                                  $mas[$key][0]['title']=$item['priznak'].' в долларах ('.$item['vvagon_id'].')';
                                  if (!isset($mas[$key][0]['count'])) {$mas[$key][0]['count']='';} $mas[$key][0]['count']+=$item['count'];
                                  if (!isset($mas[$key][0]['cena'])) {$mas[$key][0]['cena']='';} $mas[$key][0]['cena']+=$item['cena'];
                                  if (!isset($mas[$key][0]['ndc'])) {$mas[$key][0]['ndc']='';} $mas[$key][0]['ndc']+=$ndc;
                                  if (!isset($mas[$key][0]['itog'])) {$mas[$key][0]['itog']='';} $mas[$key][0]['itog']+=round($item['cena']+$ndc,2);
                                  $mas[$key][0]['color']='red';   
                                  $mas[$key][0]['rep']='pr_ubr_zhd';
                                  $mas[$key][0]['cenaed']='';
                             }     
                             else // рубли
                             {
                                  $ndc=round($item['cena']*($firm->ndc/100),2);
                                  
                                  $mas[$key][0]['title']=$item['priznak'].' в рублях'.'('.$item['vvagon_id'].')';
                                  if (!isset($mas[$key][0]['count'])) {$mas[$key][0]['count']='';} $mas[$key][0]['count']+=$item['count'];
                                  if (!isset($mas[$key][0]['cena'])) {$mas[$key][0]['cena']='';} $mas[$key][0]['cena']+=$item['cena'];
                                  if (!isset($mas[$key][0]['ndc'])) {$mas[$key][0]['ndc']='';} $mas[$key][0]['ndc']+=$ndc;
                                  if (!isset($mas[$key][0]['itog'])) {$mas[$key][0]['itog']='';} $mas[$key][0]['itog']+=round($item['cena']+$ndc,2);
                                  $mas[$key][0]['color']='green';                                  
                                  $mas[$key][0]['rep']='pr_ubr_zhd';
                                  $mas[$key][0]['cenaed']='';
                             }
                          }
                       }      
                       
                      // считаем итоги
                      $size=count($mas); $cena=0; $ndc=0;$itog=0;$cenausd=0;$ndcusd=0;$usditog=0;
                       foreach($mas as $vvagon)
                       { 
                          foreach($vvagon as $item)
                          {
                            if(!isset($item['val'])) // чтобы не просчитывать доллары, их перевод сделаем из общего кол-ва, наличие val в массиве означает что это доллар или рубль
                            {
                                if($firm->valuta==1) // тенге
                                {
                                  $indc=round($item['cena']*($firm->ndc/100),2);
                                  $iitog=round($item['cena']+$indc,2);

                                  $mas[$size][0]['title']='Итого в тенге';
                                  if (!isset($mas[$size][0]['count'])) { $mas[$size][0]['count']=0; }  $mas[$size][0]['count']+=$item['count'];
                                  if (!isset($mas[$size][0]['cena'])) {$mas[$size][0]['cena']=0;} $mas[$size][0]['cena']+=$item['cena'];
                                  if (!isset($mas[$size][0]['ndc'])) {$mas[$size][0]['ndc']=0;} $mas[$size][0]['ndc']+=$indc; 
                                  if (!isset($mas[$size][0]['itog'])) {$mas[$size][0]['itog']=0;}  $mas[$size][0]['itog']+=$iitog;
                                  $mas[$size][0]['color']='blue bgblue';   
                                  $mas[$size][0]['rep']='pr_ubr_zhd';
                                  $mas[$size][0]['cenaed']='';
                                  // usd
                                  $key_val=  array_search(3, $valid);
                                  $usdcena=round($item['cena']/$val[$key_val],2);
                                  $iusdndc=round($usdcena*($firm->ndc/100),2);
                                  $iusdtog=round($usdcena+$iusdndc,2);
        
                                  $mas[$size][1]['title']='Итого в долларах';
                                  if (!isset($mas[$size][1]['count'])) {$mas[$size][1]['count']=0; } $mas[$size][1]['count']+=$item['count']; 
                                  if (!isset($mas[$size][1]['cena'])) {$mas[$size][1]['cena']=0;} $mas[$size][1]['cena']+=$usdcena; 
                                  if (!isset($mas[$size][1]['ndc'])) {$mas[$size][1]['ndc']=0;} $mas[$size][1]['ndc']+=$iusdndc; 
                                  if (!isset($mas[$size][1]['itog'])) {$mas[$size][1]['itog']=0;} $mas[$size][1]['itog']+=$iusdtog;
                                  $mas[$size][1]['color']='red bgblue';   
                                  $mas[$size][1]['rep']='pr_ubr_zhd';
                                  $mas[$size][1]['cenaed']='';
                                  // для финального отчета по всем отчетам
                                  $cena+=$item['cena'];
                                  $ndc+=$indc;
                                  $itog+=$iitog;
                                  
                                  $cenausd+=$usdcena;
                                  $ndcusd+=$iusdndc;
                                  $usditog+=$iusdtog;
                                }
                                elseif($firm->valuta==3) // доллары
                                {
                                  $indc=round($item['cena']*($firm->ndc/100),2);
                                  $iitog=round($item['cena']+$indc,2);

                                  $mas[$size][0]['title']='Итого в долларах';
                                  if (!isset($mas[$size][0]['count'])) { $mas[$size][0]['count']=0; }  $mas[$size][0]['count']+=$item['count'];
                                  if (!isset($mas[$size][0]['cena'])) {$mas[$size][0]['cena']=0;} $mas[$size][0]['cena']+=$item['cena'];
                                  if (!isset($mas[$size][0]['ndc'])) {$mas[$size][0]['ndc']=0;} $mas[$size][0]['ndc']+=$indc; 
                                  if (!isset($mas[$size][0]['itog'])) {$mas[$size][0]['itog']=0;}  $mas[$size][0]['itog']+=$iitog;
                                  $mas[$size][0]['color']='red bgblue';   
                                  $mas[$size][0]['rep']='pr_ubr_zhd';
                                  $mas[$size][0]['cenaed']='';
                                  // usd
                                  $key_val=  array_search(3, $valid);
                                  $tgcena=round($item['cena']*$val[$key_val],2);
                                  $tgndc=round($tgcena*($kaz['ndc']/100),2);
                                  $tgitog=round($tgcena+$tgndc,2);

                                  $mas[$size][1]['title']='Итого в тенге';
                                  if (!isset($mas[$size][1]['count'])) {$mas[$size][1]['count']=0;} $mas[$size][1]['count']+=$item['count'];  
                                  if (!isset($mas[$size][1]['cena'])) {$mas[$size][1]['cena']=0;} $mas[$size][1]['cena']+=$tgcena;
                                  if (!isset($mas[$size][1]['ndc'])) {$mas[$size][1]['ndc']=0;} $mas[$size][1]['ndc']+=$tgndc; 
                                  if (!isset($mas[$size][1]['itog'])) {$mas[$size][1]['itog']=0;} $mas[$size][1]['itog']+=$tgitog;
                                  $mas[$size][1]['color']='blue bgblue';   
                                  $mas[$size][1]['rep']='pr_ubr_zhd';
                                  $mas[$size][1]['cenaed']='';
                                  // для финального отчета по всем отчетам
                                  $cena+=$tgcena;
                                  $ndc+=$tgndc;
                                  $itog+=$tgitog;
                                  
                                  $cenausd+=$item['cena'];
                                  $ndcusd+=$indc;
                                  $usditog+=$iitog;

                                }
                                else
                                {
                                  $indc=round($item['cena']*($firm->ndc/100),2);
                                  $itog=round($item['cena']+$indc,2);

                                  $mas[$size][0]['title']='Итого в рублях';
                                  if (!isset($mas[$size][0]['count'])) {$mas[$size][0]['count']=0;} $mas[$size][0]['count']+=$item['count']; 
                                  if (!isset($mas[$size][0]['cena'])) {$mas[$size][0]['cena']=0;} $mas[$size][0]['cena']+=$item['cena'];
                                  if (!isset($mas[$size][0]['ndc'])) {$mas[$size][0]['ndc']=0;} $mas[$size][0]['ndc']+=$indc;
                                  if (!isset($mas[$size][0]['itog'])) {$mas[$size][0]['itog']=0;} $mas[$size][0]['itog']+=$itog;
                                  $mas[$size][0]['color']='green bgblue';   
                                  $mas[$size][0]['rep']='pr_ubr_zhd';
                                  $mas[$size][0]['cenaed']='';
                                  
                                  $key_val=  array_search($firm->valuta, $valid);
                                  ($firm->valuta==2) ? $tgcena=round($item['cena']*$val[$key_val],2) : '';
                                  $tgndc=round($tgcena*($kaz['ndc']/100),2);
                                  $tgitog=$tgcena+$tgndc;
                                  $mas[$size][1]['title']='Итого в тенге';
                                  if (!isset($mas[$size][1]['count'])) {$mas[$size][1]['count']=0;} $mas[$size][1]['count']+=$item['count'];   
                                  if (!isset($mas[$size][1]['cena'])) {$mas[$size][1]['cena']=0;} $mas[$size][1]['cena']+=$tgcena;
                                  if (!isset($mas[$size][1]['ndc'])) {$mas[$size][1]['ndc']=0;} $mas[$size][1]['ndc']+=$tgndc; 
                                  if (!isset($mas[$size][1]['itog'])) {$mas[$size][1]['itog']=0;} $mas[$size][1]['itog']+=$tgitog;
                                  $mas[$size][1]['color']='blue bgblue';   
                                  $mas[$size][1]['rep']='pr_ubr_zhd';
                                  $mas[$size][1]['cenaed']='';
                                  // usd
                                  $key_val=  array_search(3, $valid);
                                  $usdcena=round($tgcena/$val[$key_val],2);
                                  $iusdndc=round($usdcena*($kaz['ndc']/100),2);
                                  $iusdtog=round($usdcena+$iusdndc,2);
        
                                  $mas[$size][2]['title']='Итого в долларах';
                                  if (!isset($mas[$size][2]['count'])) {$mas[$size][2]['count']=0;} $mas[$size][2]['count']+=$item['count'];
                                  if (!isset($mas[$size][2]['cena'])) { $mas[$size][2]['cena']=0;}  $mas[$size][2]['cena']+=$usdcena;
                                  if (!isset($mas[$size][2]['ndc'])) {$mas[$size][2]['ndc']=0;} $mas[$size][2]['ndc']+=$iusdndc;
                                  if (!isset($mas[$size][2]['itog'])) {$mas[$size][2]['itog']=0;} $mas[$size][2]['itog']+=$iusdtog;
                                  $mas[$size][2]['color']='red bgblue';
                                  $mas[$size][2]['rep']='pr_ubr_zhd';
                                  $mas[$size][2]['cenaed']='';
                                  // для финального отчета по всем отчетам
                                  $cena+=$tgcena;
                                  $ndc+=$tgndc;
                                  $itog+=$tgitog;
                                  
                                  $cenausd+=$usdcena;
                                  $ndcusd+=$iusdndc;
                                  $usditog+=$iusdtog;
                                } 
                             } 
                          }
 
                       }                         
                    
                    $finally[]=['title'=>$titlename,'cenaed'=>'','cena'=>$cena,'ndc'=>$ndc,'itog'=>$itog,'ucena'=>$cenausd,'undc'=>$ndcusd,'uitog'=>$usditog,'column'=>'blue','rep'=>'pr_ubr_zhd'];   
              
                    for($i=0;$i<count($tmp);$i++)
                    {
                        for($j=0;$j<count($tmp[$i]);$j++)
                        {
                           $m[]=$tmp[$i][$j]; 
                        }
                    }
        
                    for($i=0;$i<count($mas);$i++)
                    {
                        for($j=0;$j<count($mas[$i]);$j++)
                        {
                           $m[]=$mas[$i][$j]; 
                        }
                    }
           
                    if (count($m)>0)
                    {           
                        if (empty($finish))   
                        {
                           if (empty($export))
                           {
                             $pr_ub_zhd[]=$this->ConstructTable(1,$m,$identi);  
                           } 
                           else
                           {
                              $pr_ub_zhd=$m; 
                           }   
                        }
                        else
                        {
                            $pr_ub_zhd=$finally;
                        }    
                    }
           
           }
               
           return $pr_ub_zhd;                         
        }       

 
      protected function Posrednik($tip,$id,$valid,$val,$model,$date1,$date2,$querysobs,$finish,$export)
      {
            $posrednik=[];
            $firm = Mremont::findOne(['id'=>$id]);// получаем нац валюту фирмы оказывающей услугу или ремонт    
            $kaz = (new \yii\db\Query())->select('*')->from('mremont')->where('id=1')->one();
            $m = (new \yii\db\Query())->select('id')->from('posrednik')->all(); // получаем пункты посреднников
            
            $newmas=[]; 
            $tippos=[];
            $posr_name=[];
             /** Запрос даты**/
                    $query='';
                    if ($date1!='' &&  $date2!='')
                    {
                       $query=' && (dates>="'.date('Y-m-d',strtotime($date1)).'" && dates<="'.date('Y-m-d',strtotime($date2)).'")'; 
                    }
                    else
                    {
                        if ($date1!='')
                        {
                          $query=' && dates>="'.date('Y-m-d',strtotime($date1)).'"'; 
                        }
                        elseif($date2!='')
                        {
                            $query=' && dates<="'.date('Y-m-d',strtotime($date2)).'"';
                        }
                    }   
            // разбиваем на массив посреднников, т.к. сколько посреднников столько и отчетов
            
            $kol=4;
            foreach ($m as $value) 
            {
                  $ids[]=$kol; // заносим id посредников, с 1-3 принадлежат (ТОР, ДР, КР)по этому начинаем с 4
                  $posr_name[]=$model->DataPosrednik[$value['id']]; // заносим названия посредников
                  $kol++;
            }
            
            $sh=(new \yii\db\Query())
                        ->select(['*']) 
                        ->from('vagon')
                        ->where('mremont_id='.$id.' && tip IN ('.  implode(',', $ids).')'.$query) 
                        ->all();
        
        
            $detail=(new \yii\db\Query())
                            ->select(['*']) 
                            ->from('detail')
                            ->where('mremont_id='.$id.' && tip IN ('.  implode(',', $ids).')'.$query) 
                            ->all();

            $revizia=(new \yii\db\Query())
                            ->select(['*']) 
                            ->from('revizia')
                            ->where('mremont_id='.$id.' && tip IN ('.  implode(',', $ids).')'.$query) 
                            ->all(); 
            
            $usluga=(new \yii\db\Query())
                            ->select(['*']) 
                            ->from('usluga')
                            ->where('mremont_id='.$id.' && tip IN ('.  implode(',', $ids).')'.$query) 
                            ->all();   
            
            $all=[$detail,$revizia,$usluga];            
                  
            // все id посреднников хранятся в массиве $index, выстраиваем массив для таблицы
            foreach ($sh as $value) /// начало цикла сфет-фактур посредников
            { 
                foreach ($all as $mas) // все таблицы в одном массиве 
                {
                    foreach ($mas as $item) 
                    {
                       // если vagon_id==id счета-фактуры  
                       if ($item['vagon_id']==$value['id']) 
                       {
                                if (!in_array($value['tip'], $tippos)) //если названия посредника нет в массиве
                                {
                                   $tippos[]=$value['tip']; //заносим название посредника
                                   $tkey=  array_search($value['tip'], $tippos);// ключ
                                   $index=[];
                                }
                                else
                                {
                                    $tkey=  array_search($value['tip'], $tippos);// ключ 
                                }

                                if (!in_array($value['nscheta'], $index)) //если номера счета нет в массиве
                                {
                                   $index[]=$value['nscheta']; //заносим номер счета
                                   $ikey=  array_search($value['nscheta'], $index);// ключ
                                   $x=0;
                                }
                                else
                                {
                                    $ikey=  array_search($value['nscheta'], $index);// ключ 
                                    $x=  count($tmp[$ikey]);
                                }
                               
                                
                                if (isset($item['detailspr_id']))
                                {
                                   $prbrak=$model->DataPrice[$item['detailspr_id']];
                                }
                                elseif(isset($item['reviziaspr_id']))
                                {
                                    $prbrak=$model->DataPrice[$item['reviziaspr_id']];
                                }    
                                elseif(isset($item['uslugaspr_id']))
                                {
                                    $prbrak=$model->DataPrice[$item['uslugaspr_id']];
                                }

                                
                               $cena=round($item['cenaed']*$item['kol'],2);
                               $ndc=round($cena*($firm->ndc/100),2);
                               $itog=round($cena+$ndc,2);

                               $newmas[$tkey][$ikey][$x]=[
                                                         'title'=>$prbrak,
                                                         'mremont_id'=>$model->DataMremont[$value['mremont_id']],
                                                         'count'=>$item['kol'],
                                                         'cenaed'=>$item['cenaed'],
                                                         'cena'=>$cena,  
                                                         'ndc'=>$ndc,
                                                         'itog'=>$itog,
                                                         'tip'=>$model->DataOmas[$item['tip']],
                                                         'lastname'=>$model->DataMremont[$value['mremont_id']].' '.$model->DataOmas[$item['tip']],
                                                         'rep'=>'posrednik',
                                                         ]; 
                       } // конец цикла если сопадают vagon_id=id
                    }  // конец цикла одного из массивов 
                } // конец цикла где содержатся все таблицы      
            }  // конец счет-фактур
            
            
            // подсчет  итоги всех посредников
            foreach ($newmas as $tkey=>$tmas) 
            {
                $kol=0;$cena=0;// сбрасываем при переходе к следующему посреднику
                 foreach ($tmas as $key=>$mas)
                 {  
                     foreach ($mas as $value) 
                     {
                           $kol+=$value['count']; 
                           $cena+=$value['cena'];
                           $mremont_id=$value['mremont_id'];
                           $tip=$value['tip'];
                     }
                 }
                 //  считаем итоги 
                if($firm->valuta==1) //если тенге
                {
                    $ndc=round($cena*($firm->ndc/100),2);
                    $itog=round($cena+$ndc,2);
                    $key_val=  array_search(3, $valid);
                    $cenausd=round($cena/$val[$key_val],2);
                    $ndcusd=round($cenausd*($firm->ndc/100),2);
                    $usditog=round($cenausd+$ndcusd,2);

                    $newmas[$tkey][count($newmas[$tkey])][]=['title'=>'Итого в тенге','count'=>$kol,'cenaed'=>'','cena'=>$cena,'ndc'=>$ndc,'itog'=>$itog,'color'=>'blue bgblue','mremont_id'=>$mremont_id,'rep'=>'posrednik'];
                    $newmas[$tkey][count($newmas[$tkey])][]=['title'=>'Итого в долларах','count'=>$kol,'cenaed'=>'','cena'=>$cenausd,'ndc'=>$ndcusd,'itog'=>$usditog,'color'=>'red bgblue',$mremont_id,'rep'=>'posrednik'];
                    // массив для финального отчета
                    $finally[$tkey][]=['title'=>$mremont_id.' '.$tip,'cenaed'=>'','cena'=>$cena,'ndc'=>$ndc,'itog'=>$itog,'ucena'=>$cenausd,'undc'=>$ndcusd,'uitog'=>$usditog,'column'=>'blue','rep'=>'posrednik']; 
                } 
                elseif($firm->valuta==3) //если доллары
                {
                   // ндс (в долларах) * на ставку ндс страны поставщика
                   $ndc=round($cena*($firm->ndc/100),2);
                   $key_val=  array_search(3, $valid);
                   // ндс (в тенге)
                   $tgcena= round($cena*$val[$key_val],2);
                   $tgndc=round($tgcena*($kaz['ndc']/100),2);

                   $newmas[$tkey][]=['title'=>'Итого в долларах','count'=>$kol,'cenaed'=>'','cena'=>$cena,'ndc'=>$ndc,'itog'=>round($ndc+$cena),'color'=>'red bgblue','mremont_id'=>$mremont_id,'rep'=>'posrednik'];
                   $newmas[$tkey][]=['title'=>'Итого в тенге','count'=>$kol,'cenaed'=>'','cena'=>$tgcena,'ndc'=>$tgndc,'itog'=>round($tgcena+$tgndc,2),'color'=>'blue bgblue','mremont_id'=>$mremont_id,'rep'=>'posrednik'];
                   // массив для финального отчета
                   $finally[$tkey][]=['title'=>$mremont_id.' '.$tip,'cenaed'=>'','cena'=>$tgcena,'ndc'=>$tgndc,'itog'=>round($tgcena+$tgndc,2),'ucena'=>$cena,'undc'=>$ndc,'uitog'=>round($ndc+$cena),'column'=>'blue','rep'=>'posrednik']; 
                }
                else  // если рубли или другая валюта кроме тенге и доллара
                {
                    $ndc=round($cena*($firm->ndc/100),2);
                    $itog=round($cena+$ndc,2);
                                    
                    $key_val=  array_search($firm->valuta, $valid); // получаем курс валюты кроме тенге и доллара
                    $cenatg=round($cena*$val[$key_val],2);
                    $ndctg=round($cenatg*($kaz['ndc']/100),2);
                    $itogtg=round($cenatg+$ndctg,2);

                    $key_val=  array_search(3, $valid); //индекс доллара
                    $cenausd=round($cenatg/$val[$key_val],2);
                    $ndcusd=round($cenausd*($kaz['ndc']/100),2);
                    $itogusd=round($cenausd+$ndcusd,2);

                    $newmas[$tkey][]=['title'=>'Итого в рублях','count'=>$kol,'cenaed'=>'','cena'=>$cena,'ndc'=>$ndc,'itog'=>$itog,'color'=>'green bgblue','mremont_id'=>$mremont_id,'rep'=>'posrednik'];
                    $newmas[$tkey][]=['title'=>'Итого в тенге','count'=>$kol,'cenaed'=>'','cena'=>$cenatg,'ndc'=>$ndctg,'itog'=>$itogtg,'color'=>'blue bgblue','mremont_id'=>$mremont_id,'rep'=>'posrednik'];
                    $newmas[$tkey][]=['title'=>'Итого в долларах','count'=>$kol,'cenaed'=>'','cena'=>$cenausd,'ndc'=>$ndcusd,'itog'=>$itogusd,'color'=>'red bgblue','mremont_id'=>$mremont_id,'rep'=>'posrednik'];
                    // массив для финального отчета
                    $finally[$tkey][]=['title'=>$mremont_id.' '.$tip,'cenaed'=>'','cena'=>$cenatg,'ndc'=>$ndctg,'itog'=>$itogtg,'ucena'=>$cenausd,'undc'=>$ndcusd,'uitog'=>$itogusd,'column'=>'blue','rep'=>'posrednik']; 
                }                 
             }            

            
            for($i=0;$i<count($newmas);$i++) 
            { 
               $a=[]; 
               
               for($j=0;$j<count($newmas[$i]);$j++) // один посредник
               {
                    for($k=0;$k<count($newmas[$i][$j]);$k++)
                    {
                        $a[]=$newmas[$i][$j][$k];
                    }
               }
               
               if (!empty($a))
               {
                       if (empty($finish))
                       {   
                           if (empty($export))
                           {
                             $posrednik[]=$this->ConstructTable(null,$a,$posr_name[$i]);   
                           }
                           else
                           {
                              $posrednik[]=$a; 
                           }   
                       }
                       else  // финальный массив
                       {
                            $posrednik[]=$finally[$i];
                       }    
               }              
            } 
             
           
        return $posrednik;
      }
          
      protected function Allorg($valid,$val,$model,$date1,$date2,$querysobs,$finish,$export)
      {
          // получаем список фирм
          $firms = (new \yii\db\Query())->select('*')->from('mremont')->all();  
           $itog=0;$uitog=0;              
           foreach($firms as $item)
           {
                $greben=$this->Greben(null,$item['id'],$valid,$val,$model,$date1,$date2, $querysobs,$finish,$export);    
                $tor=$this->Osn(1,$item['id'],$valid,$val,$model,$date1,$date2, $querysobs,$finish,$export);    
                $torsobs=$this->Sobstvenik(1,$item['id'],$valid,$val,$model,$date1,$date2, $querysobs,$finish,$export);    
                // ДР 
                $dr=$this->Osn(2,$item['id'],$valid,$val,$model,$date1,$date2, $querysobs,$finish,$export);    
                $drsobs=$this->Sobstvenik(2,$item['id'],$valid,$val,$model,$date1,$date2, $querysobs,$finish,$export);    
                // Уборка
                $promivka=$this->Pr_ub_zhd('Промывка',$item['id'],$valid,$val,$model,$date1,$date2, null,$finish,$export);    
                //Подача и уборка
                $uborka=$this->Pr_ub_zhd('Подача и уборка',$item['id'],$valid,$val,$model,$date1,$date2, null,$finish,$export);    
                //Жд тариф 
                $zhd=$this->Pr_ub_zhd('Жд тариф',$item['id'],$valid,$val,$model,$date1,$date2, null,$finish,$export);    
                // Посредник    
                $posrednik=$this->Posrednik(null,$item['id'],$valid,$val,$model,$date1,$date2, $querysobs,$finish,$export);    
                (!empty($greben)) ? $mas[]=$greben : '';
                (!empty($tor)) ? $mas[]=$tor : '';
                (!empty($torsobs)) ? $mas[]=$torsobs : '';
                (!empty($dr)) ? $mas[]=$dr : '';
                (!empty($drsobs)) ? $mas[]=$drsobs : '';
                (!empty($promivka)) ? $mas[]=$promivka : '';
                (!empty($uborka)) ? $mas[]=$uborka : '';
                (!empty($zhd)) ? $mas[]=$zhd : '';
                if (!empty($posrednik))  
                {
                   for($k=0;$k<count($posrednik);$k++)
                   {
                     $mas[]=$posrednik[$k];   
                   }
                }    
                
                foreach($tor as $value)
                { 
                   $itog+=$value['itog']; // тенге 
                   $uitog+=$value['uitog']; // доллары
                }   
                
                foreach($dr as $value)
                { 
                   $itog+=$value['itog']; // тенге 
                   $uitog+=$value['uitog']; // доллары
                }
                
                foreach($promivka as $value)
                { 
                   $itog+=$value['itog']; // тенге 
                   $uitog+=$value['uitog']; // доллары
                } 
                
                foreach($uborka as $value)
                { 
                   $itog+=$value['itog']; // тенге 
                   $uitog+=$value['uitog']; // доллары
                }
                
                foreach($zhd as $value)
                { 
                   $itog+=$value['itog']; // тенге 
                   $uitog+=$value['uitog']; // доллары
                }
                
                foreach($posrednik as $dval)
                { 
                  foreach($dval as $value)
                  {   
                    $itog+=$value['itog']; // тенге 
                    $uitog+=$value['uitog']; // доллары
                  }  
                }
            }
            
            $mas[]=[['title'=>'Итого расходы по предприятиям','cena'=>'','ndc'=>'','itog'=>$itog,'ucena'=>'','undc'=>'','uitog'=>$uitog,'color'=>'blue bgblue','rep'=>'all']];
           
             
            
            for ($i=0;$i<count($mas);$i++) // уменьшаем вложенность массива
            {
               for ($j=0;$j<count($mas[$i]);$j++)
               {
                 $tmp[]=$mas[$i][$j];   
               }
            }
            
           
            if(empty($export))
            {
                $a[]=$this->ConstructTable(2,$tmp,null);
            }
            else
            {
                $a=$tmp;
            }   
            
         return $allorg=$a;   
      }

      
     public function actionSave()  // сделал по одной  организации отчет, осталось по всем сразу
     {
        $model= new Vagon;
        $report=[];
        $querysobs = (new \yii\db\Query())->select('*')->from('sobstvenik')->all();  
        $valid=explode('_', $_GET['valid']);
        $val=explode('_', $_GET['val']);
        
         if($_GET['id']!='all')
         { 
                     $greben=$this->Greben(null,$_GET['id'],$valid,$val,$model,$_GET['date1'],$_GET['date2'], $querysobs,null,1);    
                       $tmp[]=$greben;
                     $tor=$this->Osn(1,$_GET['id'],$valid,$val,$model,$_GET['date1'],$_GET['date2'], $querysobs,null,1);    
                       $tmp[]=$tor;
                     $torsobs=$this->Sobstvenik(1,$_GET['id'],$valid,$val,$model,$_GET['date1'],$_GET['date2'], $querysobs,null,1);    
                     for ($i=0;$i<count($torsobs);$i++)
                     {
                       $tmp[]=$torsobs[$i];  
                     }
                     // ДР 
                     $dr=$this->Osn(2,$_GET['id'],$valid,$val,$model,$_GET['date1'],$_GET['date2'], $querysobs,null,1);    
                        $tmp[]=$dr;
                     $drsobs=$this->Sobstvenik(2,$_GET['id'],$valid,$val,$model,$_GET['date1'],$_GET['date2'], $querysobs,null,1);    
                     for ($i=0;$i<count($drsobs);$i++)
                     {
                       $tmp[]=$drsobs[$i];  
                     }
                     // Уборка
                     $promivka=$this->Pr_ub_zhd('Промывка',$_GET['id'],$valid,$val,$model,$_GET['date1'],$_GET['date2'], null,null,1);    
                     $tmp[]=$promivka; 
                     //Подача и уборка
                     $uborka=$this->Pr_ub_zhd('Подача и уборка',$_GET['id'],$valid,$val,$model,$_GET['date1'],$_GET['date2'], null,null,1);    
                     $tmp[]=$uborka;
                     //Жд тариф 
                     $zhd=$this->Pr_ub_zhd('Жд тариф',$_GET['id'],$valid,$val,$model,$_GET['date1'],$_GET['date2'], null,null,1);    
                     $tmp[]=$zhd;
                     // Посредник    
                     $posrednik=$this->Posrednik(null,$_GET['id'],$valid,$val,$model,$_GET['date1'],$_GET['date2'], $querysobs,null,1);    

                     for($i=0;$i<count($posrednik);$i++)
                     {
                        $tmp[]=$posrednik[$i];  
                     }      
                     
                     for ($i=0;$i<count($tmp);$i++)
                     {
                         if (!empty($tmp[$i]))
                         {
                          $all[]=$tmp[$i];   
                         }
                     }
         }
         else
         {
                $allorg=$this->Allorg($valid,$val,$model,$_GET['date1'],$_GET['date2'],$querysobs,1,1);  
                $tmp[]=$allorg;  
                // получаем список фирм
                $firms = (new \yii\db\Query())->select('*')->from('mremont')->all();  
                
                foreach($firms as $item)
                {    
                     $t=[];
                     $greben=$this->Greben(null,$item['id'],$valid,$val,$model,$_GET['date1'],$_GET['date2'], $querysobs,null,1);    
                     $tor=$this->Osn(1,$item['id'],$valid,$val,$model,$_GET['date1'],$_GET['date2'], $querysobs,null,1);    
                     $torsobs=$this->Sobstvenik(1,$item['id'],$valid,$val,$model,$_GET['date1'],$_GET['date2'], $querysobs,null,1);    
                     // ДР 
                     $dr=$this->Osn(2,$item['id'],$valid,$val,$model,$_GET['date1'],$_GET['date2'], $querysobs,null,1);    
                     $drsobs=$this->Sobstvenik(2,$item['id'],$valid,$val,$model,$_GET['date1'],$_GET['date2'], $querysobs,null,1);    
                     // Уборка
                     $promivka=$this->Pr_ub_zhd('Промывка',$item['id'],$valid,$val,$model,$_GET['date1'],$_GET['date2'], null,null,1);    
                     //Подача и уборка
                     $uborka=$this->Pr_ub_zhd('Подача и уборка',$item['id'],$valid,$val,$model,$_GET['date1'],$_GET['date2'], null,null,1);    
                     //Жд тариф 
                     $zhd=$this->Pr_ub_zhd('Жд тариф',$item['id'],$valid,$val,$model,$_GET['date1'],$_GET['date2'], null,null,1);    
                     // Посредник    
                     $posrednik=$this->Posrednik(null,$item['id'],$valid,$val,$model,$_GET['date1'],$_GET['date2'], $querysobs,null,1);    

                     $tmp[]=$greben;$tmp[]=$tor;
                     for ($i=0;$i<count($torsobs);$i++)
                     {
                       $tmp[]=$torsobs[$i];  
                     }
                     $tmp[]=$dr;
                     for ($i=0;$i<count($drsobs);$i++)
                     {
                       $tmp[]=$drsobs[$i];  
                     }
                     $tmp[]=$promivka;$tmp[]=$uborka;$tmp[]=$zhd;
                     for($i=0;$i<count($posrednik);$i++)
                     {
                        $tmp[]=$posrednik[$i];  
                     }    
                } 
                             
               for ($i=0;$i<count($tmp);$i++)
               {
                   if (!empty($tmp[$i]))
                   {
                     $all[]=$tmp[$i];  
                   }    
               }
                   
         }
       
        $report=$all;
        
        $style_border = [ 
                          'borders'=>['allborders'=>['style'=>  \PHPExcel_Style_Border::BORDER_THIN] ],
                          'font'=> ['name' => 'Arial','size' => 10]                       
                        ];
        $style_bold=['font'=> [
                               'bold' => true,
                               'name' => 'Arial',
                               'size' => 10,
                               'color'=>['rgb'=>'000000']
                               ],
                      'fill' => [
                                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                                    'color'=>['rgb' => 'FFFFCC']
                                ],                                
                     ];

        $normal=['font'=> ['name' => 'Arial','size' => 10,'color'=>['rgb'=>'0000ff']] ];
        $normalred=['font'=> ['name' => 'Arial','size' => 10,'color'=>['rgb'=>'c00000']] ];
        
        $blue=['font'=> ['name' => 'Arial','size' => 10,'color'=>['rgb'=>'0000ff']] ];
        $red=['font'=> ['name' => 'Arial','size' => 10,'color'=>['rgb'=>'c00000']] ];
        $green=['font'=> ['name' => 'Arial','size' => 10,'color'=>['rgb'=>'6e7e28']] ];
        
        $blueitog=['font'=> ['bold' => true,'name' => 'Arial','size' => 10,'color'=>['rgb'=>'0000ff']] ];
        $reditog=['font'=> ['bold' => true,'name' => 'Arial','size' => 10,'color'=>['rgb'=>'c00000']] ];
        
        $bgblue=[
                 'font'=> ['bold' => true,'name' => 'Arial','size' => 10,'color'=>['rgb'=>'0000ff']],
                 'fill' => ['type' => \PHPExcel_Style_Fill::FILL_SOLID,'color'=>['rgb' => 'd1e7f7']],                                
                ];
        $bgred=[
                 'font'=> ['bold' => true,'name' => 'Arial','size' => 10,'color'=>['rgb'=>'c00000']],
                 'fill' => ['type' => \PHPExcel_Style_Fill::FILL_SOLID,'color'=>['rgb' => 'd1e7f7']],                                
                ];
        $center=[
                  'alignment'=>[
                            'horizontal'=>  \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical'=>  \PHPExcel_Style_Alignment::VERTICAL_CENTER
                            ]
                ];
        $vertical_center=[
                            'alignment'=>[
                                          'vertical'=>  \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                                          'horizontal'=>\PHPExcel_Style_Alignment::HORIZONTAL_LEFT
                                         ]
                         ]; 
               
        
        $myExcel = new \PHPExcel();
        for($j=0;$j<count($report);$j++) 
         {
                if ($j!=0){ $myExcel->createSheet(); } // Создаем следующий лист
                $myExcel->setActiveSheetIndex($j);
                $sheet = $myExcel->getActiveSheet();
                //$sheet->getSheetView()->setZoomScale(85);
                if ($j==0 && $_GET['id']=='all') 
                {    
                        $titlesheet='Сводная';
                }
                else
                {
                    if (strlen($report[$j][0]['lastname'])>30) 
                    {
                        $titlesheet=$this->Strlink($report[$j][0]['lastname'],30); 
                    }
                    else
                    {
                        $titlesheet=$report[$j][0]['lastname'];
                    }    
                }   
                
                $sheet->setTitle($titlesheet);
                if ($j==0 && $_GET['id']=='all')
                {
                        $sheet->getColumnDimension('A')->setWidth(80);
                        $sheet->getColumnDimension('B')->setWidth(20);
                        $sheet->getColumnDimension('C')->setWidth(20);
                        $sheet->getColumnDimension('D')->setWidth(20);
                        $sheet->getColumnDimension('E')->setWidth(20);
                        $sheet->getColumnDimension('F')->setWidth(20);
                        $sheet->getColumnDimension('G')->setWidth(20);
                        
                        $myExcel->getActiveSheet()->getCellByColumnAndRow(0, 1)->setValue('Наименование');
                        $myExcel->getActiveSheet()->getCellByColumnAndRow(1, 1)->setValue('Общая цена (тенге)');
                        $myExcel->getActiveSheet()->getCellByColumnAndRow(2, 1)->setValue('Ндс');
                        $myExcel->getActiveSheet()->getCellByColumnAndRow(3, 1)->setValue('Итого (тенге)');
                        $myExcel->getActiveSheet()->getCellByColumnAndRow(4, 1)->setValue('Общая цена (доллары)');
                        $myExcel->getActiveSheet()->getCellByColumnAndRow(5, 1)->setValue('Ндс');
                        $myExcel->getActiveSheet()->getCellByColumnAndRow(6, 1)->setValue('Итого (доллары)'); 
                }    
                if ($report[$j][0]['rep']!='SOBS' && $report[$j][0]['rep']!='pr_ubr_zhd')
                {
                        $sheet->getColumnDimension('A')->setWidth(80);
                        $sheet->getColumnDimension('B')->setWidth(10);
                        $sheet->getColumnDimension('C')->setWidth(20);
                        $sheet->getColumnDimension('D')->setWidth(20);
                        $sheet->getColumnDimension('E')->setWidth(20);
                        $sheet->getColumnDimension('F')->setWidth(20);

                        $myExcel->getActiveSheet()->getCellByColumnAndRow(0, 1)->setValue('Наименование');
                        $myExcel->getActiveSheet()->getCellByColumnAndRow(1, 1)->setValue('Кол-во');
                        $myExcel->getActiveSheet()->getCellByColumnAndRow(2, 1)->setValue('Цена за ед.');
                        $myExcel->getActiveSheet()->getCellByColumnAndRow(3, 1)->setValue('Цена');
                        $myExcel->getActiveSheet()->getCellByColumnAndRow(4, 1)->setValue('Ндс');
                        $myExcel->getActiveSheet()->getCellByColumnAndRow(5, 1)->setValue('Итого');
                }   
                else
                {
                        $sheet->getColumnDimension('A')->setWidth(80);
                        $sheet->getColumnDimension('B')->setWidth(20);
                        $sheet->getColumnDimension('C')->setWidth(10);
                        $sheet->getColumnDimension('D')->setWidth(20);
                        $sheet->getColumnDimension('E')->setWidth(20);
                        $sheet->getColumnDimension('F')->setWidth(20);
                        $sheet->getColumnDimension('G')->setWidth(20);

                        $myExcel->getActiveSheet()->getCellByColumnAndRow(0, 1)->setValue('Наименование');
                        $myExcel->getActiveSheet()->getCellByColumnAndRow(1, 1)->setValue('Вид вагона');
                        $myExcel->getActiveSheet()->getCellByColumnAndRow(2, 1)->setValue('Кол-во');
                        $myExcel->getActiveSheet()->getCellByColumnAndRow(3, 1)->setValue('Цена за ед.');
                        $myExcel->getActiveSheet()->getCellByColumnAndRow(4, 1)->setValue('Цена');
                        $myExcel->getActiveSheet()->getCellByColumnAndRow(5, 1)->setValue('Ндс');
                        $myExcel->getActiveSheet()->getCellByColumnAndRow(6, 1)->setValue('Итого');           
                }
                
                //set_time_limit(0);
                $num=2; 
                foreach ($report[$j] as $count=>$item) 
                {  
                    $z=0;
                    if ($j==0 && $_GET['id']=='all')
                    {
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(0, $num)->setValue($item['title']);
                            $myExcel->getActiveSheet()->getStyle('A'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(1, $num)->setValue($item['cena']);
                            $myExcel->getActiveSheet()->getStyle('B'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(2, $num)->setValue($item['ndc']);
                            $myExcel->getActiveSheet()->getStyle('C'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(3, $num)->setValue($item['itog']);
                            $myExcel->getActiveSheet()->getStyle('D'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(4, $num)->setValue($item['ucena']);
                            $myExcel->getActiveSheet()->getStyle('E'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(5, $num)->setValue($item['undc']);
                            $myExcel->getActiveSheet()->getStyle('F'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(6, $num)->setValue($item['uitog']);
                            $myExcel->getActiveSheet()->getStyle('G'.$num)->getAlignment()->setWrapText(true);
                            // высота шапки
                            $sheet->getRowDimension(1)->setRowHeight(20);
                            // устанавливаем авто подбор высоты строки
                            $sheet->getRowDimension($num)->setRowHeight(-1);
                            // выравниваем текст по центру ячейки
                            $sheet->getStyle('A'.$num.':G'.$num)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                             if (isset($item['column']))
                             {
                                    if ($item['column']=='blue')
                                    {
                                      $style=$blue;   
                                    }
                                    elseif($item['column']=='green')
                                    {
                                      $style=$green;
                                      $z=1;
                                    }
                                    elseif($item['column']=='red')
                                    {
                                      $style=$red;   
                                    }
                                    elseif($item['column']=='blue bgblue')
                                    {
                                      $style=$bgblue;   
                                    }
                                    elseif($item['column']=='red bgblue')
                                    {
                                      $style=$bgred;   
                                    }
                                    elseif($item['column']=='normal')
                                    {
                                      $style=$normal;   
                                    }
                                    elseif($item['column']=='blue itog')
                                    {
                                      $style=$blueitog;   
                                    }
                                    elseif($item['column']=='red itog')
                                    {
                                      $style=$reditog;   
                                    }
                                  
                                      $sheet->getStyle('D'.$num)->applyFromArray($style);
                                      $sheet->getStyle('G'.$num)->applyFromArray($style);
                             }     
                             
                             if (isset($item['color']))
                             {
                                $sheet->getStyle('A'.$num.':G'.$num)->applyFromArray($bgblue); 
                             }    
                             
                             $sheet->getStyle('A1:G1')->applyFromArray($style_bold);
                             
                             $sheet->getStyle('A1:G1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                             $sheet->getStyle('A1:G1')->getAlignment()->setWrapText(true);
                             $sheet->getStyle('A1:G1')->applyFromArray($center);
                             $sheet->getStyle('A'.$num)->applyFromArray($vertical_center);
                             // чертим границы таблицы
                             if ($count==count($report[$j])-1)  
                             {
                                // так как с нуля в цикле идет отсчет
                                ($z==0) ? $endrow=count($report[$j])+1 : $endrow=count($report[$j])+2;  
                                $sheet->getStyle('A1:G'.$endrow)->applyFromArray($style_border); 
                                // форматирование ячейки выравнивание  текста по центру
                                $sheet->getStyle('B2:G'.$endrow)->applyFromArray($center);
                                $myExcel->getActiveSheet()->setAutoFilter('A1:G'.$endrow);
                             }                       
                    }
                    elseif ($item['rep']!='SOBS' && $item['rep']!='pr_ubr_zhd')
                    {   
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(0, $num)->setValue($item['title']);
                            $myExcel->getActiveSheet()->getStyle('A'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(1, $num)->setValue($item['count']);
                            $myExcel->getActiveSheet()->getStyle('B'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(2, $num)->setValue($item['cenaed']);
                            $myExcel->getActiveSheet()->getStyle('C'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(3, $num)->setValue($item['cena']);
                            $myExcel->getActiveSheet()->getStyle('D'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(4, $num)->setValue($item['ndc']);
                            $myExcel->getActiveSheet()->getStyle('E'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(5, $num)->setValue($item['itog']);
                            $myExcel->getActiveSheet()->getStyle('F'.$num)->getAlignment()->setWrapText(true);
                            
                            // высота шапки
                            $sheet->getRowDimension(1)->setRowHeight(20);
                            // устанавливаем авто подбор высоты строки
                            $sheet->getRowDimension($num)->setRowHeight(-1);
                            // выравниваем текст по центру ячейки
                            $sheet->getStyle('A'.$num.':F'.$num)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                             if ($item['cenaed']=='')
                             {
                                    if ($item['color']=='blue')
                                    {
                                      $style=$blue;
                                    }
                                    elseif($item['color']=='green')
                                    {
                                      $style=$green;
                                      $z=1;
                                    }
                                    elseif($item['color']=='red')
                                    {
                                      $style=$red;   
                                    }
                                    elseif($item['color']=='blue bgblue')
                                    {
                                      $style=$bgblue;   
                                    }
                                    elseif($item['color']=='red bgblue')
                                    {
                                      $style=$bgred;   
                                    }
                                    elseif($item['color']=='normal')
                                    {
                                      $style=$normal;   
                                    }
                                    elseif($item['color']=='blue itog')
                                    {
                                      $style=$blueitog;   
                                    }
                                    elseif($item['color']=='red itog')
                                    {
                                      $style=$reditog;   
                                    }
                                  
                                      $sheet->getStyle('A'.$num.':F'.$num)->applyFromArray($style);  
                             }     
                             
                             $sheet->getStyle('A1:F1')->applyFromArray($style_bold);
                             
                             $sheet->getStyle('A1:F1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                             $sheet->getStyle('A1:F1')->getAlignment()->setWrapText(true);
                             $sheet->getStyle('A1:F1')->applyFromArray($center);
                             $sheet->getStyle('A'.$num)->applyFromArray($vertical_center);
                             // чертим границы таблицы
                             if ($count==count($report[$j])-1)  
                             {
                                // так как с нуля в цикле идет отсчет
                                ($z==0) ? $endrow=count($report[$j])+1 : $endrow=count($report[$j])+2;  
                                $sheet->getStyle('A1:F'.$endrow)->applyFromArray($style_border); 
                                // форматирование ячейки выравнивание  текста по центру
                                $sheet->getStyle('B2:F'.$endrow)->applyFromArray($center);
                                $myExcel->getActiveSheet()->setAutoFilter('A1:F'.$endrow);
                             }
                    }
                    else
                    {
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(0, $num)->setValue($item['title']);
                            $myExcel->getActiveSheet()->getStyle('A'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            (isset($item['vvagon_id'])) ? $v=$item['vvagon_id'] : $v=''; 
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(1, $num)->setValue($v);
                            $myExcel->getActiveSheet()->getStyle('B'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(2, $num)->setValue($item['count']);
                            $myExcel->getActiveSheet()->getStyle('C'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(3, $num)->setValue($item['cenaed']);
                            $myExcel->getActiveSheet()->getStyle('D'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(4, $num)->setValue($item['cena']);
                            $myExcel->getActiveSheet()->getStyle('E'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(5, $num)->setValue($item['ndc']);
                            $myExcel->getActiveSheet()->getStyle('F'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(6, $num)->setValue($item['itog']);
                            $myExcel->getActiveSheet()->getStyle('G'.$num)->getAlignment()->setWrapText(true);
                            // высота шапки
                            $sheet->getRowDimension(1)->setRowHeight(20);
                            // устанавливаем авто подбор высоты строки
                            $sheet->getRowDimension($num)->setRowHeight(-1);
                            // выравниваем текст по центру ячейки
                            $sheet->getStyle('A'.$num.':G'.$num)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                             if ($item['cenaed']=='')
                             {
                                    if ($item['color']=='blue')
                                    {
                                      $style=$blue;   
                                    }
                                    elseif($item['color']=='green')
                                    {
                                      $style=$green;
                                      $z=1;
                                    }
                                    elseif($item['color']=='red')
                                    {
                                      $style=$red;   
                                    }
                                    elseif($item['color']=='blue bgblue')
                                    {
                                      $style=$bgblue;   
                                    }
                                    elseif($item['color']=='red bgblue')
                                    {
                                      $style=$bgred;   
                                    }
                                    elseif($item['color']=='normal')
                                    {
                                      $style=$normal;   
                                    }
                                    elseif($item['color']=='blue itog')
                                    {
                                      $style=$blueitog;   
                                    }
                                    elseif($item['color']=='red itog')
                                    {
                                      $style=$reditog;   
                                    }
                                  
                                      $sheet->getStyle('A'.$num.':G'.$num)->applyFromArray($style);  
                             }     
                             
                             $sheet->getStyle('A1:G1')->applyFromArray($style_bold);
                             
                             $sheet->getStyle('A1:G1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                             $sheet->getStyle('A1:G1')->getAlignment()->setWrapText(true);
                             $sheet->getStyle('A1:G1')->applyFromArray($center);
                             $sheet->getStyle('A'.$num)->applyFromArray($vertical_center);
                             // чертим границы таблицы
                             if ($count==count($report[$j])-1)  
                             {
                                // так как с нуля в цикле идет отсчет
                                ($z==0) ? $endrow=count($report[$j])+1 : $endrow=count($report[$j])+2;  
                                $sheet->getStyle('A1:G'.$endrow)->applyFromArray($style_border); 
                                // форматирование ячейки выравнивание  текста по центру
                                $sheet->getStyle('B2:G'.$endrow)->applyFromArray($center);
                                $myExcel->getActiveSheet()->setAutoFilter('A1:G'.$endrow);
                             }
                    }    
                   $num++;  
                }
         }
        
         
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $filename = "DB_Export" . date("d-m-Y") . ".xls";
        header('Content-Disposition: attachment;filename=' . $filename . ' ');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($myExcel, 'Excel5');
        $objWriter->save('php://output');  
       
        return $this->redirect(['index']);
     
    } 
      
}