<?php

namespace app\controllers;

use Yii;
use app\models\Groups;
use app\models\GroupsSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


/**
 * GroupsController implements the CRUD actions for Groups model.
 */
class GroupsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Lists all Groups models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->can('superadmin')) 
        { 
            $searchModel = new GroupsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->pagination->pageSizeParam = false;
    
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);    
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }    
    }

    /**
     * Displays a single Groups model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->user->can('superadmin')) 
        { 
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }    

    }

    /**
     * Creates a new Groups model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->can('superadmin')) 
        { 
            $model = new Groups();
    
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->db->createCommand()->batchInsert('auth_item', ['name', 'type','rule_name','created_at'], 
                [[$model->alias,1,null,date('Ymd')]])->execute();            
                return $this->redirect(['index']);
    
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }    
            
    }

    /**
     * Updates an existing Groups model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->can('superadmin')) 
        { 
            $model = $this->findModel($id);
             $ind=$model->alias; //сохранить старый алиас
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->db->createCommand("UPDATE auth_item SET name=:name_p,updated_at=:updated_at WHERE name=:name")
                             ->bindValue(':name_p', $model->alias) 
                             ->bindValue(':updated_at', date('Ymd')) 
                             ->bindValue(':name', $ind) 
                             ->execute();
    
                return $this->redirect(['index']);            
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }    
            
    }

    /**
     * Deletes an existing Groups model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->can('superadmin')) 
        { 
            $ids= explode(',',$id);
            
            for($i=0;$i<count($ids);$i++)
            {
              $gr=$this->findModel($ids[$i]);
              Yii::$app->db->createCommand("DELETE FROM auth_item WHERE name = :alias")
                                            ->bindValue(':alias', $gr->alias) 
                                            ->execute();
              $this->findModel($ids[$i])->delete();
            }
            return $this->redirect(['index']);
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }    
            
    }

    /**
     * Finds the Groups model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Groups the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (Yii::$app->user->can('superadmin')) 
        { 
            if (($model = Groups::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }    
    }


}
