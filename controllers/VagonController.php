<?php

namespace app\controllers;

use Yii;
use yii\helpers\Html;
use app\models\Vagon;
use app\models\vagonSearch;
use app\models\Vc;
use app\models\Osn;
use app\models\Spr;
use app\models\Detailspr;
use app\models\Detail;
use app\models\Revizia;
use app\models\Reviziaspr;
use app\models\Promiv;
use app\models\Promivspr;
use app\models\Zhd;
use app\models\Zhdspr;
use app\models\Clean;
use app\models\Cleanspr;
use app\models\Vvagon;
use app\models\Sobstvenik;
use app\models\Prichina;
use app\models\Usluga;
use app\models\Uslugaspr;
use app\models\Dopuslugaspr;
use app\models\Dopusluga;
use app\models\Price;
use app\models\Tips;
use app\models\Tordrkr;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\behaviors\SluggableBehavior;
/**
 * VagonController implements the CRUD actions for vagon model.
 */
class VagonController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all vagon models.
     * @return mixed
     */
    public function actionIndex()
    {
       if ( Yii::$app->user->can('author') )
       {         
            $searchModel = new VagonSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
           
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
       }
       else
       {
              throw new HttpException(403,'Доступ закрыт');        
       } 
    }

    /**
     * Displays a single vagon model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
       if ( Yii::$app->user->can('author') )
       {
            $model=$this->findModel($id);
            $spr=Prichina::findAll(['vagon_id'=>$id]); 
            $details=Detail::findAll(['vagon_id'=>$id]); 
            $revizia=Revizia::findAll(['vagon_id'=>$id]);
            $promivka=Promiv::findAll(['vagon_id'=>$id]);
            $zhd=Zhd::findAll(['vagon_id'=>$id]);
            $uborka=Clean::findAll(['vagon_id'=>$id]);
            
            return $this->render('view', ['model' => $model,
            'spr'=>$spr,'details'=>$details,'revizia'=>$revizia,'promivka'=>$promivka,
            'zhd'=>$zhd,'uborka'=>$uborka]);
       }
       else
       {
              throw new HttpException(403,'Доступ закрыт');        
       } 
    }

    /**
     * Creates a new vagon model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
       if ( Yii::$app->user->can('create-post')) 
       { 
            $model = new Vagon();
            $model->dropremont=[];// пустой массив вид ремонта
            $model->vid_vagon=[];
            $model->n_sobstvenik=[];
            // собираем массив тор др кр и пвсе посредники 
            $model->omas=[1=>'ТОР',2=>'ДР',3=>'КР'];   
            $m = (new \yii\db\Query())->select('*')->from('posrednik')->where('status=1')->all(); // получаем пункты посреднников
            foreach ($m as $value) 
            {
              $model->omas[]=$value['title'];   
            }
            // конец сбора массива
            
            if ($model->load(Yii::$app->request->post()) ) 
            {
                 // сохраняем id автора
                $model->createdBy=Yii::$app->user->id;
                 // при сохранении переворачиваем дату как надо для базы
                $model->dates=date('Y-m-d', strtotime($model->dates)); 
                ($model->drab==null)? $model->drab=null : $model->drab=date('Y-m-d', strtotime($model->drab)); 
                ($model->dbrak==null)? $model->dbrak=null : $model->dbrak=date('Y-m-d', strtotime($model->dbrak)); 
                
                if (isset($_POST['Tordrkr'][0])) {$model->remont_id=$_POST['Tordrkr'][0];} 
                elseif(isset($_POST['Promyvka'][0])) {$model->remont_id=$_POST['Promyvka'][0];}
                elseif(isset($_POST['ZHd_tarif'][0])) {$model->remont_id=$_POST['ZHd_tarif'][0];}
                elseif(isset($_POST['Podacha_i_uborka'][0])) {$model->remont_id=$_POST['Podacha_i_uborka'][0];}
                elseif(isset($_POST['Uslugi'][0])) {$model->remont_id=$_POST['Uslugi'][0];}
                
                (!empty($model->vcomp_id)) ? $model->vcomp_id=implode(',',$model->vcomp_id) : '';
                
                if ($model->save())
                {
                  /** Тор/др/кр **/
                  if ( isset($_POST['Tordrkr']) ){ 
                    for($i=0;$i<count($_POST['Tordrkr']);$i++)
                    {
                        Yii::$app->db->createCommand()->batchInsert('tordrkr', ['vagon_id', 'mremont_id','remont_id','cenaed','pricetip_id','tip','vvagon_id','sobstvenik_id','dates','kol'], 
                        [ [ $model->id,$model->mremont_id,$_POST['Tordrkr'][0],$_POST['Tordrkrdcenaed'][$i],$_POST['Tordrkrtip_id'][$i],$model->tip,$model->vvagona,$model->sobstvenik_id,$model->dates,1 ]])->execute();            
                    }
                  } 

                  /** справка 2612 **/
                  if ( isset($_POST['Spravka_2612']) ){ 
                    for($i=0;$i<count($_POST['Spravka_2612']);$i++)
                    {
                        Yii::$app->db->createCommand()->batchInsert('prichina', ['vagon_id', 'spr_id','tip','mremont_id','remont_id','vvagon_id','sobstvenik_id','dates','cenaed','pricetip_id'], 
                        [ [ $model->id,$_POST['Spravka_2612'][$i],$model->tip,$model->mremont_id,$_POST['Tordrkr'][0],$model->vvagona,$model->sobstvenik_id,$model->dates,$_POST['Tordrkrdcenaed'][0],$_POST['Spravka_2612tip_id'][$i] ]])->execute();            
                    }
                  } 
                  /** КОНЕЦ справка 2612 **/

                  /** Детали **/
                  if ( isset($_POST['Detal']) ){ 
                    for($i=0;$i<count($_POST['Detal']);$i++)
                    {
                        Yii::$app->db->createCommand()->batchInsert('detail', ['vagon_id', 'detailspr_id','tip','mremont_id','remont_id','vvagon_id','sobstvenik_id','dates','cenaed','kol','pricetip_id'], 
                        [ [ $model->id,$_POST['Detal'][$i],$model->tip,$model->mremont_id,$_POST['Detal'][0],$model->vvagona,$model->sobstvenik_id,$model->dates,$_POST['Detaldcenaed'][$i],$_POST['Detalkol'][$i],$_POST['Detaltip_id'][$i] ] ])->execute();            
                    }
                  } 
                  /** КОНЕЦ детали **/

                  /** Ревизия **/
                  if ( isset($_POST['Reviziya']) ){ 
                    for($i=0;$i<count($_POST['Reviziya']);$i++)
                    {
                        Yii::$app->db->createCommand()->batchInsert('revizia', ['vagon_id', 'reviziaspr_id','tip','mremont_id','remont_id','vvagon_id','sobstvenik_id','dates','cenaed','kol','pricetip_id'], 
                        [ [ $model->id,$_POST['Reviziya'][$i],$model->tip,$model->mremont_id,$_POST['Reviziya'][0],$model->vvagona,$model->sobstvenik_id,$model->dates,$_POST['Reviziyadcenaed'][$i],$_POST['Reviziyakol'][$i],$_POST['Reviziyatip_id'][$i] ] ])->execute();            
                    }
                  } 
                  /** КОНЕЦ ревизии **/
                
                  /** Промывка **/
                  if ( isset($_POST['Promyvka']) ){ 
                    for($i=0;$i<count($_POST['Promyvka']);$i++)
                    {
                        Yii::$app->db->createCommand()->batchInsert('promiv', ['vagon_id', 'promivspr_id','tip','mremont_id','remont_id','vvagon_id','sobstvenik_id','dates','cenaed','kol','pricetip_id'], 
                        [ [ $model->id,$_POST['Promyvka'][$i],$model->tip,$model->mremont_id,$_POST['Promyvka'][0],$model->vvagona,$model->sobstvenik_id,$model->dates,$_POST['Promyvkadcenaed'][$i],$_POST['Promyvkakol'][$i],$_POST['Promyvkatip_id'][$i] ] ])->execute();            
                    }
                  } 
                  /** КОНЕЦ промывка **/

                  /** ЖД тариф **/
                  if ( isset($_POST['ZHd_tarif']) ){ 
                    for($i=0;$i<count($_POST['ZHd_tarif']);$i++)
                    {
                        Yii::$app->db->createCommand()->batchInsert('zhd', ['vagon_id', 'zhdspr_id','tip','mremont_id','remont_id','vvagon_id','sobstvenik_id','dates','cenaed','kol','pricetip_id'], 
                        [ [ $model->id,$_POST['ZHd_tarif'][$i],$model->tip,$model->mremont_id,$_POST['ZHd_tarif'][0],$model->vvagona,$model->sobstvenik_id,$model->dates,$_POST['ZHd_tarifdcenaed'][$i],$_POST['ZHd_tarifkol'][$i],$_POST['ZHd_tariftip_id'][$i] ] ])->execute();            
                    }
                  } 
                  /** КОНЕЦ жжд тариф **/

                  /** Подача и уборка **/
                  if ( isset($_POST['Podacha_i_uborka']) ){ 
                    for($i=0;$i<count($_POST['Podacha_i_uborka']);$i++)
                    {
                        Yii::$app->db->createCommand()->batchInsert('clean', ['vagon_id', 'cleanspr_id','tip','mremont_id','remont_id','vvagon_id','sobstvenik_id','dates','cenaed','kol','pricetip_id'], 
                        [ [ $model->id,$_POST['Podacha_i_uborka'][$i],$model->tip,$model->mremont_id,$_POST['Podacha_i_uborka'][0],$model->vvagona,$model->sobstvenik_id,$model->dates,$_POST['Podacha_i_uborkadcenaed'][$i],$_POST['Podacha_i_uborkakol'][$i],$_POST['Podacha_i_uborkatip_id'][$i] ] ])->execute();            
                    }
                  } 
                  /** КОНЕЦ подача и уборка **/

                  /** Услуга **/
                  if ( isset($_POST['Uslugi']) ){ 
                    for($i=0;$i<count($_POST['Uslugi']);$i++)
                    {
                        Yii::$app->db->createCommand()->batchInsert('usluga', ['vagon_id', 'uslugaspr_id','tip','mremont_id','remont_id','vvagon_id','sobstvenik_id','dates','cenaed','kol','pricetip_id'], 
                        [ [ $model->id,$_POST['Uslugi'][$i],$model->tip,$model->mremont_id,$_POST['Uslugi'][0],$model->vvagona,$model->sobstvenik_id,$model->dates,$_POST['Uslugidcenaed'][$i],$_POST['Uslugikol'][$i],$_POST['Uslugitip_id'][$i] ] ])->execute();            
                    }
                  } 
                  /** КОНЕЦ услуга **/
                
                    return $this->redirect(['index']);
                }
                else
                {
                   return $this->render('create', ['model' => $model]);
                }            
            }
            else
            {
                return $this->render('create', ['model' => $model]);
            }
       }
       else
       {
              throw new HttpException(403,'Доступ закрыт');        
       }              
    }

 
   protected function loadchildItems($id,$join_tblname,$relation)
   {
       $mas=[];
      if($join_tblname!='prichina'){     
          $vagon = Vagon::find()
                    ->select('vagon.*')
                    ->leftJoin($join_tblname, 'vagon.id='.$join_tblname.'.vagon_id' )
                    ->where([$join_tblname.'.vagon_id' => $id])
                    ->with($relation)
                    ->all();
            foreach ($vagon as $key=>$item)
            {
               foreach($item->$relation as $ikey=>$value)
               {
                   $mas[$ikey]=$value->id;
               } 
            }
      }
      else
      {
        
         $vagon = (new \yii\db\Query())
                                    ->select(['*']) 
                                    ->from('prichina')
                                    ->where('vagon_id='.$id.' && prichina.spr_id!=""') 
                                    ->all();          
         
         foreach($vagon as $value)
         {
            $mas[]=$value['id'];
         }
      }      
       
       return $mas; 
   }

    /**
     * Updates an existing vagon model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
       $model = $this->findModel($id);
       $model->dates=date('d-m-Y', strtotime($model->dates)); 
       ($model->drab==null)? $model->drab=null : $model->drab=date('d-m-Y', strtotime($model->drab)); 
       ($model->dbrak==null)? $model->dbrak=null : $model->dbrak=date('d-m-Y', strtotime($model->dbrak)); 
       (!empty($model->vcomp_id)) ? $model->vcomp_id=explode(',',$model->vcomp_id) : '';
            // собираем массив тор др кр и пвсе посредники 
            $model->omas=[1=>'ТОР',2=>'ДР',3=>'КР'];   
            $m = (new \yii\db\Query())->select('*')->from('posrednik')->where('status=1')->all(); // получаем пункты посреднников
            foreach ($m as $value) 
            {
              $model->omas[]=$value['title'];   
            }
            // конец сбора массива

       if ( Yii::$app->user->can('update-post', ['model' => $model])) 
       { 
          if ($model->vvagona!=null){
            $model->vid_vagon=$this->loadDrop($model->vvagona,'Vvagon');
          }
          else
          {
            $model->vid_vagon=[];
          }
          
          if ($model->sobstvenik_id!=null){
            $model->n_sobstvenik=$this->loadDrop($model->sobstvenik_id,'Sobstvenik');
          }
          else
          {
            $model->n_sobstvenik=[];
          } 
         
          /** ******** Получаем все селекты TAB'ов'*******************/ 
           $dr=Prichina::findone(['spr_id'=>null,'vagon_id'=>$id]); // ищем id записи в причинах где spr_id пусто, ищем по вагону id, другие значения                    
           
           $model->tordrkr=$this->loadTags($id,'Tordrkr',$model->mremont_id);
           $model->spr2615=$this->loadTags($id,'Spravka_2612',null);
           $model->detailt=$this->loadTags($id,'Detal',$model->mremont_id);
           $model->reviz=$this->loadTags($id,'Reviziya',$model->mremont_id);
           $model->promiv=$this->loadTags($id,'Promyvka',$model->mremont_id);
           $model->zhdtarif=$this->loadTags($id,'ZHd_tarif',$model->mremont_id);
           $model->uborka=$this->loadTags($id,'Podacha_i_uborka',$model->mremont_id);
           $model->usluga=$this->loadTags($id,'Uslugi',$model->mremont_id);

           /** *********  *** Конец *** ********************************/
    
            /** ******** Получаем массивы  id записей селектов в TAB'ах при сохранении проверяет какие обновлять, какие удалять*******************/ 
           $tid=$this->loadchildItems($id,'tordrkr','tordrkrs');
           $sid=$this->loadchildItems($id,'prichina','prichinas');
           $did=$this->loadchildItems($id,'detail','details');
           $rid=$this->loadchildItems($id,'revizia','revizias');
           $pid=$this->loadchildItems($id,'promiv','promivs');
           $zid=$this->loadchildItems($id,'zhd','zhds');
           $uid=$this->loadchildItems($id,'clean','cleans');
           $usid=$this->loadchildItems($id,'usluga','uslugas');

                if ($model->load(Yii::$app->request->post()) ) 
                {
                    $model->dates=date('Y-m-d', strtotime($model->dates)); /// при сохранении переворачиваем дату как надо для базы
                    ($model->drab==null)? $model->drab=null : $model->drab=date('Y-m-d', strtotime($model->drab)); 
                    ($model->dbrak==null)? $model->dbrak=null : $model->dbrak=date('Y-m-d', strtotime($model->dbrak)); 
                    (!empty($model->vcomp_id)) ? $model->vcomp_id=implode(',',$model->vcomp_id) : '';
                         // из-за чего ремонтировали вагон сохраняем в переменную remont_id
                    if (isset($_POST['Tordrkr'][0])) {$model->remont_id=$_POST['Tordrkr'][0];} 
                    elseif(isset($_POST['Promyvka'][0])) {$model->remont_id=$_POST['Promyvka'][0];}
                    elseif(isset($_POST['ZHd_tarif'][0])) {$model->remont_id=$_POST['ZHd_tarif'][0];}
                    elseif(isset($_POST['Podacha_i_uborka'][0])) {$model->remont_id=$_POST['Podacha_i_uborka'][0];}
                    elseif(isset($_POST['Uslugi'][0])) {$model->remont_id=$_POST['Uslugi'][0];}

                    if($model->save())
                    {                  
                       /** ********* Проверяем были ли удалены или добавлены новые селекты *** **/
                                   //обновляем причину с типом ДР или КР (tip=2 или 3)
                         /*if($model->tip==2)
                         {          //  'vagon_id', 'spr_id','tip','mremont_id','remont_id','vvagon_id','sobstvenik_id','dates','cenaed','dr_id'
                                   Yii::$app->db->createCommand("UPDATE prichina SET vagon_id=:p1,tip=:p3,mremont_id=:p4,remont_id=:p5,vvagon_id=:p6,sobstvenik_id=:p7,dates=:p8,cenaed=:p9,dr_id=:p10  WHERE id=:id")
                                     ->bindValue(':p1', $id) 
                                     ->bindValue(':p3', $model->tip)
                                     ->bindValue(':p4', $model->mremont_id) 
                                     ->bindValue(':p5', $model->remont_id) 
                                     ->bindValue(':p6', $model->vvagona) 
                                     ->bindValue(':p7', $model->sobstvenik_id)
                                     ->bindValue(':p8', $model->dates)
                                     ->bindValue(':p9', $model->cenaed)
                                     ->bindValue(':p10', $model->remont_id) // заполняем поле dr_id значением id,  поля remont_id
                                     ->bindValue(':id', $_POST['torkrdr']) //  это id причины у которой нет справки 2612 (т.е. пустое поле spr_id) такое возможно если поле tip=2 или 3  (ДР,КР)
                                     ->execute();
                         } */

                        if (isset($_POST['chkTordrkr'])) {   
                            $this->Checktabs('Tordrkr',$tid,$_POST['chkTordrkr'],$id,$_POST['Tordrkr'],$model->tip,$model->mremont_id,null,$model->vvagona,$model->sobstvenik_id,$model->dates,$_POST['Tordrkrdcenaed'][0],$_POST['Tordrkrkol'][0],$_POST['Tordrkrtip_id']);
                        }
                        else  // удаляем если при обновлении убрали все селекты этого типа
                        {
                          $this->Checktabs('Tordrkr',$tid,null,$id,null,$model->tip,$model->mremont_id,null,$model->vvagona,$model->sobstvenik_id,$model->dates,null,null,null);
                        }
                        
                        if (isset($_POST['chkSpravka_2612'])) {   
                          $this->Checktabs('Spravka_2612',$sid,$_POST['chkSpravka_2612'],$id,$_POST['Spravka_2612'],$model->tip,$model->mremont_id,$_POST['Tordrkr'][0],$model->vvagona,$model->sobstvenik_id,$model->dates,$_POST['Tordrkrdcenaed'][0],null,$_POST['Spravka_2612tip_id']);
                        }
                        else  // удаляем если при обновлении убрали все селекты этого типа
                        {
                          $this->Checktabs('Spravka_2612',$sid,null,$id,null,$model->tip,$model->mremont_id,null,$model->vvagona,$model->sobstvenik_id,$model->dates,null,null,null);
                        }
                        
                       if (isset($_POST['chkDetal'])){
                         $this->Checktabs('Detal',$did,$_POST['chkDetal'],$id,$_POST['Detal'],$model->tip,$model->mremont_id,$_POST['Detal'][0],$model->vvagona,$model->sobstvenik_id,$model->dates,$_POST['Detaldcenaed'],$_POST['Detalkol'],$_POST['Detaltip_id']);
                        }
                       else  // удаляем если при обновлении убрали все селекты этого типа
                        {
                          $this->Checktabs('Detal',$did,null,$id,null,$model->tip,$model->mremont_id,null,$model->vvagona,$model->sobstvenik_id,$model->dates,null,null,null);
                        }
                        
                        if(isset($_POST['chkReviziya'])){
                           $this->Checktabs('Reviziya',$rid,$_POST['chkReviziya'],$id,$_POST['Reviziya'],$model->tip,$model->mremont_id,$_POST['Reviziya'][0],$model->vvagona,$model->sobstvenik_id,$model->dates,$_POST['Reviziyadcenaed'],$_POST['Reviziyakol'],$_POST['Reviziyatip_id']);
                        }
                        else  // удаляем если при обновлении убрали все селекты этого типа
                        {
                          $this->Checktabs('Reviziya',$rid,null,$id,null,$model->tip,$model->mremont_id,null,$model->vvagona,$model->sobstvenik_id,$model->dates,null,null,null);
                        }
                        
                       if (isset($_POST['chkPromyvka'])){    
                           $this->Checktabs('Promyvka',$pid,$_POST['chkPromyvka'],$id,$_POST['Promyvka'],$model->tip,$model->mremont_id,$_POST['Promyvka'][0],$model->vvagona,$model->sobstvenik_id,$model->dates,$_POST['Promyvkadcenaed'],$_POST['Promyvkakol'],$_POST['Promyvkatip_id']);
                        }
                        else  // удаляем если при обновлении убрали все селекты этого типа
                        {
                          $this->Checktabs('Promyvka',$pid,null,$id,null,$model->tip,$model->mremont_id,null,$model->vvagona,$model->sobstvenik_id,$model->dates,null,null,null);
                        }    
                       
                        if(isset($_POST['chkZHd_tarif'])){
                           $this->Checktabs('ZHd_tarif',$zid,$_POST['chkZHd_tarif'],$id,$_POST['ZHd_tarif'],$model->tip,$model->mremont_id,$_POST['ZHd_tarif'][0],$model->vvagona,$model->sobstvenik_id,$model->dates,$_POST['ZHd_tarifdcenaed'],$_POST['ZHd_tarifkol'],$_POST['ZHd_tariftip_id']);
                        }                   
                        else  // удаляем если при обновлении убрали все селекты этого типа
                        {
                          $this->Checktabs('ZHd_tarif',$zid,null,$id,null,$model->tip,$model->mremont_id,null,$model->vvagona,$model->sobstvenik_id,$model->dates,null,null,null);
                        }    
                        
                        if (isset($_POST['chkPodacha_i_uborka'])){
                         $this->Checktabs('Podacha_i_uborka',$uid,$_POST['chkPodacha_i_uborka'],$id,$_POST['Podacha_i_uborka'],$model->tip,$model->mremont_id,$_POST['Podacha_i_uborka'][0],$model->vvagona,$model->sobstvenik_id,$model->dates,$_POST['Podacha_i_uborkadcenaed'],$_POST['Podacha_i_uborkakol'],$_POST['Podacha_i_uborkatip_id']);
                        }
                        else  // удаляем если при обновлении убрали все селекты этого типа
                        {
                          $this->Checktabs('Podacha_i_uborka',$uid,null,$id,null,$model->tip,$model->mremont_id,null,$model->vvagona,$model->sobstvenik_id,$model->dates,null,null,null);
                        }    
                        
                        if (!empty($usid) || isset($_POST['chkUslugi'])){
                            (isset($_POST['chkUslugi'])) ? $chk=$_POST['chkUslugi'] : $chk='';
                            (isset($_POST['Uslugi'])) ? $select=$_POST['Uslugi'] : $select='';
                            (isset($_POST['Uslugi'][0])) ? $remname=$_POST['Uslugi'][0] : $remname=''; 
                            (isset($_POST['Uslugidcenaed'])) ? $ucena=$_POST['Uslugidcenaed'] : $ucena='';
                            (isset($_POST['Uslugikol'])) ? $ukol=$_POST['Uslugikol'] : $ukol='';
                            (isset($_POST['Uslugitip_id'])) ? $utip=$_POST['Uslugitip_id'] : $utip='';
                            $this->Checktabs('Uslugi',$usid,$chk,$id,$select,$model->tip,$model->mremont_id,$remname,$model->vvagona,$model->sobstvenik_id,$model->dates,$ucena,$ukol,$utip);
                        }
                        else  // удаляем если при обновлении убрали все селекты этого типа
                        {
                          $this->Checktabs('Uslugi',$usid,null,$id,null,$model->tip,$model->mremont_id,null,$model->vvagona,$model->sobstvenik_id,$model->dates,null,null,null);
                        }    
                        
                        
                        return $this->redirect(['index']);
                   }
                   else
                   {
                     return $this->render('update', ['model' => $model]);
                   }
                } else {
                    return $this->render('update', ['model' => $model]);
                }
       }
       else
       {
                  throw new HttpException(403,'Доступ закрыт');        
       }         
    }

    /**
     * Deletes an existing vagon model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
      $model = $this->findModel($id);
       if ( Yii::$app->user->can('delete-post', ['model' => $model])) 
       {
            $ids= explode(',',$id);
            
            for($i=0;$i<count($ids);$i++)
            {
              $this->findModel($ids[$i])->delete();
            }
            return $this->redirect(['index']);
       }
       else
       {
              throw new HttpException(403,'Доступ закрыт');  
       }             
    }

    /**
     * Finds the vagon model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return vagon the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
       if ( Yii::$app->user->can('author') )
       { 
            if (($model = Vagon::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
       }
       else
       {
              throw new HttpException(403,'Доступ закрыт');  
       }             
    }


    /** экшен используется при UPDATE возвращаем вид ремонта ТОР/ДР/КР станции **/
    protected  function loadOsn($id)
    {
       if ( Yii::$app->user->can('author') )
       { 
           $post = Osn::findAll(['mremont_id'=>$id]);
           foreach ($post as $val)
           {
             $mas[$val->id]=$val->remont;
           }
           return $mas;
       }    
    }

/** экшен полуает атрибут data-cena option'ов поля Вид ремонта - ТОР/ДР/КР станции **/
    protected  function attrOsn($id,$remid)
    {
       if ( Yii::$app->user->can('author') )
       { 
           $post = Osn::findAll(['mremont_id'=>$id]);
           foreach ($post as $val)
           {
              if ($val->id==$remid) {
                $mas2['data-cena']=$val->cena;
                $mas2['selected']='selected';
              }
              else
              {
                $mas2['data-cena']=$val->cena;
              }
             $mas[$val->id]=$mas2;
           }

           return $mas;
       }    
    }
    /** ************ end ******************************/

    /** функция возвращаем Вид вагона и собственника **/
    protected  function loadDrop($id,$modelname)
    {
       if ( Yii::$app->user->can('author') )
       {   
           if ($modelname=='Vvagon') 
           {
             $post = Vvagon::findOne($id);
           }
           elseif($modelname=='Sobstvenik')
           {
             $post = Sobstvenik::findOne($id);
           }
           
           $mas[$id]=$post->title;
           return $mas;
       }    
    }

    /** ******** При выборе № вагона, происходит автозаполнение вагона,
     *********** собственника и контракта  
     ***/ 
    public function actionNvagon()
    {
       $id=$_POST['id'];
       $post = Vc::findOne([$id]);

       foreach($post as $key=>$item)
       {
         if ($key=='vvagon_id')
         {
           $vvagon = Vvagon::findOne($item);
           $mas[$key]="<option value='".$item."' selected='selected'>".$vvagon->title."</option>";
           $mas['vvagontit']=$vvagon->title;
         }
         elseif($key=='sobstvenik_id')
         {
           $sobstvenik=Sobstvenik::findOne($item);
           $mas[$key]="<option value='".$item."' selected='selected'>".$sobstvenik->title."</option>";
           $mas['sobstit']=$sobstvenik->title;
         }
         else
         {
            $mas[$key]=$item;
         }
       }
        echo json_encode($mas);
        exit;
    }


    /** **** Заполняем селект вида ремонта в зависимости 
     ******* от значния в поле ремонта   
     **/ 
    public function actionOsn()
    {
       $id=$_POST['id'];
       $post = Osn::findAll(['mremont_id'=>$id]);
       for($i=0;$i<count($post);$i++) 
       {
         $mas[$i]['option']= "<option value='".$post[$i]->id."' data-cena='".$post[$i]->cena."'>".$post[$i]->remont."</option>";
       }
        
        echo json_encode($mas);
        exit;
    }

   
   
   
   /** Добавление селектов при нажатии кнопок добавить в табах **/
    public function actionAddrule()
    {
        if (Yii::$app->user->can('author')) 
        { 
            $alias=$this->getAlias($_POST['ind']);
            $i=rand(100,10000);
            $pricetip=Tips::findOne(['titlename'=>$_POST['ind']]);
           
            // получаем список
            $model = Price::findAll(['mremont_id'=>$_POST['mr'],'pricetip'=>$pricetip->id]); 
           
            
            if ($_POST['ind']=='Справка 2612')
            {   
                if (empty($model))
                {$model = Price::findAll(['mremont_id'=>null]);} 

                foreach($model as $key=>$val)
                {
                   $mas[$key]='<option value="'.$val->id.'" data-random="'.$i.'">'.$val->code.' '.$val->titlename.'</option>'; 
                }
            }
            else
            {  
                if (!empty($model))
                {
                    foreach($model as $key=>$val)
                    {
                        $mas[$key]='<option value="'.$val->id.'" data-cena="'.$val->cena.'" data-random="'.$i.'">'.$val->titlename.'</option>'; 
                    }
                }
            }
            

            $html='<div class="oneitem form-group'.$i.'">';
            $html.=Html::input('text',$alias.'tip_id[]',$_POST['pricetip'],['style'=>'display:none']);
            $html.=Html::checkbox('chk'.$alias.'[]',true,['value'=>0,'style'=>"display:none"]);
            $html.='<select name="'.$alias.'[]"  id="'.$alias.$i.'" style="display:none" >';
            $html.='<option value >- Выберите значение -</option>';
                if (!empty($model))
                {
                    for($j=0;$j<count($mas);$j++)
                     {
                        $html.=$mas[$j];
                     }
                }
            $html.='</select>';
            $html.='<span class="mabselect details"  id="'.$alias.$i.'">
                  <a id="'.$alias.$i.'" class="single" data-id="ins">
                      <span>- Выберите значение -</span>
                      <div><i></i></div>
                  </a>
                  <div class="combo"><ul id="sps'.$alias.$i.'" class="results" style="display:none;"></ul></div>
                  </span>';
             
             if($_POST['ind']!='Справка 2612')
             {
                $html.='<span class="bl"><input type="text" name="'.$alias.'kol[]" value="1" id="dkol'.$i.'" class="form-control small-input"></span>';
                $html.='<span class="bl"><input type="text" name="'.$alias.'dcenaed[]"  id="dcenaed'.$i.'" class="form-control small"></span>';
             }   
            $html.=Html::a(Html::img('/images/del.png',['class'=>'delaction']),'javascript:void(0);',['class'=>'abtn','data-id'=>$i]).'</div>';
            echo $html;
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }         

    }
    
   /** Функция возвращает селекты в табах при редактировании ***/ 
    protected  function loadTags($id,$ind,$mremont_id)
    {
        /** $id - id записи вагона
         ** $ind - идентификатор к какой таблице обращаться
         ** $mremont_id - место ремонта чтобы получить только его наименования 
         */ 
         
         
        if (Yii::$app->user->can('author')) // prichina,detail,
        { 
                $i=rand(100,10000);

               if ($ind=='Tordrkr') 
               {
                   $model = Tordrkr::findAll(['vagon_id'=>$id]); 
                   $naim='Тор/др/кр:';
                   $ind_id=1;
               }
               elseif ($ind=='Spravka_2612') 
               {
                   $model = Prichina::findAll(['vagon_id'=>$id,'dr_id'=>null]); 
                   $naim='Причины брака:';
                   $ind_id=2;
               }
               elseif ($ind=='Detal'){
                 $model = Detail::findAll(['vagon_id'=>$id]);
                 $naim='Детали:';
                 $ind_id=3;
                 /** Массив цен **/  
               }
               elseif($ind=='Reviziya'){
                 $model = Revizia::findAll(['vagon_id'=>$id]);
                 $naim='Ревизия:';
                 $ind_id=4;  
               }
               elseif($ind=='Promyvka'){
                 $model = Promiv::findAll(['vagon_id'=>$id]);
                 $naim='Промывка:';
                 $ind_id=6;   
               }
               elseif($ind=='ZHd_tarif'){
                 $model = Zhd::findAll(['vagon_id'=>$id]); 
                 $naim='Жд тариф:';
                 $ind_id=7;   
               }
               elseif($ind=='Podacha_i_uborka'){
                 $model = Clean::findAll(['vagon_id'=>$id]);
                 $naim='Уборка:';
                 $ind_id=5;   
               }
               elseif($ind=='Uslugi'){
                 $model = Usluga::findAll(['vagon_id'=>$id]);
                 $naim='Услуга:';
                 $ind_id=8;  
               }

                    
               $html='';
               // если массив вернул сттроки
               if (count($model)>0)               
               {
                   
                            foreach($model as $val)
                            {
                                // получаем все записи справочника ПРАЙС относящиеся к месту ремонта и типу наименований
                                   if ($ind=='Spravka_2612')
                                   {
                                    $m = Price::findAll(['pricetip'=>$val->pricetip_id]);
                                   }
                                   else
                                   {
                                    $m = Price::findAll(['mremont_id'=>$mremont_id,'pricetip'=>$val->pricetip_id]);
                                   }

                                   if (count($m)==0)
                                   {$m = Price::findAll(['mremont_id'=>null]); } 

                                    $i=rand(100,10000);
                                    
                                   if ($ind=='Tordrkr')
                                   {
                                      $select=$val->remont_id; // получаем запись которая будет selected
                                   }
                                   elseif ($ind=='Spravka_2612')
                                   {
                                      $select=$val->spr_id; // получаем запись которая будет selected
                                   }
                                   if ($ind=='Detal')
                                   {
                                      $select=$val->detailspr_id; // получаем запись которая будет selected
                                   }
                                   elseif ($ind=='Reviziya')
                                   {
                                      $select=$val->reviziaspr_id; // получаем запись которая будет selected
                                   }
                                   elseif($ind=='Promyvka'){
                                      $select=$val->promivspr_id; // получаем запись которая будет selected
                                   }
                                   elseif($ind=='ZHd_tarif'){
                                      $select=$val->zhdspr_id; // получаем запись которая будет selected
                                   }
                                   elseif($ind=='Podacha_i_uborka'){
                                      $select=$val->cleanspr_id; // получаем запись которая будет selected
                                   }
                                   elseif($ind=='Uslugi'){
                                      $select=$val->uslugaspr_id; // получаем запись которая будет selected
                                   }
                                   
                                   
                                  if ($ind!='Spravka_2612')
                                  {  
                                       foreach($m as $key=>$item)
                                       {  
                                        // если id в справочнике = id выбранной причины то selected
                                               if ($item->id==$select)   
                                               {
                                                  $mas[$key]='<option value="'.$item->id.'" data-cena="'.$item->cena.'"  data-random="'.$i.'"  selected="selected">'.$item->titlename.'</option>'; 
                                               }
                                               else
                                               {
                                                  $mas[$key]='<option value="'.$item->id.'" data-cena="'.$item->cena.'" data-random="'.$i.'" >'.$item->titlename.'</option>'; 
                                               }
                                       }    
                                   }
                                   else
                                   {
                                        foreach($m as $key=>$item)
                                        {  // если id в справочнике = id выбранной причины то selected
                                           if ($val->spr_id!='') // так как если был указан ДР или КР, то spr_id будет пуст
                                           {
                                               if ($item->id==$select)   
                                               {
                                                  $mas[$key]='<option value="'.$item->id.'" data-random="'.$i.'"  selected="selected" >'.$item->code.' '.$item->titlename.'</option>'; 
                                               }
                                               else
                                               {
                                                  $mas[$key]='<option value="'.$item->id.'"  data-random="'.$i.'" >'.$item->code.' '.$item->titlename.'</option>'; 
                                               }
                                           }    
                                        }    
                                   }   

                                    $html.='<div class="oneitem form-group'.$i.'">';
                                    $html.=Html::input('text',$ind.'tip_id[]',$ind_id,['style'=>'display:none']);
                                    $html.=Html::checkbox('chk'.$ind.'[]',true,['value'=>$val->id,'style'=>"display:none"]);
                                    $html.='<select name="'.$ind.'[]"  id="'.$ind.$i.'" style="display:none" data-id="ins" data-class="medium2">';
                                    $html.='<option value >- Выберите значение -</option>';
                                         for($j=0;$j<count($mas);$j++)
                                         {
                                            $html.=$mas[$j];
                                         }
                                    $html.='</select>';
                                    if ($ind!='Spravka_2612'){
                                    $html.='<span class="bl"><input type="text" name="'.$ind.'kol[]" value="'.$val->kol.'" id="dkol'.$i.'" class="form-control small-input"></span>';
                                    $html.='<span class="bl"><input type="text" name="'.$ind.'dcenaed[]" value="'.$val->cenaed.'"   id="dcenaed'.$i.'" class="form-control small"></span>';
                                    }
                                    $html.=Html::a(Html::img('/images/del.png',['class'=>'delaction']),'javascript:void(0);',['class'=>'abtn','data-id'=>$i]).'</div>';
                            }
                   
               }  
           
           return $html;
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }       
    } 
    

    protected function Checktabs($identifikator,$old,$new,$vagonid,$select,$tip,$mremont_id,$remont_id,$vvagon_id,$sobstvenik_id,$dates,$cenaed,$kol,$pricetip_id)
    {
       if($identifikator=='Tordrkr') { $tbl='tordrkr'; $p1='vagon_id'; $p2='remont_id';$p3='tip';$p4='mremont_id';$p6='vvagon_id';$p7='sobstvenik_id';$p8='dates';$p9='cenaed';$p10='kol';$p11='pricetip_id';}
       elseif ( $identifikator=='Spravka_2612') { $tbl='prichina'; $p1='vagon_id'; $p2='spr_id';$p3='tip';$p4='mremont_id';$p5='remont_id';$p6='vvagon_id';$p7='sobstvenik_id';$p8='dates';$p9='cenaed';$p11='pricetip_id'; }
       elseif($identifikator=='Detal') { $tbl='detail'; $p1='vagon_id'; $p2='detailspr_id'; $p3='tip';$p4='mremont_id';$p5='remont_id';$p6='vvagon_id';$p7='sobstvenik_id';$p8='dates';$p9='cenaed';$p10='kol';$p11='pricetip_id';}
       elseif($identifikator=='Reviziya') { $tbl='revizia'; $p1='vagon_id'; $p2='reviziaspr_id'; $p3='tip';$p4='mremont_id';$p5='remont_id';$p6='vvagon_id';$p7='sobstvenik_id';$p8='dates';$p9='cenaed';$p10='kol';$p11='pricetip_id';}
       elseif($identifikator=='Promyvka') { $tbl='promiv'; $p1='vagon_id'; $p2='promivspr_id'; $p3='tip';$p4='mremont_id';$p5='remont_id';$p6='vvagon_id';$p7='sobstvenik_id';$p8='dates';$p9='cenaed';$p10='kol';$p11='pricetip_id';}
       elseif($identifikator=='ZHd_tarif') { $tbl='zhd'; $p1='vagon_id'; $p2='zhdspr_id'; $p3='tip';$p4='mremont_id';$p5='remont_id';$p6='vvagon_id';$p7='sobstvenik_id';$p8='dates';$p9='cenaed';$p10='kol';$p11='pricetip_id';}
       elseif($identifikator=='Podacha_i_uborka') { $tbl='clean'; $p1='vagon_id'; $p2='cleanspr_id'; $p3='tip';$p4='mremont_id';$p5='remont_id';$p6='vvagon_id';$p7='sobstvenik_id';$p8='dates';$p9='cenaed';$p10='kol';$p11='pricetip_id';}
       elseif($identifikator=='Uslugi') { $tbl='usluga'; $p1='vagon_id'; $p2='uslugaspr_id'; $p3='tip';$p4='mremont_id';$p5='remont_id';$p6='vvagon_id';$p7='sobstvenik_id';$p8='dates';$p9='cenaed';$p10='kol';$p11='pricetip_id';}
                     
            if ($new==null) {$new=[];}
            if ($old==null) {$old=[];}

        if (count($old)>=count($new) ) 
        {
            for ($i=0;$i<count($old);$i++) 
            {
            	   if ( !in_array($old[$i],$new) ) //нет старого id в новом массиве
            	   {
            	      // Удаляем 
                          Yii::$app->db->createCommand("DELETE FROM ".$tbl." WHERE id = :id")
                                            ->bindValue(':id', $old[$i]) 
                                            ->execute();
            	   }
            	   else //есть старый id в новом массиве
            	   {
                      // Обновляем запись 
                       $index=array_search($old[$i], $new); // получаем индекс элемента в новом массиве
                       if ($tbl!='prichina') {
                             if ($remont_id!=''){
                                 $query=Yii::$app->db->createCommand("UPDATE ".$tbl." SET ".$p1."=:p1,".$p2."=:p2,".$p3."=:p3,".$p4."=:p4,".$p5."=:p5,".$p6."=:p6,".$p7."=:p7,".$p8."=:p8,".$p9."=:p9,".$p10."=:p10  WHERE id=:id");
                               }
                               else
                               {
                                 $query=Yii::$app->db->createCommand("UPDATE ".$tbl." SET ".$p1."=:p1,".$p2."=:p2,".$p3."=:p3,".$p4."=:p4,".$p6."=:p6,".$p7."=:p7,".$p8."=:p8,".$p9."=:p9,".$p10."=:p10 WHERE id=:id");
                               } 
                                 $query->bindValue(':p1', $vagonid); 
                                 $query->bindValue(':p2', $select[$index]); 
                                 $query->bindValue(':p3', $tip); 
                                 $query->bindValue(':p4', $mremont_id); 
                                 if ($remont_id!='') {$query->bindValue(':p5', $remont_id); }
                                 $query->bindValue(':p6', $vvagon_id); 
                                 $query->bindValue(':p7', $sobstvenik_id);
                                 $query->bindValue(':p8', $dates);
                                if ($tbl!='tordrkr')
                                {
                                 $query->bindValue(':p9', $cenaed[$index]);
                                 $query->bindValue(':p10',$kol[$index]);                                  
                                }
                                else
                                {
                                 $query->bindValue(':p9', $cenaed);
                                 $query->bindValue(':p10',$kol);                                  
                                } 
                                 $query->bindValue(':id', $old[$i]); 
                                 $query->execute();
                        } 
                       else
                        { 
                             if ($remont_id!=''){
                               $query=Yii::$app->db->createCommand("UPDATE ".$tbl." SET ".$p1."=:p1,".$p2."=:p2,".$p3."=:p3,".$p4."=:p4,".$p5."=:p5,".$p6."=:p6,".$p7."=:p7,".$p8."=:p8,".$p9."=:p9 WHERE id=:id");
                               }
                               else
                               {
                               $query=Yii::$app->db->createCommand("UPDATE ".$tbl." SET ".$p1."=:p1,".$p2."=:p2,".$p3."=:p3,".$p4."=:p4,".$p6."=:p6,".$p7."=:p7,".$p8."=:p8,".$p9."=:p9  WHERE id=:id");
                               } 
                                 $query->bindValue(':p1', $vagonid); 
                                 $query->bindValue(':p2', $select[$index]);
                                 $query->bindValue(':p3', $tip);
                                 $query->bindValue(':p4', $mremont_id); 
                                  if ($remont_id!='') {$query->bindValue(':p5', $remont_id); }
                                 $query->bindValue(':p6', $vvagon_id); 
                                 $query->bindValue(':p7', $sobstvenik_id);
                                 $query->bindValue(':p8', $dates);
                                 $query->bindValue(':p9', $cenaed);
                                 $query->bindValue(':id', $old[$i]); 
                                 $query->execute();
                        }
            	   }
            	   
            	  if(isset($new[$i]))
                  { 
                       if (!in_array($new[$i],$old))  //нет нового id в старом массиве
                	   {
                	     // Добавляем запись 
                               if ($tbl!='prichina') {
                                    if ($tbl!='tordrkr')
                                    {
                                        Yii::$app->db->createCommand()->batchInsert($tbl, [$p1, $p2, $p3, $p4, $p5, $p6, $p7, $p8, $p9, $p10,$p11], 
                                        [ [ $vagonid,$select[$i],$tip,$mremont_id,$remont_id,$vvagon_id,$sobstvenik_id,$dates,$cenaed[$i],$kol[$i],$pricetip_id[0]]  ])->execute();            
                                    }
                                    else
                                    {
                                        Yii::$app->db->createCommand()->batchInsert($tbl, [$p1, $p2, $p3, $p4, $p6, $p7, $p8, $p9, $p10,$p11], 
                                        [ [ $vagonid,$select[$i],$tip,$mremont_id,$vvagon_id,$sobstvenik_id,$dates,$cenaed[$i],$kol[$i],$pricetip_id[0]]  ])->execute();            
                                    }
                    	       }
                               else
                               {
                                    Yii::$app->db->createCommand()->batchInsert($tbl, [$p1, $p2, $p3, $p4, $p5, $p6, $p7, $p8, $p9,$p11], 
                                    [ [ $vagonid,$select[$i],$tip,$mremont_id,$remont_id,$vvagon_id,$sobstvenik_id,$dates,$cenaed,$pricetip_id[0] ] ])->execute();            
                               }
                       }
                  }    
        	}
        }
        else
        {
            for ($i=0;$i<count($new);$i++) 
            {
                   if ( !in_array($new[$i],$old)  ) //нет нового id в старом массиве
            	   {
            	      /** Вставляем **/
                               if ($tbl!='prichina') 
                               {
                                    if ($tbl!='tordrkr')
                                    {
                                        Yii::$app->db->createCommand()->batchInsert($tbl, [$p1, $p2, $p3, $p4, $p5, $p6, $p7, $p8, $p9, $p10,$p11], 
                                        [ [ $vagonid,$select[$i],$tip,$mremont_id,$remont_id,$vvagon_id,$sobstvenik_id,$dates,$cenaed[$i],$kol[$i],$pricetip_id[0]]  ])->execute();            
                                    }
                                    else
                                    {
                                        Yii::$app->db->createCommand()->batchInsert($tbl, [$p1, $p2, $p3, $p4, $p6, $p7, $p8, $p9, $p10,$p11], 
                                        [ [ $vagonid,$select[$i],$tip,$mremont_id,$vvagon_id,$sobstvenik_id,$dates,$cenaed[$i],$kol[$i],$pricetip_id[0]]  ])->execute();            
                                    }
                    	       }
                               else
                               {
                                    Yii::$app->db->createCommand()->batchInsert($tbl, [$p1, $p2, $p3, $p4, $p5, $p6, $p7, $p8, $p9,$p11], 
                                    [ [ $vagonid,$select[$i],$tip,$mremont_id,$remont_id,$vvagon_id,$sobstvenik_id,$dates,$cenaed,$pricetip_id[0] ] ])->execute();            
                               }
            	   }
            	   else //есть новый id в старом массиве
            	   {
            	      /** Обновляем **/
                       if ($tbl!='prichina') {
                            $query=Yii::$app->db->createCommand("UPDATE ".$tbl." SET ".$p1."=:p1,".$p2."=:p2,".$p3."=:p3,".$p4."=:p4,".$p5."=:p5,".$p6."=:p6,".$p7."=:p7,".$p8."=:p8,".$p9."=:p9,".$p10."=:p10  WHERE id=:id");
                                 $query->bindValue(':p1', $vagonid); 
                                 $query->bindValue(':p2', $select[$i]); 
                                 $query->bindValue(':p3', $tip); 
                                 $query->bindValue(':p4', $mremont_id); 
                                 $query->bindValue(':p5', $remont_id); 
                                 $query->bindValue(':p6', $vvagon_id); 
                                 $query->bindValue(':p7', $sobstvenik_id);
                                 $query->bindValue(':p8', $dates);
                                
                                if ($tbl!='tordrkr')
                                {
                                 $query->bindValue(':p9', $cenaed[$i]);
                                 $query->bindValue(':p10',$kol[$i]);                                  
                                }
                                else
                                {
                                 $query->bindValue(':p9', $cenaed);
                                 $query->bindValue(':p10',$kol);                                  
                                } 
                                $query->bindValue(':id', $old[$i]); 
                                 $query->execute();
                       } 
                       else
                        { 
                           Yii::$app->db->createCommand("UPDATE ".$tbl." SET ".$p1."=:p1,".$p2."=:p2,".$p3."=:p3,".$p4."=:p4,".$p5."=:p5,".$p6."=:p6,".$p7."=:p7,".$p8."=:p8,".$p9."=:p9  WHERE id=:id")
                                 ->bindValue(':p1', $vagonid) 
                                 ->bindValue(':p2', $select[$i]) 
                                 ->bindValue(':p3', $tip) 
                                 ->bindValue(':p4', $mremont_id) 
                                 ->bindValue(':p5', $remont_id) 
                                 ->bindValue(':p6', $vvagon_id) 
                                 ->bindValue(':p7', $sobstvenik_id)
                                 ->bindValue(':p8', $dates)
                                 ->bindValue(':p9', $cenaed)
                                 ->bindValue(':id', $old[$i]) 
                                 ->execute();
                       }
            	   }
            	   
            	   
                 if(isset($old[$i]))
                 {  
                       if (!in_array($old[$i],$new))  ///нет старого id в новом массиве
                	   {
                	     /** Удаляем **/
                              Yii::$app->db->createCommand("DELETE FROM ".$tbl." WHERE id = :id")
                                                ->bindValue(':id', $old[$i]) 
                                                ->execute();
                	   }
                 } 
            }	
        
        }        
        
        
    }
    
    
    public  function declOfNum($number, $titles)
    {
        $cases = array (2, 0, 1, 1, 1, 2);
        return $number." ".$titles[ ($number%100 > 4 && $number %100 < 20) ? 2 : $cases[min($number%10, 5)] ];
    }       
     
    public  function tips($id)
    {
        if ($id==1)
        { 
           return 'ТОР';  
        }
        elseif($id==2)
        {
           return 'ДР';    
        }
        elseif($id==3)
        {
           return 'КР';
        }
    }     
    
    
    public function getAlias($input)
    {
        $replace=["Є"=>"YE","І"=>"I","Ѓ"=>"G","і"=>"i","№"=>"-","є"=>"ye","г"=>"g","А"=>"A",
                  "Б"=>"B","В"=>"V","Г"=>"G","Д"=>"D","Е"=>"E","Ё"=>"YO","Ж"=>"ZH","З"=>"Z",
                  "И"=>"I","Й"=>"J","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N","О"=>"O","П"=>"P",
                  "Р"=>"R","С"=>"S","Т"=>"T","У"=>"U","Ф"=>"F","Х"=>"X","Ц"=>"C","Ч"=>"CH",
                  "Ш"=>"SH","Щ"=>"SHH","Ъ"=>"'","Ы"=>"Y","Ь"=>"","Э"=>"E","Ю"=>"YU","Я"=>"YA",
                  "а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"yo","ж"=>"zh",
                  "з"=>"z","и"=>"i","й"=>"j","к"=>"k","л"=>"l","м"=>"m","н"=>"n","о"=>"o","п"=>"p",
                  "р"=>"r","с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"x","ц"=>"c","ч"=>"ch","ш"=>"sh",
                  "щ"=>"shh","ъ"=>"","ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya"," "=>"_","—"=>"_",
                  ","=>"_","!"=>"_","@"=>"_","#"=>"-","$"=>"","%"=>"","^"=>"","&"=>"","*"=>"","("=>"",")"=>"",
                  "+"=>"","="=>"",";"=>"",":"=>"","'"=>"",'"'=>"","~"=>"","`"=>"","?"=>"",
                  "/"=>"","\\"=>"","["=>"","]"=>"","{"=>"","}"=>"","|"=>"","."=>""        
                 ];
        
        return strtr($input, $replace);
    }
    

}
