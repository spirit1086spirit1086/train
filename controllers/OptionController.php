<?php

namespace app\controllers;

use Yii;
use app\models\Option;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OptionController implements the CRUD actions for Option model.
 */
class OptionController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Option models.
     * @return mixed
     */
    public function actionIndex()
    {
        $opt= Option::findOne(['tip'=>1]);
        
        if (empty($opt))
        {
            $model = new Option();
            if ($model->load(Yii::$app->request->post()) && $model->save()) 
            {
               return $this->redirect(['index']);
            } else {
               return $this->render('index', ['model' => $model]);
            }   
        }
        else
        {
            $model = $this->findModel($opt->id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) 
            {
                return $this->render('index', ['model' => $model]);
            } else {
                return $this->render('index', ['model' => $model]);
            }
            
        }    
    }

     
    /**
     * Finds the Option model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Option the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Option::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
