<?php

namespace app\controllers;

use Yii;
use app\models\AuthItem;
use app\models\AuthitemSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * AuthitemController implements the CRUD actions for AuthItem model.
 */
class AuthitemController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AuthItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->can('superadmin')) 
        { 
                $searchModel = new AuthitemSearch();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $dataProvider->pagination->pageSizeParam = false;

                return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]);
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }        
    }

    /**
     * Displays a single AuthItem model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->user->can('superadmin')) 
        { 
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }        

    }

    /**
     * Creates a new AuthItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
     
        if (Yii::$app->user->can('superadmin')) 
        { 
            $model = new AuthItem();
    
            if ($model->load(Yii::$app->request->post())) 
            {   
                $model->type=2;
                $model->created_at=date('Ymd');
                
                ($model->rule_name==null) ?  $model->rule_name=null : $model->rule_name='isAuthor';   
                
                if ($model->save())
                {
                   return $this->redirect(['index']);
                }
                else
                {
                  return $this->render('create', ['model' => $model]);
                }
            } 
            else 
            {
                return $this->render('create', ['model' => $model]);
            }
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }     
    }

    /**
     * Updates an existing AuthItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        if (Yii::$app->user->can('superadmin')) 
        { 
            $model = $this->findModel($id);
            if ($model->load(Yii::$app->request->post())) 
            {
                $model->updated_at=date('Ymd');
                ($model->rule_name==null) ?  $model->rule_name=null : $model->rule_name='isAuthor';                   
                
                if ($model->save())
                {
                   return $this->redirect(['index']);
                }
                else
                {
                    return $this->render('update', ['model' => $model]);
                }
    
            } 
            else 
            {
                return $this->render('update', ['model' => $model]);
            }
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }        

    }

    /**
     * Deletes an existing AuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        if (Yii::$app->user->can('superadmin')) 
        { 
            $ids= explode(',',$id);
            
            for($i=0;$i<count($ids);$i++)
            {
              $this->findModel($ids[$i])->delete();
            }
            return $this->redirect(['index']);
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }        

    }

    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AuthItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (Yii::$app->user->can('superadmin')) 
        { 
    
            if (($model = AuthItem::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }        
            
    }
    
    
    
}
