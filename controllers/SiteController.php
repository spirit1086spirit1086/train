<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\RestoreForm;
use app\models\User;
use app\models\Option;
use app\models\Vagon;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


     public function actionCreatewidget()
     {
            return $this->render('menuwidget');
            return $this->render('valutawidget');
     }


    public function actionIndex()
    {
        $model = new LoginForm();
        $vagon= new Vagon();
        $opt= Option::findOne(['tip'=>1]);
        $last=(new \yii\db\Query)->select('*')->from('vagon')->orderBy(['dates'=>SORT_DESC])->limit(5)->all();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('index', ['model' => $model,'option'=>$opt,'last'=>$last,'vagon'=>$vagon]);
        }
    }

    public function actionLogout()
    {
         date_default_timezone_set('Asia/Almaty'); 
         Yii::$app->db->createCommand("UPDATE users SET lastlogin=:lastlogin WHERE id=:id")->bindValues([':lastlogin'=>date('Y-m-d H:i:s'),':id'=>Yii::$app->user->id]) ->execute();
        Yii::$app->user->logout();
        return $this->goHome();
    }
    
    
    public function actionRestore()
    {
        $model = new RestoreForm();
        if ($model->load(Yii::$app->request->post()) && $model->Restoremail()) 
        {
            $session = Yii::$app->session->setFlash('sending email', 'Пароль отправлен на ваш E-mail.');
                Yii::$app->mailer->compose()
                     ->setFrom('spirit1086@mail.ru')
                     ->setTo($model->email)
                     ->setSubject('Восстановление пароля')
                     ->setTextBody('Пароль')
                     ->send();  
            return $this->refresh();                               
        }
        else
        {
          return $this->render('restore', ['model' => $model]);
        }

    }
    
    public function actionHiddenmenu()
    {
        Yii::$app->session->setFlash('hiddenmenu', $_POST['perem']);
        $_SESSION['hiddenmenu']=$_POST['perem'];
    }
    
    public function actionStatus()
    {
        if ($_POST['tip']=='user')
        {
            Yii::$app->db->createCommand("UPDATE users SET active=:active WHERE id=:id")
                             ->bindValue(':active', $_POST['val']) 
                             ->bindValue(':id', $_POST['id']) 
                             ->execute(); 
        }    
        elseif ($_POST['tip']=='posrednik') 
        {
           Yii::$app->db->createCommand("UPDATE posrednik SET status=:status WHERE id=:id")
                             ->bindValue(':status', $_POST['val']) 
                             ->bindValue(':id', $_POST['id']) 
                             ->execute();   
        }   
        elseif ($_POST['tip']=='company') 
        {
           Yii::$app->db->createCommand("UPDATE company SET status=:status WHERE id=:id")
                             ->bindValue(':status', $_POST['val']) 
                             ->bindValue(':id', $_POST['id']) 
                             ->execute();   
        }   
        elseif ($_POST['tip']=='tsho') 
        {
           Yii::$app->db->createCommand("UPDATE tsho SET status=:status WHERE id=:id")
                             ->bindValue(':status', $_POST['val']) 
                             ->bindValue(':id', $_POST['id']) 
                             ->execute();   
        } 
        elseif ($_POST['tip']=='mremont') 
        {
           Yii::$app->db->createCommand("UPDATE mremont SET status=:status WHERE id=:id")
                             ->bindValue(':status', $_POST['val']) 
                             ->bindValue(':id', $_POST['id']) 
                             ->execute();   
        }
        elseif ($_POST['tip']=='sobstvenik') 
        {
           Yii::$app->db->createCommand("UPDATE sobstvenik SET status=:status WHERE id=:id")
                             ->bindValue(':status', $_POST['val']) 
                             ->bindValue(':id', $_POST['id']) 
                             ->execute();   
        } 
        elseif ($_POST['tip']=='vvagon') 
        {
           Yii::$app->db->createCommand("UPDATE vvagon SET status=:status WHERE id=:id")
                             ->bindValue(':status', $_POST['val']) 
                             ->bindValue(':id', $_POST['id']) 
                             ->execute();   
        }
        elseif ($_POST['tip']=='vc') 
        {
           Yii::$app->db->createCommand("UPDATE vc SET status=:status WHERE id=:id")
                             ->bindValue(':status', $_POST['val']) 
                             ->bindValue(':id', $_POST['id']) 
                             ->execute();   
        }
        elseif ($_POST['tip']=='price') 
        {
           Yii::$app->db->createCommand("UPDATE price SET status=:status WHERE id=:id")
                             ->bindValue(':status', $_POST['val']) 
                             ->bindValue(':id', $_POST['id']) 
                             ->execute();   
        }
        elseif ($_POST['tip']=='tips') 
        {
           Yii::$app->db->createCommand("UPDATE tips SET status=:status WHERE id=:id")
                             ->bindValue(':status', $_POST['val']) 
                             ->bindValue(':id', $_POST['id']) 
                             ->execute();   
        }
    }   
    
    public function Colschet($p) 
    {
       if ($p=='all') 
       {
           $alls=(new \yii\db\Query)->select('*')->from('vagon')->all();
       }
       elseif($p=='today')
       {
           $alls=(new \yii\db\Query)->select('*')->from('vagon')->where('dates='.date('Y-m-d'))->all(); 
       }   
       
       return count($alls);
    }
    
    public function Lastlogin($uid) 
    {
        $user=User::findOne($uid);
        return date('d-m-Y H:i:s', strtotime($user->lastlogin));
    }
    
    
    
    public function Day($perem)
    {
       $day=  substr($perem, 0,4).'-'.substr($perem, 5,2).'-'.substr($perem, 8,2).'T00:00:01.000-06:00';

       return $day;
    }
    
    
    
    public function actionGraph() 
    {
        $index=[];
        $mas=(new \yii\db\Query)->select('*')->from('vagon')->orderBy('dates')->all();
        foreach ($mas as $value) 
        {
            if (!in_array($value['dates'], $index))
            {
                $index[]=$value['dates'];
                $key=  array_search($value['dates'], $index);
            }
            else
            {
                $key=  array_search($value['dates'], $index);
            }   
            
            $tmp[$key][]=$value['dates'];
        }
        
        unset($index);
        // считаем кол-во совпадений
        for ($i=0;$i<count($tmp);$i++)
        {
            $kol=0;
            for ($j=0;$j<count($tmp[$i]);$j++)
            {
                $kol+=1;
                $dates=$this->Day($tmp[$i][$j]);
            }
            
            $index[]=[$dates,$kol];
        }
        
         echo json_encode($index);
    }
    
}
