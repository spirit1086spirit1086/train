<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\UserSearch;
use app\models\Groups;
use app\models\AuthAssignment;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\rbac\ManagerInterface;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }



    /**
     * Lists all User models.  if ( Yii::$app->user->can('update-post', ['model' => $model])) 
     * @return mixed
     */
    public function actionIndex()
    {
       if (Yii::$app->user->can('administrator')) 
       {
            $searchModel = new UserSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->pagination->pageSizeParam = false;    
        
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
       }
       else
       {
              throw new HttpException(403,'Доступ закрыт');
       }   
            
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
         $model = $this->findModel($id); // пишем так чтобы получить массив записи и проверить я Автор записи или нет
       if ( Yii::$app->user->can('update-user', ['model' => $model]))  
       {
            return $this->render('view', [
                'model' => $model,
            ]);
       }
       else
       {
              throw new HttpException(403,'Доступ закрыт');
       }   
            
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
       if (Yii::$app->user->can('create-user')) 
       {
                $model = new User();
                $model->scenario = 'register';
                  
                if ($model->load(Yii::$app->request->post()) ) 
                {    
                     date_default_timezone_set('Asia/Almaty');
                     $model->createdBy=Yii::$app->user->id;
                     if ($model->save())
                     {   
                        //добавление роли при создании записи
                        $auth = Yii::$app->authManager;
                        $role=$this->findAliasgroup($model->group_id);
                        $authorRole = $auth->getRole($role->alias);
                        $auth->assign($authorRole, $model->getId());  
                        return $this->redirect(['index']);
                     }
                     else
                     {
                        return $this->render('create', ['model' => $model]);
                     }   
                }
                else 
                {
                    return $this->render('create', ['model' => $model]);
                }
       } 
       else
       {
        throw new HttpException(403,'Доступ закрыт');
       }
        
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
           $model = $this->findModel($id);

       if ( Yii::$app->user->can('update-user', ['model' => $model]))  
       {
           $old_group=$model->group_id;
             if ($model->load(Yii::$app->request->post()) && $model->save()) 
             { 
                        //добавление роли при обновлении записи
                        $auth = Yii::$app->authManager;
                        /**
                         *  если изменено поле группы то удаляем из таблицы assignment пользователя 
                         *  так как будет записан тот же пользователь, но с другой ролью, иначе будет записан
                         *  один пользователь с двумя видами ролей 
                        **/
                        if ($old_group!=$model->group_id) 
                        {
                            $this->findAssign($id)->delete();  
                            $role=$this->findAliasgroup($model->group_id);
                            $authorRole = $auth->getRole($role->alias);
                            $auth->assign($authorRole, $model->getId());  
                        }
                        
                   return $this->redirect(['index']);
             } else {
                  return $this->render('update', ['model' => $model]);
             }
       }
       else
       {
              throw new HttpException(403,'Доступ закрыт');
       }   
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
       $model = $this->findModel($id);// пишем так чтобы получить массив записи и проверить я Автор записи или нет
        
       if ( Yii::$app->user->can('delete-user', ['model' => $model]))  
       {
            $ids= explode(',',$id);
            
            for($i=0;$i<count($ids);$i++)
            {
                $this->findAssign($ids[$i])->delete();  
                $this->findModel($ids[$i])->delete();
            }
            return $this->redirect(['index']);
       }
       else
       {
              throw new HttpException(403,'Доступ закрыт');
       }   
            
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
       if (Yii::$app->user->can('administrator')) 
       {
            if (($model = User::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
       }
       else
       {
              throw new HttpException(403,'Доступ закрыт');
       }   
            
    }
    

    protected function findAliasgroup($id)
    {
       if (Yii::$app->user->can('administrator')) 
       {
            if (($model = Groups::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
       }
       else
       {
              throw new HttpException(403,'Доступ закрыт');
       }   
            
    }


    protected function findAssign($id)
    {
       if (Yii::$app->user->can('administrator')) 
       {
            if (($model = AuthAssignment::findOne(['user_id' => $id])) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
       }
       else
       {
              throw new HttpException(403,'Доступ закрыт');
       }   
            
    }
    
    
    
}