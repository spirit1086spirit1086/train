<?php

namespace app\controllers;
use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Vagon;
use app\models\VagonSearch;
use app\models\Mremont;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

class SchetController extends  Controller
{
    public function actionIndex()
    {
        if ( Yii::$app->user->can('author') )
       {           
            $model= new Vagon;
            
            $report=[];
            
             if ( Yii::$app->request->post() ) /**  ТОР 1  | ДР 2 | КР 3 **/ 
             {
               
                 if ($_POST['firm']!='all') // получить данные одной фирмы или всех
                 {    
                        $tor=$this->Tor_dr(1, $_POST['firm'], $model, $_POST['date1'], $_POST['date2'],null);  
                        $dr=$this->Tor_dr(2, $_POST['firm'], $model, $_POST['date1'], $_POST['date2'],null);  
                        $teh=$this->Pr_ubr_zhd('teh', $_POST['firm'], $model, $_POST['date1'], $_POST['date2'],null);
                        
                        $all=[$tor,$dr,$teh];

                        $posrednik=$this->Posrednik('posrednik', $_POST['firm'], $model, $_POST['date1'], $_POST['date2'],null);

                        for($i=0;$i<count($posrednik);$i++)
                        {
                          $all[]=$posrednik[$i];  
                        }
                        
                        for ($i=0;$i<count($all);$i++)
                        {
                           if (!empty($all[$i]))
                           { 
                             $report[]=$all[$i];
                           } 
                        }
                 }
                 else 
                 {
                     // получаем список фирм
                     $firms = (new \yii\db\Query())->select('*')->from('mremont')->all();
                     foreach($firms as $key=>$item)
                     {
                        $tor=$this->Tor_dr(1, $item['id'], $model, $_POST['date1'], $_POST['date2'],null);  
                        $dr=$this->Tor_dr(2, $item['id'], $model, $_POST['date1'], $_POST['date2'],null);  
                        $teh=$this->Pr_ubr_zhd('teh', $item['id'], $model, $_POST['date1'], $_POST['date2'],null);
                        $posrednik=$this->Posrednik('posrednik', $item['id'], $model, $_POST['date1'], $_POST['date2'],null);
                      
                        $all[$key]=[$tor,$dr,$teh];
                        
                        for($i=0;$i<count($posrednik);$i++)
                        {
                            array_push($all[$key], $posrednik[$i]);
                        }
                        
                     }    
                     
                     for ($i=0;$i<count($all);$i++)
                     {
                        for ($j=0;$j<count($all[$i]);$j++)
                        {
                           if (!empty($all[$i][$j]))
                           { 
                             $report[]=$all[$i][$j];
                           }
                        }   
                     }
                 }
                      
                return $this->render('index',['model'=>$model,'report'=>$report]);
             }
             else
             {
                return $this->render('index',['model'=>$model]);
             }
        }
        else
        {
            throw new HttpException(403,'Доступ закрыт');  
        }         
    }
    
    
    protected function ConstructTable($tip,$array,$identi)
    {
        $random=rand(100,10000);
        $dataProvider = new ArrayDataProvider(['allModels' => $array,'pagination' => ['pageSize' => '100000000']]);    
        if($tip==1)
        {    
            $label='"'.$array[0]['mremont_id'].'" '.$identi;
            $html=GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'layout'=>'{items}',
                                    'tableOptions' =>['class' => 'dop','id'=>$random],
                                    'rowOptions' => function ($allModels){
                                                        (isset($allModels['color'])) ? $color=$allModels['color'] : $color='';
                                                        return ['class'=>$color];
                                                        },
                                    'columns' =>[
                                                   [
                                                     'attribute' => 'nscheta',
                                                     'label'=>'Номер счета',
                                                     'value'=> function ($allModels)  {  (isset($allModels['nscheta']))  ? $nscheta=$allModels['nscheta'] : $nscheta='';
                                                                                            return $nscheta;
                                                                                      },   
                                                   ], 
                                                   [
                                                    'headerOptions'=>['class'=>'c'],
                                                    'contentOptions'=>['class'=>'c p6'],
                                                    'attribute' => 'dates',
                                                    'label'=>'Дата',
                                                    'value'=> function ($allModels)  {  (isset($allModels['dates']))  ? $dates=$allModels['dates'] : $dates='';
                                                                                            return $dates;
                                                                                      },      
                                                    ], 
                                                    [
                                                     'headerOptions'=>['class'=>'c'],
                                                     'contentOptions'=>['class'=>'c'],
                                                     'attribute' => 'mremont_id',
                                                     'label'=>'Место ремонта',  
                                                     'value'=> function ($allModels)  {  (isset($allModels['mremont_id']))  ? $mremont_id=$allModels['mremont_id'] : $mremont_id='';
                                                                                            return $mremont_id;
                                                                                      },         
                                                    ], 
                                                    [
                                                     'headerOptions'=>['class'=>'c'],
                                                     'contentOptions'=>['class'=>'c'],
                                                     'attribute' => 'nvagon_id',
                                                     'label'=>'Номер вагона',
                                                     'value'=> function ($allModels)  {  (isset($allModels['nvagon_id']))  ? $nvagon_id=$allModels['nvagon_id'] : $nvagon_id='';
                                                                                            return $nvagon_id;
                                                                                      },   
                                                    ], 
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'vvagona',
                                                      'label'=>'Вид вагона',
                                                      'value'=> function ($allModels)  {  (isset($allModels['vvagona']))  ? $vvagona=$allModels['vvagona'] : $vvagona='';
                                                                                            return $vvagona;
                                                                                      },  
                                                    ], 
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'sobstvenik_id',
                                                      'label'=>'Собственник',
                                                      'value'=> function ($allModels)  {  (isset($allModels['sobstvenik_id']))  ? $sobstvenik_id=$allModels['sobstvenik_id'] : $sobstvenik_id='';
                                                                                            return $sobstvenik_id;
                                                                                      },  
                                                    ], 
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'nkontr',
                                                      'label'=>'Номер контракта',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['nkontr']))  ? $nkontr=$allModels['nkontr'] : $nkontr='';
                                                                                            return $nkontr;
                                                                                       },    
                                                    ], 
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c p6'],
                                                      'attribute' => 'drab',
                                                      'label'=>'Дата работы',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['drab']))  ? $drab=$allModels['drab'] : $drab='';
                                                                                            return $drab;
                                                                                       },  
                                                    ],                     
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c p6'],
                                                      'attribute' => 'dbrak',
                                                      'label'=>'Дата заброковки',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['dbrak']))  ? $dbrak=$allModels['dbrak'] : $dbrak='';
                                                                                            return $dbrak;
                                                                                       },  
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'vcomp_id',
                                                      'label'=>'Выставление на компании',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['vcomp_id']))  ? $vcomp_id=$allModels['vcomp_id'] : $vcomp_id='';
                                                                                            return $vcomp_id;
                                                                                       },    
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'tsho_id',
                                                      'label'=>'Возмещение от ТШО',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['tsho_id']))  ? $tsho_id=$allModels['tsho_id'] : $tsho_id='';
                                                                                            return $tsho_id;
                                                                                       },      
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      //'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'spr_id',
                                                      'label'=>'Справка 2612',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['spr_id']))  ? $spr_id=$allModels['spr_id'] : $spr_id='';
                                                                                            return $spr_id;
                                                                                       },        
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      //'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'prbrak',
                                                      'label'=>'Причина брака',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['prbrak']))  ? $prbrak=$allModels['prbrak'] : $prbrak='';
                                                                                            return $prbrak;
                                                                                       },   
                                                      'format'=>'html'                                         
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'kol',
                                                      'label'=>'Кол-во',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['kol']))  ? $kol=$allModels['kol'] : $kol='';
                                                                                            return $kol;
                                                                                       },            
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'cenaed',
                                                      'label'=>'Цена за ед.',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['cenaed']))  ? $cenaed=$allModels['cenaed'] : $cenaed='';
                                                                                            return $cenaed;
                                                                                       },              
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'cena',
                                                      'label'=>'Цена',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['cena']))  ? $cena=$allModels['cena'] : $cena='';
                                                                                            return $cena;
                                                                                       },                
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'ndc',
                                                      'label'=>'Ндс',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['ndc']))  ? $ndc=$allModels['ndc'] : $ndc='';
                                                                                            return $ndc;
                                                                                       },                 
                                                    ], 
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'itog',
                                                      'label'=>'Итог',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['itog']))  ? $itog=$allModels['itog'] : $itog='';
                                                                                            return $itog;
                                                                                       },                
                                                    ],  
                                        
                                                ],
                                ]); 
             $mas=['label'=>$label,'content'=>$html];                                                                           
        }
        elseif($tip==2)
        {  
            $label='"'.$array[0]['mremont_id'].'" '.$identi;
            $html=GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'layout'=>'{items}',
                                    'tableOptions' =>['class' => 'dop','id'=>$random],
                                    'rowOptions' => function ($allModels){
                                                        (isset($allModels['color'])) ? $color=$allModels['color'] : $color='';
                                                        return ['class'=>$color];
                                                        },
                                    'columns' =>[
                                                   [
                                                     'attribute' => 'nscheta',
                                                     'label'=>'Номер счета',
                                                     'value'=> function ($allModels)  {  (isset($allModels['nscheta']))  ? $nscheta=$allModels['nscheta'] : $nscheta='';
                                                                                            return $nscheta;
                                                                                      },   
                                                   ], 
                                                   [
                                                    'headerOptions'=>['class'=>'c'],
                                                    'contentOptions'=>['class'=>'c p6'],
                                                    'attribute' => 'dates',
                                                    'label'=>'Дата',
                                                    'value'=> function ($allModels)  {  (isset($allModels['dates']))  ? $dates=$allModels['dates'] : $dates='';
                                                                                            return $dates;
                                                                                      },      
                                                    ], 
                                                    [
                                                     'headerOptions'=>['class'=>'c'],
                                                     'contentOptions'=>['class'=>'c'],
                                                     'attribute' => 'mremont_id',
                                                     'label'=>'Место ремонта',  
                                                     'value'=> function ($allModels)  {  (isset($allModels['mremont_id']))  ? $mremont_id=$allModels['mremont_id'] : $mremont_id='';
                                                                                            return $mremont_id;
                                                                                      },         
                                                    ], 
                                                    [
                                                     'headerOptions'=>['class'=>'c'],
                                                     'contentOptions'=>['class'=>'c'],
                                                     'attribute' => 'nvagon_id',
                                                     'label'=>'Номер вагона',
                                                     'value'=> function ($allModels)  {  (isset($allModels['nvagon_id']))  ? $nvagon_id=$allModels['nvagon_id'] : $nvagon_id='';
                                                                                            return $nvagon_id;
                                                                                      },   
                                                    ], 
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'vvagona',
                                                      'label'=>'Вид вагона',
                                                      'value'=> function ($allModels)  {  (isset($allModels['vvagona']))  ? $vvagona=$allModels['vvagona'] : $vvagona='';
                                                                                            return $vvagona;
                                                                                      },  
                                                    ], 
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'sobstvenik_id',
                                                      'label'=>'Собственник',
                                                      'value'=> function ($allModels)  {  (isset($allModels['sobstvenik_id']))  ? $sobstvenik_id=$allModels['sobstvenik_id'] : $sobstvenik_id='';
                                                                                            return $sobstvenik_id;
                                                                                      },  
                                                    ], 
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'nkontr',
                                                      'label'=>'Номер контракта',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['nkontr']))  ? $nkontr=$allModels['nkontr'] : $nkontr='';
                                                                                            return $nkontr;
                                                                                       },    
                                                    ], 
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      //'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'prbrak',
                                                      'label'=>'Причина брака',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['prbrak']))  ? $prbrak=$allModels['prbrak'] : $prbrak='';
                                                                                            return $prbrak;
                                                                                       },   
                                                      'format'=>'html'                                         
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'kol',
                                                      'label'=>'Кол-во',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['kol']))  ? $kol=$allModels['kol'] : $kol='';
                                                                                            return $kol;
                                                                                       },            
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'cenaed',
                                                      'label'=>'Цена за ед.',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['cenaed']))  ? $cenaed=$allModels['cenaed'] : $cenaed='';
                                                                                            return $cenaed;
                                                                                       },              
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'cena',
                                                      'label'=>'Цена',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['cena']))  ? $cena=$allModels['cena'] : $cena='';
                                                                                            return $cena;
                                                                                       },                
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'ndc',
                                                      'label'=>'Ндс',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['ndc']))  ? $ndc=$allModels['ndc'] : $ndc='';
                                                                                            return $ndc;
                                                                                       },                 
                                                    ], 
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'itog',
                                                      'label'=>'Итог',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['itog']))  ? $itog=$allModels['itog'] : $itog='';
                                                                                            return $itog;
                                                                                       },                
                                                    ],  
                                        
                                                ],
                                ]);  
             $mas=['label'=>$label,'content'=>$html];                                                                           
        }
        elseif($tip=='teh')
        {
            $label='"'.$array[0]['mremont_id'].'" '.$identi;
             $html=GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'layout'=>'{items}',
                                    'tableOptions' =>['class' => 'dop','id'=>$random],
                                    'rowOptions' => function ($allModels){
                                                        (isset($allModels['color'])) ? $color=$allModels['color'] : $color='';
                                                        return ['class'=>$color];
                                                        },
                                    'columns' =>[
                                                   [
                                                     'attribute' => 'nscheta',
                                                     'label'=>'Номер счета',
                                                     'value'=> function ($allModels)  {  (isset($allModels['nscheta']))  ? $nscheta=$allModels['nscheta'] : $nscheta='';
                                                                                            return $nscheta;
                                                                                      },   
                                                   ], 
                                                   [
                                                    'headerOptions'=>['class'=>'c'],
                                                    'contentOptions'=>['class'=>'c p6'],
                                                    'attribute' => 'dates',
                                                    'label'=>'Дата',
                                                    'value'=> function ($allModels)  {  (isset($allModels['dates']))  ? $dates=$allModels['dates'] : $dates='';
                                                                                            return $dates;
                                                                                      },      
                                                    ], 
                                                    [
                                                     'headerOptions'=>['class'=>'c'],
                                                     'contentOptions'=>['class'=>'c'],
                                                     'attribute' => 'mremont_id',
                                                     'label'=>'Место ремонта',  
                                                     'value'=> function ($allModels)  {  (isset($allModels['mremont_id']))  ? $mremont_id=$allModels['mremont_id'] : $mremont_id='';
                                                                                            return $mremont_id;
                                                                                      },         
                                                    ], 
                                                    [
                                                     'headerOptions'=>['class'=>'c'],
                                                     'contentOptions'=>['class'=>'c'],
                                                     'attribute' => 'nvagon_id',
                                                     'label'=>'Номер вагона',
                                                     'value'=> function ($allModels)  {  (isset($allModels['nvagon_id']))  ? $nvagon_id=$allModels['nvagon_id'] : $nvagon_id='';
                                                                                            return $nvagon_id;
                                                                                      },   
                                                    ], 
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'vvagona',
                                                      'label'=>'Вид вагона',
                                                      'value'=> function ($allModels)  {  (isset($allModels['vvagona']))  ? $vvagona=$allModels['vvagona'] : $vvagona='';
                                                                                            return $vvagona;
                                                                                      },  
                                                    ], 
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'sobstvenik_id',
                                                      'label'=>'Собственник',
                                                      'value'=> function ($allModels)  {  (isset($allModels['sobstvenik_id']))  ? $sobstvenik_id=$allModels['sobstvenik_id'] : $sobstvenik_id='';
                                                                                            return $sobstvenik_id;
                                                                                      },  
                                                    ], 
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'nkontr',
                                                      'label'=>'Номер контракта',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['nkontr']))  ? $nkontr=$allModels['nkontr'] : $nkontr='';
                                                                                            return $nkontr;
                                                                                       },    
                                                    ], 
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c p6'],
                                                      'attribute' => 'drab',
                                                      'label'=>'Дата работы',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['drab']))  ? $drab=$allModels['drab'] : $drab='';
                                                                                            return $drab;
                                                                                       },  
                                                    ],                     
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'tip',
                                                      'label'=>'ТОР/ДР',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['tip']))  ? $tip=$allModels['tip'] : $tip='';
                                                                                            return $tip;
                                                                                       },  
                                                    ], 
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'vcomp_id',
                                                      'label'=>'Выставление на компании',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['vcomp_id']))  ? $vcomp_id=$allModels['vcomp_id'] : $vcomp_id='';
                                                                                            return $vcomp_id;
                                                                                       },    
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'tsho_id',
                                                      'label'=>'Возмещение от ТШО',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['tsho_id']))  ? $tsho_id=$allModels['tsho_id'] : $tsho_id='';
                                                                                            return $tsho_id;
                                                                                       },      
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'attribute' => 'prbrak',
                                                      'label'=>'Причина брака',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['prbrak']))  ? $prbrak=$allModels['prbrak'] : $prbrak='';
                                                                                            return $prbrak;
                                                                                       },   
                                                      'format'=>'html'                                         
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'kol',
                                                      'label'=>'Кол-во',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['kol']))  ? $kol=$allModels['kol'] : $kol='';
                                                                                            return $kol;
                                                                                       },            
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'cenaed',
                                                      'label'=>'Цена за ед.',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['cenaed']))  ? $cenaed=$allModels['cenaed'] : $cenaed='';
                                                                                            return $cenaed;
                                                                                       },              
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'cena',
                                                      'label'=>'Цена',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['cena']))  ? $cena=$allModels['cena'] : $cena='';
                                                                                            return $cena;
                                                                                       },                
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'ndc',
                                                      'label'=>'Ндс',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['ndc']))  ? $ndc=$allModels['ndc'] : $ndc='';
                                                                                            return $ndc;
                                                                                       },                 
                                                    ], 
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'itog',
                                                      'label'=>'Итог',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['itog']))  ? $itog=$allModels['itog'] : $itog='';
                                                                                            return $itog;
                                                                                       },                
                                                    ],  
                                        
                                                ],
                                ]);       
              $mas=['label'=>$label,'content'=>$html];                                                                                
        }
        elseif($tip=='posrednik')
        {
            
            $html=GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'layout'=>'{items}',
                                    'tableOptions' =>['class' => 'dop','id'=>$random],
                                    'rowOptions' => function ($allModels){
                                                        (isset($allModels['color'])) ? $color=$allModels['color'] : $color='';
                                                        return ['class'=>$color];
                                                        },
                                    'columns' =>[
                                                   [
                                                     'headerOptions'=>['class'=>'c'],  
                                                     'contentOptions'=>['class'=>'p6'],  
                                                     'attribute' => 'nscheta',
                                                     'label'=>'Номер счета',
                                                     'value'=> function ($allModels)  {  (isset($allModels['nscheta']))  ? $nscheta=$allModels['nscheta'] : $nscheta='';
                                                                                            return $nscheta;
                                                                                      },   
                                                   ], 
                                                   [
                                                    'headerOptions'=>['class'=>'c'],
                                                    'contentOptions'=>['class'=>'c p6'],
                                                    'attribute' => 'dates',
                                                    'label'=>'Дата',
                                                    'value'=> function ($allModels)  {  (isset($allModels['dates']))  ? $dates=$allModels['dates'] : $dates='';
                                                                                            return $dates;
                                                                                      },      
                                                    ], 
                                                    [
                                                     'headerOptions'=>['class'=>'c'],
                                                     'contentOptions'=>['class'=>'c'],
                                                     'attribute' => 'mremont_id',
                                                     'label'=>'Место ремонта',  
                                                     'value'=> function ($allModels)  {  (isset($allModels['mremont_id']))  ? $mremont_id=$allModels['mremont_id'] : $mremont_id='';
                                                                                            return $mremont_id;
                                                                                      },         
                                                    ], 
                                                    [
                                                     'headerOptions'=>['class'=>'c'],
                                                     'contentOptions'=>['class'=>'c'],
                                                     'attribute' => 'nvagon_id',
                                                     'label'=>'Номер вагона',
                                                     'value'=> function ($allModels)  {  (isset($allModels['nvagon_id']))  ? $nvagon_id=$allModels['nvagon_id'] : $nvagon_id='';
                                                                                            return $nvagon_id;
                                                                                      },   
                                                    ], 
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'vvagona',
                                                      'label'=>'Вид вагона',
                                                      'value'=> function ($allModels)  {  (isset($allModels['vvagona']))  ? $vvagona=$allModels['vvagona'] : $vvagona='';
                                                                                            return $vvagona;
                                                                                      },  
                                                    ], 
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'sobstvenik_id',
                                                      'label'=>'Собственник',
                                                      'value'=> function ($allModels)  {  (isset($allModels['sobstvenik_id']))  ? $sobstvenik_id=$allModels['sobstvenik_id'] : $sobstvenik_id='';
                                                                                            return $sobstvenik_id;
                                                                                      },  
                                                    ], 
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'nkontr',
                                                      'label'=>'Номер контракта',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['nkontr']))  ? $nkontr=$allModels['nkontr'] : $nkontr='';
                                                                                            return $nkontr;
                                                                                       },    
                                                    ], 
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c p6'],
                                                      'attribute' => 'drab',
                                                      'label'=>'Дата работы',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['drab']))  ? $drab=$allModels['drab'] : $drab='';
                                                                                            return $drab;
                                                                                       },  
                                                    ],                     
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c p6'],
                                                      'attribute' => 'dbrak',
                                                      'label'=>'Дата заброковки',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['dbrak']))  ? $dbrak=$allModels['dbrak'] : $dbrak='';
                                                                                            return $dbrak;
                                                                                       },  
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'vcomp_id',
                                                      'label'=>'Выставление на компании',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['vcomp_id']))  ? $vcomp_id=$allModels['vcomp_id'] : $vcomp_id='';
                                                                                            return $vcomp_id;
                                                                                       },    
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'tsho_id',
                                                      'label'=>'Возмещение от ТШО',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['tsho_id']))  ? $tsho_id=$allModels['tsho_id'] : $tsho_id='';
                                                                                            return $tsho_id;
                                                                                       },      
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      //'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'prbrak',
                                                      'label'=>'Причина брака',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['prbrak']))  ? $prbrak=$allModels['prbrak'] : $prbrak='';
                                                                                            return $prbrak;
                                                                                       },   
                                                      'format'=>'html'                                         
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'kol',
                                                      'label'=>'Кол-во',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['kol']))  ? $kol=$allModels['kol'] : $kol='';
                                                                                            return $kol;
                                                                                       },            
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'cenaed',
                                                      'label'=>'Цена за ед.',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['cenaed']))  ? $cenaed=$allModels['cenaed'] : $cenaed='';
                                                                                            return $cenaed;
                                                                                       },              
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'cena',
                                                      'label'=>'Цена',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['cena']))  ? $cena=$allModels['cena'] : $cena='';
                                                                                            return $cena;
                                                                                       },                
                                                    ],
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'ndc',
                                                      'label'=>'Ндс',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['ndc']))  ? $ndc=$allModels['ndc'] : $ndc='';
                                                                                            return $ndc;
                                                                                       },                 
                                                    ], 
                                                    [
                                                      'headerOptions'=>['class'=>'c'],
                                                      'contentOptions'=>['class'=>'c'],
                                                      'attribute' => 'itog',
                                                      'label'=>'Итог',  
                                                      'value'=> function ($allModels)  {  (isset($allModels['itog']))  ? $itog=$allModels['itog'] : $itog='';
                                                                                            return $itog;
                                                                                       },                
                                                    ],  
                                        
                                                ],
                                ]);
               
               $label='"'.$array[0]['mremont_id'].'" '.$array[0]['tip'];                                                                        
               $mas=['label'=>$label,'content'=>$html];                                                                         
        }    
        
                      
        
                               
        return $mas;                   
    }     
    
    public function Tor_dr($tip,$id,$model,$date1,$date2,$export) 
    {
        $firm = Mremont::findOne(['id'=>$id]); 
        $dateperiod=''; $sh=[]; $prichina=[];$detail=[];$revizia=[];$usluga=[];$tor_dr=[];$a=[];
         
         if ($date1!='' &&  $date2!='')
         {
            $dateperiod=' && (dates>="'.date('Y-m-d',strtotime($date1)).'" && dates<="'.date('Y-m-d',strtotime($date2)).'")'; 
         }
         else
         {
            if ($date1!='')
            {
              $dateperiod=' && dates>="'.date('Y-m-d',strtotime($date1)).'"'; 
            }
            elseif($date2!='')
            {
              $dateperiod=' && dates<="'.date('Y-m-d',strtotime($date2)).'"';
            }
         }    
         
        $sh = (new \yii\db\Query())
                    ->select(['*']) 
                    ->from('vagon')
                    ->where('mremont_id='.$id.' && tip='.$tip.$dateperiod)
                    ->all();   
         
        if ($tip==1)
        {
            $prichina = (new \yii\db\Query())
                       ->select(['*']) 
                       ->from('prichina')
                       ->where('mremont_id='.$id.' && tip='.$tip.$dateperiod)
                       ->all();   
        }
        elseif($tip==2)
        {
            $prichina = (new \yii\db\Query())
                        ->select(['*']) 
                        ->from('tordrkr')
                        ->where('mremont_id='.$id.' && tip='.$tip.$dateperiod)
                        ->all();    
        }
                      
        $detail = (new \yii\db\Query())
                        ->select(['*']) 
                        ->from('detail')
                        ->where('mremont_id='.$id.' && tip='.$tip.$dateperiod)
                        ->all();    
                      
        $revizia=(new \yii\db\Query())
                        ->select(['*']) 
                        ->from('revizia')
                        ->where('mremont_id='.$id.' && tip='.$tip.$dateperiod) 
                        ->all();   

        $usluga=(new \yii\db\Query())
                        ->select(['*']) 
                        ->from('usluga')
                        ->where('mremont_id='.$id.' && tip='.$tip.$dateperiod) 
                        ->all();        
        
        $promiv = (new \yii\db\Query())
                    ->select(['*']) 
                    ->from('promiv')
                    ->where('mremont_id='.$id.' && tip='.$tip.$dateperiod)
                    ->all();
        
        $clean= (new \yii\db\Query())
                    ->select(['*']) 
                    ->from('clean')
                    ->where('mremont_id='.$id.' && tip='.$tip.$dateperiod)
                    ->all();
        
        $zhd=(new \yii\db\Query())
                    ->select(['*']) 
                    ->from('zhd')
                    ->where('mremont_id='.$id.' && tip='.$tip.$dateperiod)
                    ->all();
        
        
        $index=[]; $tmp=[];
        foreach ($sh as $value) // проходим по счет фактурам вагонов
        {
           
            $vtmp=[];
            // ввыставление на компанию, не сколько перечисленны через запятую переводим в массив
            if (!empty($value['vcomp_id']))  
            {
                $vcomp=  explode(',', $value['vcomp_id']);
                for ($i=0;$i<count($vcomp);$i++)
                {
                   $vtmp[]=$model->DataCompany[$vcomp[$i]]; 
                }
                $v=implode(',', $vtmp);
            }
            else 
            {
                $v='';
            }
            
            (!empty($value['tsho_id'])) ? $tsho=$model->DataTsho[$value['tsho_id']] : $tsho='';
            
            
            (!empty($value['remont_id'])) ? $t=$model->DataPrice[$value['remont_id']] : $t='';    
            $str='';
            (!empty($export)) ? $br="\n" : $br="<br/>"; 
            (!empty($value['vu23'])) ?  $str.=$br."ВУ23-".$value['vu23'] : $str.="";
            (!empty($value['vu22'])) ?  $str.=$br."ВУ22-".$value['vu22'] : $str.="";
            (!empty($value['vu36'])) ?  $str.=$br."ВУ36-".$value['vu36'] : $str.=""; 
            (!empty($value['akt'])) ?  $str.=$br."АКТ-".$value['akt'] : $str.="";  
             
            (!empty($value['vvagona'])) ? $vvagon=$model->DataVvagon[$value['vvagona']] : $vvagon='';
            (!empty($value['sobstvenik_id'])) ? $sobstvenik_id=$model->DataSobstvenik[$value['sobstvenik_id']] : $sobstvenik_id='';
            
            
             $ndc_pr=$firm->ndc/100;
             $spr_id=[];
             $k=0;   
             
                // ищем справка 2612 относящие к счет-фактуре вагона, если у счет-фактуры есть справка 2612 значит сохраняем в массив эту счет-фактуру
                foreach ($prichina as $keyind=>$pritem) 
                {
                   if ($pritem['vagon_id']==$value['id'])
                   {   
                       $k=1;     
                   }    
                }
            
            
                        
                if ($k==1) 
                {
                        if (!in_array($value['nscheta'], $index)) //если номера счета нет в массиве
                        {
                             $index[]=$value['nscheta']; //заносим номер счета
                             $ikey=  array_search($value['nscheta'], $index);// ключ
                             $x=0;
                        }
                        else
                        {
                             $ikey=  array_search($value['nscheta'], $index);// ключ 
                             $x=  count($tmp[$ikey]);
                        }

                        (!empty($value['dates'])) ? $dates=date('d-m-Y', strtotime($value['dates'])) : $dates='';
                        (!empty($value['drab'])) ? $drab=date('d-m-Y', strtotime($value['drab'])) : $drab='';
                        (!empty($value['dbrak'])) ? $dbrak=date('d-m-Y', strtotime($value['dbrak'])) : $dbrak='';
                        
                        if ($tip==1)
                        {    
                          $tmp[$ikey][$x]=[
                                            'nscheta'=>$value['nscheta'],
                                            'dates'=>$dates,
                                            'mremont_id'=>$model->DataMremont[$value['mremont_id']],
                                            'nvagon_id'=>$model->DataVc[$value['nvagon_id']],
                                            'vvagona'=>$vvagon,
                                            'sobstvenik_id'=>$sobstvenik_id,
                                            'nkontr'=>$value['nkontr'],
                                            'drab'=>$drab,
                                            'dbrak'=>$dbrak,
                                            'vcomp_id'=>$v,
                                            'tsho_id'=>$tsho,
                                            'tip'=>$model->DataOmas[$value['tip']],
                                            'xlstip'=>$model->DataOmas[$value['tip']],
                                            'lastname'=>$model->DataOmas[$value['tip']]
                                         ];
                        }
                        else
                        {
                                     $tmp[$ikey][$x]=[
                                            'nscheta'=>$value['nscheta'],
                                            'dates'=>$dates,
                                            'mremont_id'=>$model->DataMremont[$value['mremont_id']],
                                            'nvagon_id'=>$model->DataVc[$value['nvagon_id']],
                                            'vvagona'=>$vvagon,
                                            'sobstvenik_id'=>$sobstvenik_id,
                                            'nkontr'=>$value['nkontr'],
                                            'drab'=>'',
                                            'dbrak'=>'',
                                            'vcomp_id'=>'',
                                            'tsho_id'=>'', 
                                            'prbrak'=>$t,
                                            'tip'=>$model->DataOmas[$value['tip']],  
                                            'xlstip'=>$model->DataOmas[$value['tip']],
                                            'lastname'=>$model->DataOmas[$value['tip']]
                                            ];
                        }
                        
                        
                        // ищем справка 2612 относящие к счет-фактуре вагона
                        foreach ($prichina as $keyind=>$pritem) 
                        {
                           if ($pritem['vagon_id']==$value['id'])
                           {   
                               //$k=1;     

                                if ($tip==1)
                                {  
                                    if (isset($pritem['spr_id']))
                                    {
                                      $spr_id[]=$model->DataPrice[$pritem['spr_id']];  
                                    }    
                                    $tmp[$ikey][$x]['spr_id']=  implode(',',$spr_id);
                                    $tmp[$ikey][$x]['prbrak']=$t.$br."2612 - ".implode(',',$spr_id).$str;   
                                }
                                    $tmp[$ikey][$x]['kol']=1;
                                    $tmp[$ikey][$x]['cenaed']=$pritem['cenaed'];
                                    $tmp[$ikey][$x]['cena']=$pritem['cenaed'];
                                    $tmp[$ikey][$x]['ndc']=$pritem['cenaed']*$ndc_pr;
                                    $tmp[$ikey][$x]['itog']=  round($tmp[$ikey][$x]['cena']+$tmp[$ikey][$x]['ndc'],2);
                           }    
                        }
                        
                        // ищем детали
                        foreach ($detail as $dval) 
                        {
                           if ($dval['vagon_id']==$value['id'])
                           {
                               $cena=round($dval['cenaed']*$dval['kol'],2);
                               $ndc=round($cena*$ndc_pr,2);
                               $itog=  round($cena+$ndc, 2);
                               $tmp[$ikey][]=[
                                                'nscheta'=>$tmp[$ikey][$x]['nscheta'],
                                                'dates'=>$dates,
                                                'mremont_id'=>$tmp[$ikey][$x]['mremont_id'],
                                                'nvagon_id'=>$tmp[$ikey][$x]['nvagon_id'],
                                                'vvagona'=>$tmp[$ikey][$x]['vvagona'],
                                                'sobstvenik_id'=>$tmp[$ikey][$x]['sobstvenik_id'],
                                                'nkontr'=>$tmp[$ikey][$x]['nkontr'],
                                                'drab'=>$drab,
                                                'dbrak'=>$dbrak,
                                                'vcomp_id'=>$v,
                                                'tsho_id'=>$tsho,
                                                'spr_id'=>implode(',',$spr_id),                        
                                              'prbrak'=>$model->DataPrice[$dval['detailspr_id']],
                                              'kol'=>$dval['kol'],
                                              'cenaed'=>$dval['cenaed'],
                                              'cena'=>$cena, 
                                              'ndc'=>$ndc,
                                              'itog'=>$itog,
                                              'xlstip'=>$model->DataOmas[$value['tip']],
                                             ];
                           }    
                        }

                        // ищем ревизии
                        foreach ($revizia as $itemrevizia) 
                        {
                           if ($itemrevizia['vagon_id']==$value['id'])
                           {
                               $cena=round($itemrevizia['cenaed']*$itemrevizia['kol'],2);
                               $ndc=round($cena*$ndc_pr,2);
                               $itog=  round($cena+$ndc, 2);
                               $tmp[$ikey][]=[
                                                'nscheta'=>$tmp[$ikey][$x]['nscheta'],
                                                'dates'=>$dates,
                                                'mremont_id'=>$tmp[$ikey][$x]['mremont_id'],
                                                'nvagon_id'=>$tmp[$ikey][$x]['nvagon_id'],
                                                'vvagona'=>$tmp[$ikey][$x]['vvagona'],
                                                'sobstvenik_id'=>$tmp[$ikey][$x]['sobstvenik_id'],
                                                'nkontr'=>$tmp[$ikey][$x]['nkontr'],
                                                'drab'=>$drab,
                                                'dbrak'=>$dbrak,
                                                'vcomp_id'=>$v,
                                                'tsho_id'=>$tsho,
                                                'spr_id'=>implode(',',$spr_id),                        
                                              'prbrak'=>$model->DataPrice[$itemrevizia['reviziaspr_id']],
                                              'kol'=>$itemrevizia['kol'],
                                              'cenaed'=>$itemrevizia['cenaed'],
                                              'cena'=>$cena, 
                                              'ndc'=>$ndc,
                                              'itog'=>$itog,
                                              'xlstip'=>$model->DataOmas[$value['tip']],
                                             ];
                           }    
                        }

                        // ищем уcлуги
                        foreach ($usluga as $itemusluga) 
                        {
                          if ($itemusluga['vagon_id']==$value['id'])
                           {
                               $cena=round($itemusluga['cenaed']*$itemusluga['kol'],2);
                               $ndc=round($cena*$ndc_pr,2);
                               $itog=  round($cena+$ndc, 2);
                               $tmp[$ikey][]=[
                                                'nscheta'=>$tmp[$ikey][$x]['nscheta'],
                                                'dates'=>$dates,
                                                'mremont_id'=>$tmp[$ikey][$x]['mremont_id'],
                                                'nvagon_id'=>$tmp[$ikey][$x]['nvagon_id'],
                                                'vvagona'=>$tmp[$ikey][$x]['vvagona'],
                                                'sobstvenik_id'=>$tmp[$ikey][$x]['sobstvenik_id'],
                                                'nkontr'=>$tmp[$ikey][$x]['nkontr'],
                                                'drab'=>$drab,
                                                'dbrak'=>$dbrak,
                                                'vcomp_id'=>$v,
                                                'tsho_id'=>$tsho,
                                                'spr_id'=>implode(',',$spr_id),                        
                                              'prbrak'=>$model->DataPrice[$itemusluga['uslugaspr_id']],
                                              'kol'=>$itemusluga['kol'],
                                              'cenaed'=>$itemusluga['cenaed'],
                                              'cena'=>$cena, 
                                              'ndc'=>$ndc,
                                              'itog'=>$itog,
                                              'xlstip'=>$model->DataOmas[$value['tip']],
                                             ];
                           }    
                        }
                        
                        // промывка, жд тариф и уборка здесь будут только в том случаем если все было в одной счет-фактуре 
                        
                        foreach ($clean as $item) 
                        {
                          if ($item['vagon_id']==$value['id'])
                           {
                               $cena=round($item['cenaed']*$item['kol'],2);
                               $ndc=round($cena*$ndc_pr,2);
                               $itog=  round($cena+$ndc, 2);
                               $tmp[$ikey][]=[
                                                'nscheta'=>$tmp[$ikey][$x]['nscheta'],
                                                'dates'=>$dates,
                                                'mremont_id'=>$tmp[$ikey][$x]['mremont_id'],
                                                'nvagon_id'=>$tmp[$ikey][$x]['nvagon_id'],
                                                'vvagona'=>$tmp[$ikey][$x]['vvagona'],
                                                'sobstvenik_id'=>$tmp[$ikey][$x]['sobstvenik_id'],
                                                'nkontr'=>$tmp[$ikey][$x]['nkontr'],
                                                'drab'=>$drab,
                                                'dbrak'=>$dbrak, 
                                                'vcomp_id'=>$v,
                                                'tsho_id'=>$tsho,
                                                'spr_id'=>implode(',',$spr_id),
                                                'prbrak'=>$model->DataPrice[$item['cleanspr_id']],
                                                'kol'=>$item['kol'],
                                                'cenaed'=>$item['cenaed'],
                                                'cena'=>$cena, 
                                                'ndc'=>$ndc,
                                                'itog'=>$itog,
                                                'xlstip'=>$model->DataOmas[$value['tip']],
                                             ];
                           }    
                        }
                        
                        foreach ($zhd as $item) 
                        {
                          if ($item['vagon_id']==$value['id'])
                           {
                               $cena=round($item['cenaed']*$item['kol'],2);
                               $ndc=round($cena*$ndc_pr,2);
                               $itog=  round($cena+$ndc, 2);
                               $tmp[$ikey][]=[
                                                'nscheta'=>$tmp[$ikey][$x]['nscheta'],
                                                'dates'=>$dates,
                                                'mremont_id'=>$tmp[$ikey][$x]['mremont_id'],
                                                'nvagon_id'=>$tmp[$ikey][$x]['nvagon_id'],
                                                'vvagona'=>$tmp[$ikey][$x]['vvagona'],
                                                'sobstvenik_id'=>$tmp[$ikey][$x]['sobstvenik_id'],
                                                'nkontr'=>$tmp[$ikey][$x]['nkontr'],
                                                'drab'=>$drab,
                                                'dbrak'=>$dbrak,
                                                'vcomp_id'=>$v,
                                                'tsho_id'=>$tsho,
                                                'spr_id'=>implode(',',$spr_id),  
                                                'prbrak'=>$model->DataPrice[$item['cleanspr_id']],
                                                'kol'=>$item['kol'],
                                                'cenaed'=>$item['cenaed'],
                                                'cena'=>$cena, 
                                                'ndc'=>$ndc,
                                                'itog'=>$itog,
                                                'xlstip'=>$model->DataOmas[$value['tip']], 
                                             ];
                           }    
                        }
                        
                        foreach ($promiv as $item) 
                        {
                          if ($item['vagon_id']==$value['id'])
                           {
                               $cena=round($item['cenaed']*$item['kol'],2);
                               $ndc=round($cena*$ndc_pr,2);
                               $itog=  round($cena+$ndc, 2);
                               $tmp[$ikey][]=[
                                                'nscheta'=>$tmp[$ikey][$x]['nscheta'],
                                                'dates'=>$dates,
                                                'mremont_id'=>$tmp[$ikey][$x]['mremont_id'],
                                                'nvagon_id'=>$tmp[$ikey][$x]['nvagon_id'],
                                                'vvagona'=>$tmp[$ikey][$x]['vvagona'],
                                                'sobstvenik_id'=>$tmp[$ikey][$x]['sobstvenik_id'],
                                                'nkontr'=>$tmp[$ikey][$x]['nkontr'],
                                                'drab'=>$drab,
                                                'dbrak'=>$dbrak,
                                                'vcomp_id'=>$v,
                                                'tsho_id'=>$tsho,
                                                'spr_id'=>implode(',',$spr_id),
                                                'prbrak'=>$model->DataPrice[$item['cleanspr_id']],
                                                'kol'=>$item['kol'],
                                                'cenaed'=>$item['cenaed'],
                                                'cena'=>$cena, 
                                                'ndc'=>$ndc,
                                                'itog'=>$itog,
                                                'xlstip'=>$model->DataOmas[$value['tip']],
                                             ];
                           }    
                        }
                } //проверка на наличие причин
        } // конец группировки счет-фактур
        
        // подсчет  всех счет-фактур
        foreach ($tmp as $key=>$mas) 
        {
            $kol='';$cena='';$ndc='';$itog='';$xlstip='';
            foreach ($mas as $z=>$value) 
            {
              if ($z==0) {$xlstip=$value['tip'];}
              $kol+=$value['kol']; 
              $cena+=$value['cena'];
              $ndc+=$value['ndc'];
              $itog+=$value['itog'];
            }
            
            $tmp[$key][]=[
                          'nscheta'=>'',
                          'dates'=>'',
                          'mremont_id'=>'',
                          'nvagon_id'=>'',
                          'vvagona'=>'',
                          'sobstvenik_id'=>'',
                          'nkontr'=>'',
                          'drab'=>'',
                          'dbrak'=>'',
                          'vcomp_id'=>'',
                          'tsho_id'=>'',
                          'spr_id'=>'',
                          'prbrak'=>'',
                          'kol'=>$kol,
                          'cenaed'=>'', 
                          'cena'=>$cena,
                          'ndc'=>$ndc,
                          'itog'=>$itog,
                          'color'=>'blue',
                          'xlstip'=>$xlstip
                         ];
        }
        
        // уменьшаем вложенность
        for ($i=0;$i<count($tmp);$i++)
        {
           for($j=0;$j<count($tmp[$i]);$j++)
           {
              $a[]=$tmp[$i][$j]; 
           }
        }
        
        if ($tip==1){ $identi='ТОР';} elseif ($tip==2){ $identi='ДР';} if ($tip==3){ $identi='КР';}  
        
        if (!empty($a))    
        {    
            if (empty($export))
            {
                $tor_dr=$this->ConstructTable($tip, $a, $identi);
            }
            else
            {
                $tor_dr=$a;
            }    
        }    
    
       return $tor_dr;  
    }
    
    
    public function Pr_ubr_zhd($tip,$id,$model,$date1,$date2,$export) 
    {
        $firm = Mremont::findOne(['id'=>$id]); 
        $ndc_pr=$firm->ndc/100;
        $dateperiod='';  $index=[];$tmp=[];$pr_ubr_zhd=[];
         
         if ($date1!='' &&  $date2!='')
         {
            $dateperiod=' && (dates>="'.date('Y-m-d',strtotime($date1)).'" && dates<="'.date('Y-m-d',strtotime($date2)).'")'; 
         }
         else
         {
            if ($date1!='')
            {
              $dateperiod=' && dates>="'.date('Y-m-d',strtotime($date1)).'"'; 
            }
            elseif($date2!='')
            {
              $dateperiod=' && dates<="'.date('Y-m-d',strtotime($date2)).'"';
            }
         }    
         
        $sh = (new \yii\db\Query())
                    ->select(['*']) 
                    ->from('vagon')
                    ->where('mremont_id='.$id.$dateperiod)
                    ->all();     
        
        $promiv = (new \yii\db\Query())
                    ->select(['*']) 
                    ->from('promiv')
                    ->where('mremont_id='.$id.$dateperiod)
                    ->all();
        
        $clean= (new \yii\db\Query())
                    ->select(['*']) 
                    ->from('clean')
                    ->where('mremont_id='.$id.$dateperiod)
                    ->all();
        
        $zhd=(new \yii\db\Query())
                    ->select(['*']) 
                    ->from('zhd')
                    ->where('mremont_id='.$id.$dateperiod)
                    ->all();
        
        /// для проверки стоит ли заносить данные в массив, т.е. шли эти услуги отдельными счет фактурами или до кучи в одной (в одной выставляют нерезиденты РК)

        $prichina=(new \yii\db\Query())
                       ->select(['*']) 
                       ->from('tordrkr')
                       ->where('mremont_id='.$id.$dateperiod)
                       ->all();
        ///////////////////////////////////////////////////////////// 
        $all=[$promiv,$clean,$zhd];

                
         foreach($sh as $value)  // проверяем есть ли в таблице счет-фактур id'шники = vagon_id в таблицах промывка, жд тариф, уборка 
         {
                $k=0;   
             
                // ищем тор/др/кр относящие к счет-фактуре вагона, если у счет-фактуры есть id в таблице тор/др/кр значит не будем заносить в массив эту счет-фактуру
                foreach ($prichina as $pritem) 
                {
                   if ($pritem['vagon_id']==$value['id'])
                   {   
                       $k=1;     
                   }    
                }
             
                if ($k==0)
                {    
                        $vtmp=[];
                        // ввыставление на компанию, не сколько перечисленны через запятую переводим в массив
                        if (!empty($value['vcomp_id']))  
                        {
                           $vcomp=  explode(',', $value['vcomp_id']);
                           for ($i=0;$i<count($vcomp);$i++)
                           {
                             $vtmp[]=$model->DataCompany[$vcomp[$i]]; 
                           }
                           $v=implode(',', $vtmp);
                        }
                        else 
                        {
                          $v='';
                        }

                        (isset($value['tsho_id'])) ? $tsho=$model->DataTsho[$value['tsho_id']] : $tsho=''; 
                        (isset($value['vvagona'])) ? $vvagon=$model->DataVvagon[$value['vvagona']] : $vvagon='';
                        (isset($value['sobstvenik_id'])) ? $sobstvenik_id=$model->DataSobstvenik[$value['sobstvenik_id']] : $sobstvenik_id='';
                        (!empty($value['dates'])) ? $dates=date('d-m-Y', strtotime($value['dates'])) : $dates='';
                        (!empty($value['drab'])) ? $drab=date('d-m-Y', strtotime($value['drab'])) : $drab='';

                        for ($i=0;$i<count($all);$i++)
                        {
                            foreach($all[$i] as $item) // проходим таблицы promiv,zhd,clean
                            {
                                 if ($item['vagon_id']==$value['id'])
                                 {
                                     if (!in_array($value['nscheta'], $index)) //если номера счета нет в массиве
                                     {
                                        $index[]=$value['nscheta']; //заносим номер счета
                                        $ikey=  array_search($value['nscheta'], $index);// ключ
                                        $x=0;
                                     }
                                     else
                                     {
                                        $ikey=  array_search($value['nscheta'], $index);// ключ 
                                        $x=  count($tmp[$ikey]);
                                     }   

                                     if (isset($item['cleanspr_id']))
                                     {
                                         $prbrak=$model->DataPrice[$item['cleanspr_id']];
                                     }
                                     elseif(isset($item['promivspr_id']))
                                     {
                                         $prbrak=$model->DataPrice[$item['promivspr_id']];
                                     }    
                                     elseif(isset($item['zhdspr_id']))
                                     {
                                         $prbrak=$model->DataPrice[$item['zhdspr_id']];
                                     }

                                     $cena=$item['cenaed']*$item['kol'];
                                     $ndc=  round($cena*$ndc_pr, 2);
                                     $itog=  round($cena+$ndc, 2);

                                     $tmp[$ikey][$x]=[
                                                'nscheta'=>$value['nscheta'],
                                                'dates'=>$dates,
                                                'mremont_id'=>$model->DataMremont[$value['mremont_id']],
                                                'nvagon_id'=>$model->DataVc[$value['nvagon_id']],
                                                'vvagona'=>$vvagon,
                                                'sobstvenik_id'=>$sobstvenik_id,
                                                'nkontr'=>$value['nkontr'],
                                                'drab'=>$drab,
                                                'tip'=>$model->DataOmas[$value['tip']], 
                                                'vcomp_id'=>$v,
                                                'tsho_id'=>$tsho, 
                                                'prbrak'=>$prbrak,
                                                'kol'=>$item['kol'],
                                                'cenaed'=>$item['cenaed'],
                                                'cena'=>$cena,
                                                'ndc'=>$ndc,
                                                'itog'=>$itog, 
                                                'xlstip'=>'Техпд,промывка',
                                                'lastname'=>'Техпд,промывка',
                                                ];                                   
                                 } // конец совпадения id    
                            } //конец цикла прохода одной из таблиц promiv,zhd,clean    

                        } // конец цикла таблиц promiv,zhd,clean 
                }        
         } // конец цикла таблицы vagon (в ней все все счет-фактуры)
        
         
         // подсчет  всех счет-фактур
        foreach ($tmp as $key=>$mas) 
        {
            $kol=0;$cena=0;$ndc=0;$itog=0;// сбрасываем при переходе к следующей счет фактуре
            foreach ($mas as $value) 
            {
              $kol+=$value['kol']; 
              $cena+=$value['cena'];
              $ndc+=$value['ndc'];
              $itog+=$value['itog'];
            }
            
            $tmp[$key][]=[
                          'nscheta'=>'',
                          'dates'=>'',
                          'mremont_id'=>'',
                          'nvagon_id'=>'',
                          'vvagona'=>'',
                          'sobstvenik_id'=>'',
                          'nkontr'=>'',
                          'drab'=>'',
                          'tip'=>'',
                          'vcomp_id'=>'',
                          'tsho_id'=>'',
                          'prbrak'=>'',
                          'kol'=>$kol,
                          'cenaed'=>'',  
                          'cena'=>$cena,
                          'ndc'=>$ndc,
                          'itog'=>$itog,
                          'color'=>'blue',
                          'xlstip'=>'Техпд,промывка' ];
        }
        
        // уменьшаем вложенность
        for ($i=0;$i<count($tmp);$i++)
        {
           for($j=0;$j<count($tmp[$i]);$j++)
           {
              $a[]=$tmp[$i][$j]; 
           }
        }
        
        
        if (!empty($a))    
        {    
            if (empty($export))
            {
                $pr_ubr_zhd=$this->ConstructTable($tip, $a, 'ТехПД,промывка');
            }
            else
            {
                $pr_ubr_zhd=$a;
            }    
        }    
    
       return $pr_ubr_zhd;  
        
    }       
    
    public function Posrednik($tip,$id,$model,$date1,$date2,$export) 
    {
        $firm = Mremont::findOne(['id'=>$id]); 
        $m = (new \yii\db\Query())->select('id')->from('posrednik')->all(); // получаем пункты посреднников
        $ndc_pr=$firm->ndc/100;
        $dateperiod=''; $tmp=[];$posrednik=[];$tippos=[];$p=[];

         if ($date1!='' &&  $date2!='')
         {
             $dateperiod=' && (dates>="'.date('Y-m-d',strtotime($date1)).'" && dates<="'.date('Y-m-d',strtotime($date2)).'")'; 
         }
         else
         {
              if ($date1!='')
              {
                   $dateperiod=' && dates>="'.date('Y-m-d',strtotime($date1)).'"'; 
              }
              elseif($date2!='')
              {
                   $dateperiod=' && dates<="'.date('Y-m-d',strtotime($date2)).'"';
              }
         }
         
        // разбиваем на массив посреднников, т.к. сколько посреднников столько и отчетов
        $kol=4;
        foreach ($m as $value) 
        {
                  $ids[]=$kol; // заносим id посредников, с 1-3 принадлежат (ТОР, ДР, КР)по этому начинаем с 4
                  $posr_name[]=$model->DataPosrednik[$value['id']]; // заносим названия посредников
                  $kol++;
        }
         
        $sh=(new \yii\db\Query())
                        ->select(['*']) 
                        ->from('vagon')
                        ->where('mremont_id='.$id.' && tip IN ('.  implode(',', $ids).')'.$dateperiod) 
                        ->all();
        
        
        $detail=(new \yii\db\Query())
                        ->select(['*']) 
                        ->from('detail')
                        ->where('mremont_id='.$id.' && tip IN ('.  implode(',', $ids).')'.$dateperiod) 
                        ->all();
            
        $revizia=(new \yii\db\Query())
                        ->select(['*']) 
                        ->from('revizia')
                        ->where('mremont_id='.$id.' && tip IN ('.  implode(',', $ids).')'.$dateperiod) 
                        ->all(); 
            
        $usluga=(new \yii\db\Query())
                        ->select(['*']) 
                        ->from('usluga')
                        ->where('mremont_id='.$id.' && tip IN ('.  implode(',', $ids).')'.$dateperiod) 
                        ->all();   
            
        $all=[$detail,$revizia,$usluga];
        
        // все id посреднников хранятся в массиве $index, выстраиваем массив для таблицы
        foreach ($sh as $value) /// начало цикла сфет-фактур посредников
        { 
                $vtmp=[];
                // ввыставление на компанию, не сколько перечисленны через запятую переводим в массив
                if (!empty($value['vcomp_id']))  
                {
                    $vcomp=  explode(',', $value['vcomp_id']);
                    for ($i=0;$i<count($vcomp);$i++)
                    {
                       $vtmp[]=$model->DataCompany[$vcomp[$i]]; 
                    }
                    $v=implode(',', $vtmp);
                }
                else 
                {
                    $v='';
                }
            
                (!empty($value['dates'])) ? $dates=date('d-m-Y', strtotime($value['dates'])) : $dates='';
                (!empty($value['tsho_id'])) ? $tsho=$model->DataTsho[$value['tsho_id']] : $tsho='';
                (!empty($value['vvagona'])) ? $vvagon=$model->DataVvagon[$value['vvagona']] : $vvagon='';
                (!empty($value['sobstvenik_id'])) ? $sobstvenik_id=$model->DataSobstvenik[$value['sobstvenik_id']] : $sobstvenik_id='';
                (!empty($value['drab'])) ? $drab=date('d-m-Y', strtotime($value['drab'])) : $drab='';
                (!empty($value['dbrak'])) ? $dbrak=date('d-m-Y', strtotime($value['dbrak'])) : $dbrak='';
                (!empty($value['nvagon_id'])) ? $nvagon_id=$model->DataVc[$value['nvagon_id']] : $nvagon_id='';

                foreach ($all as $mas) // все таблицы в одном массиве 
                {
                    foreach ($mas as $item) 
                    {
                       // если vagon_id==id счета-фактуры  
                       if ($item['vagon_id']==$value['id']) 
                       {
                            if (!in_array($value['tip'], $tippos)) //если названия посредника нет в массиве
                            {
                               $tippos[]=$value['tip']; //заносим название посредника
                               $tkey=  array_search($value['tip'], $tippos);// ключ
                               $index=[];
                            }
                            else
                            {
                                $tkey=  array_search($value['tip'], $tippos);// ключ 
                            }
                            
                            if (!in_array($value['nscheta'], $index)) //если номера счета нет в массиве
                            {
                               $index[]=$value['nscheta']; //заносим номер счета
                               $ikey=  array_search($value['nscheta'], $index);// ключ
                               $x=0;
                            }
                            else
                            {
                                $ikey=  array_search($value['nscheta'], $index);// ключ 
                                $x=  count($tmp[$ikey]);
                            }

                            if (isset($item['detailspr_id']))
                            {
                               $prbrak=$model->DataPrice[$item['detailspr_id']];
                            }
                            elseif(isset($item['reviziaspr_id']))
                            {
                                $prbrak=$model->DataPrice[$item['reviziaspr_id']];
                            }    
                            elseif(isset($item['uslugaspr_id']))
                            {
                                $prbrak=$model->DataPrice[$item['uslugaspr_id']];
                            }

                            $cena=$item['cenaed']*$item['kol'];
                            $ndc=  round($cena*$ndc_pr, 2);
                            $itog=  round($cena+$ndc, 2);

                            $tmp[$tkey][$ikey][$x]=[
                                           'nscheta'=>$value['nscheta'],
                                           'dates'=>$dates,
                                           'mremont_id'=>$model->DataMremont[$item['mremont_id']],
                                           'nvagon_id'=>$nvagon_id,
                                           'vvagona'=>$vvagon,
                                           'sobstvenik_id'=>$sobstvenik_id,
                                           'nkontr'=>$value['nkontr'],
                                           'drab'=>$drab,
                                           'dbrak'=>$dbrak,
                                           'vcomp_id'=>$v,
                                           'tsho_id'=>$tsho,
                                           'prbrak'=>$prbrak,
                                           'kol'=>$item['kol'],
                                           'cenaed'=>$item['cenaed'],
                                           'cena'=>$cena,
                                           'ndc'=>$ndc,
                                           'itog'=>$itog,  
                                           'tip'=>$model->DataOmas[$item['tip']],
                                           'xlstip'=>$model->DataOmas[$item['tip']], 
                                           'lastname'=>$model->DataOmas[$value['tip']]  
                                           ];

                       } // конец условия что совпадают id вагона и id    
                    }    
                } // конец массива всех таблиц 
        } // конец цикла счет-фактур

        // подсчет  всех счет-фактур
       foreach ($tmp as $tkey=>$tmas) 
        {
            foreach ($tmas as $key=>$mas)
            {  
                $kol=0;$cena=0;$ndc=0;$itog=0;$xlstip='';// сбрасываем при переходе к следующей счет фактуре
                foreach ($mas as $z=>$value) 
                {
                      if ($z==0) {$xlstip=$value['tip'];} 
                      $kol+=$value['kol']; 
                      $cena+=$value['cena'];
                      $ndc+=$value['ndc'];
                      $itog+=$value['itog'];
                }

                $tmp[$tkey][$key][]=[
                                     'nscheta'=>'',
                                     'dates'=>'',
                                     'mremont_id'=>'',
                                     'nvagon_id'=>'',
                                     'vvagona'=>'',
                                     'sobstvenik_id'=>'',
                                     'nkontr'=>'',
                                     'drab'=>'',
                                     'dbrak'=>'',
                                     'vcomp_id'=>'',
                                     'tsho_id'=>'',
                                     'prbrak'=>'',
                                     'tip'=>'',
                                     'kol'=>$kol,
                                     'cenaed'=>'',
                                     'cena'=>$cena,
                                     'ndc'=>$ndc,
                                     'itog'=>$itog,
                                     'color'=>'blue',
                                     'xlstip'=>$xlstip
                                    ];
            }    
        }
         
        // уменьшаем вложенность
        for ($i=0;$i<count($tmp);$i++)
        {
           $a=[]; 
           for($j=0;$j<count($tmp[$i]);$j++) // ТКС, АКС
           {             
              for($k=0;$k<count($tmp[$i][$j]);$k++)
              {
                $a[]=$tmp[$i][$j][$k];   
              }              
           }
           
            if (!empty($a))    
            {  
                if (empty($export))    
                {  
                   $p[]=$this->ConstructTable($tip, $a, null);
                }
                else
                { 
                   $p[]=$a;
                } 
            }   
           
        }
      
          $posrednik=$p;
        return $posrednik;
    } 
    
   public function actionSave()
   {
        $model= new Vagon;
        $report=[];
        $querysobs = (new \yii\db\Query())->select('*')->from('sobstvenik')->all();  
        
         if($_GET['id']!='all')
         { 
            $tor=$this->Tor_dr(1,$_GET['id'],$model,$_GET['date1'],$_GET['date2'],1);    
            $dr=$this->Tor_dr(2,$_GET['id'],$model,$_GET['date1'],$_GET['date2'],1);    
            $pr_ubr_zhd=$this->Pr_ubr_zhd('teh',$_GET['id'],$model,$_GET['date1'],$_GET['date2'],1);    
            
            $tmp=[$tor,$dr,$pr_ubr_zhd];
            
            for ($i=0;$i<count($tmp);$i++)
            {
              if (!empty($tmp[$i]))
              {
                 $all[]=$tmp[$i]; 
              }    
            }
            
            $posrednik=$this->Posrednik('posrednik',$_GET['id'],$model,$_GET['date1'],$_GET['date2'], 1);    
            
            for($i=0;$i<count($posrednik);$i++)
            {
              $all[]=$posrednik[$i];  
            }
         }
         else
         {
            $firms = (new \yii\db\Query())->select('id')->from('mremont')->all();  
            foreach($firms as $item)
            {
                $tor=$this->Tor_dr(1,$item['id'],$model,$_GET['date1'],$_GET['date2'],1);    
                $dr=$this->Tor_dr(2,$item['id'],$model,$_GET['date1'],$_GET['date2'],1);    
                $pr_ubr_zhd=$this->Pr_ubr_zhd('teh',$item['id'],$model,$_GET['date1'],$_GET['date2'],1);
                $tmp=[$tor,$dr,$pr_ubr_zhd];
                for($i=0;$i<count($tmp);$i++)
                {
                    if (!empty($tmp[$i]))
                    {
                      $all[]=$tmp[$i];  
                    } 
                }
                
                $posrednik=$this->Posrednik('posrednik',$item['id'],$model,$_GET['date1'],$_GET['date2'], 1);    
            
                for($i=0;$i<count($posrednik);$i++)
                {
                  $all[]=$posrednik[$i];  
                }
            }           
         }
         
        $report=$all;

        $style_border = [ 
                          'borders'=>['allborders'=>['style'=>  \PHPExcel_Style_Border::BORDER_THIN] ],
                          'font'=> ['name' => 'Arial','size' => 10]                       
                        ];
        $style_bold=['font'=> [
                               'bold' => true,
                               'name' => 'Arial',
                               'size' => 10,
                               'color'=>['rgb'=>'000000']
                               ],
                      'fill' => [
                                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                                    'color'=>['rgb' => 'FFFFCC']
                                ],                                
                     ];

        $normal=['font'=> ['name' => 'Arial','size' => 10,'color'=>['rgb'=>'0000ff']] ];
        $normalred=['font'=> ['name' => 'Arial','size' => 10,'color'=>['rgb'=>'c00000']] ];
        
        $blue=['font'=> ['name' => 'Arial','size' => 10,'color'=>['rgb'=>'0000ff']] ];
        $red=['font'=> ['name' => 'Arial','size' => 10,'color'=>['rgb'=>'c00000']] ];

        $blueitog=['font'=> ['bold' => true,'name' => 'Arial','size' => 10,'color'=>['rgb'=>'0000ff']] ];
        $reditog=['font'=> ['bold' => true,'name' => 'Arial','size' => 10,'color'=>['rgb'=>'c00000']] ];
        
        $bgblue=[
                 'font'=> ['bold' => true,'name' => 'Arial','size' => 10,'color'=>['rgb'=>'0000ff']],
                 'fill' => ['type' => \PHPExcel_Style_Fill::FILL_SOLID,'color'=>['rgb' => 'd1e7f7']],                                
                ];
        $bgred=[
                 'font'=> ['bold' => true,'name' => 'Arial','size' => 10,'color'=>['rgb'=>'c00000']],
                 'fill' => ['type' => \PHPExcel_Style_Fill::FILL_SOLID,'color'=>['rgb' => 'd1e7f7']],                                
                ];
        $center=[
                  'alignment'=>[
                            'horizontal'=>  \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical'=>  \PHPExcel_Style_Alignment::VERTICAL_CENTER
                            ]
                ];
        $vertical_center=[
                            'alignment'=>[
                                          'vertical'=>  \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                                          'horizontal'=>\PHPExcel_Style_Alignment::HORIZONTAL_LEFT
                                         ]
                         ]; 
               
      $myExcel = new \PHPExcel();

       for($j=0;$j<count($report);$j++) 
       {
                if ($j!=0){ $myExcel->createSheet(); }
                $myExcel->setActiveSheetIndex($j);
                $sheet = $myExcel->getActiveSheet();
                $sheet->getSheetView()->setZoomScale(85);
                $sheet->setTitle($report[$j][0]['mremont_id'].' '.$report[$j][0]['lastname']);
        
                if ($report[$j][0]['xlstip']=='ТОР')
                {
                    $sheet->getColumnDimension('A')->setWidth(10);
                    $sheet->getColumnDimension('B')->setWidth(10);
                    $sheet->getColumnDimension('C')->setWidth(10);
                    $sheet->getColumnDimension('D')->setWidth(10);
                    $sheet->getColumnDimension('E')->setWidth(10);
                    $sheet->getColumnDimension('F')->setWidth(15);
                    $sheet->getColumnDimension('G')->setWidth(10);
                    $sheet->getColumnDimension('H')->setWidth(10);
                    $sheet->getColumnDimension('I')->setWidth(10);
                    $sheet->getColumnDimension('J')->setWidth(14);
                    $sheet->getColumnDimension('K')->setWidth(14);
                    $sheet->getColumnDimension('L')->setWidth(25);
                    $sheet->getColumnDimension('M')->setWidth(40);
                    $sheet->getColumnDimension('N')->setWidth(10);
                    $sheet->getColumnDimension('O')->setWidth(10);
                    $sheet->getColumnDimension('P')->setWidth(10);
                    $sheet->getColumnDimension('Q')->setWidth(10);
                    $sheet->getColumnDimension('R')->setWidth(10);
                    
                    
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(0, 1)->setValue('Номер счета');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(1, 1)->setValue('Дата');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(2, 1)->setValue('Место ремонта');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(3, 1)->setValue('Номер вагона');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(4, 1)->setValue('Вид вагона');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(5, 1)->setValue('Собственник');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(6, 1)->setValue('Номер контракта');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(7, 1)->setValue('Дата работы');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(8, 1)->setValue('Дата забраковки');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(9, 1)->setValue('Выставление на компании');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(10, 1)->setValue('Возмещение от ТШО');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(11, 1)->setValue('Справка 2612');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(12, 1)->setValue('Причина брака');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(13, 1)->setValue('Кол-во');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(14, 1)->setValue('Цена за ед.');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(15, 1)->setValue('Цена');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(16, 1)->setValue('НДС');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(17, 1)->setValue('Итого');
                }
                elseif ($report[$j][0]['xlstip']=='ДР')
                {
                    $sheet->getColumnDimension('A')->setWidth(10);
                    $sheet->getColumnDimension('B')->setWidth(10);
                    $sheet->getColumnDimension('C')->setWidth(10);
                    $sheet->getColumnDimension('D')->setWidth(10);
                    $sheet->getColumnDimension('E')->setWidth(10);
                    $sheet->getColumnDimension('F')->setWidth(10);
                    $sheet->getColumnDimension('G')->setWidth(10);
                    $sheet->getColumnDimension('H')->setWidth(40);
                    $sheet->getColumnDimension('I')->setWidth(10);
                    $sheet->getColumnDimension('J')->setWidth(10);
                    $sheet->getColumnDimension('K')->setWidth(10);
                    $sheet->getColumnDimension('L')->setWidth(10);
                    $sheet->getColumnDimension('M')->setWidth(10);
                    
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(0, 1)->setValue('Номер счета');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(1, 1)->setValue('Дата');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(2, 1)->setValue('Место ремонта');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(3, 1)->setValue('Номер вагона');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(4, 1)->setValue('Вид вагона');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(5, 1)->setValue('Собственник');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(6, 1)->setValue('Номер контракта');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(7, 1)->setValue('Причина брака');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(8, 1)->setValue('Кол-во');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(9, 1)->setValue('Цена за ед.');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(10, 1)->setValue('Цена');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(11, 1)->setValue('НДС');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(12, 1)->setValue('Итого');
                }
                elseif ($report[$j][0]['xlstip']=='Техпд,промывка')
                {
                    $sheet->getColumnDimension('A')->setWidth(10);
                    $sheet->getColumnDimension('B')->setWidth(15);
                    $sheet->getColumnDimension('C')->setWidth(15);
                    $sheet->getColumnDimension('D')->setWidth(15);
                    $sheet->getColumnDimension('E')->setWidth(15);
                    $sheet->getColumnDimension('F')->setWidth(15);
                    $sheet->getColumnDimension('G')->setWidth(15);
                    $sheet->getColumnDimension('H')->setWidth(15);
                    $sheet->getColumnDimension('I')->setWidth(10);
                    $sheet->getColumnDimension('J')->setWidth(15);
                    $sheet->getColumnDimension('K')->setWidth(15);
                    $sheet->getColumnDimension('L')->setWidth(40);
                    $sheet->getColumnDimension('M')->setWidth(10);
                    $sheet->getColumnDimension('N')->setWidth(10);
                    $sheet->getColumnDimension('O')->setWidth(10);
                    $sheet->getColumnDimension('P')->setWidth(10);
                    $sheet->getColumnDimension('Q')->setWidth(10);
                    
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(0, 1)->setValue('Номер счета');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(1, 1)->setValue('Дата');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(2, 1)->setValue('Место ремонта');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(3, 1)->setValue('Номер вагона');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(4, 1)->setValue('Вид вагона');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(5, 1)->setValue('Собственник');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(6, 1)->setValue('Номер контракта');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(7, 1)->setValue('Дата работы');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(8, 1)->setValue('ТОР/ДР');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(9, 1)->setValue('Выставление на компании');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(10, 1)->setValue('Возмещение от ТШО');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(11, 1)->setValue('Причина брака');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(12, 1)->setValue('Кол-во');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(13, 1)->setValue('Цена за ед.');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(14, 1)->setValue('Цена');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(15, 1)->setValue('НДС');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(16, 1)->setValue('Итого');
                }
                else
                {
                    $sheet->getColumnDimension('A')->setWidth(10);
                    $sheet->getColumnDimension('B')->setWidth(15);
                    $sheet->getColumnDimension('C')->setWidth(15);
                    $sheet->getColumnDimension('D')->setWidth(15);
                    $sheet->getColumnDimension('E')->setWidth(15);
                    $sheet->getColumnDimension('F')->setWidth(15);
                    $sheet->getColumnDimension('G')->setWidth(15);
                    $sheet->getColumnDimension('H')->setWidth(15);
                    $sheet->getColumnDimension('I')->setWidth(10);
                    $sheet->getColumnDimension('J')->setWidth(15);
                    $sheet->getColumnDimension('K')->setWidth(15);
                    $sheet->getColumnDimension('L')->setWidth(40);
                    $sheet->getColumnDimension('M')->setWidth(10);
                    $sheet->getColumnDimension('N')->setWidth(10);
                    $sheet->getColumnDimension('O')->setWidth(10);
                    $sheet->getColumnDimension('P')->setWidth(10);
                    $sheet->getColumnDimension('Q')->setWidth(10);
                    
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(0, 1)->setValue('Номер счета');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(1, 1)->setValue('Дата');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(2, 1)->setValue('Место ремонта');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(3, 1)->setValue('Номер вагона');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(4, 1)->setValue('Вид вагона');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(5, 1)->setValue('Собственник');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(6, 1)->setValue('Номер контракта');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(7, 1)->setValue('Дата работы');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(8, 1)->setValue('Дата забраковки');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(9, 1)->setValue('Выставление на компании');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(10, 1)->setValue('Возмещение от ТШО');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(11, 1)->setValue('Причина брака');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(12, 1)->setValue('Кол-во');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(13, 1)->setValue('Цена за ед.');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(14, 1)->setValue('Цена');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(15, 1)->setValue('НДС');
                    $myExcel->getActiveSheet()->getCellByColumnAndRow(16, 1)->setValue('Итого');
                }
                //set_time_limit(0);
                $num=2;
                foreach ($report[$j] as $count=>$item) 
                {
                    
                       if ($item['xlstip']=='ТОР')
                        { 
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(0, $num)->setValue($item['nscheta']);
                            $myExcel->getActiveSheet()->getStyle('A'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(1, $num)->setValue($item['dates']);
                            $myExcel->getActiveSheet()->getStyle('B'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(2, $num)->setValue($item['mremont_id']);
                            $myExcel->getActiveSheet()->getStyle('C'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(3, $num)->setValue($item['nvagon_id']);
                            $myExcel->getActiveSheet()->getStyle('D'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(4, $num)->setValue($item['vvagona']);
                            $myExcel->getActiveSheet()->getStyle('E'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(5, $num)->setValue($item['sobstvenik_id']);
                            $myExcel->getActiveSheet()->getStyle('F'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(6, $num)->setValue($item['nkontr']);
                            $myExcel->getActiveSheet()->getStyle('G'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(7, $num)->setValue($item['drab']);
                            $myExcel->getActiveSheet()->getStyle('H'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(8, $num)->setValue($item['dbrak']);
                            $myExcel->getActiveSheet()->getStyle('I'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(9, $num)->setValue($item['vcomp_id']);
                            $myExcel->getActiveSheet()->getStyle('J'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(10, $num)->setValue($item['tsho_id']);
                            $myExcel->getActiveSheet()->getStyle('K'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(11, $num)->setValue($item['spr_id']);
                            $myExcel->getActiveSheet()->getStyle('L'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(12, $num)->setValue($item['prbrak']);
                            $myExcel->getActiveSheet()->getStyle('M'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(13, $num)->setValue($item['kol']);
                            $myExcel->getActiveSheet()->getStyle('N'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(14, $num)->setValue($item['cenaed']);
                            $myExcel->getActiveSheet()->getStyle('O'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(15, $num)->setValue($item['cena']);
                            $myExcel->getActiveSheet()->getStyle('P'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(16, $num)->setValue($item['ndc']);
                            $myExcel->getActiveSheet()->getStyle('Q'.$num)->getAlignment()->setWrapText(true);
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(17, $num)->setValue($item['itog']); 
                            $myExcel->getActiveSheet()->getStyle('R'.$num)->getAlignment()->setWrapText(true);
                                
                            // высота шапки
                            $sheet->getRowDimension(1)->setRowHeight(40);
                            // устанавливаем авто подбор высоты строки
                            $sheet->getRowDimension($num)->setRowHeight(-1);
                            // выравниваем текст по центру ячейки
                            $sheet->getStyle('A'.$num.':K'.$num)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                            $sheet->getStyle('N'.$num.':R'.$num)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                             if ($item['cenaed']=='')
                             {
                                 if ($item['color']=='blue')
                                 {
                                   $style=$blue;    
                                 }
                                 $sheet->getStyle('A'.$num.':R'.$num)->applyFromArray($style);  
                             }     
                             
                             $sheet->getStyle('A1:R1')->applyFromArray($style_bold);
                             
                             $sheet->getStyle('A1:R1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                             $sheet->getStyle('A1:R1')->getAlignment()->setWrapText(true);
                             $sheet->getStyle('A1:R1')->applyFromArray($center);
                             $sheet->getStyle('L'.$num.':R'.$num)->applyFromArray($vertical_center);
                             // чертим границы таблицы
                             if ($count==count($report[$j])-1)  
                             {
                                $endrow=count($report[$j])+1; 
                                $sheet->getStyle('A1:R'.$endrow)->applyFromArray($style_border); 
                                // форматирование ячейки выравнивание  текста по центру
                                $sheet->getStyle('A2:K'.$endrow)->applyFromArray($center);
                                $sheet->getStyle('N2:R'.$endrow)->applyFromArray($center);
                                $myExcel->getActiveSheet()->setAutoFilter('A1:R'.$endrow);
                             }
                       } 
                       elseif ($item['xlstip']=='ДР')
                       {
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(0, $num)->setValue($item['nscheta']);
                            $myExcel->getActiveSheet()->getStyle('A'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(1, $num)->setValue($item['dates']);
                            $myExcel->getActiveSheet()->getStyle('B'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(2, $num)->setValue($item['mremont_id']);
                            $myExcel->getActiveSheet()->getStyle('C'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(3, $num)->setValue($item['nvagon_id']);
                            $myExcel->getActiveSheet()->getStyle('D'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(4, $num)->setValue($item['vvagona']);
                            $myExcel->getActiveSheet()->getStyle('E'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(5, $num)->setValue($item['sobstvenik_id']);
                            $myExcel->getActiveSheet()->getStyle('F'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(6, $num)->setValue($item['nkontr']);
                            $myExcel->getActiveSheet()->getStyle('G'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(7, $num)->setValue($item['prbrak']);
                            $myExcel->getActiveSheet()->getStyle('H'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(8, $num)->setValue($item['kol']);
                            $myExcel->getActiveSheet()->getStyle('I'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(9, $num)->setValue($item['cenaed']);
                            $myExcel->getActiveSheet()->getStyle('J'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(10, $num)->setValue($item['cena']);
                            $myExcel->getActiveSheet()->getStyle('K'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(11, $num)->setValue($item['ndc']);
                            $myExcel->getActiveSheet()->getStyle('L'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(12, $num)->setValue($item['itog']);
                            $myExcel->getActiveSheet()->getStyle('M'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            //$sheet->getStyle('A'.$num.':M'.$num)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                            // высота шапки
                            $sheet->getRowDimension(1)->setRowHeight(40);
                            // устанавливаем авто подбор высоты строки
                            $sheet->getRowDimension($num)->setRowHeight(-1);
                            // выравниваем текст по центру ячейки
                            $sheet->getStyle('A'.$num.':M'.$num)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                             if ($item['cenaed']=='')
                             {
                                 if ($item['color']=='blue')
                                 {
                                   $style=$blue;    
                                 }
                                 $sheet->getStyle('A'.$num.':M'.$num)->applyFromArray($style);  
                             }     
                             
                             $sheet->getStyle('A1:M1')->applyFromArray($style_bold);
                             
                             $sheet->getStyle('A1:M1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                             $sheet->getStyle('A1:M1')->getAlignment()->setWrapText(true);
                             $sheet->getStyle('A1:M1')->applyFromArray($center);
                             $sheet->getStyle('H'.$num)->applyFromArray($vertical_center);
                             // чертим границы таблицы
                             if ($count==count($report[$j])-1)  
                             {
                                $endrow=count($report[$j])+1; 
                                $sheet->getStyle('A1:M'.$endrow)->applyFromArray($style_border); 
                                // форматирование ячейки выравнивание  текста по центру
                                $sheet->getStyle('A2:G'.$endrow)->applyFromArray($center);
                                $sheet->getStyle('I2:M'.$endrow)->applyFromArray($center);
                                $myExcel->getActiveSheet()->setAutoFilter('A1:M'.$endrow);
                             }
                            
                       }
                       elseif ($item['xlstip']=='Техпд,промывка') 
                        {
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(0, $num)->setValue($item['nscheta']);
                            $myExcel->getActiveSheet()->getStyle('A'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(1, $num)->setValue($item['dates']);
                            $myExcel->getActiveSheet()->getStyle('B'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(2, $num)->setValue($item['mremont_id']);
                            $myExcel->getActiveSheet()->getStyle('C'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(3, $num)->setValue($item['nvagon_id']);
                            $myExcel->getActiveSheet()->getStyle('D'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(4, $num)->setValue($item['vvagona']);
                            $myExcel->getActiveSheet()->getStyle('E'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(5, $num)->setValue($item['sobstvenik_id']);
                            $myExcel->getActiveSheet()->getStyle('F'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(6, $num)->setValue($item['nkontr']);
                            $myExcel->getActiveSheet()->getStyle('G'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(7, $num)->setValue($item['drab']);
                            $myExcel->getActiveSheet()->getStyle('H'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(8, $num)->setValue($item['tip']);
                            $myExcel->getActiveSheet()->getStyle('I'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(9, $num)->setValue($item['vcomp_id']);
                            $myExcel->getActiveSheet()->getStyle('J'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(10, $num)->setValue($item['tsho_id']);
                            $myExcel->getActiveSheet()->getStyle('K'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(11, $num)->setValue($item['prbrak']);
                            $myExcel->getActiveSheet()->getStyle('L'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(12, $num)->setValue($item['kol']);
                            $myExcel->getActiveSheet()->getStyle('M'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(13, $num)->setValue($item['cenaed']);
                            $myExcel->getActiveSheet()->getStyle('N'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(14, $num)->setValue($item['cena']);
                            $myExcel->getActiveSheet()->getStyle('O'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(15, $num)->setValue($item['ndc']);
                            $myExcel->getActiveSheet()->getStyle('P'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(16, $num)->setValue($item['itog']);
                            $myExcel->getActiveSheet()->getStyle('Q'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            // высота шапки
                            $sheet->getRowDimension(1)->setRowHeight(40);
                            // устанавливаем авто подбор высоты строки
                            $sheet->getRowDimension($num)->setRowHeight(-1);
                            // выравниваем текст по центру ячейки
                            $sheet->getStyle('A'.$num.':Q'.$num)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                             if ($item['cenaed']=='')
                             {
                                 if ($item['color']=='blue')
                                 {
                                   $style=$blue;    
                                 }
                                 $sheet->getStyle('A'.$num.':Q'.$num)->applyFromArray($style);  
                             }     
                             
                             $sheet->getStyle('A1:Q1')->applyFromArray($style_bold);
                             
                             $sheet->getStyle('A1:Q1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                             $sheet->getStyle('A1:Q1')->getAlignment()->setWrapText(true);
                             $sheet->getStyle('A1:Q1')->applyFromArray($center);
                             $sheet->getStyle('L'.$num)->applyFromArray($vertical_center);
                             // чертим границы таблицы
                             if ($count==count($report[$j])-1)  
                             {
                                $endrow=count($report[$j])+1; 
                                $sheet->getStyle('A1:Q'.$endrow)->applyFromArray($style_border); 
                                // форматирование ячейки выравнивание  текста по центру
                                $sheet->getStyle('A2:K'.$endrow)->applyFromArray($center);
                                $sheet->getStyle('M2:Q'.$endrow)->applyFromArray($center);
                                
                                $myExcel->getActiveSheet()->setAutoFilter('A1:Q'.$endrow);
                             }                                     
                        }
                       else
                       {
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(0, $num)->setValue($item['nscheta']);
                            $myExcel->getActiveSheet()->getStyle('A'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(1, $num)->setValue($item['dates']);
                            $myExcel->getActiveSheet()->getStyle('B'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(2, $num)->setValue($item['mremont_id']);
                            $myExcel->getActiveSheet()->getStyle('C'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(3, $num)->setValue($item['nvagon_id']);
                            $myExcel->getActiveSheet()->getStyle('D'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(4, $num)->setValue($item['vvagona']);
                            $myExcel->getActiveSheet()->getStyle('E'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(5, $num)->setValue($item['sobstvenik_id']);
                            $myExcel->getActiveSheet()->getStyle('F'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(6, $num)->setValue($item['nkontr']);
                            $myExcel->getActiveSheet()->getStyle('G'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(7, $num)->setValue($item['drab']);
                            $myExcel->getActiveSheet()->getStyle('H'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(8, $num)->setValue($item['dbrak']);
                            $myExcel->getActiveSheet()->getStyle('I'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(9, $num)->setValue($item['vcomp_id']);
                            $myExcel->getActiveSheet()->getStyle('J'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(10, $num)->setValue($item['tsho_id']);
                            $myExcel->getActiveSheet()->getStyle('K'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(11, $num)->setValue($item['prbrak']);
                            $myExcel->getActiveSheet()->getStyle('L'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(12, $num)->setValue($item['kol']);
                            $myExcel->getActiveSheet()->getStyle('M'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(13, $num)->setValue($item['cenaed']);
                            $myExcel->getActiveSheet()->getStyle('N'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(14, $num)->setValue($item['cena']);
                            $myExcel->getActiveSheet()->getStyle('O'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(15, $num)->setValue($item['ndc']);
                            $myExcel->getActiveSheet()->getStyle('P'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                            $myExcel->getActiveSheet()->getCellByColumnAndRow(16, $num)->setValue($item['itog']);  
                            $myExcel->getActiveSheet()->getStyle('Q'.$num)->getAlignment()->setWrapText(true);// авто перенос текста в ячейки
                            
                           // высота шапки
                            $sheet->getRowDimension(1)->setRowHeight(40);
                            // устанавливаем авто подбор высоты строки
                            $sheet->getRowDimension($num)->setRowHeight(-1);
                            // выравниваем текст по центру ячейки
                            $sheet->getStyle('A'.$num.':Q'.$num)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                             if ($item['cenaed']=='')
                             {
                                 if ($item['color']=='blue')
                                 {
                                   $style=$blue;    
                                 }
                                 $sheet->getStyle('A'.$num.':Q'.$num)->applyFromArray($style);  
                             }     
                             
                             $sheet->getStyle('A1:Q1')->applyFromArray($style_bold);
                             
                             $sheet->getStyle('A1:Q1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                             $sheet->getStyle('A1:Q1')->getAlignment()->setWrapText(true);
                             $sheet->getStyle('A1:Q1')->applyFromArray($center);
                             $sheet->getStyle('L'.$num)->applyFromArray($vertical_center);
                             // чертим границы таблицы
                             if ($count==count($report[$j])-1)  
                             {
                                $endrow=count($report[$j])+1; 
                                $sheet->getStyle('A1:Q'.$endrow)->applyFromArray($style_border); 
                                // форматирование ячейки выравнивание  текста по центру
                                $sheet->getStyle('A2:K'.$endrow)->applyFromArray($center);
                                $sheet->getStyle('M2:Q'.$endrow)->applyFromArray($center);
                                
                                $myExcel->getActiveSheet()->setAutoFilter('A1:Q'.$endrow);
                             }
                       }    
                         
                  $num++;
                }
        
        }
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $filename = "DB_Export" . date("d-m-Y") . ".xls";
        header('Content-Disposition: attachment;filename=' . $filename . ' ');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($myExcel, 'Excel5');
        $objWriter->save('php://output');  
       
        return $this->redirect(['index']);
    }

 

}
