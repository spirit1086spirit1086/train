<?php

namespace app\controllers;

use Yii;

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\AuthItemChild;
use app\models\AuthItem;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * AuthitemchildController implements the CRUD actions for authitemchild model.
 */
class AuthitemchildController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

  
    /**
     * Lists all authitemchild models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->can('superadmin')) 
        { 
            $rules = $this->findModel(); // название правил
            $model = new AuthItemChild();
           
            if ($model->load(Yii::$app->request->post()) ) {
                
                $this->delModel();

                for ($i=0;$i<count($_POST['AuthItemChild']['parent']);$i++)
                {
                    Yii::$app->db->createCommand()->batchInsert('auth_item_child', ['parent', 'child'], 
                    [[ $_POST['AuthItemChild']['parent'][$i],$_POST['AuthItemChild']['child'][$i] ]])->execute();            
                }
               return $this->refresh();     
            } else {                                       
                return $this->render('index', ['model' => $model,'rules'=>$rules]);
            }
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }         
    }




    protected function findModel()
    {
        if (Yii::$app->user->can('superadmin')) 
        { 
                if (($model = AuthItemChild::find()->all()) !== null) {
                    return $model;
                } else {
                    throw new NotFoundHttpException('The requested page does not exist.');
                }
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }         
    }
    
    protected function delModel()
    {
        if (Yii::$app->user->can('superadmin')) 
        { 
                if (($model = AuthItemChild::deleteAll()) !== null) {
                    return $model;
                } else {
                    throw new NotFoundHttpException('The requested page does not exist.');
                }
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }         
    }
    
    
    public function actionAddrule()
    {
        if (Yii::$app->user->can('superadmin')) 
        { 
            $i=rand(100,10000);
            $model = new AuthItemChild();
            $html=Html::dropDownList('AuthItemChild[parent][]',null,$model->DataParent0,['prompt'=>'- Выберите значение -','id'=>'parent'.$i,'data-class'=>'mini','style'=>'display:none']);
            $html.='<span class="mabselect" style="width:200px;display:inline-block;vertical-align:top;margin:3px 3px 3px 0" id="parent'.$i.'">
                  <a id="parent'.$i.'" class="single">
                      <span>- Выберите значение -</span>
                      <div><i></i></div>
                  </a>
                  <div class="combo"><ul id="spsparent'.$i.'" class="results" style="display:none;"></ul></div>
                </span>';
            $html.=Html::dropDownList('AuthItemChild[child][]',null,$model->DataChild0,['prompt'=>'- Выберите значение -','id'=>'child'.$i,'data-class'=>'mini','style'=>'display:none']);
            $html.=' <span class="mabselect" style="width:200px;display:inline-block;vertical-align:top;margin:3px 3px 3px 0" id="child'.$i.'">
                  <a id="child'.$i.'" class="single">
                      <span>- Выберите значение -</span>
                      <div><i></i></div>
                  </a>
                  <div class="combo"><ul id="spschild'.$i.'" class="results" style="display:none;"></ul></div>
                </span> ';
            $html.=Html::a('Удалить','javascript:void(0);',['class'=>'btncancel delrule','data-id'=>$i]);
            echo $html;
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }         

    }   
    
    public function actionDelrule()
    {
        if (Yii::$app->user->can('superadmin')) 
        { 
            Yii::$app->db->createCommand("DELETE FROM auth_item_child WHERE parent = :parent and child=:child")
                         ->bindValue(':parent', $_POST['parent'])
                         ->bindValue(':child', $_POST['child'])
                         ->execute();
        }
        else
        {
          throw new HttpException(403,'Доступ закрыт');            
        }         

    }     
    
}
