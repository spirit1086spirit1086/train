<?php

namespace app\controllers;

use Yii;
use app\models\Price;
use app\models\PriceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;;
/**
 * PriceController implements the CRUD actions for Price model.
 */
class PriceController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Price models.
     * @return mixed
     */
    public function actionIndex()
    {
        
       if ( Yii::$app->user->can('author') )
       { 
            $searchModel = new PriceSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
       }
       else
       {
              throw new HttpException(403,'Доступ закрыт');        
       } 
    }


    /**
     * Creates a new Price model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      
       if ( Yii::$app->user->can('create-post')) 
       { 
            $model = new Price();

            if ($model->load(Yii::$app->request->post()) ) {
                $model->createdBy=Yii::$app->user->id;
                $model->status=1;
                if ($model->save()) 
                {
                    return $this->redirect(['index']);
                }
                else
                {
                    return $this->render('create', ['model' => $model]);
                }
            } else {
                return $this->render('create', ['model' => $model]);
            }
       }
       else
       {
              throw new HttpException(403,'Доступ закрыт');        
       }             
    }

    /**
     * Updates an existing Price model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

       if ( Yii::$app->user->can('update-post', ['model' => $model])) 
       { 
              if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    return $this->redirect(['index']);
              } else {
                    return $this->render('update', ['model' => $model]);
              }
       }
       else
       {
              throw new HttpException(403,'Доступ закрыт');        
       }             
    }

    /**
     * Deletes an existing Price model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
       $model = $this->findModel($id);
       if ( Yii::$app->user->can('delete-post', ['model' => $model])) 
       {
            $ids= explode(',',$id);
            
            for($i=0;$i<count($ids);$i++)
            {
              $this->findModel($ids[$i])->delete();
            }
            return $this->redirect(['index']);
       }
       else
       {
               return $this->redirect(['index']);        
       }             
    }

    /**
     * Finds the Price model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Price the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
       if ( Yii::$app->user->can('author')) 
       {
            if (($model = Price::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
       }
       else
       {
              throw new HttpException(403,'Доступ закрыт');        
       }             
    }
}
