<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tsho".
 *
 * @property integer $id
 * @property string $title
 *
 * @property Schet[] $schets
 */
class Tsho extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tsho';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['createdBy','status'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchets()
    {
        return $this->hasMany(Schet::className(), ['tsho' => 'id']);
    }
}
