<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prichina".
 *
 * @property integer $id
 * @property integer $vagon_id
 * @property integer $spr_id
 * @property integer $tip
 * @property integer $mremont_id
 * @property integer $remont_id
 * @property string $dates
 * @property integer $vvagon_id
 * @property integer $sobstvenik_id
 * @property integer $cenaed
 *
 * @property Mremont $mremont
 * @property Osn $remont
 * @property Sobstvenik $sobstvenik
 * @property Spr $spr
 * @property Vagon $vagon
 * @property Vvagon $vvagon
 */
class Prichina extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prichina';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vagon_id', 'spr_id', 'tip', 'mremont_id', 'remont_id', 'vvagon_id', 'sobstvenik_id','dr_id'], 'integer'],
            [['dates'], 'safe'],
            [['cenaed'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vagon_id' => 'Vagon ID',
            'spr_id' => 'Spr ID',
            'tip' => 'Tip',
            'mremont_id' => 'Mremont ID',
            'remont_id' => 'Remont ID',
            'dates' => 'Dates',
            'vvagon_id' => 'Vvagon ID',
            'sobstvenik_id' => 'Sobstvenik ID',
            'cenaed' => 'Cenaed',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMremont()
    {
        return $this->hasOne(Mremont::className(), ['id' => 'mremont_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemont()
    {
        return $this->hasOne(Osn::className(), ['id' => 'remont_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSobstvenik()
    {
        return $this->hasOne(Sobstvenik::className(), ['id' => 'sobstvenik_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpr()
    {
        return $this->hasOne(Spr::className(), ['id' => 'spr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVagon()
    {
        return $this->hasOne(Vagon::className(), ['id' => 'vagon_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVvagon()
    {
        return $this->hasOne(Vvagon::className(), ['id' => 'vvagon_id']);
    }
}
