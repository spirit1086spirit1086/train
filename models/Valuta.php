<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "valuta".
 *
 * @property integer $id
 * @property string $title
 * @property integer $createdBy
 */
class Valuta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'valuta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'createdBy'], 'required'],
            [['createdBy'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
            'createdBy' => 'Created By',
        ];
    }
}
