<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "vc".
 *
 * @property integer $id
 * @property integer $title
 * @property integer $vvagon_id
 * @property integer $sobstvenik_id
 * @property integer $nkotrakt
 *
 * @property Sobstvenik $sobstvenik
 * @property Vvagon $vvagon
 */
class Vc extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vc';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nvg', 'nkotrakt'], 'required'],
            [['nvg', 'vvagon_id', 'sobstvenik_id', 'nkotrakt','createdBy','status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nvg' => '№ вагона',
            'vvagon_id' => 'Тип вагона',
            'sobstvenik_id' => 'Собственник',
            'nkotrakt' => '№ контракта',
            'createdBy'=>'Автор',
            'status'=>'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSobstvenik()
    {
        return $this->hasOne(Sobstvenik::className(), ['id' => 'sobstvenik_id']);
    }

    public function getDataSobstvenik() 
    { 
        $models=(new \yii\db\Query)->select('*')->from('sobstvenik')->where('status=1')->all();
        return ArrayHelper::map($models, 'id', 'title');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVvagon()
    {
        return $this->hasOne(Vvagon::className(), ['id' => 'vvagon_id']);
    }
    
    public function getDataVvagon() 
    { 
        $models=(new \yii\db\Query)->select('*')->from('vvagon')->where('status=1')->all();
        return ArrayHelper::map($models, 'id', 'title');
    }
    
    
}
