<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "revizia".
 *
 * @property integer $id
 * @property integer $reviziaspr_id
 * @property integer $vagon_id
 * @property integer $tip
 * @property integer $mremont_id
 * @property integer $remont_id
 * @property integer $vvagon_id
 * @property integer $sobstvenik_id
 * @property string $dates
 * @property string $cenaed
 * @property string $ndc
 * @property integer $kol
 *
 * @property Mremont $mremont
 * @property Osn $remont
 * @property Vvagon $vvagon
 * @property Sobstvenik $sobstvenik
 * @property Reviziaspr $reviziaspr
 * @property Vagon $vagon
 */
class Revizia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'revizia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reviziaspr_id', 'vagon_id', 'tip', 'mremont_id', 'remont_id', 'vvagon_id', 'sobstvenik_id', 'kol'], 'integer'],
            [['dates'], 'safe'],
            [['cenaed', 'ndc'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reviziaspr_id' => 'Reviziaspr ID',
            'vagon_id' => 'Vagon ID',
            'tip' => 'Tip',
            'mremont_id' => 'Mremont ID',
            'remont_id' => 'Remont ID',
            'vvagon_id' => 'Vvagon ID',
            'sobstvenik_id' => 'Sobstvenik ID',
            'dates' => 'Dates',
            'cenaed' => 'Cenaed',
            'ndc' => 'Ndc',
            'kol' => 'Kol',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMremont()
    {
        return $this->hasOne(Mremont::className(), ['id' => 'mremont_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemont()
    {
        return $this->hasOne(Osn::className(), ['id' => 'remont_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVvagon()
    {
        return $this->hasOne(Vvagon::className(), ['id' => 'vvagon_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSobstvenik()
    {
        return $this->hasOne(Sobstvenik::className(), ['id' => 'sobstvenik_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviziaspr()
    {
        return $this->hasOne(Reviziaspr::className(), ['id' => 'reviziaspr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVagon()
    {
        return $this->hasOne(Vagon::className(), ['id' => 'vagon_id']);
    }
}
