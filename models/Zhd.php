<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "zhd".
 *
 * @property integer $id
 * @property integer $zhdspr_id
 * @property integer $vagon_id
 * @property integer $tip
 * @property integer $mremont_id
 * @property integer $remont_id
 * @property integer $vvagon_id
 * @property integer $sobstvenik_id
 * @property string $dates
 * @property string $cenaed
 * @property string $ndc
 * @property integer $kol
 *
 * @property Vagon $vagon
 * @property Zhdspr $zhdspr
 */
class Zhd extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zhd';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['zhdspr_id', 'vagon_id', 'tip', 'mremont_id', 'remont_id', 'vvagon_id', 'sobstvenik_id', 'kol'], 'integer'],
            [['dates'], 'safe'],
            [['cenaed', 'ndc'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'zhdspr_id' => 'Zhdspr ID',
            'vagon_id' => 'Vagon ID',
            'tip' => 'Tip',
            'mremont_id' => 'Mremont ID',
            'remont_id' => 'Remont ID',
            'vvagon_id' => 'Vvagon ID',
            'sobstvenik_id' => 'Sobstvenik ID',
            'dates' => 'Dates',
            'cenaed' => 'Cenaed',
            'ndc' => 'Ndc',
            'kol' => 'Kol',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVagon()
    {
        return $this->hasOne(Vagon::className(), ['id' => 'vagon_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZhdspr()
    {
        return $this->hasOne(Zhdspr::className(), ['id' => 'zhdspr_id']);
    }
}
