<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "company".
 *
 * @property integer $id
 * @property string $title
 * @property integer $posrednik_id
 * @property integer $createdBy
 *
 * @property Posrednik $posrednik
 * @property Vagon[] $vagons
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [[ 'createdBy','status'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
            'status' => 'Статус',
            'createdBy' => 'Created By',
        ];
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVagons()
    {
        return $this->hasMany(Vagon::className(), ['vcomp_id' => 'id']);
    }
}
