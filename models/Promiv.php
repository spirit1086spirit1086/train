<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "promiv".
 *
 * @property integer $id
 * @property integer $promivspr_id
 * @property integer $vagon_id
 * @property integer $tip
 * @property integer $mremont_id
 * @property integer $remont_id
 * @property integer $vvagon_id
 * @property integer $sobdtvenik_id
 * @property string $dates
 * @property string $cenaed
 * @property string $ndc
 * @property integer $kol
 *
 * @property Sobstvenik $sobdtvenik
 * @property Mremont $mremont
 * @property Osn $remont
 * @property Vvagon $vvagon
 * @property Promivspr $promivspr
 * @property Vagon $vagon
 */
class Promiv extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promiv';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promivspr_id', 'vagon_id', 'tip', 'mremont_id', 'remont_id', 'vvagon_id', 'sobdtvenik_id', 'kol'], 'integer'],
            [['dates'], 'safe'],
            [['cenaed', 'ndc'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'promivspr_id' => 'Promivspr ID',
            'vagon_id' => 'Vagon ID',
            'tip' => 'Tip',
            'mremont_id' => 'Mremont ID',
            'remont_id' => 'Remont ID',
            'vvagon_id' => 'Vvagon ID',
            'sobdtvenik_id' => 'Sobdtvenik ID',
            'dates' => 'Dates',
            'cenaed' => 'Cenaed',
            'ndc' => 'Ndc',
            'kol' => 'Kol',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSobdtvenik()
    {
        return $this->hasOne(Sobstvenik::className(), ['id' => 'sobdtvenik_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMremont()
    {
        return $this->hasOne(Mremont::className(), ['id' => 'mremont_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemont()
    {
        return $this->hasOne(Osn::className(), ['id' => 'remont_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVvagon()
    {
        return $this->hasOne(Vvagon::className(), ['id' => 'vvagon_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromivspr()
    {
        return $this->hasOne(Promivspr::className(), ['id' => 'promivspr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVagon()
    {
        return $this->hasOne(Vagon::className(), ['id' => 'vagon_id']);
    }
}
