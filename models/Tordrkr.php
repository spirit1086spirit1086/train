<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tordrkr".
 *
 * @property integer $id
 * @property integer $vagon_id
 * @property integer $mremont_id
 * @property integer $pricetip_id
 * @property integer $remont_id
 * @property integer $tip
 * @property integer $vvagon_id
 * @property integer $sobstvenik_id
 * @property string $dates
 * @property string $cenaed
 *
 * @property Price $remont
 * @property Vagon $vagon
 */
class Tordrkr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tordrkr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vagon_id', 'mremont_id', 'pricetip_id', 'remont_id', 'tip', 'vvagon_id', 'sobstvenik_id'], 'integer'],
            [['dates', 'cenaed'], 'required'],
            [['dates'], 'safe'],
            [['cenaed'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vagon_id' => 'Vagon ID',
            'mremont_id' => 'Mremont ID',
            'pricetip_id' => 'Pricetip ID',
            'remont_id' => 'Remont ID',
            'tip' => 'Tip',
            'vvagon_id' => 'Vvagon ID',
            'sobstvenik_id' => 'Sobstvenik ID',
            'dates' => 'Dates',
            'cenaed' => 'Cenaed',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemont()
    {
        return $this->hasOne(Price::className(), ['id' => 'remont_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVagon()
    {
        return $this->hasOne(Vagon::className(), ['id' => 'vagon_id']);
    }
}
