<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tips".
 *
 * @property integer $id
 * @property string $titlename
 * @property integer $createdBy
 *
 * @property Price[] $prices
 */
class Tips extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tips';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['titlename'], 'required'],
            [['createdBy','status'], 'integer'],
            [['titlename'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titlename' => 'Наименование',
            'createdBy' => 'Created By',
            'status'=>'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrices()
    {
        return $this->hasMany(Price::className(), ['pricetip' => 'id']);
    }
    
    
    
}
