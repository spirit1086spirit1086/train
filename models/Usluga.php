<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usluga".
 *
 * @property integer $id
 * @property integer $uslugaspr_id
 * @property integer $vagon_id
 * @property integer $tip
 * @property integer $mremont_id
 * @property integer $remont_id
 * @property integer $vvagon_id
 * @property integer $sobstvenik_id
 * @property string $dates
 * @property string $cenaed
 * @property string $ndc
 * @property integer $kol
 *
 * @property Vagon $vagon
 * @property Uslugaspr $uslugaspr
 */
class Usluga extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usluga';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uslugaspr_id', 'vagon_id', 'tip', 'mremont_id', 'remont_id', 'vvagon_id', 'sobstvenik_id', 'kol'], 'integer'],
            [['dates'], 'safe'],
            [['cenaed', 'ndc'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uslugaspr_id' => 'Uslugaspr ID',
            'vagon_id' => 'Vagon ID',
            'tip' => 'Tip',
            'mremont_id' => 'Mremont ID',
            'remont_id' => 'Remont ID',
            'vvagon_id' => 'Vvagon ID',
            'sobstvenik_id' => 'Sobstvenik ID',
            'dates' => 'Dates',
            'cenaed' => 'Cenaed',
            'ndc' => 'Ndc',
            'kol' => 'Kol',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVagon()
    {
        return $this->hasOne(Vagon::className(), ['id' => 'vagon_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUslugaspr()
    {
        return $this->hasOne(Uslugaspr::className(), ['id' => 'uslugaspr_id']);
    }
}
