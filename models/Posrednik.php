<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posrednik".
 *
 * @property integer $id
 * @property string $title
 * @property integer $createdBy
 *
 * @property Company[] $companies
 */
class Posrednik extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posrednik';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['createdBy','status'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
            'status' => 'Статус',
            'createdBy' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['posrednik_id' => 'id']);
    }
}
