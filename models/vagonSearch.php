<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vagon;

/**
 * vagonSearch represents the model behind the search form about `app\models\vagon`.
 */
class VagonSearch extends vagon
{
    public $mremont;
    public $vc;
    public $vvagon;
    public $sobstvenik;
    public $tsho;
    public $company;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id',  'mremont_id', 'remont_id','nvagon_id', 'vvagona', 'sobstvenik_id', 'nkontr', 'tsho_id','ndc'], 'integer'],
            [['nscheta','dates', 'drab', 'tip', 'dbrak',  'opis', 'cenaed', 'mremont','vc','vvagon','sobstvenik','tsho','company','vcomp_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    public function Omas() 
    {
                    // собираем массив тор др кр и пвсе посредники 
            $omas=[1=>'ТОР',2=>'ДР',3=>'КР'];   
            $m = (new \yii\db\Query())->select('id,title')->from('posrednik')->all(); // получаем пункты посреднников
            foreach ($m as $value) 
            {
              $omas[]=$value['title'];   
            }
            // конец сбора массива
       return $omas;     
    }
    
    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vagon::find();
        $query->joinWith(['mremont','vc','vvagon','sobstvenik','tsho']); // навзавние связи ,'company'

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => Yii::$app->params['pageSize']],
        ]);

        $dataProvider->sort->attributes['mremont'] = [
            'asc' => ['mremont.title' => SORT_ASC],
            'desc' => ['mremont.title' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['vc'] = [
            'asc' => ['vc.nvg' => SORT_ASC],
            'desc' => ['vc.nvg' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['vvagon'] = [
            'asc' => ['vvagon.title' => SORT_ASC],
            'desc' => ['vvagon.title' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['sobstvenik'] = [
            'asc' => ['sobstvenik.title' => SORT_ASC],
            'desc' => ['sobstvenik.title' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['tsho'] = [
            'asc' => ['tsho.title' => SORT_ASC],
            'desc' => ['tsho.title' => SORT_DESC],
        ];
/*        $dataProvider->sort->attributes['company'] = [
            'asc' => ['company.title' => SORT_ASC],
            'desc' => ['company.title' => SORT_DESC],
        ];
*/
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if(date('Y-m-d', strtotime($this->dates))=='1970-01-01')
        {
           $this->dates=null; 
        }
        else
        {
            $this->dates=date('Y-m-d', strtotime($this->dates));
            //$query=' && (dates>="'.$date1.'" && dates<="'.$date2.'")'; 
        }

        $query->andFilterWhere([
            'id' => $this->id,
            //'nscheta' => $this->nscheta,
            'dates' =>$this->dates,
            'tip' => $this->tip,
            'remont_id'=>$this->remont_id,
            'vagon.mremont_id' => $this->mremont_id,
            'nvagon_id' => $this->nvagon_id,
            'vvagona' => $this->vvagona,
            'vagon.sobstvenik_id' => $this->sobstvenik_id,
            //'nkontr' => $this->nkontr,
            'drab' => $this->drab,
            'dbrak' => $this->dbrak,
            'vcomp_id' => $this->vcomp_id,
            'tsho_id' => $this->tsho_id,
            'ndc' => $this->ndc,
        ]);

     
      $query->andFilterWhere(['like', 'opis', $this->opis])
            ->andFilterWhere(['like', 'cenaed', $this->cenaed])
            ->andFilterWhere(['like', 'mremont.title', $this->mremont])
            ->andFilterWhere(['like', 'vc.nvg', $this->vc])
            ->andFilterWhere(['like', 'vvagon.title', $this->vvagon])
            ->andFilterWhere(['like', 'sobstvenik.title', $this->sobstvenik])
            ->andFilterWhere(['like', 'tsho.title', $this->tsho])
            //->andFilterWhere(['like', 'company.title', $this->company])
            ->andFilterWhere(['like', 'nscheta',$this->nscheta])
            ->andFilterWhere(['like', 'nkontr',$this->nkontr])
            ->orderby('id DESC');

        return $dataProvider;
    }
}
