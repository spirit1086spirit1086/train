<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vc;

    
/**
 * vcSearch represents the model behind the search form about `app\models\vc`.
 */
class VcSearch extends vc
{
    
    public $sobstvenik;
    public $vvagon;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nvg', 'vvagon_id', 'sobstvenik_id', 'nkotrakt','createdBy','status'], 'integer'],
            [['sobstvenik','vvagon'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vc::find();
        $query->joinWith(['sobstvenik','vvagon']); // навзавние связи

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => Yii::$app->params['pageSize']],
        ]);

       $dataProvider->sort->attributes['sobstvenik'] = [
            'asc' => ['sobstvenik.title' => SORT_ASC],
            'desc' => ['sobstvenik.title' => SORT_DESC],
        ];

       $dataProvider->sort->attributes['vvagon'] = [
            'asc' => ['vvagon.title' => SORT_ASC],
            'desc' => ['vvagon.title' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'vvagon_id' => $this->vvagon_id,
            'sobstvenik_id' => $this->sobstvenik_id,
            'nkotrakt' => $this->nkotrakt,
            'createdBy' => $this->createdBy,
        ])
            ->andFilterWhere(['like', 'sobstvenik.title', $this->sobstvenik])
            ->andFilterWhere(['like', 'vvagon.title', $this->vvagon])
            ->andFilterWhere(['like', 'nvg', $this->nvg])
            ->orderby('id DESC');

        return $dataProvider;
    }
}
