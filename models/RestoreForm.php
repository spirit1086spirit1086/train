<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class RestoreForm extends Model
{
    public $email;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email'], 'required'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'E-mail',
        ];
    }  
    

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function Restoremail()
    {
        $email=User::findOne(['email' => $this->email]);
        if (!$email) 
        {
           $this->addError('email', 'E-mail нет в базе');
        }
        else
        {
            return $email;
        }
    }


}
