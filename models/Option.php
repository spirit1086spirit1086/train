<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "option".
 *
 * @property integer $id
 * @property string $company
 * @property string $adress
 * @property integer $kol
 * @property string $bin
 */
class Option extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'option';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company', 'adress', 'kol', 'bin','tip'], 'required'],
            [['kol','tip'], 'integer'],
            [['company', 'adress', 'bin'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company' => 'Компания',
            'adress' => 'Адрес',
            'kol' => 'Кол-во',
            'bin' => 'Бин',
            'tip'=>'Тип',
        ];
    }
}
