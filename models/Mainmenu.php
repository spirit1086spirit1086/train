<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "mainmenu".
 *
 * @property integer $id
 * @property string $title
 * @property string $link
 * @property string $cclass
 * @property integer $parent
 *
 * @property Mainmenu $parent0
 * @property Mainmenu[] $mainmenus
 */
class Mainmenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mainmenu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'],'required'],
            [['parent','position'], 'integer'],
            [['link'],'default', 'value'=>'#'],
            [['title', 'link', 'cclass'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'link' => 'Ссылка',
            'cclass' => 'Класс иконки',
            'parent' => 'Родитель',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent0()
    {
        return $this->hasOne(Mainmenu::className(), ['id' => 'parent']);
    }

   
    public function getDataParent0() { 
        $models = Mainmenu::find()->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainmenus()
    {
        return $this->hasMany(Mainmenu::className(), ['parent' => 'id']);
    }
    
    
    
}
