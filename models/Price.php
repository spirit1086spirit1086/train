<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "price".
 *
 * @property integer $id
 * @property integer $mremont_id
 * @property integer $pricetip
 * @property integer $code
 * @property string $titlename
 * @property string $cena
 *
 * @property Tips $pricetip0
 * @property Mremont $mremont
 */
class Price extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mremont_id',  'code','createdBy','status'], 'integer'],
            [['titlename','pricetip'], 'required'],
            [['titlename', 'cena'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mremont_id' => 'Место ремонта',
            'pricetip' => 'Типы',
            'code' => 'Код',
            'titlename' => 'Наименование',
            'cena' => 'Цена',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPricetip0()
    {
        return $this->hasOne(Tips::className(), ['id' => 'pricetip']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMremont()
    {
        return $this->hasOne(Mremont::className(), ['id' => 'mremont_id']);
    }
    
    public function getDataMremont() 
    { 
        $models=(new \yii\db\Query)->select('*')->from('mremont')->where('status=1')->all();
        return ArrayHelper::map($models, 'id', 'title');
    }    

    public function getTips()
    {
        return $this->hasOne(Tips::className(), ['id' => 'pricetip']);
    }
    
    public function getDataTips() 
    { 
        $models=(new \yii\db\Query)->select('*')->from('tips')->where('status=1')->all();
        return ArrayHelper::map($models, 'id', 'titlename');
    }    

}
