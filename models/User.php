<?php
namespace app\models;
use Yii;
use yii\validators\CompareValidator;
use yii\helpers\ArrayHelper;
    
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{

    public $password_repeat;  
    public $authKey;
    public $accessToken;

    public static function tableName()
    {
        return 'users';
    }
    
    public function rules()
    {
        return [
            [['username', 'name'], 'required'],
            [['lang_id', 'group_id','createdBy'], 'integer'],
            [['username', 'name', 'password','email', 'active'], 'string', 'max' => 255],
            [['password'], 'string', 'min' => 6],
            [['group_id'],'default', 'value' => 3],
            [['password_repeat'], 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли не совпадают','skipOnEmpty'=>false],
            [['password_repeat','lastlogin'], 'safe'],
            /**
             *  при регистрации или создании пользователя делаем пароль обязательным полем, 
             *  при обновлении не обязательным у меня выводится при обновлении пустое поле
             *  чтобы поле не перезаписало хэш пароля
             **/
            [['password_repeat','password'], 'required', 'on'=>'register'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Логин',
            'name' => 'ФИО',
            'password' => 'Пароль',
            'lang_id' => 'Язык',
            'group_id' => 'Группа',
            'active' => 'Статус',
            'createdBy'=>'Автор',
            'password_repeat'=>'Повторите пароль',
        ];
    }  
    
   /**
    * **************** Связи ******************* 
    * */
    public function getGroups()
    {
       return $this->hasOne(Groups::className(), ['id' => 'group_id']);
    }

    public function getDataGroups() { // все группы видны только superadmin'у
        if (Yii::$app->user->id!=1) 
        {
            $sql = 'SELECT * FROM groups WHERE id!=:id and id!=:id2';
            $models = Groups::findBySql($sql, [':id' => 1,':id2' => 2])->asArray()->all();
        }
        else
        {
            $models = Groups::find([])->asArray()->all();
        }
        return ArrayHelper::map($models, 'id', 'groupname');
    }
    
    
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'createdBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['createdBy' => 'id']);
    }
    
      
    /**
     * ************************************************
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
         return static::findOne(['id' => $id]);
    }


    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
         return static::findOne(['username' => $username]);
    }


  public function validatePassword($password)
    {
        return $this->password === md5($password);
    }
    
    
   //********  сохранение
    public function beforeSave($insert)
    {
        /**
         *** Пустой пароль мы получим только при обновлении пользователя,
         ** на это имеет право только зарегистрированый пользователь с привилегиями
         *  далее вытаскиваем из базы пароль этого пользователя и присваиваем переменой не шифруя его
         **/ 
        if ($this->password!='')
        {
           return $this->password = $this->hashPassword($this->password);
        }
        else
        {
            $user=User::findOne($this->getId());
           return $this->password = $user->password; 
        }
    }

    public function hashPassword($password)
    {
       return md5($password);
    } 

/*********************/
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }


    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

 
}
