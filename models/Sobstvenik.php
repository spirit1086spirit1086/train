<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "sobstvenik".
 *
 * @property integer $id
 * @property string $title
 * @property integer $createdBy
 *
 * @property Clean[] $cleans
 * @property Detail[] $details
 * @property Prichina[] $prichinas
 * @property Promiv[] $promivs
 * @property Revizia[] $revizias
 * @property Vagon[] $vagons
 * @property Vc[] $vcs
 */
class Sobstvenik extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sobstvenik';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['createdBy','type','status'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
            'status' => 'Статус',
            'type'=>'Связь',
            'createdBy' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getType()
    {
        return $this->hasone(Sobstvenik::className(), ['id' => 'type']);
    }

    public function getDataType() { 
        $models = (new \yii\db\Query())->select('*')->from('sobstvenik')->where('status=1')->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    public function getCleans()
    {
        return $this->hasMany(Clean::className(), ['sobstvenik_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetails()
    {
        return $this->hasMany(Detail::className(), ['sobstvenik_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrichinas()
    {
        return $this->hasMany(Prichina::className(), ['sobstvenik_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromivs()
    {
        return $this->hasMany(Promiv::className(), ['sobstvenik_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRevizias()
    {
        return $this->hasMany(Revizia::className(), ['sobstvenik_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVagons()
    {
        return $this->hasMany(Vagon::className(), ['sobstvenik_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVcs()
    {
        return $this->hasMany(Vc::className(), ['sobstvenik_id' => 'id']);
    }
}
