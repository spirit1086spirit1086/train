<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Mremont;

/**
 * MremontSearch represents the model behind the search form about `app\models\Mremont`.
 */
class MremontSearch extends Mremont
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','createdBy','valuta','status'], 'integer'],
            [['title','ndc'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Mremont::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'valuta' => $this->valuta,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
              ->orderby('ID DESC');

        return $dataProvider;
    }
}
