<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "vagon".
 *
 * @property integer $id
 * @property integer $nscheta
 * @property string $dates
 * @property integer $tip
 * @property integer $mremont
 * @property integer $nvagon
 * @property integer $vvagona
 * @property integer $sobstvenik
 * @property integer $nkontr
 * @property string $drab
 * @property string $dbrak
 * @property integer $vcomp
 * @property integer $tsho
 * @property integer $spr
 * @property string $detail
 * @property string $opis
 * @property string $cenaed
 * @property string $cena
 * @property string $ndc
 * @property string $itog
 *
 * @property Clean[] $cleans
 * @property Promiv[] $promivs
 * @property Revizia[] $revizias
 * @property Company $vcomp0
 * @property Sobstvenik $sobstvenik0
 * @property Spr $spr0
 * @property Tsho $tsho0
 * @property Vvagon $vvagona0
 * @property Mremont $mremont0
 * @property Zhd[] $zhds
 */
class Vagon extends \yii\db\ActiveRecord
{
    public $dropremont;
    public $attr;
    public $vid_vagon;
    public $n_sobstvenik;
    public $spr2615;
    public $detailt;   
    public $reviz;
    public $promiv;
    public $zhdtarif;
    public $uborka;
    public $ndcupd;
    public $dr_id;
    public $usluga;
    public $dopusluga;
    public $tordrkr;
    public $omas;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vagon';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tip','dates','mremont_id'], 'required'],
            [['remont_id','tip', 'mremont_id', 'nvagon_id', 'vvagona', 'sobstvenik_id', 'nkontr', 'tsho_id', 'tsho_id','createdBy','remont_id','ndc','vu22kol','aktkol'], 'integer'],
            [['dates', 'drab', 'dbrak','vcomp_id'], 'safe'],
            [['opis','vu22','vu23','vu36','akt','cenaed'], 'string'],
            [[ 'nscheta','cenaed'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nscheta' => '№ Счет-фактуры',
            'dates' => 'Дата',
            'tip' => 'ТОР/ДР/КР',
            'remont_id' => 'Причина',
            'mremont_id' => 'Место ремонта',
            'nvagon_id' => '№ вагона',
            'vvagona' => 'Вид вагона',
            'sobstvenik_id' => 'Собственник',
            'nkontr' => '№ контракта',
            'drab' => 'Дата работы',
            'dbrak' => 'Дата заброковки',
            'vcomp_id' => 'Выставление на компании',
            'tsho_id' => 'Возмещение от ТШО',
            'opis' => 'Описание',
            'cenaed' => 'Цена за ед.',
            'ndc' => 'НДС,%',
            'vu23'=>'Форма ВУ 23',
            'vu22'=>'Форма ВУ 22',
            'vu22kol'=>'Кол-во',
            'vu36'=>'Форма ВУ 36',
            'akt'=>'АКТ',
            'aktkol'=>'Кол-во',
        ];
    }


    public function getPrice()
    {
        return $this->hasOne(Price::className(), [ 'id' => 'titlename']);
    }

    public function getDataPrice() 
    { 
        $models =  Price::find([])->asArray()->all();
        return ArrayHelper::map($models, 'id', 'titlename');
    }

    public function getDataValuta() 
    { 
        $models = Valuta::find([])->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    } 
    
    public function getDataOmas() 
    {
        $omas=[ 
                ['id'=>1,'title'=>'ТОР'],
                ['id'=>2,'title'=>'ДР'],
                ['id'=>3,'title'=>'КР']
              ];   
        $m = (new \yii\db\Query)->select('*')->from('posrednik')->where('status=1')->all(); // получаем пункты посреднников
        $kol=count($omas)+1;
        foreach ($m as $value) 
        {
              $omas[]=['id'=>$kol,'title'=>$value['title']];
              $kol++;
        }
       return ArrayHelper::map($omas, 'id', 'title'); 
    }
            

    
    public function getDataDetailspr() 
    { 
        $models =  Detailspr::find([])->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }
    

    
    public function getTips()
    {
        return $this->hasOne(Tips::className(), [ 'id' => 'titlename']);
    }

    public function getDataTips() 
    { 
        $models = (new \yii\db\Query)->select('*')->from('tips')->where('status=1')->all();
        return ArrayHelper::map($models, 'id', 'titlename');
    }


    public function getVc()
    {
        return $this->hasOne(Vc::className(), [ 'id' => 'nvagon_id']);
    }

    public function getDataVc() 
    { 
        $models = (new \yii\db\Query)->select('*')->from('vc')->where('status=1')->all();
        return ArrayHelper::map($models, 'id', 'nvg');
    }


    public function getDataSpr() 
    { 
        $models = Spr::find([])->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCleans()
    {
        return $this->hasMany(Clean::className(), ['vagon_id' => 'id']);
    }

    public function getDataCleanspr() 
    { 
        $models = Cleanspr::find([])->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromivs()
    {
        return $this->hasMany(Promiv::className(), ['vagon_id' => 'id']);
    }

    public function getDataPromivspr() 
    { 
        $models = Promivspr::find([])->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataRevizia() 
    { 
        $models = Reviziaspr::find([])->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    public function getRevizias()
    {
        return $this->hasMany(Revizia::className(), ['vagon_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
   /* public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'vcomp_id']);
    }*/

    public function getDataCompany() 
    { 
        $models = (new \yii\db\Query)->select('*')->from('company')->where('status=1')->all();
        return ArrayHelper::map($models, 'id', 'title');
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTsho()
    {
        return $this->hasOne(Tsho::className(), ['id' => 'tsho_id']);
    }

    public function getDataTsho() 
    { 
        $models = (new \yii\db\Query)->select('*')->from('tsho')->where('status=1')->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSobstvenik()
    {
        return $this->hasOne(Sobstvenik::className(), ['id' => 'sobstvenik_id']);
    }

    public function getDataSobstvenik() 
    { 
        $models = Sobstvenik::find([])->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVvagon()
    {
        return $this->hasOne(Vvagon::className(), ['id' => 'vvagona']);
    }

    public function getDataVvagon() 
    { 
        $models = Vvagon::find([])->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMremont()
    {
        return $this->hasOne(Mremont::className(), ['id' => 'mremont_id']);
    }

    public function getDataMremont() 
    { 
        $models = (new \yii\db\Query)->select('*')->from('mremont')->where('status=1')->all();
        return ArrayHelper::map($models, 'id', 'title');
    }
   
    public function getDataPosrednik() 
    { 
        $models = Posrednik::find([])->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }
   
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZhds()
    {
        return $this->hasMany(Zhd::className(), ['vagon_id' => 'id']);
    }

    public function getDataZhdspr() 
    { 
        $models = Zhdspr::find([])->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }
    
    public function getDetails()
    {
        return $this->hasMany(Detail::className(), ['vagon_id' => 'id']);
    }

    public function getPrichinas()
    {
        return $this->hasMany(Prichina::className(), ['vagon_id' => 'id']);
    }


    public function getTordrkrs()
    {
        return $this->hasMany(Tordrkr::className(), ['vagon_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUslugas()
    {
        return $this->hasMany(Usluga::className(), ['vagon_id' => 'id']);
    }

    public function getDataUslugaspr() 
    { 
        $models = Uslugaspr::find([])->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    public function getDopuslugas()
    {
        return $this->hasMany(Dopusluga::className(), ['dopuslugaspr_id' => 'id']);
    }

    public function getDataDopUslugaspr() 
    { 
        $models = Dopuslugaspr::find([])->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }
    
    
    
    
}
