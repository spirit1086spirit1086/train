<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "mremont".
 *
 * @property integer $id
 * @property string $title
 *
 * @property Vagon[] $vagons
 */
class Mremont extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mremont';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title','valuta','ndc'], 'required'],
            [['createdBy','valuta','status'], 'integer'],
            [['title','ndc'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
            'status' => 'Статус',
            'valuta'=>'Валюта',
            'ndc'=>'Ндс,%'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVagons()
    {
        return $this->hasMany(Vagon::className(), ['mremont' => 'id']);
    }
    
    public function getDataValuta() 
    { 
        $models = Valuta::find([])->asArray()->all();
        return ArrayHelper::map($models, 'id', 'title');
    }
  
}
