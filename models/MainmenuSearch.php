<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Mainmenu;

/**
 * MainmenuSearch represents the model behind the search form about `app\models\mainmenu`.
 */
class MainmenuSearch extends mainmenu
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent', 'position'], 'integer'],
            [['title', 'link', 'cclass'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Mainmenu::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => Yii::$app->params['pageSize']],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
              'id'=>$this->title,
            //'id' => $this->id, 
            'parent' => $this->parent, // чтобы выборка была по главному меню в фильтре и по parent
            'position' => $this->position,
        ]);
        

        //$query->andFilterWhere(['like', 'title', $this->title])
                $query->andFilterWhere(['like', 'link', $this->link])
                  ->andFilterWhere(['like', 'cclass', $this->cclass]);

        return $dataProvider;
    }
}
