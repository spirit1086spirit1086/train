<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserSearch represents the model behind the search form about `app\models\user`.
 */
class UserSearch extends user
{
    
    public $groups;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'lang_id', 'group_id','createdBy'], 'integer'],
            [['username', 'name', 'password', 'groups', 'email', 'active','lastlogin'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();
        $query->joinWith(['groups']); // навзавние связи

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => Yii::$app->params['pageSize']],
        ]);

       $dataProvider->sort->attributes['groups'] = [
            'asc' => ['groups.groupname' => SORT_ASC],
            'desc' => ['groups.groupname' => SORT_DESC],
        ];


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'lang_id' => $this->lang_id,
            'group_id' => $this->group_id,
            'createdBy' => $this->createdBy,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'groups.groupname', $this->groups])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'active', $this->active])
            ->orderby('id DESC');

        return $dataProvider;
    }
}
