<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\restoreform */
/* @var $form ActiveForm */
$session = Yii::$app->session;
$this->title = 'АИС - восстановление пароля';
?>
 <div id="frmwrap">   
     
     <?php if ($session->getFlash('sending email')) {  ?>
          <div id="goodmessage"><?php echo $session->getFlash('sending email'); ?></div> 
     <?php } ?>
     <?= Html::a('&larr;Назад',['index'],['id'=>'goback']) ?>
     <div id="auth_form">
        <span class="big">АИС</span>
        <span class="small">
            <div>Автоматизированная<br/>информационная<br/>система</div>
        </span>
        <h3>Восстановление пароля</h3> 
    <?php $form = ActiveForm::begin(['id' => 'restore-form']); ?>
        <div class="bl_rel">
            <i id="email_ico"></i> 
            <?= $form->field($model, 'email')->label(false) ?>
        </div>
        <div class="frm-group">
            <?= Html::submitButton('Отправить', ['class' => 'btn_login']) ?>
        </div>
    <?php ActiveForm::end(); ?>
    </div> 
   <div id="copyright">ver 1.0 / Copyright 2015.  <?= Html::a('АМТех', 'javascript:void(0);', ['id' => 'registr']) ?> - студия разработки</div>   
   <div id="studio" >Наши контакты: <br/>Тел: +7 (775) 521-49-74<br />E-mail: spirit1086@mail.ru</div>
</div><!-- site-restore -->
