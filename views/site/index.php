<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */
if (Yii::$app->user->isGuest) { 
   $title='Авторизация';
} else {
   $title='Панель управления';
}   
$this->title = $title;

?>
<?php if (!Yii::$app->user->isGuest) {  ?>
<div id="bl100">
    <div class="inlineblock bl70">
        <div id="graph"></div>
        <span class="quickpanel">
            <div class="title">Панель быстрого доступа</div>
            <div>
            <?php echo Html::a('<span class="bitbtn"><div><i id="qschet"></i></div>Счет-фактуры</span>', ['vagon/index'])  ?>
            <?php echo Html::a('<span class="bitbtn"><div><i id="priceico"></i></div>Прайс</span>', ['price/index'])  ?>
            <?php echo Html::a('<span class="bitbtn"><div><i id="repkvart"></i></div>Отчет квартальный</span>', ['report/index'])  ?>
            <?php echo Html::a('<span class="bitbtn"><div><i id="repkvart"></i></div>Детализация счет-фактур</span>', ['schet/index'])  ?>    
            <?php echo Html::a('<span class="bitbtn"><div><i id="usersm"></i></div>Пользователи</span>', ['user/index'])  ?>
            </div>
        </span>
        <span class="quickschet">
            <div class="title">Последние счет-фактуры</div>
            <div>
            <?php  
                foreach ($last as $value) 
                {
                    echo Html::a($vagon->DataPrice[$value['remont_id']], ['vagon/update','id'=>$value['id']]);          
                } 
            ?>
            </div>    
        </span>
    </div>

    <div class="inlineblock bl29">
        <div>
            <div class="title aboutcompany">О Компании</div>
            <div class="blb"><b><?php echo $option->company;?></b>
            Численность: <?php echo $option->kol;?> чел.<br/> 
            <?php echo $option->adress;?><br/>
            БИН: <?php echo $option->bin;?>
            </div>
        </div>

        <div>
            <div class="title aboutsystem">О системе</div>
            <div class="blb"><b>Проект разработан на базе:<br/> Yii Framework 2</b>
            Последнее обновление: 25.05.2015<br/>
            Количество пользователей: неограничено<br/><br/>
            Разработчик: ИП Мамбетов<br/>
            E-mail: <a href="mailto:spirit1086@mail.ru">spirit1086@mail.ru</a><br/>
            Тел.: +7 (775) 521-49-74
           </div>    
        </div>
        
        <div>
            <div class="title aboutmonitor">Мониторинг событий</div>
            <div class="blb">Сегодня добавлено: <i><?php echo $this->context->Colschet('today') ?></i><br/>
            Всего счет-фактур: <i><?php echo $this->context->Colschet('all') ?></i>  <br/><br/>
            Дата последнего входа: <i><?php echo $this->context->Lastlogin(Yii::$app->user->id) ?></i>
            </div> 
       </div>
    </div>
</div>
<?php } else {    ?>
  <div id="frmwrap">   
     <div id="auth_form">
         <div id="logoico"></div>
        
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],
        ]); ?>
        <div class="bl_rel">
        <i id="user_ico"></i> 
        <?= $form->field($model, 'username')->label(false) ?>
        </div>
        <div class="bl_rel">
        <i id="lock"></i> 
        <?= $form->field($model, 'password')->passwordInput()->label(false) ?>
        </div>

        <span class="checkbox">
            <label>
             <input type="hidden" name="LoginForm[rememberMe]" value="0"/>
             <input type="checkbox" id="loginform-rememberme" class="ch" name="LoginForm[rememberMe]" value="1" checked="checked"  />
              Запомнить меня
           </label>
           <p class="help-block help-block-error"></p>
        </span>
    
        <?= Html::submitButton('Войти', ['class' => 'btn_login', 'name' => 'login-button']) ?> 

         <div id="auth_opcii">
             <?= Html::a('Восстановить пароль', ['restore'], ['id' => 'restore']) ?>
         </div>

        <?php ActiveForm::end(); ?>
     </div>
    <div id="copyright">ver 1.0 / Copyright 2015.  <?= Html::a('АМТех', 'javascript:void(0);', ['id' => 'registr']) ?> - студия разработки</div>
    <div id="studio" >Наши контакты: <br/>Тел: +7 (775) 521-49-74<br />E-mail: spirit1086@mail.ru</div>
  </div>
<?php }  ?>
