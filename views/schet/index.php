<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use dosamigos\datepicker\DatePicker;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\bootstrap\Alert;
/* @var $this yii\web\View */
$this->title = 'Отчеты счет-фактуры';
$this->params['breadcrumbs'][] = $this->title;
date_default_timezone_set('Asia/Almaty');
 (isset($_POST['firm'])) ? $firm=$_POST['firm'] : $firm='';
 (isset($_POST['date1'])) ? $date1=$_POST['date1'] : $date1=''; 
 (isset($_POST['date2'])) ? $date2=$_POST['date2'] : $date2='';
?>
<h1><?php echo $this->title ?></h1>

<?php echo Alert::widget ( [
    'options' => ['class' => 'alert-info'],
    'body' => 'Формируйте отчет по одной организации выбрав название из списка или  по всем организациям выбрав пункт <b>"Все организации"</b>'
] ); ?>

 <?= Html::beginForm(['index'],'post',['id'=>'report']) ?>
    
   <span class="report-bl">
        <?= Html::label('Место ремонта','mremont') ?>
        <?= Html::dropDownList('firm',$firm,array_replace($model->DataMremont,['all'=>'Все организации'] ),['prompt'=>'- Выберите значение -','data-title'=>'Место ремонта','class'=>'valuta','id'=>'firm','data-class'=>'medium2'])    ?>
   </span>

   <span class="report-bl">
        <?= Html::label('Дата начала','date1',['style'=>'margin-bottom:7px']) ?>
        <?= DatePicker::widget(['name' => 'date1','value' => $date1,'template' => '{input}{addon}','language' => 'ru','options'=>['class'=>'form-control small-input'],'clientOptions' => ['todayHighlight'=>true,'autoclose' => true,'format' => 'dd-mm-yyyy']]);?>
   </span>

   <span class="report-bl">
        <?= Html::label('Дата завершения','date2',['style'=>'margin-bottom:7px']) ?>
        <?= DatePicker::widget(['name' => 'date2','value' => $date2,'template' => '{input}{addon}','language' => 'ru','options'=>['class'=>'form-control small-input'],'clientOptions' => ['todayHighlight'=>true,'autoclose' => true,'format' => 'dd-mm-yyyy']]);?>
   </span>

   <span class="report-bl">
        <?= Html::Button('<i class="search_ico"></i>', ['class' => 'gbtnins','id'=>'reportbtn']) ?>
        <?= Html::a('<i class="delete_ico"></i>',['index'],['class' => 'gbtnins']) ?> 
   </span>
  
  <?= Html::endForm() ?>

   <div class="noselector"> 
  <?php
     if (!empty($report)) 
     { 
      echo Html::a(Html::img(Url::toRoute('@web/images/excel.png'),['id'=>'excel']).' Скачать отчет',['save','id'=>$firm,'date1'=>$date1,'date2'=>$date2]).'<br/><br/>'; 
      echo '<div id="tabscroll">'.Tabs::widget(['id'=>'vtabs','items' =>$report]).'</div>';
     }
    ?>
    </div>