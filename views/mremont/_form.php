<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap\Alert;
/* @var $this yii\web\View */
/* @var $model app\models\Mremont */
/* @var $form yii\widgets\ActiveForm */
?>

<?php //echo Alert::widget ( ['options' => ['class' => 'alert-warning'], 'body' => 'По умолчанию валюта в <b>тенге</b>.'] );?>

<div class="item-form">
    <?= Html::a('Назад',['index'],['class'=>'goback']) ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'valuta')->dropDownList($model->DataValuta,['prompt'=>'- Выберите валюту -','data-class'=>'mini']) ?>

    <?= $form->field($model, 'ndc')->textInput(['class' =>'form-control small']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить<i class="add"></i>' : 'Сохранить<i class="save_ico"></i>', ['class' => $model->isNewRecord ? 'gbtnins' : 'gbtnins']) ?>
        <?= Html::a('Отмена<i class="cancel_ico"></i>', ['index'], ['class' => 'gbtncancel ']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>
