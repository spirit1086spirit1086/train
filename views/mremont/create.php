<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Mremont */

$this->title = 'Создать место ремонта';
$this->params['breadcrumbs'][] = ['label' => 'Место ремонта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mremont-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
