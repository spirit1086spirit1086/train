<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mremont */

$this->title = 'Обновить место ремонта: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Место ремонта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="mremont-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
