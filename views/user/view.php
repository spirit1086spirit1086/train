<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\user */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'username',
            'name',
            //'lang_id',
            'groups.groupname',
            //'active',
            [
              'attribute'=>'active',     
              //'name'=>'status1',
              'format'=>'html',
              'value'=> $model->active == 1 ? "активен" : "не активен" ,                  
            ],
            'createdBy',
            'email:email',
        ],
    ]) ?>

</div>
