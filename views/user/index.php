<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
Url::remember();
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="form-griedview">
     <div class="tbl-griedview">
       <?= Html::a('Добавить<i class="add"></i>', ['create'], ['class' => 'gbtnins']) ?>
       <?= Html::a('Редактировать<i class="edit"></i>', [null], ['class' => 'gbtnedit','data-lnk'=>'update?id=']) ?>
       <?= Html::a('Удалить<i class="delete"></i>', [null], ['class' => 'gbtncancel','title'=>'удалить','data-confirm'=>'Вы уверены, что хотите удалить этот элемент?','data-method'=>'post','data-lnk'=>'/user/delete?id=']) ?>
       <?= Html::a('Сбросить фильтр<i class="filter"></i>', ['index'], ['class' => 'btnfilter']) ?>
       
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
                [
                  'class' => 'yii\grid\CheckboxColumn',
                  'options'=>['width'=>'1%'],
                  'checkboxOptions' =>['class'=>'chk']
                ],
            //'id',
            'username',
            'name',
            //'active',
                [
                  'attribute'=>'active',     
                  //'name'=>'status1',
                  'format'=>'raw',
                  'value'=> function ($data)  {return $data->active == 1 ?  '<i class="u_active" data-tip="user" data-value="0" data-id="'.$data->id.'"></i>'  : '<i class="u_unactive" data-tip="user" data-value="1" data-id="'.$data->id.'"></i>'; } ,                  
                  'headerOptions'=>['class'=>'w5'],
                  'contentOptions'=>['class'=>'w5'] 
                ],
            //'password',
            //'lang_id',
            //'group_id',
                [
                  'attribute' => 'groups',
                  'label'=>'Группа',
                  'value' => 'groups.groupname',
                  'headerOptions'=>['class'=>'w11'],
                  'contentOptions'=>['class'=>'w11'] 
                ], 
            //'createdBy',
            //'email:email',
                [
                  'attribute' => 'author',
                  'label'=>'Автор',
                  'value' => 'author.username',
                  'headerOptions'=>['class'=>'w11'],
                  'contentOptions'=>['class'=>'w11'] 
                ],
                [
                 'class' => 'yii\grid\ActionColumn',
                 'template' => '{view}',
                 /*'buttons'=>[
                                'update' => function ($url, $model, $key) {
                                        return  Html::a('Update', ['site/index']);
                                    }                 
                            ],*/
                ],
        ],
    ]); ?>
    </div>
  </div>
</div>
