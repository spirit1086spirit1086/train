<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\user */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-form">
    <?= Html::a('Назад',['index'],['class'=>'goback']) ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
    
    <?= $form->field($model, 'password')->passwordInput(['maxlength' => 255,'value'=>'']) ?>

    <?= $form->field($model, 'password_repeat')->passwordInput(['maxlength' => 255]) ?>

    <?php // $form->field($model, 'lang_id')->textInput() ?>

    <?= $form->field($model, 'group_id')->dropDownList($model->DataGroups,['prompt'=>'- Выберите значение -']) ?>

    <?= $form->field($model, 'active')->checkbox(['class'=>'fch']) ?>

    <?php //$form->field($model, 'createdBy')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить<i class="add"></i>' : 'Сохранить<i class="save_ico"></i>', ['class' => $model->isNewRecord ? 'gbtnins' : 'gbtnins']) ?>
        <?= Html::a('Отмена<i class="cancel_ico"></i>', ['user/index'], ['class' => 'gbtncancel ']) ?>

    </div>


    <?php ActiveForm::end(); ?>

</div>
