<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Posrednik */

$this->title = 'Обновить посредника: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Посредники', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="posrednik-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
