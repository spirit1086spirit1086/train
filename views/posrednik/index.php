<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\filters\VerbFilter;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PosrednikSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Посредники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posrednik-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   <div class="form-griedview">
     <div class="tbl-griedview">
       <?= Html::a('Добавить<i class="add"></i>', ['create'], ['class' => 'gbtnins']) ?>
       <?= Html::a('Редактировать<i class="edit"></i>', [null], ['class' => 'gbtnedit','data-lnk'=>'update?id=']) ?>
       <?php if (Yii::$app->user->can('superadmin')) { ?>
        <?= Html::a('Удалить<i class="delete"></i>', [null], ['class' => 'gbtncancel','title'=>'удалить','data-confirm'=>'Вы уверены, что хотите удалить этот элемент?','data-method'=>'post','data-lnk'=>'/posrednik/delete?id=']) ?>
       <?php } else { ?>
         <?= Html::a('Удалить<i class="delete"></i>', [null], ['class' => 'notactive','title'=>'удалить']) ?>
       <?php }  ?>  
       <?= Html::a('Сбросить фильтр<i class="filter"></i>', ['index'], ['class' => 'btnfilter']) ?>
               
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
                [
                  'class' => 'yii\grid\CheckboxColumn',
                  'options'=>['width'=>'1%'],
                  'checkboxOptions' =>['class'=>'chk']
                ],
            'title',
             [
                  'format'=>'raw',
                  'attribute' => 'status',
                  'value' => function ($data)  {return $data->status == 1 ?  '<i class="u_active" data-tip="posrednik" data-value="0" data-id="'.$data->id.'"></i>'  : '<i class="u_unactive" data-tip="posrednik" data-value="1" data-id="'.$data->id.'"></i>'; } ,
                  'headerOptions'=>['class'=>'w8'],
                  'contentOptions'=>['class'=>'w8'] 
             ],
        ],
    ]); ?>
   </div>
 </div>
</div>
