<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use app\models\AuthItem;
use yii\widgets\LinkPager;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Разрешения';
$this->params['breadcrumbs'][] = $this->title;
Url::remember();
?>
<div class="box">

    <h1><?= Html::encode($this->title) ?></h1>

   
   <div class="form-griedview">
     <div class="tbl-griedview">
       <?= Html::a('Добавить<i class="add"></i>', ['create'], ['class' => 'gbtnins']) ?>
       <?= Html::a('Редактировать<i class="edit"></i>', [null], ['class' => 'gbtnedit','data-lnk'=>'update?id=']) ?>
       <?= Html::a('Удалить<i class="delete"></i>', [null], ['class' => 'gbtncancel','title'=>'удалить','data-confirm'=>'Вы уверены, что хотите удалить этот элемент?','data-method'=>'post','data-lnk'=>'/authitem/delete?id=']) ?>
       <?= Html::a('Сбросить фильтр<i class="filter"></i>', ['index'], ['class' => 'btnfilter']) ?>
        
      <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'id'=>'rules-tbl',
            'pager'=>['maxButtonCount' => 10],
            'columns' => [
                [
                  'class' => 'yii\grid\CheckboxColumn',
                  'options'=>['width'=>'1%'],
                  'checkboxOptions' =>['class'=>'chk']
                ],
                'name',
                //'type',
                [
                  'attribute' => 'type',
                  //'format'=>'html',
                  'contentOptions'=>['class'=>'w5'] 
                ], 
                'description:ntext',
                'rule_name',
                [
                 'class' => 'yii\grid\ActionColumn',
                 'template' => '{view}',
                 /*'buttons'=>[
                                'update' => function ($url, $model, $key) {
                                        return  Html::a('Update', ['site/index']);
                                    }                 
                            ],*/
                ],
            ],
        ]); ?>
     </div>
   </div>
</div>

