<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\AuthItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-form">
    <?= Html::a('Назад',['index'],['class'=>'goback']) ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 64])  ?>

    <?= $form->field($model, 'type')->textInput() ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'rule_name')->dropDownList($model->DataRule,['prompt'=>'- Выберите значение -']) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить<i class="add"></i>' : 'Сохранить<i class="save_ico"></i>', ['class' => $model->isNewRecord ? 'gbtnins' : 'gbtnins']) ?>
        <?= Html::a('Отмена<i class="cancel_ico"></i>', ['authitem/index'], ['class' => 'gbtncancel ']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
