<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\models\mainmenu;
use app\components\MenuWidget;
use app\components\ValutaWidget;
/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?php $this->registerJsFile('@web/js/jquery.js',['position'=>$this::POS_HEAD]);?>
    <?php  $this->registerJsFile('@web/js/jquery.mask.js');
           $this->registerJsFile('@web/js/app.js'); ?>
    <?php  $this->registerJsFile('@web/js/apfilter.js');
           $this->registerCssFile('@web/js/filter.css');
           $this->registerJsFile('http://code.highcharts.com/highcharts.js');
           $this->registerJsFile('http://code.highcharts.com/modules/exporting.js');
    ?>    
</head>
<?php if (!Yii::$app->user->isGuest) {  ?>
<body>
<?php } else {?>
<body class="bgfon">
<?php }?>
    
<?php $this->beginBody() ?><!---auth user-->

    <?php if (!Yii::$app->user->isGuest) {  ?>
        <?= MenuWidget::widget() ?>
        <div class="contr">
            <div id="toppanel">
               <i id="tekdate"></i><span class="txt"><?php  echo date('d / m / Y')?></span>
               <i id="rate"></i> 
                <?= ValutaWidget::widget() ?>
               <div id="rbl">
                   <span class="txt">Актуальная версия 1.0</span>
                   <span id="userbl"><?php echo Yii::$app->user->identity->name; ?></span>
                   <i id="setting_ico"></i>
                   <?= Html::a('Настройки',['/option/index'],['class'=>'js']) ?> 
                   <?= Html::a('Выйти ',['/site/logout'],['data-method' => 'post','id'=>'exit']) ?>
               </div>
            </div> 
            <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]) ?>
    <?php } else { ?>         
        <div class="contr">
    <?php }  ?>         
            <?= $content ?>
        
    <?php if (!Yii::$app->user->isGuest) {  ?>
      </div>
    <?php } ?>
        

    <?php if (!Yii::$app->user->isGuest) {  ?>
         <footer class="footer"><div id="fbl">ver 1.0 / Copyright 2015.   <?= Html::a('АМТех',['javascript:void(0);']) ?> - студия разработки</div></footer>
    <?php } ?>
   

          
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
