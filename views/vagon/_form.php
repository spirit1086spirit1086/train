<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap\Tabs;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\vagon */
/* @var $form yii\widgets\ActiveForm */
date_default_timezone_set('Asia/Almaty');
?>

<div class="item-form vagon">
    <?= Html::a('Назад',['index'],['class'=>'goback']) ?>
    
    <?php $form = ActiveForm::begin(['id'=>'vagonform']); ?>

    <div class="input-groups">
      <span class="bline"><?= $form->field($model, 'nscheta')->textInput() ?></span> 
      <span class="bline"><?= $form->field($model, 'dates')->widget(DatePicker::className(), ['template' => '{input}{addon}','language' => 'ru','options'=>['class'=>'form-control small-input'],'clientOptions' => ['todayHighlight'=>true,'autoclose' => true,'format' => 'dd-mm-yyyy']]);?></span>
      <span class="bline"><?= $form->field($model, 'tip')->dropDownList($model->omas,['prompt'=>'- Выберите значение -','data-class'=>'mini']) ?></span>
    </div>
    
    <div class="input-groups">
        <span class="bline"><?= $form->field($model, 'mremont_id')->dropDownList($model->DataMremont,['prompt'=>'- Выберите значение -','data-class'=>'large']) ?></span>
        <?= Html::checkbox('torkrdr',true,['value'=>$model->dr_id,'style'=>"display:none"])?>
    </div>

    <div class="input-groups">
        <span class="bline"><?= $form->field($model, 'nvagon_id')->dropDownList($model->DataVc,['prompt'=>'- Выберите значение -','data-class'=>'mini']) ?></span>
        <span class="bline"><?= $form->field($model, 'vvagona')->dropDownList($model->vid_vagon,['prompt'=>'- Выберите значение -','data-class'=>'mini'])?></span>
    </div>

    <div class="input-groups">
        <span class="bline"><?= $form->field($model, 'sobstvenik_id')->dropDownList($model->n_sobstvenik,['prompt'=>'- Выберите значение -','data-class'=>'mini']) ?></span>
        <span class="bline"><?= $form->field($model, 'nkontr')->textInput() ?></span>
    </div>

    <div class="input-groups">
       <span class="bline"><?= $form->field($model, 'drab')->widget(DatePicker::className(), ['template' => '{input}{addon}','language' => 'ru','options'=>['class'=>'form-control small-input'],'clientOptions' => ['todayHighlight'=>true,'autoclose' => true,'format' => 'dd-mm-yyyy']]);?></span>
       <span class="bline"><?= $form->field($model, 'dbrak')->widget(DatePicker::className(), ['template' => '{input}{addon}','language' => 'ru','options'=>['class'=>'form-control small-input'],'clientOptions' => ['todayHighlight'=>true,'autoclose' => true,'format' => 'dd-mm-yyyy']]);?></span>
    </div>

    <div class="input-groups">
        <span class="bline"><?= $form->field($model, 'vcomp_id')->listBox($model->DataCompany,['data-class'=>'mini','multiple'=>'multiple'])?></span>
        <span class="bline"><?= $form->field($model, 'tsho_id')->dropDownList($model->DataTsho,['prompt'=>'- Выберите значение -','data-class'=>'mini'])?></span>
    </div>

    <div class="input-groups">
       <span class="bline">
          <div class="form-group">
              <?= Html::label('Тип прайса','pricename') ?>
              <?= Html::dropDownList('pricename',null,$model->DataTips,['prompt'=>'- Выберите значение -','id'=>'pricename','data-class'=>'mini','options'=>'']) ?>
          </div>
       </span>
       <span class="bline"><?= Html::button('Добавить',['class'=>'addopt']) ?></span>
   </div>
    
   <div class="tabbl">
      <?php echo $model->tordrkr ?>
      <?php echo $model->spr2615 ?>
      <?php echo $model->detailt ?>
      <?php echo $model->reviz ?>
      <?php echo $model->promiv ?>
      <?php echo $model->zhdtarif ?>
      <?php echo $model->uborka ?>
      <?php echo $model->usluga ?>
   </div>



    <?= $form->field($model, 'vu23')->textarea(['style'=>'height:100px']) ?>
  
      <?= $form->field($model, 'vu22')->textarea(['style'=>'height:100px']) ?>
      <?php //$form->field($model, 'vu22kol')->textInput() ?>

    <?= $form->field($model, 'vu36')->textarea(['style'=>'height:100px']) ?>

      <?= $form->field($model, 'akt')->textarea(['style'=>'height:100px']) ?>
      <?php // $form->field($model, 'aktkol')->textInput() ?>    

    <?php // $form->field($model, 'opis')->textarea(['style'=>'height:220px'])?>

    <div class="form-group" style="margin-top: 50px;">
       <?= Html::submitButton($model->isNewRecord ? 'Добавить<i class="add"></i>' : 'Сохранить<i class="save_ico"></i>', ['class' => $model->isNewRecord ? 'gbtnins' : 'gbtnins'])?>
       <?= Html::a('Отмена<i class="cancel_ico"></i>', ['/vagon/index'], ['class' => 'gbtncancel '])?>
    </div>

   <?php ActiveForm::end(); ?>

</div>


 




