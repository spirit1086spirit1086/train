<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\vagon */

$this->title = 'Добавить счет-фактуру';
$this->params['breadcrumbs'][] = ['label' => 'Счет фактуры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vagon-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
