<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\vagon */

(!empty($model->nvagon_id)) ? $this->title = 'Обновить счет-фактуру вагона: ' . ' ' . $model->DataVc[$model->nvagon_id] : $this->title = 'Обновить счет-фактуру: ';
$this->params['breadcrumbs'][] = ['label' => 'Счет-фактуры', 'url' => ['index']];
(!empty($model->nvagon_id)) ? $this->params['breadcrumbs'][] = $model->DataVc[$model->nvagon_id] : '';
date_default_timezone_set('Asia/Almaty');
?>
<div class="vagon-update">

    <h1><?= Html::encode($this->title) ?></h1>
    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
