<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\vagon */

$this->title = 'Вагон № '.$model->DataVc[$model->nvagon_id];
$this->params['breadcrumbs'][] = ['label' => 'Вагоны', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-view">

    <h1><?= Html::encode($this->title) ?></h1>

   <table id="record-view">
        <tr>
           <th class="p8">№ Счет фактуры: </th>
           <th class="p8">Дата: </th>
           <th>Место ремонта:</th>
           <th class="p8">Причина брака:</th>
           <th >Дата работы:</th>
           <th >Дата забраковки:</th>
           <th >№ вагона:</th>
           <th >Тип вагона:</th>
           <th >Собственник:</th>
           <th >№ контракта:</th>
           <th >Выставление на компании:</th>
        </tr>
        <tr>
            <td><?php echo $model->nscheta; ?></td>
            <td><?php echo date('d-m-Y', strtotime($model->dates)); ?></td>
            <td><?php echo $model->DataMremont[$model->mremont_id]; ?></td>
            <td><?php echo $model->DataOsn[$model->remont_id]; ?></td>
            <td><?php echo date('d-m-Y', strtotime($model->drab)); ?></td>
            <td><?php echo date('d-m-Y', strtotime($model->dbrak)); ?></td>
            <td><?php echo $model->DataVc[$model->nvagon_id]; ?></td>
            <td><?php echo $model->DataVvagon[$model->vvagona]; ?></td>
            <td><?php echo $model->DataSobstvenik[$model->sobstvenik_id]; ?></td>
            <td><?php echo $model->nkontr; ?></td>
            <td><?php echo $model->DataCompany[$model->vcomp_id]; ?></td>
        </tr>             
   </table> 

   <table  id="record-view">
        <tr>
           <th class="p8">Возмещение от ТШО:</th>
           <th class="p8">Цена за ед.</th>           
           <th class="p8">НДС,%:</th>
           <th>Описание:</th>
        </tr>
        <tr>
           <td><?php echo $model->DataTsho[$model->tsho_id]; ?></td>
           <td><?php echo $model->cenaed; ?></td>
           <td><?php echo $model->ndc; ?></td>
           <td class="ll"><?php echo $model->opis; ?></td>
        </tr>
   </table>
   
   
   <div id="titbl">Справка 2612:</div> 
    <?php foreach($spr as $value) { ?>
      <div class="pp"><?php echo $model->DataSpr[$value->spr_id] ; ?></div>
    <?php } ?>
    
     <?php for ($i=0;$i<5;$i++) { ?>
           <?php if ($i==0) 
                 { 
                    echo '<div id="titbl">Детали:</div>';
                    $sps=$details;
                 }
                 elseif ($i==1) 
                 {
                    echo '<div id="titbl">Ревизия:</div>';
                    $sps=$revizia;
                    
                 }
                 elseif ($i==2) 
                 {
                    echo '<div id="titbl">Промывка:</div>';
                    $sps=$promivka;
                 }
                 elseif ($i==3) 
                 {
                    echo '<div id="titbl">ЖД тариф:</div>';
                    $sps=$zhd;
                 }
                 elseif ($i==4) 
                 {
                    echo '<div id="titbl">Уборка:</div>';
                    $sps=$uborka;
                 }
           ?>
           <table  class="dop">
                <tr>
                   <th>Наименование:</th>
                   <th class="p8 c">Кол-во:</th>
                   <th class="p11 c">Цена за ед.</th>
                   <th class="p11 c">Цена</th>
                   <th class="p11 c">НДС</th>
                   <th class="p11 c">Итог</th>
                </tr>
            <?php foreach($sps as $value) { ?>
                       <?php if ($i==0) 
                             { 
                                $title=$model->DataDetailspr[$value->detailspr_id]; 
                             }
                             elseif ($i==1) 
                             {
                                $title=$model->DataReviziaspr[$value->reviziaspr_id];
                             }
                             elseif ($i==2) 
                             {
                                $title=$model->DataPromivspr[$value->promivspr_id];
                             }
                             elseif ($i==3) 
                             {
                                $title=$model->DataZhdspr[$value->zhdspr_id];
                             }
                             elseif ($i==4) 
                             {
                                $title=$model->DataCleanspr[$value->cleanspr_id];
                             }
                       ?>
                <tr>
                   <td><?php echo $title; ?></td>
                   <td class="c"><?php echo $value->kol; ?></td>
                   <td class="c"><?php echo $value->cenaed; ?></td>
                   <td class="c"><?php $cena=round($value->cenaed*$value->kol,2);echo $cena; ?></td>
                   <td class="c"><?php $ndc=round($cena*($model->ndc/100),2);echo $ndc; ?></td>
                   <td class="c"><?php echo round($ndc+$cena,2); ?></td>
                </tr>
            <?php } ?>
           </table>
    <?php } ?>




</div>
