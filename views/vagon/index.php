<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use dosamigos\datepicker\DatePicker;
use dosamigos\datepicker\DateRangePicker;
/* @var $this yii\web\View */
/* @var $searchModel app\models\vagonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
Url::remember();
$this->title = 'Счет-фактуры';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="vagon-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


<div class="form-griedview">
     <div class="tbl-griedview">
       <?= Html::a('Добавить<i class="add"></i>', ['create'], ['class' => 'gbtnins']) ?>
       <?= Html::a('Редактировать<i class="edit"></i>', [null], ['class' => 'gbtnedit','data-lnk'=>'update?id=']) ?>
       <?= Html::a('Удалить<i class="delete"></i>', [null], ['class' => 'gbtncancel','title'=>'удалить','data-confirm'=>'Вы уверены, что хотите удалить этот элемент?','data-method'=>'post','data-lnk'=>'/vagon/delete?id=']) ?>
       <?= Html::a('Сбросить фильтр<i class="filter"></i>', ['index'], ['class' => 'btnfilter']) ?>
      
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
                [
                  'class' => 'yii\grid\CheckboxColumn',
                  'options'=>['width'=>'1%'],
                  'checkboxOptions' =>['class'=>'chk']
                ],   
                [
                  'attribute' => 'nscheta',
                  'headerOptions'=>['class'=>'w4'],
                  'contentOptions'=>['class'=>'w4'] 
                ], 
                [
                  'attribute' => 'dates',
                  'headerOptions'=>['class'=>'w11'],
                  'contentOptions'=>['class'=>'w11'],
                  'value'=>'dates',
                  'format' => ['date', 'php:d-m-Y'],
                  'filter' => DatePicker::widget
                              ([
                                'model' => $searchModel,
                                'attribute' => 'dates',
                                'language' => 'ru',
                                'clientOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd',
                                    'todayHighlight'=>true,
                                    //'multidate'=>true,
                                    //'multidateSeparator'=>":"
                                ]
                              ])
                ], 
                [
                  'attribute' => 'vc',
                  'label'=>'№ вагона',
                  'value' => 'vc.nvg',
                  'headerOptions'=>['class'=>'w8'],
                  'contentOptions'=>['class'=>'w8'] 
                ], 
                [
                  'attribute' => 'mremont',
                  'label'=>'Место ремонта',
                  'value' => 'mremont.title',
                  'headerOptions'=>['class'=>'w11'],
                  'contentOptions'=>['class'=>'w11'],
                  'format'=>'html', 
                  'filter' => Html::activeDropDownList($searchModel,'mremont_id',$searchModel->DataMremont,['prompt'=>'- Поле фильтра -'])    //,'class'=>'nostyle'              
                ], 
                [
                  'attribute' => 'tip',
                  'format'=>'raw',
                  'value'=> function ($data)  { return $data->DataOmas[$data->tip]; },                  
                  'headerOptions'=>['class'=>'w8'],
                  'contentOptions'=>['class'=>'w8'],
                  'format'=>'html', 
                  'filter' => Html::activeDropDownList($searchModel,'tip',$searchModel->Omas(),['prompt'=>'- Тип -'])    //,'class'=>'nostyle'              
                ], 
                [
                  'attribute' => 'nkontr',
                  'headerOptions'=>['class'=>'w8'],
                  'contentOptions'=>['class'=>'w8'] 
                ], 
                [
                  'attribute' => 'vvagon',
                  'label'=>'Тип вагона',
                  'value' => 'vvagon.title',
                  'headerOptions'=>['class'=>'w6'],
                  'contentOptions'=>['class'=>'w6'],
                  'format'=>'html', 
                  'filter' => Html::activeDropDownList($searchModel,'vvagona',$searchModel->DataVvagon,['prompt'=>'- тип -'])    //,'class'=>'nostyle'              
                ], 
                [
                  'attribute' => 'sobstvenik',
                  'label'=>'Собственник',
                  'value' => 'sobstvenik.title',
                  'headerOptions'=>['class'=>'w11'],
                  'contentOptions'=>['class'=>'w11'],
                  'format'=>'html', 
                  'filter' => Html::activeDropDownList($searchModel,'sobstvenik_id',$searchModel->DataSobstvenik,['prompt'=>'- собственник -'])    //,'class'=>'nostyle'              
                ], 
                [
                  'attribute' => 'vcomp_id',
                  //'attribute' => 'company',
                  'label'=>'Выставление на компании',
                  //'value' => 'company.title',
                  'value' => function ($data)  
                            {
                                 $mas=[];$str=''; 
                                 $mas=explode(',',$data->vcomp_id);
                                 for ($i=0;$i<count($mas);$i++) 
                                 { 
                                    ($mas[$i]!='') ? $str.=$data->DataCompany[$mas[$i]].'<br/>' : $str=''; 
                                 } 
                                return $str;
                            }, 
                  'headerOptions'=>['class'=>'w11'],
                  'contentOptions'=>['class'=>'w11'],
                  'format'=>'html', 
                  'filter' => Html::activeDropDownList($searchModel,'vcomp_id',$searchModel->DataCompany,['prompt'=>'- статус -'])    //,'class'=>'nostyle'              
                ], 
                [//'tsho.title'
                  'attribute' => 'tsho',
                  'label'=>'Возмещение от ТШО',
                  'value' => function ($data) { (!empty($data->tsho_id)) ? $tsho=$data->DataTsho[$data->tsho_id] : $tsho=''; return $tsho; },
                  'headerOptions'=>['class'=>'w8'],
                  'contentOptions'=>['class'=>'w8'],
                  'format'=>'html', 
                  'filter' => Html::activeDropDownList($searchModel,'tsho_id',$searchModel->DataTsho,['prompt'=>'- статус -'])    //,'class'=>'nostyle'              
                ], 
                [
                  'attribute' => 'remont_id',
                  'value' => function ($data)  { return $data->DataPrice[$data->remont_id]; },
                  'headerOptions'=>['class'=>'w11'],
                  'contentOptions'=>['class'=>'w11'],
                  'format'=>'html', 
                ], 
            // 'drab',
            // 'dbrak',
            // 'opis:ntext',
        ],
    ]); ?>

  </div>
 </div>
</div>
