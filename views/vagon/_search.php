<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\vagonSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vagon-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nscheta') ?>

    <?= $form->field($model, 'dates') ?>

    <?= $form->field($model, 'tip') ?>

    <?= $form->field($model, 'mremont') ?>

    <?php // echo $form->field($model, 'nvagon') ?>

    <?php // echo $form->field($model, 'vvagona') ?>

    <?php // echo $form->field($model, 'sobstvenik') ?>

    <?php // echo $form->field($model, 'nkontr') ?>

    <?php // echo $form->field($model, 'drab') ?>

    <?php // echo $form->field($model, 'dbrak') ?>

    <?php // echo $form->field($model, 'vcomp') ?>

    <?php // echo $form->field($model, 'tsho') ?>

    <?php // echo $form->field($model, 'spr') ?>

    <?php // echo $form->field($model, 'detail') ?>

    <?php // echo $form->field($model, 'opis') ?>

    <?php // echo $form->field($model, 'cenaed') ?>

    <?php // echo $form->field($model, 'cena') ?>

    <?php // echo $form->field($model, 'ndc') ?>

    <?php // echo $form->field($model, 'itog') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
