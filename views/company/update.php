<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\company */

$this->title = 'Обновить компанию: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Компании', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="company-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', ['model' => $model]) ?>

</div>
