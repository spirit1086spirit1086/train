<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\vc */

$this->title = 'Обновить вагон/контракт: ' . ' ' . $model->nvg;
$this->params['breadcrumbs'][] = ['label' => 'Вагон/контракт', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->nvg;
?>
<div class="vc-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
