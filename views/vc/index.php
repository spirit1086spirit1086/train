<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\filters\AccessControl;
/* @var $this yii\web\View */
/* @var $searchModel app\models\vcSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
Url::remember();
$this->title = 'Вагон/контракт';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vc-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   <div class="form-griedview">
     <div class="tbl-griedview">
       <?= Html::a('Добавить<i class="add"></i>', ['create'], ['class' => 'gbtnins']) ?>
       <?= Html::a('Редактировать<i class="edit"></i>', [null], ['class' => 'gbtnedit','data-lnk'=>'update?id=']) ?>
       <?php if (Yii::$app->user->can('superadmin')) {?>
       <?= Html::a('Удалить<i class="delete"></i>', [null], ['class' => 'gbtncancel','title'=>'удалить','data-confirm'=>'Вы уверены, что хотите удалить этот элемент?','data-method'=>'post','data-lnk'=>'/vc/delete?id=']) ?>
     <?php } else { ?>
       <?= Html::a('Удалить<i class="delete"></i>', [null], ['class' => 'notactive','title'=>'удалить']) ?>
     <?php } ?>
       <?= Html::a('Сбросить фильтр<i class="filter"></i>', ['index'], ['class' => 'btnfilter']) ?>
        
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
                [
                  'class' => 'yii\grid\CheckboxColumn',
                  'options'=>['width'=>'1%'],
                  'checkboxOptions' =>['class'=>'chk']
                ],               
          //  'id',
            'nvg',
            //'vvagon_id',
                [
                  'attribute' => 'vvagon',
                  'label'=>'Тип вагона',
                  'value' => 'vvagon.title',
                  'headerOptions'=>['class'=>'w16'],
                  'contentOptions'=>['class'=>'w16'],
                  'format'=>'html', 
                  'filter' => Html::activeDropDownList($searchModel,'vvagon_id',$searchModel->DataVvagon,['prompt'=>'- Поле фильтра -'])    //,'class'=>'nostyle'              
                ], 
            //'sobstvenik_id',
                [
                  'attribute' => 'sobstvenik',
                  'label'=>'Собственник',
                  'value' => 'sobstvenik.title',
                  'headerOptions'=>['class'=>'w16'],
                  'contentOptions'=>['class'=>'w16'],
                  'format'=>'html', 
                  'filter' => Html::activeDropDownList($searchModel,'sobstvenik_id',$searchModel->DataSobstvenik,['prompt'=>'- Поле фильтра -'])    //,'class'=>'nostyle'              
                ], 
            //'nkotrakt',
                [
                  'attribute' => 'nkotrakt',
                  'headerOptions'=>['class'=>'w11'],
                  'contentOptions'=>['class'=>'w11'] 
                ], 
                [
                  'format'=>'raw',
                  'attribute' => 'status',
                  'value' => function ($data)  {return $data->status == 1 ?  '<i class="u_active" data-tip="vc" data-value="0" data-id="'.$data->id.'"></i>'  : '<i class="u_unactive" data-tip="vc" data-value="1" data-id="'.$data->id.'"></i>'; } ,
                  'headerOptions'=>['class'=>'w8'],
                  'contentOptions'=>['class'=>'w8'] 
                ],
           // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
      </div>
    </div>
</div>
