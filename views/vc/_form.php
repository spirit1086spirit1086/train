<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\vc */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-form">
    <?= Html::a('Назад',['index'],['class'=>'goback']) ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nvg')->textInput(['class'=>'form-control small-input']) ?>

    <?= $form->field($model, 'vvagon_id')->dropDownList($model->DataVvagon,['prompt'=>'- Выберите значение -']) ?>

    <?= $form->field($model, 'sobstvenik_id')->dropDownList($model->DataSobstvenik,['prompt'=>'- Выберите значение -']) ?>

    <?= $form->field($model, 'nkotrakt')->textInput(['class'=>'form-control small-input']) ?>

   <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить<i class="add"></i>' : 'Сохранить<i class="save_ico"></i>', ['class' => $model->isNewRecord ? 'gbtnins' : 'gbtnins']) ?>
        <?= Html::a('Отмена<i class="cancel_ico"></i>', ['vc/index'], ['class' => 'gbtncancel ']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>
