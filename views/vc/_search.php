<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\vcSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vc-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nvagon') ?>

    <?= $form->field($model, 'vvagon_id') ?>

    <?= $form->field($model, 'sobstvenik_id') ?>

    <?= $form->field($model, 'nkotrakt') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
