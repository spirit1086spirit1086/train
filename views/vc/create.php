<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\vc */

$this->title = 'Создать вагон/контракт';
$this->params['breadcrumbs'][] = ['label' => 'Вагон/контракт', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vc-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
