<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Права';
$this->params['breadcrumbs'][] = ['label' => 'Группы', 'url' => ['groups/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="authitemchild-index">

    <h3><?= Html::encode($this->title) ?></h3>


    <?php $form = ActiveForm::begin(['id'=>'rules-form']); ?>
     <div><span class="title">Parent</span><span class="title">Child</span></div>  
    <?php for($i=0;$i<count($rules);$i++) { ?>
        <?= Html::dropDownList('AuthItemChild[parent][]',$rules[$i]['parent'],$model->DataParent0,['prompt'=>'- Выберите значение -','id'=>'parent'.$i,'data-class'=>'mini']) ?>
        <?= Html::dropDownList('AuthItemChild[child][]',$rules[$i]['child'],$model->DataChild0,['prompt'=>'- Выберите значение -','id'=>'child'.$i,'data-class'=>'mini']) ?>
        <?= Html::a('Удалить','javascript:void(0);',['class'=>'btncancel delrule','data-id'=>$i]) ?>
    <?php } ?> 

    <div class="btn-group">
        <?= Html::submitButton('Сохранить<i class="ok_ico"></i>', ['class'=>'btnok']) ?>
        <?= Html::a('Отмена<i class="cancel_ico"></i>',['/groups/index'],['class'=>'btncancel']) ?>
        <?= Html::a('Добавить правило','javascript:void(0);', ['class'=>'btnok','id'=>'addrule']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
