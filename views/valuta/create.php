<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Valuta */

$this->title = 'Добавить валюту';
$this->params['breadcrumbs'][] = ['label' => 'Валюта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="valuta-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
