<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Valuta */

$this->title = 'Обновить валюту: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Валюта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="valuta-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
