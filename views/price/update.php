<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Price */

$this->title = 'Обновить прайсы: ' . ' ' . $model->titlename;
$this->params['breadcrumbs'][] = ['label' => 'Прайсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->titlename];
?>
<div class="price-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
