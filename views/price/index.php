<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\filters\AccessControl;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PriceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Прайсы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   <div class="form-griedview">
     <div class="tbl-griedview">
       <?= Html::a('Добавить<i class="add"></i>', ['create'], ['class' => 'gbtnins']) ?>
       <?= Html::a('Редактировать<i class="edit"></i>', [null], ['class' => 'gbtnedit','data-lnk'=>'update?id=']) ?>
       <?php if (Yii::$app->user->can('superadmin')) {?>
        <?= Html::a('Удалить<i class="delete"></i>', [null], ['class' => 'gbtncancel','title'=>'удалить','data-confirm'=>'Вы уверены, что хотите удалить этот элемент?','data-method'=>'post','data-lnk'=>'/price/delete?id=']) ?>
       <?php } else { ?>
        <?= Html::a('Удалить<i class="delete"></i>', [null], ['class' => 'notactive','title'=>'удалить']) ?>
       <?php } ?>
       <?= Html::a('Сбросить фильтр<i class="filter"></i>', ['index'], ['class' => 'btnfilter']) ?>
        
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
                [
                  'class' => 'yii\grid\CheckboxColumn',
                  'options'=>['width'=>'1%'],
                  'checkboxOptions' =>['class'=>'chk']
                ],   
            'id',
                [
                  'attribute' => 'mremont',
                  'label'=>'Место ремонта',
                  //'value' => 'mremont.title',
                  'value'=> function ($data)  { if ($data->mremont_id!='') {return $data->DataMremont[$data->mremont_id];} else {return '';} },
                  'headerOptions'=>['class'=>'w11'],
                  'contentOptions'=>['class'=>'w11'],
                  'format'=>'html', 
                  'filter' => Html::activeDropDownList($searchModel,'mremont_id',$searchModel->DataMremont,['prompt'=>'- Поле фильтра -'])    //,'class'=>'nostyle'              
                ], 
                [
                  'attribute' => 'tips',
                  'label'=>'Типы',
                  'value' => 'tips.titlename',
                  'headerOptions'=>['class'=>'w11'],
                  'contentOptions'=>['class'=>'w11'],
                  'format'=>'html', 
                  'filter' => Html::activeDropDownList($searchModel,'pricetip',$searchModel->DataTips,['prompt'=>'- Поле фильтра -'])    //,'class'=>'nostyle'              
                ], 
                [
                  'attribute' => 'code',
                  'headerOptions'=>['class'=>'w7'],
                  'contentOptions'=>['class'=>'w7'],
                  'value'=> function ($data)  { if ($data->code!='') {return $data->code;} else {return '';} },
                ], 
            'titlename',
                [
                  'attribute' => 'cena',
                  'headerOptions'=>['class'=>'w11'],
                  'contentOptions'=>['class'=>'w11'],
                ], 
                [
                  'format'=>'raw',
                  'attribute' => 'status',
                  'value' => function ($data)  {return $data->status == 1 ?  '<i class="u_active" data-tip="price" data-value="0" data-id="'.$data->id.'"></i>'  : '<i class="u_unactive" data-tip="price" data-value="1" data-id="'.$data->id.'"></i>'; } ,
                  'headerOptions'=>['class'=>'w8'],
                  'contentOptions'=>['class'=>'w8'] 
                ],
            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
  </div>
</div>
