<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Price */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-form">
    <?= Html::a('Назад',['index'],['class'=>'goback']) ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'mremont_id')->dropDownList($model->DataMremont,['prompt'=>'- Выберите значение -','data-class'=>'mini']) ?>
 
    <?= $form->field($model, 'pricetip')->dropDownList($model->DataTips,['prompt'=>'- Выберите значение -','data-class'=>'mini']) ?>

    <?= $form->field($model, 'code')->textInput(['class'=>'form-control xx-small-input']) ?>

    <?= $form->field($model, 'titlename')->textInput(['maxlength' => 255,'class'=>'form-control large-input']) ?>

    <?= $form->field($model, 'cena')->textInput(['maxlength' => 255,'class'=>'form-control small']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить<i class="add"></i>' : 'Сохранить<i class="save_ico"></i>', ['class' => $model->isNewRecord ? 'gbtnins' : 'gbtnins']) ?>
        <?= Html::a('Отмена<i class="cancel_ico"></i>', ['index'], ['class' => 'gbtncancel ']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>
