<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tips */

$this->title = 'Обновить тип: ' . ' ' . $model->titlename;
$this->params['breadcrumbs'][] = ['label' => 'Типы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->titlename];
?>
<div class="tips-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
