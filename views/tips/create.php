<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tips */

$this->title = 'Типы';
$this->params['breadcrumbs'][] = ['label' => 'Типы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tips-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
