<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\filters\AccessControl;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Типы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tips-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="form-griedview">
     <div class="tbl-griedview">
       <?= Html::a('Добавить<i class="add"></i>', ['create'], ['class' => 'gbtnins']) ?>
       <?= Html::a('Редактировать<i class="edit"></i>', [null], ['class' => 'gbtnedit','data-lnk'=>'update?id=']) ?>
       <?php if (Yii::$app->user->can('superadmin')) {?>
        <?= Html::a('Удалить<i class="delete"></i>', [null], ['class' => 'gbtncancel','title'=>'удалить','data-confirm'=>'Вы уверены, что хотите удалить этот элемент?','data-method'=>'post','data-lnk'=>'/mremont/delete?id=']) ?>
       <?php } else { ?>
         <?= Html::a('Удалить<i class="delete"></i>', [null], ['class' => 'notactive','title'=>'удалить']) ?>
       <?php } ?>  
       <?= Html::a('Сбросить фильтр<i class="filter"></i>', ['index'], ['class' => 'btnfilter']) ?>
      
       <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
           [
              'class' => 'yii\grid\CheckboxColumn',
              'options'=>['width'=>'1%'],
              'checkboxOptions' =>['class'=>'chk']
            ],
            [
              'attribute'=>'id',
              'headerOptions'=>['class'=>'w8'],
              'contentOptions'=>['class'=>'w8']
            ],
            'titlename',
            [
              'format'=>'raw',
              'attribute' => 'status',
              'value' => function ($data)  {return $data->status == 1 ?  '<i class="u_active" data-tip="tips" data-value="0" data-id="'.$data->id.'"></i>'  : '<i class="u_unactive" data-tip="tips" data-value="1" data-id="'.$data->id.'"></i>'; } ,
              'headerOptions'=>['class'=>'w8'],
              'contentOptions'=>['class'=>'w8'] 
            ],  
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
   </div>
  </div>

</div>
