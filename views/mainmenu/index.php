<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use app\models\AuthItem;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Меню';
$this->params['breadcrumbs'][] = $this->title;
Url::remember();
?>
<div class="mainmenu-index">

    <h1><?= Html::encode($this->title) ?></h1>


   <div class="form-griedview">
     <div class="tbl-griedview">
       <?= Html::a('Добавить<i class="add"></i>', ['create'], ['class' => 'gbtnins']) ?>
       <?= Html::a('Редактировать<i class="edit"></i>', [null], ['class' => 'gbtnedit','data-lnk'=>'update?id=']) ?>
       <?= Html::a('Удалить<i class="delete"></i>', [null], ['class' => 'gbtncancel','title'=>'удалить','data-confirm'=>'Вы уверены, что хотите удалить этот элемент?','data-method'=>'post','data-lnk'=>'/mainmenu/delete?id=']) ?>
       <?= Html::a('Сбросить фильтр<i class="filter"></i>', ['index'], ['class' => 'btnfilter']) ?>
      
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
                [
                  'class' => 'yii\grid\CheckboxColumn',
                  'options'=>['width'=>'1%'],
                  'checkboxOptions' =>['class'=>'chk']
                ],
                [
                  'attribute' => 'id',
                  'contentOptions'=>['class'=>'w5'],
                  'filter'=>'' 
                ], 
                [
                  'attribute' => 'title',
                  'format'=>'html', 
                  'filter' => Html::activeDropDownList($searchModel,'title',$searchModel->DataParent0,['prompt'=>'- Поле фильтра -']),                  
                ], 
                [
                  'attribute' => 'link',
                  'contentOptions'=>['class'=>'w20'] 
                ], 
                [
                  'attribute' => 'cclass',
                  'contentOptions'=>['class'=>'w11'] 
                ], 
                [
                  'attribute' => 'parent',
                  'contentOptions'=>['class'=>'w8'], 
                ], 
                [
                  'attribute' => 'position',
                  //'format'=>'html',
                  'contentOptions'=>['class'=>'w8'] 
                ], 
        ],
    ]); ?>
     </div>
   </div>
</div>
