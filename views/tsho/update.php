<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\tsho */

$this->title = 'Обновить: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'ТШО', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="tsho-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
