<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\tsho */

$this->title = 'Создать ТШО';
$this->params['breadcrumbs'][] = ['label' => 'ТШО', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tsho-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
