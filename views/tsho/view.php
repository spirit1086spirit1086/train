<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\tsho */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Tshos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tsho-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
        ],
    ]) ?>

</div>
