<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Option */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'О компании';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="item-form vagon">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'company')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'adress')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'kol')->textInput() ?>

    <?= $form->field($model, 'bin')->textInput(['maxlength' => 255,'class'=>'form-control dl200']) ?>

    <?= $form->field($model, 'tip')->dropDownList([1=>'Контрагент']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить',['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>