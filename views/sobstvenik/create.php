<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\sobstvenik */

$this->title = 'Добавить собственник';
$this->params['breadcrumbs'][] = ['label' => 'Собственник', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sobstvenik-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
