<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\vvagon */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-form">
    <?= Html::a('Назад',['index'],['class'=>'goback']) ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

   <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить<i class="add"></i>' : 'Сохранить<i class="save_ico"></i>', ['class' => $model->isNewRecord ? 'gbtnins' : 'gbtnins']) ?>
        <?= Html::a('Отмена<i class="cancel_ico"></i>', ['vvagon/index'], ['class' => 'gbtncancel ']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
