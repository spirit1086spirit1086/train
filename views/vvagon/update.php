<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\vvagon */

$this->title = 'Обновить тип вагона: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Вагоны', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="vvagon-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
