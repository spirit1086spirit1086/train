-- phpMyAdmin SQL Dump
-- version 4.4.3
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Май 29 2015 г., 10:39
-- Версия сервера: 5.6.24
-- Версия PHP: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `vagon`
--

-- --------------------------------------------------------

--
-- Структура таблицы `auth_assignment`
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('administrator', '12', 1427136835),
('author', '14', 1427145119),
('superadmin', '1', 1427136105);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item`
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci PACK_KEYS=0;

--
-- Дамп данных таблицы `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('administrator', 1, NULL, NULL, NULL, 20150312, 20150322),
('author', 1, NULL, NULL, NULL, 20150312, NULL),
('create-post', 2, 'Разрешение позволяет создавать записи', NULL, NULL, 20150311, NULL),
('create-user', 2, 'Разрешение позволяет создавать пользователей', NULL, NULL, 20150323, NULL),
('delete-own-post', 2, 'Разрешение позволяет удалять только свои записи', 'isAuthor', NULL, 20150323, 20150323),
('delete-own-user', 2, 'Разрешение позволяет удалять только своих пользователей', 'isAuthor', NULL, 20150322, NULL),
('delete-post', 2, 'Разрешение позволяет удалять любые записи', NULL, '', 20150311, 20150320),
('delete-user', 2, 'Разрешение позволяет удалять  пользователей', NULL, NULL, 20150322, NULL),
('superadmin', 1, NULL, NULL, NULL, 20150311, 20150311),
('update-own-post', 2, 'Разрешение позволяет редактировать свои записи', 'isAuthor', NULL, 20150311, NULL),
('update-own-user', 2, 'Разрешение позволяет обновлять только своих пользователей', 'isAuthor', NULL, 20150322, NULL),
('update-post', 2, 'Разрешение позволяет обновлять записи', NULL, '', 20150320, NULL),
('update-user', 2, 'Разрешение позволяет обновлять всех пользователей', NULL, NULL, 20150322, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item_child`
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('superadmin', 'administrator'),
('administrator', 'author'),
('author', 'create-post'),
('administrator', 'create-user'),
('author', 'delete-own-post'),
('administrator', 'delete-own-user'),
('administrator', 'delete-post'),
('delete-own-post', 'delete-post'),
('delete-own-user', 'delete-user'),
('superadmin', 'delete-user'),
('author', 'update-own-post'),
('administrator', 'update-own-user'),
('administrator', 'update-post'),
('update-own-post', 'update-post'),
('superadmin', 'update-user'),
('update-own-user', 'update-user');

-- --------------------------------------------------------

--
-- Структура таблицы `auth_rule`
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_rule`
--

INSERT INTO `auth_rule` (`name`, `data`, `created_at`, `updated_at`) VALUES
('isAuthor', 'O:19:"app\\rbac\\AuthorRule":3:{s:4:"name";s:8:"isAuthor";s:9:"createdAt";i:1427138543;s:9:"updatedAt";i:1427138543;}', 1427138543, 1427138543);

-- --------------------------------------------------------

--
-- Структура таблицы `clean`
--

CREATE TABLE IF NOT EXISTS `clean` (
  `id` int(11) NOT NULL,
  `cleanspr_id` int(11) DEFAULT NULL,
  `vagon_id` int(11) DEFAULT NULL,
  `tip` int(11) DEFAULT NULL,
  `pricetip_id` int(11) DEFAULT NULL,
  `mremont_id` int(11) DEFAULT NULL,
  `remont_id` int(11) DEFAULT NULL,
  `vvagon_id` int(11) DEFAULT NULL,
  `sobstvenik_id` int(11) DEFAULT NULL,
  `dates` date DEFAULT NULL,
  `cenaed` varchar(255) DEFAULT NULL,
  `ndc` varchar(255) DEFAULT NULL,
  `kol` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `clean`
--

INSERT INTO `clean` (`id`, `cleanspr_id`, `vagon_id`, `tip`, `pricetip_id`, `mremont_id`, `remont_id`, `vvagon_id`, `sobstvenik_id`, `dates`, `cenaed`, `ndc`, `kol`) VALUES
(1, 41, 46, 1, 5, 4, 41, 1, 6, '2015-04-14', '15230.00', NULL, 1),
(2, 2, 51, 1, 5, 2, 2, 2, 6, '2015-04-24', '3500.00', NULL, 1),
(3, 41, 65, 2, 5, 4, 41, 2, 2, '2015-05-05', '3500.00', NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `posrednik_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `createdBy` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `company`
--

INSERT INTO `company` (`id`, `title`, `posrednik_id`, `status`, `createdBy`) VALUES
(1, 'выставить ТТ-ТШО 50% ', NULL, 1, 12),
(3, 'выставить ТТ-ТШО', NULL, 1, 12),
(4, 'не выставлять', NULL, 1, 12),
(5, 'выставить ТКС-ТТ', 1, 1, 1),
(6, ' выставить ТКС-ТВТ', 1, 1, 1),
(7, 'выставить АКС-ТТ ', 2, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `detail`
--

CREATE TABLE IF NOT EXISTS `detail` (
  `id` int(11) NOT NULL,
  `vagon_id` int(11) DEFAULT NULL,
  `detailspr_id` int(11) DEFAULT NULL,
  `tip` int(11) DEFAULT NULL,
  `pricetip_id` int(11) DEFAULT NULL,
  `mremont_id` int(11) DEFAULT NULL,
  `remont_id` int(11) DEFAULT NULL,
  `vvagon_id` int(11) DEFAULT NULL,
  `sobstvenik_id` int(11) DEFAULT NULL,
  `dates` date DEFAULT NULL,
  `cenaed` varchar(255) DEFAULT NULL,
  `ndc` varchar(255) DEFAULT NULL,
  `kol` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `detail`
--

INSERT INTO `detail` (`id`, `vagon_id`, `detailspr_id`, `tip`, `pricetip_id`, `mremont_id`, `remont_id`, `vvagon_id`, `sobstvenik_id`, `dates`, `cenaed`, `ndc`, `kol`) VALUES
(65, 42, 6, 1, 3, 2, 6, 2, 4, '2015-04-02', '7600.00', NULL, 1),
(66, 42, 7, 1, 3, 2, 6, 2, 4, '2015-04-02', '10154.00', NULL, 2),
(68, 45, 6, 1, 3, 2, 16, 2, 2, '2015-04-11', '7600.00', NULL, 1),
(69, 47, 6, 1, 3, 2, 17, 3, 2, '2015-04-15', '7600.00', NULL, 1),
(70, 47, 7, 1, 3, 2, 17, 3, 2, '2015-04-15', '10154.00', NULL, 2),
(71, 49, 6, 1, 3, 2, 6, 3, 1, '2015-04-18', '7600.00', NULL, 1),
(73, 54, 9, 1, 3, 1, 14, 2, 4, '2015-04-22', '3750.00', NULL, 1),
(74, 54, 8, 1, 3, 1, 14, 2, 4, '2015-04-22', '7100.00', NULL, 1),
(75, 61, 6, 1, 3, 2, 6, 1, 1, '2015-05-14', '7600.00', NULL, 2),
(76, 64, 7, 1, 3, 2, 7, 3, 2, '2015-05-06', '10154.00', NULL, 1),
(80, 55, 9, 1, 3, 1, 9, 1, 6, '2015-04-20', '3750.00', NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL,
  `groupname` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 PACK_KEYS=0;

--
-- Дамп данных таблицы `groups`
--

INSERT INTO `groups` (`id`, `groupname`, `alias`, `description`) VALUES
(1, 'Супер администратор', 'superadmin', 'Роль позволяет выполнять все действия'),
(2, 'Администратор', 'administrator', 'Роль позволяет создавать, редактировать, удалять все записи. \r\nОграничения:\r\n1. Может создавать пользователей, редактировать и удалять только своих.\r\n2. Закрыт доступ к остальным пунктам в категории Система (исключение - Менеджер пользователей)'),
(3, 'Автор', 'author', 'Все пользователи могут создавать и редактировать только свои записи');

-- --------------------------------------------------------

--
-- Структура таблицы `mainmenu`
--

CREATE TABLE IF NOT EXISTS `mainmenu` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `cclass` varchar(255) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mainmenu`
--

INSERT INTO `mainmenu` (`id`, `title`, `link`, `cclass`, `parent`, `position`) VALUES
(4, 'Система', '#', 'sys_ico', NULL, 5),
(5, 'Менеджер пользователей', '/user/index', 'addusr_ico', 4, 1),
(6, 'Группы', '/groups/index', 'group_ico', 4, 2),
(7, 'Меню', '/mainmenu/index', 'menu_ico', 4, 4),
(8, 'Разрешения', '/authitem/index', 'rules_ico', 4, 6),
(9, 'Права', '/authitemchild/index', 'rights_ico', 4, 5),
(10, 'Счет-фактуры', '/vagon/index', 'vagon_ico', NULL, 2),
(11, 'Панель управления', '/site/index', 'control_ico', NULL, 1),
(14, 'Справочники', '#', 'spavochnik_ico', NULL, 3),
(15, 'Выставление на компании', '/company/index', 'directory_ico', 14, 2),
(17, 'Собственник', '/sobstvenik/index', 'directory_ico', 14, 5),
(19, 'Возмещение от ТШО', '/tsho/index', 'directory_ico', 14, 3),
(20, 'Типы вагонов', '/vvagon/index', 'directory_ico', 14, 6),
(22, 'Место ремонта', '/mremont/index', 'directory_ico', 14, 4),
(26, 'Вагон/контракт', '/vc/index', 'directory_ico', 14, 7),
(28, 'Отчеты', '#', 'spr_ico', NULL, 4),
(30, 'Типы', '/tips/index', 'tips_ico', 4, 3),
(31, 'Прайс', '/price/index', 'directory_ico', 14, 8),
(32, 'Посредники', '/posrednik/index', 'directory_ico', 14, 1),
(33, 'Валюта', '/valuta/index', 'valuta_ico', 4, 7),
(34, 'Квартальные', '/report/index', 'otch_ico', 28, 1),
(35, 'Детализация счет-фактур', '/schet/index', 'otch_ico', 28, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `mremont`
--

CREATE TABLE IF NOT EXISTS `mremont` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `valuta` int(11) DEFAULT NULL,
  `ndc` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `createdBy` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mremont`
--

INSERT INTO `mremont` (`id`, `title`, `valuta`, `ndc`, `status`, `createdBy`) VALUES
(1, 'Болат Жол', 1, '12', 1, 1),
(2, 'Акжайык-7', 1, '12', 1, 12),
(3, 'Титран-Экспресс', 2, '18', 1, 1),
(4, 'РЖД', 2, '18', 1, 1),
(5, 'НК КТЖ', 1, '12', 1, 1),
(6, 'Рип-Газ', 1, '12', 1, 1),
(7, 'Украинская ЖД', 3, '20', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `option`
--

CREATE TABLE IF NOT EXISTS `option` (
  `id` int(11) NOT NULL,
  `company` varchar(255) CHARACTER SET utf8 NOT NULL,
  `adress` varchar(255) CHARACTER SET utf8 NOT NULL,
  `kol` int(11) NOT NULL,
  `bin` varchar(255) CHARACTER SET utf8 NOT NULL,
  `tip` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `option`
--

INSERT INTO `option` (`id`, `company`, `adress`, `kol`, `bin`, `tip`) VALUES
(1, 'ТОО "ТексолТранс"', 'г.Атырау, Республика Казахстан', 26, '108697456268', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `posrednik`
--

CREATE TABLE IF NOT EXISTS `posrednik` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `createdBy` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `posrednik`
--

INSERT INTO `posrednik` (`id`, `title`, `status`, `createdBy`) VALUES
(1, 'ТКС', 1, 1),
(2, 'АКС', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `price`
--

CREATE TABLE IF NOT EXISTS `price` (
  `id` int(11) NOT NULL,
  `mremont_id` int(11) DEFAULT NULL,
  `pricetip` int(11) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  `titlename` varchar(255) NOT NULL,
  `cena` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `createdBy` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `price`
--

INSERT INTO `price` (`id`, `mremont_id`, `pricetip`, `code`, `titlename`, `cena`, `status`, `createdBy`) VALUES
(2, 2, 5, NULL, 'Подача и уборка вагонов', '3500.00', 1, 1),
(3, 2, 5, NULL, 'Подача и уборка вагонов Доссор Сервис', '311.67', 1, 1),
(4, 1, 5, NULL, 'Подача и уборка вагонов', '3982.86', 1, 1),
(5, 1, 5, NULL, 'Подача и уборка вагонов Доссор Сервис', '3500.00', 1, 1),
(6, 2, 3, NULL, 'Клин фрикционной тележки', '7600.00', 1, 1),
(7, 2, 3, NULL, 'Опорная балка авторежима б/у', '10154.00', 1, 1),
(8, 1, 3, NULL, 'Концевой кран', '7100.00', 1, 1),
(9, 1, 3, NULL, 'Балочка центрирующая', '3750.00', 1, 1),
(10, NULL, 2, 103, 'Обрыв или трещина маятниковой подвески', '', 1, 1),
(11, NULL, 2, 305, 'Завышение/занижение фрикц клина относительно опорной поверхности надресной балки', '', 1, 1),
(12, NULL, 2, 150, 'Поломка лестницы', '', 1, 1),
(13, NULL, 2, 102, 'Тонкий гребень', '', 1, 1),
(14, 1, 1, NULL, 'Текущий отцепочный ремонт без учета замены деталей и узлов ст. Кульсары', '34500.00', 1, 1),
(15, 2, 1, NULL, 'Текущий отцепочный ремонт на ст.Акжайык', '56769.00', 1, 1),
(16, 2, 1, NULL, 'Текущий отцепочный ремонт на ст.Кульсары', '6000.00', 1, 1),
(17, 2, 1, NULL, 'Текущий отцепочный ремонт на ст.Доссор', '43668.00', 1, 1),
(18, 2, 1, NULL, 'Вагон-цистерна 4-осн (нефть), без ремонта колесных пар', '355000.00', 1, 1),
(19, 2, 1, NULL, 'Вагон-цистерна 4-осн (газ), без ремонта колесных пар', '295000.00', 1, 1),
(20, 2, 1, NULL, 'Крытый вагон, без ремонта колесных пар', '360000.00', 1, 1),
(21, 2, 6, NULL, 'Промывка вагона', '325.05', 1, 1),
(22, 2, 6, NULL, 'Промывка вагона 11-20см', '35200.00', 1, 1),
(23, 2, 6, NULL, 'Промывка вагона 21-30см', '389.70', 1, 1),
(24, 2, 6, NULL, 'Промывка вагона 31-40см', '580.00', 1, 1),
(25, 2, 6, NULL, 'Промывка вагона 41-50см', '680.00', 1, 1),
(26, 1, 6, NULL, 'Промывка вагона', '244.85', 1, 1),
(27, 1, 6, NULL, 'Промывка вагона 11-20см', '36727.50', 1, 1),
(28, 1, 6, NULL, 'Промывка вагона 21-30см', '489.70', 1, 1),
(29, 1, 6, NULL, 'Промывка вагона 31-40см', '580.00', 1, 1),
(30, 1, 6, NULL, 'Промывка вагона 41-50см', '680.00', 1, 1),
(31, 2, 7, NULL, 'Жд тариф', '10200.00', 1, 1),
(32, 2, 7, NULL, 'Жд тариф Доссор', '4230.00', 1, 1),
(33, 1, 7, NULL, 'Жд тариф', '9356.00', 1, 1),
(34, 1, 7, NULL, 'Жд тариф Доссор', '3768.00', 1, 1),
(35, 2, 4, NULL, 'Полная ревизия колесных пар с обточкой и с дефектоскопированием', '28053.00', 1, 1),
(36, 2, 4, NULL, 'Промежуточная ревизия колесных пар с обточкой и с дефектоскопированием', '12005.00', 1, 1),
(37, 1, 4, NULL, 'Полная ревизия колесных пар с обточкой и с дефектоскопированием', '30304.00', 1, 1),
(38, 1, 4, NULL, 'Промежуточная ревизия колесных пар с обточкой и с дефектоскопированием', '11001.00', 1, 1),
(39, 3, 8, NULL, 'Модернизация вагона-цистерны для нефтепродуктов с присвоением модели 15-9543 по техническим условиям ЦДЛР.661352.1813', '90000.00', 1, 1),
(40, 4, 1, NULL, 'Текущий отцепочный ремонт ст. Асарайская', '', 1, 1),
(41, 4, 5, NULL, 'Подача и уборка вагонов', '', 1, 1),
(42, 6, 8, NULL, 'Удаление кислорода из котловой части вагона-цистерны', '20542.00', 1, 1),
(43, 6, 8, NULL, 'Полная покраска 75 м3', '120700.00', 1, 1),
(44, 2, 8, NULL, 'разгрузка колесных пар ', '2460.00', 1, 1),
(45, 4, 1, NULL, 'Деповской ремонт цистерна 4-х осн. крытый', '', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `prichina`
--

CREATE TABLE IF NOT EXISTS `prichina` (
  `id` int(11) NOT NULL,
  `vagon_id` int(11) DEFAULT NULL,
  `dr_id` int(11) DEFAULT NULL,
  `spr_id` int(11) DEFAULT NULL,
  `tip` int(11) DEFAULT NULL,
  `pricetip_id` int(11) DEFAULT NULL,
  `mremont_id` int(11) DEFAULT NULL,
  `remont_id` int(11) DEFAULT NULL,
  `vvagon_id` int(11) DEFAULT NULL,
  `sobstvenik_id` int(11) DEFAULT NULL,
  `dates` date DEFAULT NULL,
  `cenaed` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `prichina`
--

INSERT INTO `prichina` (`id`, `vagon_id`, `dr_id`, `spr_id`, `tip`, `pricetip_id`, `mremont_id`, `remont_id`, `vvagon_id`, `sobstvenik_id`, `dates`, `cenaed`) VALUES
(69, 42, NULL, 10, 1, 2, 2, 17, 2, 4, '2015-04-02', '43668.00'),
(70, 42, NULL, 11, 1, 2, 2, 17, 2, 4, '2015-04-02', '43668.00'),
(71, 45, NULL, 13, 1, 2, 2, 16, 2, 2, '2015-04-11', '6000.00'),
(72, 46, NULL, 11, 1, 2, 4, 40, 1, 6, '2015-04-14', '7641.69'),
(73, 47, NULL, 11, 1, 2, 2, 17, 3, 2, '2015-04-15', '43668.00'),
(74, 47, NULL, 12, 1, 2, 2, 17, 3, 2, '2015-04-15', '43668.00'),
(77, 54, NULL, 10, 1, 2, 1, 14, 2, 4, '2015-04-22', '34500.00'),
(79, 61, NULL, 13, 1, 2, 2, 16, 1, 1, '2015-05-14', '6000.00'),
(80, 64, NULL, 10, 1, 2, 2, 17, 3, 2, '2015-05-06', '43668.00'),
(81, 49, NULL, 13, 1, 2, 2, 15, 3, 1, '2015-04-18', '56769.00'),
(82, 55, NULL, 10, 1, 2, 1, 14, 1, 6, '2015-04-20', '34500.00');

-- --------------------------------------------------------

--
-- Структура таблицы `promiv`
--

CREATE TABLE IF NOT EXISTS `promiv` (
  `id` int(11) NOT NULL,
  `promivspr_id` int(11) DEFAULT NULL,
  `vagon_id` int(11) DEFAULT NULL,
  `tip` int(11) DEFAULT NULL,
  `pricetip_id` int(11) DEFAULT NULL,
  `mremont_id` int(11) DEFAULT NULL,
  `remont_id` int(11) DEFAULT NULL,
  `vvagon_id` int(11) DEFAULT NULL,
  `sobstvenik_id` int(11) DEFAULT NULL,
  `dates` date DEFAULT NULL,
  `cenaed` varchar(255) DEFAULT NULL,
  `ndc` varchar(255) DEFAULT NULL,
  `kol` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `promiv`
--

INSERT INTO `promiv` (`id`, `promivspr_id`, `vagon_id`, `tip`, `pricetip_id`, `mremont_id`, `remont_id`, `vvagon_id`, `sobstvenik_id`, `dates`, `cenaed`, `ndc`, `kol`) VALUES
(1, 22, 44, 1, 6, 2, 22, 1, 1, '2015-04-08', '35200.00', NULL, 1),
(2, 23, 62, 1, 6, 2, 23, 1, 4, '2015-04-10', '389.70', NULL, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `revizia`
--

CREATE TABLE IF NOT EXISTS `revizia` (
  `id` int(11) NOT NULL,
  `reviziaspr_id` int(11) DEFAULT NULL,
  `vagon_id` int(11) DEFAULT NULL,
  `tip` int(11) DEFAULT NULL,
  `pricetip_id` int(11) DEFAULT NULL,
  `mremont_id` int(11) DEFAULT NULL,
  `remont_id` int(11) DEFAULT NULL,
  `vvagon_id` int(11) DEFAULT NULL,
  `sobstvenik_id` int(11) DEFAULT NULL,
  `dates` date DEFAULT NULL,
  `cenaed` varchar(255) DEFAULT NULL,
  `ndc` varchar(255) DEFAULT NULL,
  `kol` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `revizia`
--

INSERT INTO `revizia` (`id`, `reviziaspr_id`, `vagon_id`, `tip`, `pricetip_id`, `mremont_id`, `remont_id`, `vvagon_id`, `sobstvenik_id`, `dates`, `cenaed`, `ndc`, `kol`) VALUES
(1, 35, 49, 1, 4, 2, 35, 3, 1, '2015-04-18', '28053.00', NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `sobstvenik`
--

CREATE TABLE IF NOT EXISTS `sobstvenik` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sobstvenik`
--

INSERT INTO `sobstvenik` (`id`, `title`, `status`, `type`, `createdBy`) VALUES
(1, 'Карготранс на ТТ', 1, NULL, 12),
(2, 'ТВТ', 1, NULL, 12),
(4, 'ТТ', 1, NULL, 12),
(5, 'ТТ через ТТ-ТШО', 1, 4, 12),
(6, 'ТВТ через ТТ-ТШО', 1, 2, 12);

-- --------------------------------------------------------

--
-- Структура таблицы `tips`
--

CREATE TABLE IF NOT EXISTS `tips` (
  `id` int(11) NOT NULL,
  `titlename` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `createdBy` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tips`
--

INSERT INTO `tips` (`id`, `titlename`, `status`, `createdBy`) VALUES
(1, 'Тор/др/кр', 1, 1),
(2, 'Справка 2612', 1, 1),
(3, 'Деталь', 1, 1),
(4, 'Ревизия', 1, 1),
(5, 'Подача и уборка', 1, 1),
(6, 'Промывка', 1, 1),
(7, 'Жд тариф', 1, 1),
(8, 'Услуги', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tordrkr`
--

CREATE TABLE IF NOT EXISTS `tordrkr` (
  `id` int(11) NOT NULL,
  `vagon_id` int(11) DEFAULT NULL,
  `mremont_id` int(11) DEFAULT NULL,
  `pricetip_id` int(11) DEFAULT NULL,
  `remont_id` int(11) DEFAULT NULL,
  `tip` int(11) DEFAULT NULL,
  `vvagon_id` int(11) DEFAULT NULL,
  `sobstvenik_id` int(11) DEFAULT NULL,
  `kol` int(11) DEFAULT NULL,
  `dates` date NOT NULL,
  `cenaed` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tordrkr`
--

INSERT INTO `tordrkr` (`id`, `vagon_id`, `mremont_id`, `pricetip_id`, `remont_id`, `tip`, `vvagon_id`, `sobstvenik_id`, `kol`, `dates`, `cenaed`) VALUES
(8, 42, 2, 1, 17, 1, 2, 4, 1, '2015-04-02', '43668.00'),
(9, 43, 2, 1, 19, 2, 1, 1, 1, '2015-04-10', '295000.00'),
(10, 45, 2, 1, 16, 1, 2, 2, 1, '2015-04-11', '6000.00'),
(11, 46, 4, 1, 40, 1, 1, 6, 1, '2015-04-14', '7641.69'),
(12, 47, 2, 1, 17, 1, 3, 2, 1, '2015-04-15', '43668.00'),
(13, 49, 2, 1, 15, 1, 3, 1, 1, '2015-04-18', '56769.00'),
(14, 50, 2, 1, 18, 2, 3, 1, 1, '2015-04-22', '355000.00'),
(18, 54, 1, 1, 14, 1, 2, 4, 1, '2015-04-22', '34500.00'),
(19, 55, 1, 1, 14, 1, 1, 6, 1, '2015-04-20', '34500.00'),
(20, 61, 2, 1, 16, 1, 1, 1, 1, '2015-05-14', '6000.00'),
(21, 63, 2, 1, 19, 2, 1, 6, 1, '2015-05-05', '295000.00'),
(22, 64, 2, 1, 17, 1, 3, 2, 1, '2015-05-06', '43668.00'),
(24, 65, 4, 1, 45, 2, 2, 2, 1, '2015-05-05', '280000.00');

-- --------------------------------------------------------

--
-- Структура таблицы `tsho`
--

CREATE TABLE IF NOT EXISTS `tsho` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `createdBy` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tsho`
--

INSERT INTO `tsho` (`id`, `title`, `status`, `createdBy`) VALUES
(1, 'Не выставлять', 1, 12),
(2, 'Не оплатили', 1, 12),
(3, 'Оплатили', 1, 12);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `lastlogin` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 PACK_KEYS=0;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `username`, `name`, `password`, `lang_id`, `group_id`, `active`, `createdBy`, `email`, `lastlogin`) VALUES
(1, 'sp1r!t', 'Мамбетов Арман Бахитжанович', '919a2cb83288c02eedee02c0574e91b3', 1, 1, '1', 1, 'spirit1086@mail.ru', '2015-05-29 13:34:24'),
(12, 'admin', 'Админ Админов', '19bc86ae1c2aa18d8451fbe3c2afc2de', NULL, 2, '1', 1, '', '0000-00-00 00:00:00'),
(14, 'demo', 'Демо пользователь', 'e10adc3949ba59abbe56e057f20f883e', NULL, 3, '1', 12, '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `usluga`
--

CREATE TABLE IF NOT EXISTS `usluga` (
  `id` int(11) NOT NULL,
  `uslugaspr_id` int(11) DEFAULT NULL,
  `vagon_id` int(11) DEFAULT NULL,
  `tip` int(11) DEFAULT NULL,
  `pricetip_id` int(11) DEFAULT NULL,
  `mremont_id` int(11) DEFAULT NULL,
  `remont_id` int(11) DEFAULT NULL,
  `vvagon_id` int(11) DEFAULT NULL,
  `sobstvenik_id` int(11) DEFAULT NULL,
  `dates` date DEFAULT NULL,
  `cenaed` varchar(255) DEFAULT NULL,
  `ndc` varchar(255) DEFAULT NULL,
  `kol` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `usluga`
--

INSERT INTO `usluga` (`id`, `uslugaspr_id`, `vagon_id`, `tip`, `pricetip_id`, `mremont_id`, `remont_id`, `vvagon_id`, `sobstvenik_id`, `dates`, `cenaed`, `ndc`, `kol`) VALUES
(12, 42, 57, 4, 8, 6, 42, 2, 2, '2015-04-27', '20542.00', NULL, 1),
(13, 44, 58, 4, 8, 2, 44, 0, 0, '2015-05-05', '2460.00', NULL, 1),
(14, 44, 59, 4, 8, 2, 44, 0, 0, '2015-05-04', '2460.00', NULL, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `vagon`
--

CREATE TABLE IF NOT EXISTS `vagon` (
  `id` int(11) NOT NULL,
  `nscheta` varchar(255) DEFAULT NULL,
  `dates` date NOT NULL,
  `tip` int(11) NOT NULL,
  `remont_id` int(11) DEFAULT NULL,
  `mremont_id` int(11) DEFAULT NULL,
  `nvagon_id` int(11) DEFAULT NULL,
  `vvagona` int(11) DEFAULT NULL,
  `sobstvenik_id` int(11) DEFAULT NULL,
  `nkontr` int(11) DEFAULT NULL,
  `drab` date DEFAULT NULL,
  `dbrak` date DEFAULT NULL,
  `vcomp_id` varchar(255) NOT NULL,
  `tsho_id` int(11) DEFAULT NULL,
  `opis` text,
  `cenaed` varchar(255) DEFAULT NULL,
  `ndc` int(11) NOT NULL,
  `vu23` text,
  `vu22` text,
  `vu22kol` int(11) DEFAULT NULL,
  `vu36` text,
  `akt` text,
  `aktkol` int(11) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `vagon`
--

INSERT INTO `vagon` (`id`, `nscheta`, `dates`, `tip`, `remont_id`, `mremont_id`, `nvagon_id`, `vvagona`, `sobstvenik_id`, `nkontr`, `drab`, `dbrak`, `vcomp_id`, `tsho_id`, `opis`, `cenaed`, `ndc`, `vu23`, `vu22`, `vu22kol`, `vu36`, `akt`, `aktkol`, `createdBy`) VALUES
(42, '1', '2015-04-02', 1, 17, 2, 1, 2, 4, 967794, '2015-04-02', '2015-04-10', '3', 1, NULL, NULL, 0, '', '', NULL, '', 'подкачено - 4 шт', NULL, 1),
(43, '2', '2015-04-10', 2, 19, 2, 2, 1, 1, 23568, '2015-04-11', NULL, '3', 2, NULL, NULL, 0, '', '', NULL, '', '', NULL, 1),
(44, '3', '2015-04-08', 1, 22, 2, 2, 1, 1, 23568, '2015-04-10', NULL, '1', 1, NULL, NULL, 0, '', '', NULL, '', '', NULL, 1),
(45, '4', '2015-04-11', 1, 16, 2, 4, 2, 2, 9635, '2015-04-14', NULL, '', NULL, NULL, NULL, 0, '', '', NULL, '', '', NULL, 1),
(46, '5', '2015-04-14', 1, 40, 4, 5, 1, 6, 8963, '2015-04-15', NULL, '3', 3, NULL, NULL, 0, '', '', NULL, '', '', NULL, 1),
(47, '6', '2015-04-15', 1, 17, 2, 6, 3, 2, 96345, '2015-04-17', NULL, '3', 3, NULL, NULL, 0, '', '', NULL, '', '', NULL, 1),
(48, '7', '2015-04-14', 2, 31, 2, 7, 2, 6, 63257, '2015-04-15', NULL, '3', 3, NULL, NULL, 0, '', '', NULL, '', '', NULL, 1),
(49, '8', '2015-04-18', 1, 15, 2, 8, 3, 1, 56348, '2015-04-21', NULL, '', NULL, NULL, NULL, 0, '', '', NULL, '', '', NULL, 1),
(50, '9', '2015-04-22', 2, 18, 2, 8, 3, 1, 56348, '2015-04-23', NULL, '1', 3, NULL, NULL, 0, '', '', NULL, '', '', NULL, 1),
(51, '10', '2015-04-24', 1, 2, 2, 7, 2, 6, 63257, '2015-04-25', NULL, '4', 2, NULL, NULL, 0, '', '', NULL, '', '', NULL, 1),
(54, '11', '2015-04-22', 1, 14, 1, 1, 2, 4, 967794, '2015-04-23', NULL, '', NULL, NULL, NULL, 0, '', '', NULL, '', '', NULL, 1),
(55, '12', '2015-04-20', 1, 14, 1, 5, 1, 6, 8963, '2015-04-21', NULL, '1', NULL, NULL, NULL, 0, '', '', NULL, '', '', NULL, 1),
(57, '13', '2015-04-27', 4, 42, 6, 4, 2, 2, 9635, '2015-04-28', NULL, '3,6', 1, NULL, NULL, 0, '', '', NULL, '', '', NULL, 1),
(58, '14', '2015-05-05', 4, 44, 2, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, 0, '', '', NULL, '', '', NULL, 1),
(59, '15', '2015-05-04', 4, 44, 2, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, 0, '', '', NULL, '', '', NULL, 1),
(60, '16', '2015-05-08', 2, 31, 2, 3, 1, 4, 12358, NULL, NULL, '1', 3, NULL, NULL, 0, '', '', NULL, '', '', NULL, 1),
(61, '1', '2015-05-14', 1, 16, 2, 2, 1, 1, 23568, '2015-05-15', NULL, '3', NULL, NULL, NULL, 0, '', '', NULL, '', 'Пишем что-то в акте', NULL, 1),
(62, '3', '2015-04-10', 1, 23, 2, 3, 1, 4, 12358, '2015-04-13', NULL, '3', 1, NULL, NULL, 0, '', '', NULL, '', '', NULL, 1),
(63, '2', '2015-05-05', 2, 19, 2, 5, 1, 6, 8963, NULL, NULL, '', NULL, NULL, NULL, 0, '', '', NULL, '', '', NULL, 1),
(64, '4', '2015-05-06', 1, 17, 2, 6, 3, 2, 96345, '2015-05-07', '2015-05-12', '3', 1, NULL, NULL, 0, '', '', NULL, '', '', NULL, 1),
(65, '6', '2015-05-05', 2, 45, 4, 4, 2, 2, 9635, '2015-05-07', NULL, '3', NULL, NULL, NULL, 0, '', '', NULL, '', '', NULL, 1),
(66, '1', '2015-04-02', 1, 17, 2, 1, 2, 4, 967794, '2015-04-02', '2015-04-10', '3', 1, NULL, NULL, 0, '', '', NULL, '', 'подкачено - 4 шт', NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `valuta`
--

CREATE TABLE IF NOT EXISTS `valuta` (
  `id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `createdBy` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `valuta`
--

INSERT INTO `valuta` (`id`, `title`, `createdBy`) VALUES
(1, 'тенге', 1),
(2, 'рубль', 1),
(3, 'доллар', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `vc`
--

CREATE TABLE IF NOT EXISTS `vc` (
  `id` int(11) NOT NULL,
  `nvg` int(11) NOT NULL,
  `vvagon_id` int(11) DEFAULT NULL,
  `sobstvenik_id` int(11) DEFAULT NULL,
  `nkotrakt` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `createdBy` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `vc`
--

INSERT INTO `vc` (`id`, `nvg`, `vvagon_id`, `sobstvenik_id`, `nkotrakt`, `status`, `createdBy`) VALUES
(1, 52625894, 2, 4, 967794, 1, 1),
(2, 489715452, 1, 1, 23568, 1, 1),
(3, 1598673, 1, 4, 12358, 1, 1),
(4, 963878, 2, 2, 9635, 1, 1),
(5, 334578, 1, 6, 8963, 1, 1),
(6, 45698, 3, 2, 96345, 1, 1),
(7, 32567, 2, 6, 63257, 1, 1),
(8, 563248, 3, 1, 56348, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `vvagon`
--

CREATE TABLE IF NOT EXISTS `vvagon` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `createdBy` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `vvagon`
--

INSERT INTO `vvagon` (`id`, `title`, `status`, `createdBy`) VALUES
(1, 'Газ', 1, 12),
(2, 'Крытый', 1, 12),
(3, 'Нефть', 1, 12);

-- --------------------------------------------------------

--
-- Структура таблицы `zhd`
--

CREATE TABLE IF NOT EXISTS `zhd` (
  `id` int(11) NOT NULL,
  `zhdspr_id` int(11) DEFAULT NULL,
  `vagon_id` int(11) DEFAULT NULL,
  `tip` int(11) DEFAULT NULL,
  `pricetip_id` int(11) DEFAULT NULL,
  `mremont_id` int(11) DEFAULT NULL,
  `remont_id` int(11) DEFAULT NULL,
  `vvagon_id` int(11) DEFAULT NULL,
  `sobstvenik_id` int(11) DEFAULT NULL,
  `dates` date DEFAULT NULL,
  `cenaed` varchar(255) DEFAULT NULL,
  `ndc` varchar(255) DEFAULT NULL,
  `kol` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `zhd`
--

INSERT INTO `zhd` (`id`, `zhdspr_id`, `vagon_id`, `tip`, `pricetip_id`, `mremont_id`, `remont_id`, `vvagon_id`, `sobstvenik_id`, `dates`, `cenaed`, `ndc`, `kol`) VALUES
(1, 31, 48, 2, 7, 2, 31, 2, 6, '2015-04-14', '10200.00', NULL, 1),
(2, 31, 60, 2, 7, 2, 31, 1, 4, '2015-05-08', '10200.00', NULL, 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Индексы таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Индексы таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Индексы таблицы `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `clean`
--
ALTER TABLE `clean`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_clean_mremont` (`mremont_id`),
  ADD KEY `FK_clean_vvagon` (`vvagon_id`),
  ADD KEY `FK_clean_sobstvenik` (`sobstvenik_id`),
  ADD KEY `FK_clean_vagon` (`vagon_id`),
  ADD KEY `FK_clean_price` (`cleanspr_id`),
  ADD KEY `FK_clean_osn` (`remont_id`);

--
-- Индексы таблицы `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_company_posrednik` (`posrednik_id`);

--
-- Индексы таблицы `detail`
--
ALTER TABLE `detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_detail_mremont` (`mremont_id`),
  ADD KEY `FK_detail_vvagon` (`vvagon_id`),
  ADD KEY `FK_detail_sobstvenik` (`sobstvenik_id`),
  ADD KEY `FK_detail_schet` (`vagon_id`),
  ADD KEY `FK_detail_price` (`detailspr_id`),
  ADD KEY `FK_detail_osn` (`remont_id`);

--
-- Индексы таблицы `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `mainmenu`
--
ALTER TABLE `mainmenu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `mremont`
--
ALTER TABLE `mremont`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `option`
--
ALTER TABLE `option`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `posrednik`
--
ALTER TABLE `posrednik`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `price`
--
ALTER TABLE `price`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_price_mremont` (`mremont_id`),
  ADD KEY `FK_price_tips` (`pricetip`);

--
-- Индексы таблицы `prichina`
--
ALTER TABLE `prichina`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_prichina_vagon` (`vagon_id`),
  ADD KEY `FK_prichina_mremont` (`mremont_id`),
  ADD KEY `FK_prichina_vvagon` (`vvagon_id`),
  ADD KEY `FK_prichina_sobstvenik` (`sobstvenik_id`),
  ADD KEY `FK_prichina_spr` (`spr_id`),
  ADD KEY `FK_prichina_osn` (`remont_id`),
  ADD KEY `FK_prichina_osn_2` (`dr_id`);

--
-- Индексы таблицы `promiv`
--
ALTER TABLE `promiv`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK__vagon` (`vagon_id`),
  ADD KEY `FK_promiv_mremont` (`mremont_id`),
  ADD KEY `FK_promiv_vvagon` (`vvagon_id`),
  ADD KEY `FK_promiv_sobstvenik` (`sobstvenik_id`),
  ADD KEY `FK_promiv_osn` (`remont_id`),
  ADD KEY `FK__promivspr` (`promivspr_id`);

--
-- Индексы таблицы `revizia`
--
ALTER TABLE `revizia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_revizia_vagon` (`vagon_id`),
  ADD KEY `FK_revizia_mremont` (`mremont_id`),
  ADD KEY `FK_revizia_sobstvenik` (`sobstvenik_id`),
  ADD KEY `FK_revizia_osn` (`remont_id`),
  ADD KEY `FK_revizia_reviziaspr` (`reviziaspr_id`),
  ADD KEY `FK_revizia_vvagon` (`vvagon_id`);

--
-- Индексы таблицы `sobstvenik`
--
ALTER TABLE `sobstvenik`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_sobstvenik_sobstvenik` (`type`);

--
-- Индексы таблицы `tips`
--
ALTER TABLE `tips`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tordrkr`
--
ALTER TABLE `tordrkr`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_tordrkr_vagon` (`vagon_id`),
  ADD KEY `FK_tordrkr_price` (`remont_id`);

--
-- Индексы таблицы `tsho`
--
ALTER TABLE `tsho`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `FK_users_users` (`createdBy`);

--
-- Индексы таблицы `usluga`
--
ALTER TABLE `usluga`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_usluga_vagon` (`vagon_id`),
  ADD KEY `FK_usluga_uslugaspr` (`uslugaspr_id`);

--
-- Индексы таблицы `vagon`
--
ALTER TABLE `vagon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_schet_vvagon` (`vvagona`),
  ADD KEY `FK_vagon_mremont` (`mremont_id`),
  ADD KEY `FK_vagon_vc` (`nvagon_id`),
  ADD KEY `FK_schet_sobstvenik` (`sobstvenik_id`),
  ADD KEY `FK_schet_tsho` (`tsho_id`),
  ADD KEY `FK_schet_company` (`vcomp_id`),
  ADD KEY `FK_vagon_osn` (`remont_id`);

--
-- Индексы таблицы `valuta`
--
ALTER TABLE `valuta`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `vc`
--
ALTER TABLE `vc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_vc_vvagon` (`vvagon_id`),
  ADD KEY `FK_vc_sobstvenik` (`sobstvenik_id`);

--
-- Индексы таблицы `vvagon`
--
ALTER TABLE `vvagon`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `zhd`
--
ALTER TABLE `zhd`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_zhd_vagon` (`vagon_id`),
  ADD KEY `FK_zhd_zhdspr` (`zhdspr_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `clean`
--
ALTER TABLE `clean`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `detail`
--
ALTER TABLE `detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT для таблицы `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `mainmenu`
--
ALTER TABLE `mainmenu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT для таблицы `mremont`
--
ALTER TABLE `mremont`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `option`
--
ALTER TABLE `option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `posrednik`
--
ALTER TABLE `posrednik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `price`
--
ALTER TABLE `price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT для таблицы `prichina`
--
ALTER TABLE `prichina`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT для таблицы `promiv`
--
ALTER TABLE `promiv`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `revizia`
--
ALTER TABLE `revizia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `sobstvenik`
--
ALTER TABLE `sobstvenik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `tips`
--
ALTER TABLE `tips`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `tordrkr`
--
ALTER TABLE `tordrkr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT для таблицы `tsho`
--
ALTER TABLE `tsho`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `usluga`
--
ALTER TABLE `usluga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `vagon`
--
ALTER TABLE `vagon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT для таблицы `valuta`
--
ALTER TABLE `valuta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `vc`
--
ALTER TABLE `vc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `vvagon`
--
ALTER TABLE `vvagon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `zhd`
--
ALTER TABLE `zhd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `clean`
--
ALTER TABLE `clean`
  ADD CONSTRAINT `FK_clean_mremont` FOREIGN KEY (`mremont_id`) REFERENCES `mremont` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_clean_osn` FOREIGN KEY (`remont_id`) REFERENCES `price` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_clean_price` FOREIGN KEY (`cleanspr_id`) REFERENCES `price` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_clean_sobstvenik` FOREIGN KEY (`sobstvenik_id`) REFERENCES `sobstvenik` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_clean_vagon` FOREIGN KEY (`vagon_id`) REFERENCES `vagon` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_clean_vvagon` FOREIGN KEY (`vvagon_id`) REFERENCES `vvagon` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `company`
--
ALTER TABLE `company`
  ADD CONSTRAINT `FK_company_posrednik` FOREIGN KEY (`posrednik_id`) REFERENCES `posrednik` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `detail`
--
ALTER TABLE `detail`
  ADD CONSTRAINT `FK_detail_mremont` FOREIGN KEY (`mremont_id`) REFERENCES `mremont` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_detail_osn` FOREIGN KEY (`remont_id`) REFERENCES `price` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_detail_price` FOREIGN KEY (`detailspr_id`) REFERENCES `price` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_detail_schet` FOREIGN KEY (`vagon_id`) REFERENCES `vagon` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_detail_sobstvenik` FOREIGN KEY (`sobstvenik_id`) REFERENCES `sobstvenik` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_detail_vvagon` FOREIGN KEY (`vvagon_id`) REFERENCES `vvagon` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `mainmenu`
--
ALTER TABLE `mainmenu`
  ADD CONSTRAINT `FK_mainmenu_mainmenu` FOREIGN KEY (`parent`) REFERENCES `mainmenu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `price`
--
ALTER TABLE `price`
  ADD CONSTRAINT `FK_price_mremont` FOREIGN KEY (`mremont_id`) REFERENCES `mremont` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_price_tips` FOREIGN KEY (`pricetip`) REFERENCES `tips` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `prichina`
--
ALTER TABLE `prichina`
  ADD CONSTRAINT `FK_prichina_mremont` FOREIGN KEY (`mremont_id`) REFERENCES `mremont` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_prichina_osn` FOREIGN KEY (`remont_id`) REFERENCES `price` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_prichina_osn_2` FOREIGN KEY (`dr_id`) REFERENCES `price` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_prichina_sobstvenik` FOREIGN KEY (`sobstvenik_id`) REFERENCES `sobstvenik` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_prichina_spr` FOREIGN KEY (`spr_id`) REFERENCES `price` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_prichina_vagon` FOREIGN KEY (`vagon_id`) REFERENCES `vagon` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_prichina_vvagon` FOREIGN KEY (`vvagon_id`) REFERENCES `vvagon` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `promiv`
--
ALTER TABLE `promiv`
  ADD CONSTRAINT `FK__promivspr` FOREIGN KEY (`promivspr_id`) REFERENCES `price` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK__vagon` FOREIGN KEY (`vagon_id`) REFERENCES `vagon` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_promiv_mremont` FOREIGN KEY (`mremont_id`) REFERENCES `mremont` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_promiv_osn` FOREIGN KEY (`remont_id`) REFERENCES `price` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_promiv_sobstvenik` FOREIGN KEY (`sobstvenik_id`) REFERENCES `sobstvenik` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_promiv_vvagon` FOREIGN KEY (`vvagon_id`) REFERENCES `vvagon` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `revizia`
--
ALTER TABLE `revizia`
  ADD CONSTRAINT `FK_revizia_mremont` FOREIGN KEY (`mremont_id`) REFERENCES `mremont` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_revizia_osn` FOREIGN KEY (`remont_id`) REFERENCES `price` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_revizia_reviziaspr` FOREIGN KEY (`reviziaspr_id`) REFERENCES `price` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_revizia_sobstvenik` FOREIGN KEY (`sobstvenik_id`) REFERENCES `sobstvenik` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_revizia_vagon` FOREIGN KEY (`vagon_id`) REFERENCES `vagon` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_revizia_vvagon` FOREIGN KEY (`vvagon_id`) REFERENCES `vvagon` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `sobstvenik`
--
ALTER TABLE `sobstvenik`
  ADD CONSTRAINT `FK_sobstvenik_sobstvenik` FOREIGN KEY (`type`) REFERENCES `sobstvenik` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tordrkr`
--
ALTER TABLE `tordrkr`
  ADD CONSTRAINT `FK_tordrkr_price` FOREIGN KEY (`remont_id`) REFERENCES `price` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_tordrkr_vagon` FOREIGN KEY (`vagon_id`) REFERENCES `vagon` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FK_users_users` FOREIGN KEY (`createdBy`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `usluga`
--
ALTER TABLE `usluga`
  ADD CONSTRAINT `FK_usluga_uslugaspr` FOREIGN KEY (`uslugaspr_id`) REFERENCES `price` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_usluga_vagon` FOREIGN KEY (`vagon_id`) REFERENCES `vagon` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `vagon`
--
ALTER TABLE `vagon`
  ADD CONSTRAINT `FK_schet_sobstvenik` FOREIGN KEY (`sobstvenik_id`) REFERENCES `sobstvenik` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_schet_tsho` FOREIGN KEY (`tsho_id`) REFERENCES `tsho` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_schet_vvagon` FOREIGN KEY (`vvagona`) REFERENCES `vvagon` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_vagon_mremont` FOREIGN KEY (`mremont_id`) REFERENCES `mremont` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_vagon_osn` FOREIGN KEY (`remont_id`) REFERENCES `price` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_vagon_vc` FOREIGN KEY (`nvagon_id`) REFERENCES `vc` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `vc`
--
ALTER TABLE `vc`
  ADD CONSTRAINT `FK_vc_sobstvenik` FOREIGN KEY (`sobstvenik_id`) REFERENCES `sobstvenik` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_vc_vvagon` FOREIGN KEY (`vvagon_id`) REFERENCES `vvagon` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `zhd`
--
ALTER TABLE `zhd`
  ADD CONSTRAINT `FK_zhd_vagon` FOREIGN KEY (`vagon_id`) REFERENCES `vagon` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_zhd_zhdspr` FOREIGN KEY (`zhdspr_id`) REFERENCES `price` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
