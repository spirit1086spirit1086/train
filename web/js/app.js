$(document).ready(function($) 
{

   $('.noselector').mousedown(function(){ return false; });
                     
    // клик по строки таблицы отчетов, выделяем или снимаем выделение
    $('table.dop td').click(function(ev){
         num=$(this).parents('tr').data('key'); 
         el = $( this ).parent('tr').parent('tbody').parent('table').attr('id');
         table='table#'+el;
                   
          if (ev.shiftKey) 
          {      
                start=$(table+' tr.tr_active').last().data('key'); 
                 $('tbody tr', table).each(function() {
                      for(i=start;i<=num;i++)
                      {
                         if ($(this).data('key')==i)
                         {
                           $(this).addClass('tr_active');                
                         }
                      }
                 });  
          }
          else
          {
               $(this).parents('tr').toggleClass('tr_active');
          }
                
    });
    
    /** **/
   valuta = document.getElementsByClassName("valuta"); 
      
   $('#reportbtn').click(function(){
       msg=[];
       for (i = 0; i < valuta.length; i++)    
       {
          if ($(valuta[i]).val()=='')
          {
             msg.push($(valuta[i]).data('title')); 
          }    
       }
       
       if (msg.length==0)
       {
             $("#report").submit(); 
       }
       else
       {
            alert('Заполните поле:' + msg.join());  
       }
   });
  
   /** Окно оповещение popup ** */
  
   $(".popup").css('display','block');
   
   $("body").on( "click","#wrapper .alert i.close", function()
    {
       $(".popup").removeAttr('style');
    }); 
  /**
  ******** добавляем селекты в ТАБАХ *******
  ******************************************
  ***/
  $("body").on( "click",".abtn", function()
   {
        if (confirm("Удалить запись?")) 
        {
              id=$(this).data('id');
              $('.form-group'+id).remove();
              $(this).remove();
        } 
   });
  
  $('.addopt').click(function()
     {
            id='#'+$(this).attr('id');
            if (id!='#pr') // если нажата была кнопка добавить деталь, проверяем заполнено ли место ремонта и НДС 
            {
                mremont=$('select#vagon-mremont_id option:selected').val(); // id место ремонта нужна для фильтрации по деталям
                ndcp=$('#ndcp').val();
            }
            else if(id=='#pr')
            {
              mremont=1; // полю справка 2612 место ремонта не нужно список для всех один и тот же
              ndcp=1; 
            }
      
            pricename=$('#pricename').val();  
            
            fields=['vagon-mremont_id','pricename'];
            flabel=['Место ремонта','Тип прайса'];
            fmessage=[];
            for (i=0;i<fields.length;i++)
            {
                if ( $('#'+fields[i]+' option:selected').val()=='')
                {
                   fmessage.push(flabel[i]);
                }
            }
      
           if (fmessage.length==0)
           {
                $.post("/vagon/addrule", {ind:$('select#pricename option:selected').text(),mr:mremont,pricetip:$('select#pricename').val(),_csrf: yii.getCsrfToken() })
                 .done(function( data ) {
                     $(".tabbl" ).append(data);
                  }) 
           }
           else  
           {
             alert('Заполните поле: ' + fmessage.join());
           }
     });   
 
   /****
   ******************************************
   ***** Подсчитваем цену детали ************
   *****/
   $("body").on( "click",".details .results li.child", function()
   {    
         nums=$(this).data('random');
         $('#dkol'+nums).val(1);
         $('#dcenaed'+nums).val($(this).data('cena'));
   });

   $("body").on( "click",".mabselect .results li.child", function()
   {  
         nums=$(this).data('random');
         $('#dkol'+nums).val(1);
         $('#dcenaed'+nums).val($(this).data('cena'));
   });

   
   $("body").on( "click","#sumdetail", function()
   {  // считаем цены за ремонт
         nums=$(this).data('id'); 

            ndc=$('#ndcp').val()/100; // поле ндс в процентах
            d=$('#dkol'+nums).val()*$('#dcenaed'+nums).val();
         $('#dcena'+nums).val( d.toFixed(2) );
            n=$('#dcena'+nums).val()*ndc;
         $('#dndc'+nums).val( n.toFixed(2) ) ;
            cena= $('#dcena'+nums).val();
            ndc_cena=$('#dndc'+nums).val();
            itog=parseFloat(cena) + parseFloat(ndc_cena);
         $('#ditog'+nums).val( itog.toFixed(2));
   });

  /*************************************************
  **************************************************
  **/

    // выбираем место ремонта получаем названия ТОР/ДР/КР
   $("body").on( "click","#vagon-mremont_id.mabselect .results li.child", function(){ 
          // из-за смены место ремонта отчищаем все табы 
             $(".tabbl" ).html('');
    });
    
   $("body").on( "click","#vagon-remont_id.mabselect .results li.child", function()
   {  
         str=$('span#usd').text();// получаем текущий курс доллара вверху панели 
         $('#usdp').val(str.substring(4));
         $('#vagon-cenaed').val($(this).data('cena'));
         $('#vagon-cena').val($(this).data('cena'));
         $('#vagon-ndc').val('') ; // отчищаем если был сменен option селекта
         $('#vagon-itog').val('');
            
   });
   
   $("body").on( "click","#rem", function()
   {  // считаем цены за ремонт
         ndc=$('#ndcp').val()/100;
         $('#vagon-ndc').val( $('#vagon-cena').val()*ndc ) ;
         cena= $('#vagon-cena').val();
         ndc_cena=$('#vagon-ndc').val();
         $('#vagon-itog').val( parseFloat(cena) + parseFloat(ndc_cena));
   });


    // выбираем номер вагона и срабатывает автозаполнение тип вагона, собственник, номер контракта
   $("body").on( "click","#vagon-nvagon_id.mabselect .results li.child", function(){ 
         if ($(this).data('value')!='')
         {
             $.post( "/vagon/nvagon", { id: $(this).data('value'),_csrf: yii.getCsrfToken() })
              .done(function( data ) {
                mas=JSON.parse(data);
                $('select#vagon-vvagona').html(mas.vvagon_id);
                $('a#vagon-vvagona span').text(mas.vvagontit);
                $('select#vagon-sobstvenik_id').html(mas.sobstvenik_id);
                $('a#vagon-sobstvenik_id span').text(mas.sobstit);
                $('#vagon-nkontr').val(mas.nkotrakt);
              });
         } 
         else
         {
                $('select#vagon-vvagona').html('- Выберите значение -');
                $('a#vagon-vvagona span').text('- Выберите значение -');
                $('select#vagon-sobstvenik_id').html('- Выберите значение -');
                $('a#vagon-sobstvenik_id span').text('- Выберите значение -');
                $('#vagon-nkontr').val('');
         }    
    });
    // маска для ввода цен
   $("input.small").mask("#0.00", {reverse: true});
     /***
     ************* Инфа обо мне на главной *****************
     **/
   $("body").on('click','#registr',function()
   {
       if ($(this).hasClass( "act" ))
       { 
         $('#registr').removeClass('act');
         $("#studio").slideUp('slow');         
       }
       else
       { 
         $(this).addClass('act');
         $( "#studio" ).slideDown('slow');
       }
   });
   
   
     $("body").click(function (event) 
     {
	    if ($(event.target).closest("#registr").length === 0) {
         $("#studio").slideUp('slow');  
         $('#registr').removeClass('act');
	    }
     });
   
 
    /**
    ******************* ОПЕРАЦИИ С МЕНЮ ******************************
    ***/
  
   $("body").on('click','#navbar ul li div',function()
   {
       cl=$(this).attr('id'); // полуаем id ссылки
       
       if ($(this).hasClass( "act" ))
       { 
           //удаляем у всех элементов классы чтобы вернуть в исходный вид меню
           $('#navbar ul li div').removeClass('act');
           $('i.menunav').removeClass('menunav_act');  
           $('#navbar ul li  ul').slideUp('slow'); 
       }
       else
       {
           $('#navbar ul li div').removeClass('act');
           $('i.menunav').removeClass('menunav_act');  
           $('#navbar ul li ul').slideUp('slow'); 

           $(this).addClass('act');
           $('i.'+cl).addClass('menunav_act');
           $('ul.'+cl).slideDown('slow'); 
           
       }
   });

  $("body").on('click','.hiddenmenu',function()
   {
       if ($(this).hasClass( "act" ))
        { 
          $(this).removeClass('act');
          $(this).text('Скрыть меню');
           $("#fbl").removeAttr('style');
           $("#navbar").removeAttr('style');
           $("#wrap_sidebar").removeAttr('style');
           $("#wrapper").removeAttr('style');        
          $.post( "/site/hiddenmenu", { perem: 0, _csrf: yii.getCsrfToken() })
        }
        else
        {
           $(this).addClass('act');
           $(this).text('Показать меню');
           $("#navbar").css('display','none');
            $("#wrap_sidebar").css('display','none');
           $("#wrapper").css('margin-left','0');
           $("#fbl").css('margin','0 10px');
           $.post( "/site/hiddenmenu", { perem: 1, _csrf: yii.getCsrfToken() } )
        }
   });
      
   /***
   ******************************Выделение строки  checkbox************************************
   **/   
    //_csrf: yii.getCsrfToken()
   
     $('#copy').click(function()
     {
         id= $('#copy').data('val');
         $.post( $(this).data('lnk'), { id: id, _csrf: yii.getCsrfToken() })
          .done(function( data ) {
              location.reload(true);  
         }); 
     });
     
           
     $('.chk').click(function()
     {
       $(this).parents('tr').toggleClass('tr_active');
        k=0;
        $('.chk:checked').each(function()
          {
                k=$(this).val();
          });
         edtlnk=$('.gbtnedit').data('lnk');
         $('.gbtnedit').attr('href',edtlnk+k);
         $('#copy').attr('data-val',k);
  
            k=0;
            $('.chk:checked').each(function()
              {
                  if (k=='') 
                  {
                    k=$(this).val();
                  } 
                  else
                  {
                    k=k+','+$(this).val();
                  }
              });
 
         dellnk=$('.gbtncancel').data('lnk');
         $('.gbtncancel').attr('href',dellnk+k);
      });           
    
     
      $('.select-on-check-all').click(function() 
      {
         if($('.select-on-check-all').prop("checked")==true)
         {
           $('tbody tr').addClass('tr_active');

            k=0;
            $('.chk').each(function()
              {
                    k=$(this).val();
              });
             edtlnk=$('.gbtnedit').data('lnk');
             $('.gbtnedit').attr('href',edtlnk+k);
             $('#copy').attr('data-val',k);
      
                k=0;
                $('.chk').each(function()
                  {
                      if (k=='') 
                      {
                        k=$(this).val();
                      } 
                      else
                      {
                        k=k+','+$(this).val();
                      }
                  });
     
             dellnk=$('.gbtncancel').data('lnk');
             $('.gbtncancel').attr('href',dellnk+k);
         }
         else
         {
           $('tbody tr').removeClass('tr_active');
           $('.gbtnedit').attr('href','');
           $('.gbtncancel').attr('href','');
           $('#copy').attr('data-val','');
         }
              
      }); 
      
      /****** Активный не активный юзер ******
      ****************************************
      ******/
    
     $('.u_active').click(function()
     {
        $.post( "/site/status", { id:$(this).data('id'), val: $(this).data('value'), tip: $(this).data('tip'), _csrf: yii.getCsrfToken() })
         .done(function( data ) {
             window.location.reload(true);
          })        
     });           
           

     $('.u_unactive').click(function()
     {
        $.post( "/site/status", { id:$(this).data('id'), val: $(this).data('value'),tip: $(this).data('tip'), _csrf: yii.getCsrfToken() })
         .done(function( data ) {
             window.location.reload(true);
          })        
     });           
           
       
    /**
    **  Добавляем и удаляем правила select'ы
    **/   
           
     $("body").on('click','.delrule',function()
     {
        num=$(this).data('id');
        p=$('select#parent'+num+' option:selected').val();
        c=$('select#child'+num+' option:selected').val();

        $.post("/authitemchild/delrule", {parent:p,child:c,_csrf: yii.getCsrfToken() })
         .done(function( data ) {
             window.location.reload(true);
          }) 
                 
     });           
           

     $('#addrule').click(function()
     {
        $.post("/authitemchild/addrule", {_csrf: yii.getCsrfToken() })
         .done(function( data ) {
             $( ".btn-group" ).before(data);
          }) 
     });           
           
   

    mydate=new Date();
    god=mydate.getFullYear();
    statistic=[];
  
    $.post("/site/graph", {_csrf: yii.getCsrfToken() })
   .done(function( data ) {
         obj = jQuery.parseJSON(data);
                   
           for (i=0;i<obj.length;i++)
           {
               d=new Date(obj[i][0]);
               obj[i][0]= d.valueOf();
           }    
           
           
           
          // диаграмма
           Highcharts.setOptions({
               lang: {
                   loading: 'Загрузка...',
                   months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                   weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                   shortMonths: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'],
                   exportButtonTitle: "Экспорт",
                   printButtonTitle: "Печать",
                   rangeSelectorFrom: "С",
                   rangeSelectorTo: "По",
                   rangeSelectorZoom: "Период",
                   downloadPNG: 'Скачать PNG',
                   downloadJPEG: 'Скачать JPEG',
                   downloadPDF: 'Скачать PDF',
                   downloadSVG: 'Скачать SVG',
                   printChart: 'Напечатать график'
               },
                 
       });

           $('#graph').highcharts({
           chart: {
               type: 'spline'
           },
           title: {
               text: 'Счет-фактуры'
           },
           subtitle: {
               text: 'график активности ввода счет-фактур'
           },
           xAxis: {
               type: 'datetime',
               dateTimeLabelFormats: { // don't display the dummy year
                   month: ' %b',
                   year: '%b'
               },

               //title: { text: '(разбивка по месяцам)'}
           },
           yAxis: {
               title: {
                   text: 'графа кол-во записей' 
               },
               min: 0
           },
           tooltip: {
               headerFormat: '<b>{series.name}</b><br>',
               pointFormat: '{point.x:%e %b}, Кол-во записей: {point.y:.0f} ',
           },

           plotOptions: {
               spline: {
                   marker: {
                       enabled: true
                   }
               }
           },

           series: [{
               color: '#66b132',    
               name: 'Данные на '+god+' год',
               // Define the data points. All series have a dummy year
               // of 1970/71 in order to be compared on the same x axis. Note
               // that in JavaScript, months start at 0 for January, 1 for February etc.
               data: obj,
                   }]
           });
         
         
           
    });     
    
 
     
    
    
    
    
});